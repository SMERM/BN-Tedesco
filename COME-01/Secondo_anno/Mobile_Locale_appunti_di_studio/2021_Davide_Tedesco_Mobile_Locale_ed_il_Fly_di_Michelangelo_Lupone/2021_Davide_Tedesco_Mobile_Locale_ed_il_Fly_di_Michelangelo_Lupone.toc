\babel@toc {italian}{}
\babel@toc {italian}{}
\contentsline {section}{\numberline {I} Introduzione}{4}{section.1}%
\contentsline {subsection}{\numberline {I}I sistemi di sintesi digitale del suono}{4}{subsection.1.1}%
\contentsline {subsection}{\numberline {II}Il CRM}{4}{subsection.1.2}%
\contentsline {subsection}{\numberline {III}Il Fly 10}{4}{subsection.1.3}%
\contentsline {subsection}{\numberline {IV}Il Fly 30}{4}{subsection.1.4}%
\contentsline {subsection}{\numberline {V}\textit {Mobile Locale} di Michelangelo Lupone}{6}{subsection.1.5}%
\contentsline {subsubsection}{\numberline {V.1}L'autore}{6}{subsubsection.1.5.1}%
\contentsline {subsubsection}{\numberline {V.2}Il brano}{6}{subsubsection.1.5.2}%
\contentsline {section}{\numberline {II} Le funzionalità del Fly 30}{8}{section.2}%
\contentsline {subsection}{\numberline {I}Le sorgenti}{8}{subsection.2.1}%
\contentsline {subsection}{\numberline {II}Il filtro e gli effetti}{9}{subsection.2.2}%
\contentsline {subsubsection}{\numberline {II.1}Il filtro}{9}{subsubsection.2.2.1}%
\contentsline {subsubsection}{\numberline {II.2}Gli inviluppi}{9}{subsubsection.2.2.2}%
\contentsline {subsubsection}{\numberline {II.3}Il riverbero}{10}{subsubsection.2.2.3}%
\contentsline {subsection}{\numberline {III}Il \textit {meter}}{10}{subsection.2.3}%
\contentsline {subsection}{\numberline {IV}Il pannello inferiore}{10}{subsection.2.4}%
\contentsline {subsection}{\numberline {V}Il posteriore}{11}{subsection.2.5}%
\contentsline {section}{\numberline {III} Il \textit {porting} del Fly 30 in FAUST}{12}{section.3}%
\contentsline {subsection}{\numberline {I}FAUST e \textit {SEAM}}{12}{subsection.3.1}%
\contentsline {subsection}{\numberline {II}Realizzazione della matrice in FAUST}{12}{subsection.3.2}%
\contentsline {subsubsection}{\numberline {II.1}Il \texttt {checkbox}}{12}{subsubsection.3.2.1}%
\contentsline {subsubsection}{\numberline {II.2}Il \texttt {par}}{13}{subsubsection.3.2.2}%
\contentsline {subsubsection}{\numberline {II.3}La matrice del VCS3 in FAUST}{13}{subsubsection.3.2.3}%
\contentsline {section}{\numberline {IV} Perchè il Fly 30}{14}{section.4}%
\contentsline {subsection}{\numberline {I}L'importanza di una matrice all'interno di un sintetizzatore}{14}{subsection.4.1}%
\contentsline {subsection}{\numberline {II}La matrice del VCS3}{14}{subsection.4.2}%
\contentsline {subsection}{\numberline {III}Le possibilità di espansione di una matrice}{16}{subsection.4.3}%
\contentsline {subsection}{\numberline {IV}Come approciarsi alla realizzazione di \textit {patches}}{17}{subsection.4.4}%
\contentsline {subsection}{\numberline {V}Il VCS3 ed \textit {Incontro}}{19}{subsection.4.5}%
\contentsline {section}{\numberline {V} Conclusioni}{21}{section.5}%
\contentsline {section}{\numberline {VI}Strumenti per la comprensione del Fly 30}{22}{section.6}%
\contentsline {subsection}{\numberline {I}Tabella riassuntiva delle caratteristiche dell'EMS VCS3}{22}{subsection.6.1}%
\contentsline {subsection}{\numberline {II}Rappresentazione della matrice del VCS3}{23}{subsection.6.2}%
\contentsline {subsection}{\numberline {III}Schemi descrittivi del Fly 30 e delle sue caratteristiche}{24}{subsection.6.3}%
\contentsline {section}{\numberline {VII} Bibliografia, filmografia \& sitografia}{24}{section.7}%
\contentsline {section}{Elenco delle figure e delle tabelle}{26}{Item.27}%
