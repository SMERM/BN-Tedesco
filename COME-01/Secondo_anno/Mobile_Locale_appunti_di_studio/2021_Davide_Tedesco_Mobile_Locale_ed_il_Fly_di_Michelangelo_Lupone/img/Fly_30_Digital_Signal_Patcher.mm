<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#111111" CREATED="1629838815316" ID="ID_24826106" MODIFIED="1631974376941" TEXT="Fly 30 - Digital Signal Patcher">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#990000" CREATED="1631974294659" ID="ID_1488293449" MODIFIED="1631979956721" POSITION="right" TEXT="frequenza di campionamento di default a 22050 Hz">
<edge COLOR="#990000" WIDTH="1"/>
</node>
<node COLOR="#990000" CREATED="1631974323682" ID="ID_725649408" MODIFIED="1631979938183" POSITION="right" TEXT="operazioni aritmetiche effettuate in floating point">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631974385439" ID="ID_1196925600" MODIFIED="1631979938184" TEXT="con precisione di 2^(-38)">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#990000" CREATED="1631959264893" ID="ID_767723005" MODIFIED="1631979938180" POSITION="left" TEXT="i menu">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1629838888358" ID="ID_1010580011" MODIFIED="1631979938181" TEXT="FILE">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1629838891497" ID="ID_182012142" MODIFIED="1631979938181" TEXT="EDIT">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1629838895973" ID="ID_1308026026" MODIFIED="1631979938182" TEXT="MODL">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1629838911981" ID="ID_1575800728" MODIFIED="1631979938182" TEXT="HELP">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1629838907515" ID="ID_1062675799" MODIFIED="1629839697551" POSITION="left" TEXT="linea di stato">
<edge COLOR="#990000" WIDTH="1"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#990000" CREATED="1631974377271" ID="ID_228860441" MODIFIED="1631979938178" POSITION="right" TEXT="si prevede l&apos;uso di tabelle editabili dall&apos;utente">
<edge COLOR="#990000" WIDTH="1"/>
</node>
<node COLOR="#990000" CREATED="1631974480107" ID="ID_399437497" MODIFIED="1631979938177" POSITION="right" TEXT="ogni modulo necessita oltre ai valori da assegnare, di un valore associato (tranne i moduli ADD, SUB e MUL)">
<edge COLOR="#990000" WIDTH="1"/>
</node>
<node COLOR="#990000" CREATED="1631974660761" ID="ID_1835861326" MODIFIED="1631979938173" POSITION="right" TEXT="ampiezza e frequenza sono generalmente indicate in dB ed Hz">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631974684682" ID="ID_257657546" MODIFIED="1631979938174" TEXT="per il modulo OSC in modalit&#xe0; standard queste unit&#xe0; di misura sono omesse, in virt&#xf9; di due valori">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631974803609" ID="ID_1956078061" MODIFIED="1631979938172" TEXT="valore di amplificazione">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631974836781" ID="ID_1395885430" MODIFIED="1631979938173" TEXT="valore di incremento">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631974845831" ID="ID_161792375" MODIFIED="1631979938176" TEXT="queste unit&#xe0; di misura sono omesse, per una maggiore creativit&#xe0;">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
</node>
</map>
