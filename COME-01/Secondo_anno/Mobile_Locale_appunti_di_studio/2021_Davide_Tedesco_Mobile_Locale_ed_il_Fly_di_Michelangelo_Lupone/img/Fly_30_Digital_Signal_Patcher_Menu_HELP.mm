<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#00b439" CREATED="1631959193301" ID="ID_1924061176" MODIFIED="1631979760579" TEXT="Menu HELP">
<edge COLOR="#00b439" WIDTH="2"/>
<node COLOR="#990000" CREATED="1629838938224" ID="ID_1755314256" MODIFIED="1631979766966" POSITION="right" TEXT="general">
<edge COLOR="#990000" WIDTH="1"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629839025043" ID="ID_1099079205" MODIFIED="1631979766967" TEXT="funzionamento del Digital Signal Patcher">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1629838943830" ID="ID_1545768347" MODIFIED="1631979766964" POSITION="right" TEXT="keywords">
<edge COLOR="#990000" WIDTH="1"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629839040758" ID="ID_734399575" MODIFIED="1631979766965" TEXT="parole chiave">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1629838950879" ID="ID_1694295909" MODIFIED="1631979766962" POSITION="right" TEXT="keyboard">
<edge COLOR="#990000" WIDTH="1"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629839050284" ID="ID_1035057974" MODIFIED="1631979766963" TEXT="comandi relativi ai tasti">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1629838956336" ID="ID_31489111" MODIFIED="1631979766961" POSITION="right" TEXT="formulas">
<edge COLOR="#990000" WIDTH="1"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629839062506" ID="ID_907046584" MODIFIED="1631979766961" TEXT="formule presenti nell&apos;ambiente">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1629838963074" ID="ID_272775387" MODIFIED="1631979766951" POSITION="right" TEXT="modules">
<edge COLOR="#990000" WIDTH="1"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629839073866" ID="ID_144928134" MODIFIED="1631979766952" TEXT="+">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1629839078957" ID="ID_146851548" MODIFIED="1631979766953" TEXT="-">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1629839081568" ID="ID_713818244" MODIFIED="1631979766954" TEXT="*">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1629839085196" ID="ID_1307074126" MODIFIED="1631979766956" TEXT="+ per la fase in modulo 65536">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1629839103349" ID="ID_639133873" MODIFIED="1631979766957" TEXT="TAB">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1629839109908" ID="ID_378109992" MODIFIED="1631979766947" TEXT="lettura della forma d&apos;onda dell&apos;indirizzo A">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#111111" CREATED="1629839144242" ID="ID_1216509388" MODIFIED="1631979766959" TEXT="MEM">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
</node>
</node>
</map>
