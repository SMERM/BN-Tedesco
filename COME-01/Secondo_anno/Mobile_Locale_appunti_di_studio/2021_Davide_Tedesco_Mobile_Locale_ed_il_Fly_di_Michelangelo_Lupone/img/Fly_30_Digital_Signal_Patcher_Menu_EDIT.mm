<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1631955938211" ID="ID_581262659" MODIFIED="1631956178724" TEXT="Menu EDIT">
<node COLOR="#990000" CREATED="1629839000921" ID="ID_1273647168" MODIFIED="1631979826046" POSITION="right" TEXT="JOIN">
<edge COLOR="#990000" WIDTH="1"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1631959505830" ID="ID_79285507" MODIFIED="1631979826047" TEXT="permette la connessione dei moduli elencati nel menu MODL">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631959545153" ID="ID_348671146" MODIFIED="1631979826047" TEXT="&#xe8; possibile pi&#xf9; ingressi ad un&apos;unica uscita, ma non il contrario">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631959578500" ID="ID_1186744101" MODIFIED="1631979826043" TEXT="ci&#xf3; &#xe8; possibile con il modulo ADD">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631959616070" ID="ID_620338053" MODIFIED="1631979826049" TEXT="utilizzabile per mezzo del mouse">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631959694133" ID="ID_289533618" MODIFIED="1631979826050" TEXT="possono essere creati dei fili volanti">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
<node COLOR="#111111" CREATED="1631959718052" ID="ID_594248941" MODIFIED="1631979826039" TEXT="che non terminano in nessun ingresso di moduli">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631960298197" ID="ID_513847627" MODIFIED="1631979826052" TEXT="matita rossa">
<edge COLOR="#111111" WIDTH="thin"/>
<icon BUILTIN="pencil"/>
</node>
</node>
<node COLOR="#990000" CREATED="1631955293751" ID="ID_1691967996" MODIFIED="1631979826036" POSITION="right" TEXT="MOVE">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631960131288" ID="ID_1682552086" MODIFIED="1631979826036" TEXT="per lo spostamento di moduli">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#990000" CREATED="1631955298039" ID="ID_95831392" MODIFIED="1631979826017" POSITION="right" TEXT="VALUE">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631960192223" ID="ID_591281758" MODIFIED="1631979826018" TEXT="assegna un valore associato ad uno qualsiasi dei moduli elencati nel menu MODL">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631960222658" ID="ID_1490343754" MODIFIED="1631979826019" TEXT="ADD, SUB e MUL non richiedono assegnazione">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631960254524" ID="ID_1029649008" MODIFIED="1631979826020" TEXT="i valori memorizzati e quelli da assegnare appaiono nella linea di stato">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631960445014" ID="ID_1020155205" MODIFIED="1631979826021" TEXT="matita gialla">
<edge COLOR="#111111" WIDTH="thin"/>
<icon BUILTIN="pencil"/>
</node>
<node COLOR="#111111" CREATED="1631960525855" ID="ID_922828548" MODIFIED="1631979826021" TEXT="accetta solo valori congrui a quelli che il modulo possa accettare">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631960562516" ID="ID_1705965589" MODIFIED="1631979826014" TEXT="bad value range is x xxxx">
<edge COLOR="#111111" WIDTH="thin"/>
<font ITALIC="true" NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631960636466" ID="ID_922546099" MODIFIED="1631979826022" TEXT="moduli che accettano valori dal comando VALUE">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631955305467" ID="ID_715890611" MODIFIED="1631979825998" TEXT="PHS">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631960689185" ID="ID_401882883" MODIFIED="1631979825996" TEXT="generatore d&apos;onda a dente di sega">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631960704102" ID="ID_1704262178" MODIFIED="1631979825994" TEXT="richiesto il valore iniziale assoluto della fase">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1631955318524" ID="ID_718557956" MODIFIED="1631979825999" TEXT="TAB, MEM, OSC">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631960740151" ID="ID_772040607" MODIFIED="1631979825992" TEXT="numero identificatore della tabella a cui si vogliono apportare modifiche">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631960793205" ID="ID_1421566654" MODIFIED="1631979825992" TEXT="valori ammissibili da 1 a 16">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631960823580" ID="ID_1174235967" MODIFIED="1631979826000" TEXT="ENV">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631960831580" ID="ID_1001061247" MODIFIED="1631979825988" TEXT="numero identificatore dell&apos;inviluppo prescelto">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631960793205" ID="ID_547564308" MODIFIED="1631979825989" TEXT="valori ammissibili da 1 a 16">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631955333493" ID="ID_124499788" MODIFIED="1631979826002" TEXT="DLY">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631960859579" ID="ID_1673420587" MODIFIED="1631979825984" TEXT="valore iniziale da assegnare alla memoria delay di 1 posizione">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631960881426" ID="ID_1544740928" MODIFIED="1631979825985" TEXT="valori ammissibili: qualsiasi valore reale floating point">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631955343160" ID="ID_1469492082" MODIFIED="1631979826003" TEXT="DLN &amp; DLS">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631960902830" ID="ID_1991966339" MODIFIED="1631979825979" TEXT="valore di ritardo in termini di unit&#xe0; logaritmiche">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631960935495" ID="ID_182596386" MODIFIED="1631979825980" TEXT="esso lavora in base alle celle di memoria implicate">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631960948157" ID="ID_1443892350" MODIFIED="1631979825977" TEXT="lavora con potenze di 2">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631960971473" ID="ID_140611272" MODIFIED="1631979825981" TEXT="10 realizzer&#xe0; una linea di ritardo di 2^(10)  celle di memoria">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1631960793205" ID="ID_369733513" MODIFIED="1631979825982" TEXT="valori ammissibili da 1 a 16">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631955360534" ID="ID_1639232965" MODIFIED="1631979826006" TEXT="FLT">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631961096984" ID="ID_988883303" MODIFIED="1631979825972" TEXT="filtro di secondo ordine IIR">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631961112234" ID="ID_1816149722" MODIFIED="1631979825973" TEXT="fornire il valore iniziale di innesco del filtro stesso">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631960881426" ID="ID_126846005" MODIFIED="1631979825973" TEXT="valori ammissibili: qualsiasi valore reale floating point">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631955363758" ID="ID_1114685723" MODIFIED="1631979826007" TEXT="RND">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631961142089" ID="ID_923140736" MODIFIED="1631979825968" TEXT="valore di innesco del rumore bianco">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631960881426" ID="ID_581742577" MODIFIED="1631979825968" TEXT="valori ammissibili: qualsiasi valore reale floating point">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631955366240" ID="ID_1396672896" MODIFIED="1631979826009" TEXT="IFP">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631961184247" ID="ID_448644772" MODIFIED="1631979825963" TEXT="viene richiesto il valore di soglia (K) da assegnare al primo ingresso (A)">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631961225259" ID="ID_1856014175" MODIFIED="1631979825964" TEXT="valori superiori a K determinano i valori per l&apos;ingresso di B mentre valori inferiori per l&apos;ingresso C">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631961352980" ID="ID_957271203" MODIFIED="1631979825964" TEXT="esso &#xe8; una modulo con implementazione di rudimentali istruzioni condizionali">
<edge COLOR="#111111" WIDTH="thin"/>
<font ITALIC="true" NAME="Garamond" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1631960881426" ID="ID_1875676610" MODIFIED="1631979825965" TEXT="valori ammissibili: qualsiasi valore reale">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631955370337" ID="ID_286874393" MODIFIED="1631979826011" TEXT="INP &amp; OUT">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631961798512" ID="ID_1639278082" MODIFIED="1631979825958" TEXT="valori ammissibili da 1 a 4">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631961818033" ID="ID_1597055776" MODIFIED="1631979825956" TEXT="tanti quanti i canali di conversione disponibili">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631961840400" ID="ID_1227465607" MODIFIED="1631979825957" TEXT="con un&apos;unica scheda, solo 2 canai disponibili">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1631955387120" ID="ID_327112438" MODIFIED="1631979825931" POSITION="left" TEXT="TABLES">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631961907070" ID="ID_480053441" MODIFIED="1631979825932" TEXT="apre un editor per i moduli OSC, ENV, TAB, MEM">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631961944061" ID="ID_1920727610" MODIFIED="1631979825934" TEXT="selezionare il modulo di cui editare la tabella">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631962041381" ID="ID_125320048" MODIFIED="1631979825935" TEXT="F1 per editare le tabelle successive">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631962057234" ID="ID_735112365" MODIFIED="1631979825936" TEXT="F2 per editare le tabelle precedenti">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631962088594" ID="ID_1875897418" MODIFIED="1631979825937" TEXT="vi sono due tipi di editor">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631962096088" ID="ID_223326565" MODIFIED="1631979825922" TEXT="per le tabelle">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631962395227" ID="ID_876311330" MODIFIED="1631979825919">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <img width="200" src="editor_tab.png" height="100" />
    </p>
  </body>
</html></richcontent>
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631963112531" ID="ID_530420767" MODIFIED="1631979825910" TEXT="vi &#xe8; un valore di ampiezza">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631963132596" ID="ID_1708927530" MODIFIED="1631979825911" TEXT="vi &#xe8; un valore della fase espressa in gradi">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1631962101455" ID="ID_1691974117" MODIFIED="1631979825923" TEXT="per gli inviluppi">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631962899356" ID="ID_329712165" MODIFIED="1631979825890">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <img width="200" src="editor_env.png" height="100" />
    </p>
  </body>
</html></richcontent>
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631963362288" ID="ID_1280191356" MODIFIED="1631979825878" TEXT="tripla di valori">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631963338000" ID="ID_367318148" MODIFIED="1631979825794" TEXT="ampiezza">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631963344129" ID="ID_1684226915" MODIFIED="1631979825795" TEXT="curvatura">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631963469460" ID="ID_1830409782" MODIFIED="1631979825791" TEXT="1 indica una retta">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631963485465" ID="ID_336462205" MODIFIED="1631979825791" TEXT="altri valori generano curve esponenziali se &gt;1">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631963501653" ID="ID_944631118" MODIFIED="1631979825792" TEXT="altri valori generano curve logaritmiche se &lt;1">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631963350921" ID="ID_920992338" MODIFIED="1631979825797" TEXT="durata">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631963548887" ID="ID_74297947" MODIFIED="1631979825786" TEXT="durate in secondi di ciascun segmento">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631963730236" ID="ID_1269858200" MODIFIED="1631979825787" TEXT="se la durata &#xe8; nulla e il valore finale &#xe8; maggiore del valore precedente, la tabella non verr&#xe0; ciclata">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631963378246" ID="ID_1931798552" MODIFIED="1631979825798" TEXT="abbiamo un massimo di 8 triple alla volta ovvero 8 segmenti">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
</node>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1631955396816" ID="ID_1030173289" MODIFIED="1631979825781" POSITION="left" TEXT="DELETE">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631970205397" ID="ID_1973124488" MODIFIED="1631979825782" TEXT="effettua cancellazioni di moduli e connessioni">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631970221047" ID="ID_1649219831" MODIFIED="1631979825781" TEXT="una alla volta">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1631955398812" ID="ID_904719996" MODIFIED="1631979825777" POSITION="left" TEXT="SHOVAL">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631970238268" ID="ID_521764650" MODIFIED="1631979825778" TEXT="visualizza i valori assegnati a un modulo mediante il comando VALUE">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631970281496" ID="ID_1174741538" MODIFIED="1631979825779" TEXT="esso &#xe8; un comando del tipo on/off, pu&#xf2;essere attivato o disattivato">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#990000" CREATED="1631955403530" ID="ID_746052863" MODIFIED="1631979825774" POSITION="left" TEXT="SHOCON">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631970371132" ID="ID_1323186335" MODIFIED="1631979825775" TEXT="visualizza i numeri di identificazione assegnati alle connessioni">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631970281496" ID="ID_1008728072" MODIFIED="1631979825775" TEXT="esso &#xe8; un comando del tipo on/off, pu&#xf2;essere attivato o disattivato">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#990000" CREATED="1631955408461" ID="ID_1487019370" MODIFIED="1631979825771" POSITION="left" TEXT="REDRAW">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631970418367" ID="ID_1304109148" MODIFIED="1631979825772" TEXT="&#xe8; utile per poter ridisegnare la schermata, realizza un refresh della schermata">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#990000" CREATED="1631955410927" ID="ID_1338705624" MODIFIED="1631979825766" POSITION="left" TEXT="MODIFY">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631970473205" ID="ID_1221825892" MODIFIED="1631979825767" TEXT="&#xe8; il comando per la modifica in tempo reale dei valori in floating point assegnati">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631970513412" ID="ID_1278288261" MODIFIED="1631979825767" TEXT="agisce sui valori dei moduli VAL">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631970652812" ID="ID_1562376132" MODIFIED="1631979825764" TEXT="esponenzialmente">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631970540213" ID="ID_1303991852" MODIFIED="1631979825768" TEXT="utilizzabile solo dopo il comando PLAY">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631970624610" ID="ID_1776787907" MODIFIED="1631979825769" TEXT="si possono modificare due parametri">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631970633057" ID="ID_1224621654" MODIFIED="1631979825761" TEXT="il valore">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631970637971" ID="ID_1310076037" MODIFIED="1631979825762" TEXT="lo step incrementale">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1631955415181" ID="ID_737624546" MODIFIED="1631979825753" POSITION="left" TEXT="SWITCH">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631970704250" ID="ID_1380768437" MODIFIED="1631979825754" TEXT="per controllare i selettori di programma dal Digital Signal Patcher">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631970844850" ID="ID_1838010294" MODIFIED="1631979825752" TEXT="di default essi sono ad off">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631970724200" ID="ID_1466043414" MODIFIED="1631979825755" TEXT="i selettori sono">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631970729303" ID="ID_1512027778" MODIFIED="1631979825746" TEXT="NODWLOWAV">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631970810604" ID="ID_1889127472" MODIFIED="1631979825744" TEXT="on: non carica le forme d&apos;onda al PLAY">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631970746992" ID="ID_715111358" MODIFIED="1631979825747" TEXT="NODWLOWENV">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631970810604" ID="ID_1988339594" MODIFIED="1631979825742" TEXT="on: non carica gli inviluppi a al PLAY">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631970758070" ID="ID_331440442" MODIFIED="1631979825748" TEXT="NOINTERP">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631970838876" ID="ID_603688736" MODIFIED="1631979825740" TEXT="on: non compila nuovamente il disegno del patch, ma manda in esecuzione l&apos;ultimo file">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631970773359" ID="ID_580891234" MODIFIED="1631979825749" TEXT="FREQOSC">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631970897090" ID="ID_913060045" MODIFIED="1631979825738" TEXT="on: primo ingresso di OSC in Hertz e non in step incrementale">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1631970776454" ID="ID_10840843" MODIFIED="1631979825750" TEXT="TIMEDLN">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631970926083" ID="ID_346108867" MODIFIED="1631979825736" TEXT="on: esprime il ritardo del modulo DLN in millisecondi e non in celle di memoria di ritardo">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1631955419886" ID="ID_1183674310" MODIFIED="1631979825732" POSITION="left" TEXT="ADJCON">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631970954867" ID="ID_962509509" MODIFIED="1631979825733" TEXT="elimina imperfezioni nel disegno delle connessioni">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#990000" CREATED="1631955424189" ID="ID_198757050" MODIFIED="1631979825728" POSITION="left" TEXT="CHECK">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631970970569" ID="ID_993259829" MODIFIED="1631979825729" TEXT="mostra una tabella con informazioni sul patch corrente">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1631971299452" ID="ID_13814427" MODIFIED="1631979825726">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="check.png" width="75" height="100" />
    </p>
  </body>
</html></richcontent>
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1631955428550" ID="ID_698027103" MODIFIED="1631979825709" POSITION="left" TEXT="BOARD">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631971502510" ID="ID_714511007" MODIFIED="1631979825710" TEXT="per selezionare il numero di scheda da utilizzare, se se ne possiedono pi&#xf9; di una">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#990000" CREATED="1631955436606" ID="ID_561359884" MODIFIED="1631979825706" POSITION="left" TEXT="DELORF">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631971545893" ID="ID_1100055960" MODIFIED="1631979825707" TEXT="cancella moduli e connessioni senza collegamenti">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631971753138" ID="ID_458297616" MODIFIED="1631979825707" TEXT="comando selezionato solo con il mouse">
<edge COLOR="#111111" WIDTH="thin"/>
<font BOLD="true" NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1631955438695" ID="ID_1928194325" MODIFIED="1631979825702" POSITION="left" TEXT="DELCOF">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631971557533" ID="ID_848591261" MODIFIED="1631979825703" TEXT="cancella tutte le connessioni che hanno un ingresso o un&apos;uscita non collegati">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631971753138" ID="ID_1282674132" MODIFIED="1631979825704" TEXT="comando selezionato solo con il mouse">
<edge COLOR="#111111" WIDTH="thin"/>
<font BOLD="true" NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1631955442731" ID="ID_1193874744" MODIFIED="1631979825690" POSITION="left" TEXT="DELMOF">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631971557533" ID="ID_1069346742" MODIFIED="1631979825697" TEXT="cancella tutti i moduli ccancella tutti i moduli che hanno un ingresso o un&apos;uscita non collegati he hanno un ingresso o un&apos;uscita non collegati">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631971753138" ID="ID_208354943" MODIFIED="1631979825700" TEXT="comando selezionato solo con il mouse">
<edge COLOR="#111111" WIDTH="thin"/>
<font BOLD="true" NAME="Garamond" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1631955446775" ID="ID_1152266264" MODIFIED="1631979825676" POSITION="left" TEXT="DELFLO">
<edge COLOR="#990000" WIDTH="1"/>
<node COLOR="#111111" CREATED="1631971545893" ID="ID_644232669" MODIFIED="1631979825678" TEXT="cancella moduli e connessioni che hanno un ingresso o un&apos;uscita non collegati ">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1631971753138" ID="ID_1050194668" MODIFIED="1631979825679" TEXT="comando selezionato solo con il mouse">
<edge COLOR="#111111" WIDTH="thin"/>
<font BOLD="true" NAME="Garamond" SIZE="12"/>
</node>
</node>
</node>
</map>
