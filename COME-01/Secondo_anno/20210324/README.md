# Appunti della lezione di mercoledì 24 marzo 2021

## Introduzione a mobile locale e perchè lo trattiamo?

Mobile Locale è un brano per percussioni ed elettronica del 1991 di Lupone.

Su di esso è stata costruita l'evoluzione dal FLY10 al FLY30.

Lupone non riesce a ragionare a compartimenti stagni, tra lo strumento e la sua musica...

### Triangolo magico della musica di Lupone

A Salerno nel Festival Aumentazioni, Lupone traccia 3 punti e li pone in connessione fra loro.

![tri](tri.png)

Esso è un triangolo di relazioni tra:
- l'opera che sta componendo e lavorando -> sorta di contenitore aperto
- lo strumento -> diretta relazione bidirezionale tra opera e strumento
- suono

Fa capire in maniera solida come questa struttura di relazioni caratterizzi sempre l'opera di Lupone, neanche un brano acusmatico si scosta da questo schema.

Non vi è alcun brano che non segua questo schema.

Forma del respiro(un solo filtro, tanti modi di fare)...

Mauro Cardi, Libra per vibrafono ed elettronica -> vibrafono agisce completamente parallelo all'elettronica, senza un tutt'uno sonoro.

Questo è importante, perchè Lupone anche scrivendo un brano per percussione e Live Electronics, pone le basi per il concetto di aumentazione.

Non poteva inizialmente realizzare, ma doveva pensare.

![tri2](tri2.png)
Il triangolo diviene teatraedro inserendo l'ascolto, principio fondamentale per Lupone.

Non si può procedere per step di scrittura, ma Lupone ha bisogno fisicamente di un riscontro auditivo.

Lupone ha bisogno di un riscontro auditivo che non può essere lontano dal tempo reale.

Bisogno di avere una reazione immediata al gesto.

### Macchina che lo proietta verso l'aumentazione

Lupone non scrive processi di elaborazione statica, ma processi con una loro vita interna.

Processi con principi di vitalità interna, con una risposta al sollecito elettrico mai uguale.

Questo è il passo lunare verso l'aumentazione, riuscendo in termini di contributo timbrico a crearla.

Si capisce che Lupone scrive un brano per un palco, con un suono elettronico che deve sovrastare il suono acustico con una trasfigurazione del gesto.

Effetto generato alla fine della catena, che legato al timbro che l'ha generata, che diviene un unico oggetto sonoro.

### Spazio?
(ampliamento del triangolo->teatraedro)

Il FLY30 era un'estensione di quello che accade sul palco.

Il setup della partitura ha un materiale misto con una grande ricerca...

Idea di aumentare la percussione ha portato ad un aumento fisico degli strumenti.

L'unione tra spazio e ascolto è realizzata dalla figura che realizza il brano.

Abbiamo 3 ingredienti slegati fra loro che non costruiscono mobile locale, come si fa a unire tutto in un unico spazio di ascolto!

Idea di Mobile e Locale, in cui vi è una sorta di ambilvalenza poetica, con ogni frase che ha all'interno ha la componente mobile e l'ictus locale.

(lezione di composizione di Ruggeri correggendo l'esecuzione)

Limare Mobile Locale da parte di Lupone, è stato trasfigurare il pezzo.

## 3 questioni fondamentali su Mobile Locale

[Referenze di Mobile Locale](https://github.com/s-e-a-m/References/tree/master/Lupone-Michelangelo)

1. codice del FLY non fornito ancora da Lupone
2. frequenza di campionamento a 22050 del FLY, urge fare un lavoro di archeologia -> un elemento della catena ha bisogno di potenza di calcolo utile per
3. anche se FAUST è un tool di dettaglio estremamente usabile, alcune questioni non sono facilmente risolvibili in termini di linguaggio alto

![*](*.png)
Asterischi che indicando che indicano che si possa fare solamente con un solo FLY30

Il QA e QF hanno indice di lettura che si muove avanti ed indietro in maniera oscillatoria, ed in funzione della velocità di lettura, l'oggetto QA e QF:
- può generare rumore
- incrociare la testina di scrittura

La memoria del FLY30 era: dritta, interpolata lineare, interpolata dritta...

Entro un oggetto in una memoria, leggo l'oggetto in memoria con un oscillatore, sto realizzando FM.

## Considerazioni generali

(Impegno personale nell'investire con i propri soldi ed il proprio tempo.)

(Sperimentazioni sul Synket è nella tradizione della scuola Romana)

SIM -> società fondata da Nottoli, Lupone etc... soldi, investimenti ed esubero tecnologico che fine hanno fatto?

Investimenti realizzati sulla ricerca vera, e non l'intrattenimento...

Lo sguardo storico non ci deve trarre in inganno...

Bisogna capire e guardare bene negli occhi i meccanismi che non hanno fatto progredire ciò.

Nell'arco di 12 anni circa sono morti:
- Evangelisti
- Guaccero
- Nono
- Macchi

Peso a livello storico...

Evangelisti ha smesso di scrivere per fare musica pratica a livello sociale, gesto forte che puoi fare solo se stai in vita.

Nuova consonanza è cambiata talmente tanto che fa fare il premio Franco Evangelisti...

Atti che hanno reso impossibile alla scena romana di emergere.

1972 simposio musicale organizzato sulla grafia musicale...

## Atteggiamento di rivendicazione

C'è questo atteggiamento che la scuola romana deve avere per non perdere la storia.

>L'arte non è educata

Leonardo Zaccone

_____

Scrivere su webinar, pensieri ed altro...

Se c'è una cosa che deve cambiare è l'indifferenza...

## Gestione del brano

Il brano era stato pensato come unica applicazione fluida...

Ma il tape è molto vincolante per il brano e dunque diveniva rigido in termini esecutivi.

Si è pensato di realizzare un vst per ogni oggetto utile.

I VST hanno aperto ad un altro mondo, ovvero avendo impostato il tape ed i plugin, si è pensato alle automazioni, pensando tutto autocontrollato e molto semplice da mettere in piedi.

Brano scritto con l'idea di mixer, e l'idea di avere un mixer che colleghi tutti i processi è già contenuta nel brano di Lupone.

Le mandate ed i ritorni al FLY apparivano con un mixer analogico...

**Il vantaggio più evidente, è che apparentemente il lavoro con vst suona meglio rispetto ad un'unica applicazione.**

Analisi fatte a livello numerico hanno fatto riscontrare che quando hai un software host che ti permette di sincronizzare buffer, tape etc... Assicurandosi che tutto è iperlineare ed in fase, si ha un controllo sull'impacchettamento di ciò che esce dal convertitore.

- unico host? funziona meglio??

Cosa suona meglio fra CSound, SuperCollider etc...

## Fin dove la sostenibilità deve arrivare?

Cosa deve passare? Alla fine di un percorso creativo si può scegliere lo strumento!

Per virtuosizzare bisogna partire da una base solida.

Non si parla piú dell'importanza di come realizzare lo strumento, ma si tratta di capire l'unico mezzo per realizzare il brano è il mixer.

Alla fine dei mesi di prove, vi è un video con la paritura proiettata, perchè non si può girare una pagina ogni 20 secondi.

La partitura scritta da Lupone è una partitura di composizione...
