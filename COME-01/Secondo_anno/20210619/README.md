# Appunti della lezione di sabato 19 giugno 2021

Macchinafunzionante FAUST

IR del KYMA per far funzionare tutto

Filtro RC realizzat per essere realizzato con computer analogico, e non con machina digitale.

 ![rev](revschr.png)

## Opzione per diagrammi esplosi di FAUST

```
faust2svg -f 0 -fc 10000 nomefile.dsp
```

## Issue creata per brano di Di Scipio

Problema di partenza all'avvio, con volumi alzati

## Cose di cui abbiamo parlato

- [Yamaha QL1](https://www.thomann.de/it/yamaha_ql1.htm?sid=477fea105c86740e5e388f8541cf8d0a)

- modellazione di filtri RC in FAUST

__________________

Guardare issues github
