# Appunti della lezione di giovedì 23 settembre 2021

La progettazione dovrebbe essere collegiale in concomitanza con tutte le parti.

Progetto didattico ed utilizzo didattico dei progetti.

Direzione della scuola di musica elettronica...

___

## Di Scipio
Punti di arrivio di Di Scipio

Ecosistemico 3 e altro brano di Di Scipio

IR di un KYMA per avere una convoluzione ed averla per poterla usare in futuro.

Articolo sul Dialogue di Boulez

### IR
>Quando si fa risposta all'impulso come proposto da Vidolin e Bernardini, ciò funzionerà solamente se il segnale diretto da la risposta alla strumento.

Il tempo che si investe per prendere la risposta all'impulso potrebbe essere troppo breve per poter prendere lo strumento.

La tecnica che si usa per la sala accademica è funzionante solo per la sala accademica che ha uno spazio in mezzo che descrive l'ambiente circostante.

(Burst di Linkwitz con risposte spaziali analizzabili nelle pause tra gli impulsi.)

Provare a scrivere un dettaglio e nell'ottica della sostenibilità c'è un problema di tecnica.

(Fasi di Lupone brano per portare i brani a compimento.)

### Tenere in piedi i brani

Tenere in piedi i brani vuol dire finire i brani.

- Fasi di Lupone
- Mobile Locale di Lupone
- Risonanze Erranti di Nono

### Nono PPPD

Paper da realizzare...

### Di Scipio

Si presta ad essere un modo per cambiare metodo di applicazione...

Di Scipio è un buon trampolino verso l'estero.

Cercare di non perdere tempo per lavorare e realizzare i brani.

### Cosa fare?

Abbiamo fatto per ora il Signal Flow 1a, successivamente faremo il Signal Flow 1b.

Abbiamo visto che lo schema ha una rappresentazione grafica che non segue un significato logico chiaro.

![sf1a](sf1a.png)

Siamo arrivati ai local max.

Brano per tromba, Modi di interferenza 1 di Di Scipio.
