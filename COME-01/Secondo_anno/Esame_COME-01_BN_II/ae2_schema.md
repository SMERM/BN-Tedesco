---
markmap:
  initialExpandLevel: 0
---


# AUDIBLE ECOSYSTEMICS n.2

## L'autore
### Agostino Di Scipio

## Anno di realizzazione

### 2003
----

## Il feedback

### Studio sulle soglie

### Ascolto, controllo e utilizzo

----

## La partitura

### I problemi riscontrati

#### Una partitura frammentata

#### Elementi da decifrare

#### Diverse versioni della partitura

##### 2017 versione da cui si è realizzato il porting

----
## Il nostro porting in FAUST

### Realizzazione di una libreria

### Testing e suddivisione nei vari signal flow

### Comprensione approfondita del funzionamento del sistema
----
## Il nostro schema finale

### Uno schema complessivo

### Migliore comprensione globale del brano

### Definizione di uno schema unico

----

## Il futuro

### Lo studio del Kyma

### Il brano in concerto

### Scalabilità del brano

### Sviluppo di un brano con il feedback

### SEAM
