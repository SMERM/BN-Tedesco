# Appunti della lezione di mercoledì 30 giugno 2021

Filtri butterworth del terzo ordine nel signal flow 1a, l'amp follower che utilizziamo ha come parametro il tempo di rilascio e non il tempo di attacco.

Si usa il rilascio e non l'attacco per avere un'attenuazione piú graduale.

>Avere tempi di attacco e tempi di rilascio dell'integratore sarebbe diverso per la gestione dei picchi e dell'ampiezza.

## L'amp follower

Vediamo che l'highpass fornisce piú materiale e dipende dal materiale che si sta utilizzando...

![amp.png](amp.png)

La comparazione fra i due amp follower fa highpass e lowpass...

Tutto ciò che non è in comune viene fuori, abbiamo le ampiezza di tutto ció che non vi è in comune fra le due bande, alta e bassa.

>Viene contraddetto il contributo stazionario...

>var2 = rough estimate of the center frequency in the spectrum of the room’s background noise (spectral centroid): to evaluate at rehearsal time, in a situation of "silence".

Per avere una risposta piú caotica non si da mai la ragione a chi da piú contributo.

![sottrazione_amp_follower.png](sottrazione_amp_follower.png)

```
(fi.highpass(3,var2) : an.amp_follower(0.05)),
(fi.lowpass(3,var2)  : an.amp_follower(0.1))
```

`map 1-x` realizza l'operazione di 1-sensitivity...

Correggere `localmax` che è un oggetto e deve stare in libreria.

>Gli integratori devono essere fuori dalla sensitivity...

Realizzazione di un delay con feedback...

Realizzazione del signal flow 1a di Di Scipio fino al delay di 12 secondi.

___
Codice su seam...

___
Masterclass di elettroacustica a Trevi in Umbria per studenti ed approfondimento con sessione di registrazione...

## Cose di cui abbiamo parlato

- [https://github.com/grame-cncm/faust/issues/571](https://github.com/grame-cncm/faust/issues/571)
