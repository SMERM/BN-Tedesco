# Appunti della lezione di mercoledì 7 aprile 2021

## Baggiani

- Twins
- Memoria
- Operina

Brani già eseguiti a EMUFest:
- Anabasi
- Percorso sulla fascia mutevole del suono

Concerto di Baggiani, concerto il 7 Settembre anniversario della morte.

[biografia di Baggiani](http://www.cidim.it/cidim/content/314619?id=242110)

Baggiani era di scuola strutturalista ed estremamente formato e strutturato.

L'elettronica di Anabasi venne realizzata da Massimo Massimi, cercando di coinvolgere Massimo nel concerto anche per realizzare tutto in Faust.

## Registrazioni
### Registrazione Mobile Locale

Parte finale persa di Mobile Locale dal concerto.

Registrazioni poi da realizzare.

### Registrazione altri brani

Altri brani da registrare prossimamente, Feldman e Cardi

## Vecchi corsi di conservatorio

Essi erano di fatto dei corsi sperimentali.

## Maestri di composizione a Roma

- Pelosi
- Piazza -> bravo docente
- Porena
- Matteo D'amico -> Nuova Consonanza
- Sbordoni
- Mirigliano


Scuola compositiva di composizione, non era quella a cui Guaccero e Evangelisti facevano parte. Guaccero non condivideva nulla con le scuole di composizione tradizionali.

Guaccero, Branchi, Bertoncini facevano tutto all'Aquila e non a Roma.

Frosinone è sempre stato un punto di riferimento per la composizione elettroacustica, poi cambiò direzione quando Nottoli vení a Roma.

Quando Nottoli studiava con Guaccero e Bertoncini, arriva da chitarrista a composizione facendo parte di un altro ambiente.

Nottoli aveva un collegamento con le società di industria, con la Rai...

L'unico acusmatico diffuso alla Scala di Milano è Solve e coagula di Nottoli.

Nottoli fece il concerto all'Angelica a Bologna nel 2018/2019, con musica acusmatica e live, con sette isole ed altri brani...

AngelicA è un festival di riferimento in ambito internazionale.

## Mobile Locale

Files scaricati

## Marco di Gasbarro

Note di esecuzione per mobile locale.

## Sciarrino

Il suono, il silenzio, l'ascolto - Sciarrino

Diagrammi a blocchi, aggiunta delle informazioni utili per la composizione, visione spaziale e poliformo.

Non ragiona mai in termini di note, alla fine finisco sul pentagramma degli agglomerati di suono, in funzione dei diagrammi a blocchi.

## I am Sitting in a Room

Modifica al codice in Faust, per STonE.

Aggiornare LAIS tutte le versioni non STone con il trick con microfono aggiuntivo.

1. input con microfono per l'incipit e microfono per il processo
2. router per alternare microfoni di ingresso e altri microfoni -> `si.smoo` prima del "*" per essere sicuri che non vi siano click

### `si.smoo`

È un filtro IIR, della tipologia one pole, in cui abbiamo sempre:
- un meccanismo di feedback, che rende infinita la risposta all'impulso

Oscillazione del filtro IIR
![iir](iir.png)

Il biquad è una tipologia di filtro complesso parametrizabile per ottenere diversi tipi di filtri del primo e secondo ordine.

Se mettiamo 0 al complemento, passa il segnale ma non viene moltiplicato per nulla.

Il filtro è il tipico filtro passa basso che da attenuazione a Nyquist, in cui la linea di ritardo è di un campione.

La curva passa-basso di questo tipo è a Nyquist, perchè possiamo rappresentare con un solo campione solo valore alto o basso del segnale.

In funzione dell'ampiezza del feedback attenua più o meno materiale, ma a Nyquist non ho nulla, perchè ho la controfase perfetta.

>La parte interna al feedback, è molto piú potente di ciò che entra nel filtro, quindi il segnale che sta transitando all'interno del feedback è simile a se stesso all'interno del feedback.

Il tempo di interpolazione lo gestisci con il tempo di feedback.

Un FIR, funziona bene solo se i coefficienti sono bilanciati.
![fir](fir.png)

(FPGA seminario FAUST, Sanfilippo(edge of chaos), lfsaw(processamento a 1 bit DSD, lavorare con FAUST fuori dalla frequenza di campionamento), Rosenborg(libereria per oggetti elettronici per replicare condensatori))

_____

Affrontare il discorso tecnico sapendo che la discretizzazione si porta dietro una perdita.

## Nottoli

Brani Ground, con elettronica da saldare.

Andare sull'analogico, facendo un restauro sul monocordo di Nottoli.

Il restauro digitale, lavora su roba fatta due anni fa.

>Non rimettere in funzione, ma conservare, mantenendo integro ciò che c'è e non ricreando dal nulla.

## SEAM perchè?

- maggior sensibilità della nostra generazione
- esigenza storico-culturale
- quantità di materiale sempre meno scremata

Dare un seguito a ciò che si fa, impoverendo lo spessore tecnico e culturale di quello che si sta facendo.

## Agostino Di Scipio

- presentazione del libro
- concerto a Parma di Di Scipio e Marchidis

Catologo di Di Scipio delle sue opere per una galleria d'arte.

Agostino Di Scipio ha sicuramente dei collaboratori.

Metodo didattico e di gestione del proprio impegno.

Articoli per JSTOR, spillati a mano.

______

Nottoli, fare una ricostruzione e farlo partecipare alla lezione.
