# Appunti della lezione di mercoledì 26 maggio 2021

Integrazione nella pagina di librerie di FAUST...

Primo concerto realizzato con applicazione di FAUST...

***

##  Agostino Di Scipio - Ecosistemico Audibile 2a/2b

![e](e.png)

Perchè è della Scuola di Musica Elettronica Romana?

Le influenze di Di Scipio sono provenienti dalla Scuola di Musica Elettronica Romana, osserviamo la partitura dell'Ecosistemico Audibile 2a e 2b, e poi il porting di Davide Maggio.

### Aspetti estetici di Agostino Di Scipio

#### 1 Aspetto linguistico

Aspetto linguistico dell'ecosistema in cui Di Scipio fa un passo nei confronti della composizione con un passaggio poetico e politico, come John Cage con nucleo poetico e tecnico che portano ad una vitalità sonora indipendente di un controllo compositivo.

Si sposta il problema immediatamente su questioni di ascolto. Per cui mentre in una composizione tradizionale vi sono elementi in cui vi sono trame e percorsi.

In Di Scipio non vi è un labirinto precostituito e vi è un labirinto da esplorare con le orecchie.

Una registrazione dell'ecosistemico audibile è una reliquia che non sostituisce un'intera opera.

>Di Scipio è comunque riuscito a creare uno spazio per la pubblicazione di questi luoghi...

Un brano come quello di Nottoli, portato su un CD è solo una cartolina da un solo punto di vista.

>Su CD avremo di volta in volta un pezzo del paesaggio.

**Cerchiamo ora i meccanismi che portano la struttura algoritmica ad essere il testo compositivo.**

Essendo un brano per Live Electronics, sia eseguito che autonomo, alcuni elementi avranno senso solo quando i feedback si chiudono. Ciò lo rendono un meccanismo sistemico, ogni senso viene usato per poter vivere e continuare a vivere, con una macchina che realizza una sua intelligenza.

>È importante che in una partitura in cui mancano i riferimenti alla musica tradizionale, in cui la macchina non ha del meccanico...

Di Scipio crea delle proprietà nel percorso sonoro che creano differenziazioni sul materiale e sul tipo di processo associato.

Ciò porta ad un fare musica dei grandi compositori...

Quello che Di Scipio chiama l'ecosistemico di primo livello(con _I am sitting in a room_), in cui si approfondisce una questione alla volta, un solo elemento.

Nell'ecosistemico audibile 2a, si ha un solo parametro di controllo, perno...

#### 2 Aspetto scenografico sonoro

Abbiamo comunque una macchina informatica che funziona solo in un contesto elettroacustico di tipo determinato:
- scelta dello spazio acustico
- scelta dell'apparato elettroacustico

Vediamo che la scrittura è determinata fin nei minimi dettaglio che fa uso di sistemi informatici non descritti fino alla profondità piú assoluta.

L'idea di interazione e maccanica interna dall'ecosistemico audibile...

(Di Scipio realizzò la traduzione del testo di Gabor sul quanto sonoro...)

Di Scipio ha sempre mostrato un interesse verso il pulviscolo e il grano di suono.

In Di Scipio è tutto ben strutturato, con una catena di montaggio, in cui se i pezzi non vengono uniti non abbiamo nulla.

Parlando di granulazione, una difficoltà di [Davide Maggio](https://gitlab.com/DavideMaggio/ae), fu quella di implementare il granulatore in FAUST.

(Granulatore di Markidis, con oggetto di Pure Data, vi è tutta una struttura su come allocare le risorse per ogni grano.)

Marco Markidis realizzò il porting di Pure Data con Di Scipio...

Avere ad esempio la risposta all'impulso dell'integratore aiuta a realizzare l'integratore in momenti diversi...

Avere accesso al porting di Pure Data fa capire l'interpretazione di un altro esecutore.

Ad esempio il filtro Low Pass del quinto ordine, che tipologia del filtro è?

Un livello di interpretazione, solo conoscendo bene la macchina quando si è dal vivo fa capire come realizzare il brano.

### Descrizione della partitura: a cosa serve un mixer analogico in questo caso?

![r](r.png)

Per non aggiungere un ritardo nella catena acustica, è preferibile per via della responsività maggiore e per via del filtraggio che non esiste, non avendo ritardi del segnale.

La qualità prodotta dal mixer, influenza il risultato finale prodotto:
![m](m.png)

### I microfoni e il controllo di essi

I microfoni 1 e 2 vanno

I microfoni 3 e 4, devono essere in delle posizioni che creano feedback con frequenze diverse.

Le distanze da tenere in conto sono quelle che creano diverse riflessioni...

>Poche volte si sente dire che con una posizione di microfono diversa si realizza un feedback diverso...

Brano per i luoghi...

**2 posizioni differenti che causino feedback differenti, realizzano frequenze differenti.**

Nella cultura romana, crescere con una consapevolezza del feedback non deve far descrivere fenomeni ovvi, come l'esistenza di zone di un ambiente che creiino dei ventri e dei nodi come se fossero delle corde.

La scoperta di luoghi con nodi nasce dall'ascolto del luogo.

>Il primo elemento dopo gli ingressi microfonici è  un equalizzatore.

### I 2 tipi di feedback in Di Scipio

2 feedback si intrecciano e la relazione fra i due crea la poetica...

### I microfoni 3 e 4

Essi sono invece fuori dall'area degli speaker e permettono di percepire diversi pattern e riflessioni acustiche.

### Gli speaker

Essi possono essere da un minimo di 6 in poi...

>Il musicista sceglie una precisione sul risultato in base al risultato che si vuole.

### Differenza di precisione della scrittura

Qualsiasi partitura di Mark Stabat inserisce precisione fino al centesimo.

(differenza fra Netti, Di Scipio e Marc Sabat è che vi è un aspetto timbrico con differenza di scrittura.)

>Spesso si confondono i livelli di complessità con la scrittura dell'altezza mentre in altri momenti è piú importante la scrittura fedele del timbro. La scrittura è il veicolo della precisione ma non la si ottiene mai con i vincoli dell'azione. Ma scrivendosi il luogo sonoro che si crea.

A ciò, se è insufficiente la scrittura ed i limiti posti, si aggiunge il tape.

### Dopo il porting, la ri-nascita dell'esecutore

Nasce l'interprete dopo che l'unico interprete è stato il compositore, con un esecutore che supera le aspettative del compositore.

>Si ha il concetto della trascrizione di Paganini per capire come superare Paganini...

### Risposte all'impulso dei componenti

Il risultato di una macchina è basato sulle risposte all'impulso...

La risposta all'impulso per noi elettroacustici è come una nota, ovvero l'impronta di un sistema.

Quale tipologia di dati ci si aspetta da una risposta all'impulso.

### KYMA macchina usata da Di Scipio

Il KYMA è un hardware che attaccato al computer sfrutta le potenzialità software del computer per programmare, e quelle harwdare per DSP.

Sappiamo che le macchine a livello di calcolo sono imprecise, per via dei diversi accessi che si fa ai registri.

![Kyma-software.png](Kyma-software.png)

[https://kyma.symbolicsound.com/](https://kyma.symbolicsound.com/)

[https://kyma.symbolicsound.com/whats-new-in-kyma-7/](https://kyma.symbolicsound.com/whats-new-in-kyma-7)

![symbolic-sound-capybara-320-1219-8_1024x1024.jpeg](symbolic-sound-capybara-320-1219-8_1024x1024.jpeg)

### Strumenti per fare elettroacustica

Strumento al centro dell'eseguibilità, che abbia quindi una risposta analogica con processo e gestione digitale.

>Importanza della Scuola basata sul giudizio uditivo, e che costruire un qualcosa cambia sia il percorso che il risultato. Nel momento in cui la musica è fatta di utilizzo e non piú di creazione, è veramente tutto finito.

Per comprendere il modo in cui si fa un feedback è prendere il microfono e navigare la stanza e vedere che cambia la relazione con l'ambiente, ciò è un procedere per conoscenza che è avvenuto con anni di lavoro.

>Consapevolezza che non è nel tipo di filtro la chiave del brano, ma nella comprensione di un compositore con Agostino Di Scipio.

***

Distanza di Nottoli da Nuova Consonanza è uno smettere di realizzazione della computer music com'era ed un avvicinarsi agli strumenti, che sancí la fine degli acusmatici di Nottoli.

***

Abbiamo la possibilità di vedere un Synket restaurato dal CRM che è concepito da un ottica strumentaria, ovvero uno struemento autonomo.

>Miniaturizzare per rendere standalone uno strumento...

***

Cercheremo in futuro di provare il brano di Di Scipio a studio da Giuseppe...
