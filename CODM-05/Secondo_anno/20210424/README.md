# Appunti della lezione di sabato 24 Aprile 2021

## Webern e Bartok
Webern ha una razionalità medievale, in cui i fini ed i mezzi sembrano coincidere.

Webern voleva rifondare la musica a partire dai fiamminghi, facendo fuori il fardello armonico e recupare una comprensione orizzontale della visione musicale.

Webern non si fa inquadrare in uno slogan.

25 Marzo, 140esimo anniversario della nascita di Bela Bartok, dimenticato da tutti...

Perchè Bartok non lo celebra nessuno?

Cosa è mancato ad una figura come Bela Bartok?

A Bartok è mancato il concetto pilota, qualcosa del "compositore", un interpretante dal punto di vista semiologico.

Stravinsky aveva un cinismo intollerabile, ed era un antisovietico che andò via dalla Russia, ed il suo essere russo ma dissidente, lo hanno sorretto.

Facciamo sempre più fatica a capire chi siano i compositori importanti.

Schoberg è rimasto vivo, grazie alla memoria e grazie allo stigma che lo ha reso decisivo...

Bartok è stato un esule, ed è scappato da un regime fascista, ma non era ebreo...

Etc...

Bartok non distrusse la tonalità, ma l'ha superata, inglobandola nel sistema suo personale(musica per archi celesta e percussioni, secondo movimento Adagio del secondo concerto per pianoforte, in cui vi sono gli archi con la sordina che si muovono in maniera omoritmica)

Dall'altra parte ha fatto entrare la musica popolare, inserendola con l'occhio dell'etnomusicologo...

Bartok ha fatto convivere in una multidimensionalità con qualcosa a metà tra una modalità esotica e una modalità geometrica, incastonamento del materiale musicale che funziona, che non è incastonata...

Nella nona di Dvorak, la sentiamo su moduli timbrici, con armonia su settimo grado alterato, con effetto di folklore.

Raccolta di canti popolari armonizzati da Beethoven in cui si trovano armonizzazioni molto strane. (Raccolta di melodie popolare irlandesi Beethoven)

>Le competizioni sono per i cavalli(Diceva Bartok)

[Bietti sulla musica popolare di Beethoven](https://www.youtube.com/watch?v=SahSKXx1Z-g&ab_channel=LezionidiStoriaFestival)

Bartok si percepiva come insegnante di pianoforte...

Bartok morí in miseria, facendo la fame a New York...

Ascoltare i Mikrokosmos di Bartok!

Bartok ragionava come se fosse un elemento popolare, un portare di prima istanza, non interpretato.

Attraverso la ricerca, Bartok è riuscito a percepire cosa lega canti popolari di zone diverse.

>L'identità nazionale con la musica non c'entra nulla, la musica popolare scivola continuamente...

Bartok non era un personaggio, ed oggi allora facciamo fatica a ricordarci di lui, perchè non ha avuto i salvagenti che hanno avuto gli altri.

**Illeggibilità del contenuto musicale in quanto tale, che indirizza su un piano alto rispetto a quello puramente artistico.**
***
Mila è sempre un po' crociano quando cerca di spiegarci il presente.
***
## La canzo medievale
La "canzò" medievale in cui testo e musica nascevano insieme e in un epoca più tarda vennero raggruppati in canzoneri, che contenevano:
- la vida -> vita dell'autore
- ogni canzo aveva la sua ragione(razó) con un come e un perchè, che cercano di dare una plausibilità storica, senza questo cappello introduttivo, non si poteva dare una leggibilità

La reinessance di Mozart vi è stata all'inizio degli anni '90, sull'onda del film, anche se egli era una persona qualsiasi anche un po' anonimo.

## Allevi
Perchè esiste un qualcosa come Giovanni Allevi?

È riuscito a occupare una nicchia di mercato, che indica che la musica deve essere orecchiabile.

Allevi come fenomeno, si è costruito dei salvagenti concettuali che non hanno nulla a che vedere con la sua musica.

Se Allevi fosse stato un musicista di valore maggiore di quello che è, solamente costruendosi il personaggio avrebbe potuto avere successo.

Senza essere giovane pazzerello, non avrebbe avuto successo, in una grotta che realizza disagio culturale.

>Dietro l'innocenza di Allevi vi è la sindrome della maggioranza forte che si sente vittima della minoranza debole.

Lettura dell'estesica di un musicista come passaggio attraverso degli interpretanti(metafora presa dalla semiologia di Pierce), un segno non è fatto come un oggetto chiuso fatto di significato ed un significante, ma che rimanda all'infinito a significanti, ovvero interpretanti.

>Bartok interprentanti di fatto non ne ha.

Abbiamo usato quindi l'interpretante come una chiave di lettura.

L'europeizzazione della cultura di Cristo passa attraverso la germanizzazione.

## Berio e Ludovico Einaudi

Gesto distruttivo di Berio dicendo che Ludovico Einaudi era il suo unico erede musicale.

L'aura nobile mise Einaudi nella posizione di identificarsi culturalmente in modo diverso, piú borghese.

Questione di critica e di giornalismo...

Avendo una classe dirigente che non riconosce la cultura di un certo livello.

Richard Clayderman, Steven Slacks...

Musica strumentale per playboy, e nessuno diceva che erano grandi compositori, neanche loro stessi.

>Vi erano pochi generi ed una grande sicurezza musicale...

Oggi non vi è una certezza musicale.

Idea delle letture...

>Manco a Bartok il rimorchiatore.

***
## Lucier e IASIAR
Celebrazione di Alvin Lucier con "I am sitting in a room" il 14 Maggio 2021.

In I am sitting in a room, si parla d'altro, e ancorare ed il porre Lucier solo a questo brano è limitato e limitante.

Alvin Lucier diviene un aspetto di I am sitting in a room, purtroppo, il destino è un qualcosa che prende vita propria e diviene rituale, ovvero di un processo.

## Bartok e Adorno

Adorno aveva paura che smettendo di essere calda la dodecafonia divenisse un genere.

Bartok a questo punto non vi era arrivato.

Bartok concerto per viola, con convergenza fra geometria e canto popolare, mentre il concerto per orchestra, è piú arioso, comprensibile e sorridente e non vi è uno stacco netto, ma un continuum tra il Bartok piú celebrale e quello piú melodico.

Nel 1950, Bartok era vietato da suonare a Parigi, con un suo senso, perchè la musica diviene un dispositivo di conservazione sociale.

Si mettono a rischio altre strutture.

[libro sulla vita di Bartok](https://www.ibs.it/arte-di-bela-bartok-ebook-massimo-mila/e/9788858662465)
***

## Vox 5

_Ascolto di Vox 5_

Lettura dell'articolo di Vox per la bibliografia...

Quadro che inizia e finisce con un Turner ma che racconta di fatto un Pollock.

Ripresa di qualsiasi tipo mnemonica, è come se tutto ciò che viene ripreso, si staglia degli elementi caotici.

Naturalezza della contrapposizione fra oggetti sonori, con realtà come si presenta e realtà manipolata o modificata.

Wishart non si crea il problema di dare un'impronta soggettiva di dare una realtà data alla sua gestualità, con il continuum che è pervasivo.

Gesto corragioso, di Wishart, che mette l'accento sulla disinvoltura con cui Wishart mette insieme due teatri e sono diversi con una tradizione che fa divenire tutto plausibile.

Musica di Wishart che mette in luce delle relazioni che nella realtà delle esperienze sonoro sono implicite.

Continuum che viene realizzato con figure profilate.

### Cuore della questione è nella molteplicità degli ascolti

1. primo ascolto, arranchiamo nell'ascolto
2. subito dopo, diveniamo predittivi

**Qual'è l'ascolto più leggittimo?**

L'ascolto deve procedere ad una leggerezza parallela con la musica.

Piú si ascolta un brano, piú gli appuntamenti attesi ti creano forma.

Segmentazione di un tragitto naturale, che accorciano la percezione del viaggio.

L'idea dell'ascolto come percorso che puó essere compiuto una volta o piú, ci pone un problema per confrontare le intenzioni di Wishart con conseguenze inattese di un ascolto sapiente.

### Questo brano che ha la capacità di connettere regioni del continuum che non esistono insieme in genere, è un brano che costruisce esperienze che prima non c'era.

Brano che mi fa ascoltare il cavallo con la voce che c'è dentro.

Si può ascoltare il brano one shot?

Idea del continuum, che stimola la visione del processo e fa stagliare elementi.

### Componente virtuosistica

Registrazione lunga della pioggia, è un fare ammenda degli aspetti virtuosistici?

La parte finale è quasi uno sfondo che risalta fuori da sotto.

Le operazioni di contorno creano discorso, ed il paesaggio sonoro ci suggerisce in questo brano che siamo in un paesaggio ed invece siamo in una fotografia.

Equivoco del brano...

Visione panteistica, che si lega alla sostanza di Spinoza e quindi al continuum di Wishart.

Wishart è un classico perchè, nomina le cose con molta semplicità.

Il problema dell'essere tra l'oggettività e la soggettività, cercando di risolvere il se e l'io delle cose è il tema di una composizione come questa.

Un autore diviene un classico, puntando una luce ben precisa su una tematica e perchè cerca di dare un'esperienza complessiva dell'elettroacustico.

Tutta la contrapposizione tra uomo e mondo, sembrano superare la dialettica.

>La musica concreta è per Schaeffer musica che non ha alcun livello astratto di simbolizzazioni. Musica che non si puó notare e stoccare come astrazione.

La musica è concreta perchè esaurisce il simbolo e la materia, mentre nella musica astratta i simboli rimandano alla materia.

Quando la musica non gode di un livello astratto, allora è musica concreta.

(Schaeffer aveva costruito: editor multitraccia, delay multitap, e time streching e pitch shifter)

Quando ci si pone sulla creazione di oggetti sonori...

***

Bartok era uno dei motivi che fece nascere la scintilla tra Pizzaleo e Mario Bertoncini.

Bertoncini è stato un grande interprete bartokiano e suonò i concerti per pianoforte.

Dal punto di vista dell'avanguardia essere appassionati del pianoforte e di bartok può essere considerato un atteggiamento di conservazione.

Bartok è stato guardato con sospetto, perchè Adorno capiva che Bartok non era Stravinsky, con una tonalità contraddetta, modalità deformata.

- concerto per orchestra di Bartok -> Gioco delle coppie con il corale di ottoni -> parte del recitativo realizzata da un tamburo con 5 accordi realizzati dai tromboni, sublimazione delle forme del passato senza citarle (grande capacità di Bartok)

## Bibliografia

- Pizzaleo - Musica elettroacustica a Roma -> tutto
- Pizzaleo - Liutaio elettronico -> parte sul Synket
- Pizzaleo - MEV -> capitoli teorici sul MEV (improvvisazione)
- Wishart -> primi 4 capitoli di On Sonic Art
- Wishart -> articolo del 1988 in cui descrive il processo compositivo di Vox 5
