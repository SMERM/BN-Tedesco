# Appunti della lezione di Sabato 19 Dicembre 2020

**_Appunti di Giovanni_**


Gelmetti è Veronese. Figura chiave della musica romana ma era Veronese.

Gino Marinuzzi Jr. è milanese.

Riascoltare un po’ la prima oretta di lezione
Discussione intorno al Live Electronics, al concetto di acusmatico e alla definizione di strumento “elettronico”  lo strumento deve avere una serie di discontinuità (che possono essere gestuali o di altra natura…)

Lyotard – La condizione del postmoderno e delle grandi narrazioni (vedere gli appunti del I BN)
La condizione cristiana e quella illuminista. La fine delle grandi narrazioni è la fine della visione terrena e di quella ultraterrena. Queste due visioni vanno in crisi.

Pizzaleo afferma: “non sono sicuro che il postmoderno sia finito del tutto”.

La definizione di Live Electronics ci sfugge nei suoi dettagli… Che cosa è il Live Electronics?
L’esperienza del pezzo di laurea di Davide Tedesco (tutte le tracce registrate invece che live). 

**_Appunti di gabri_**

Chi dice che Roma é simile a Milano mente:

Studio di Milano nasce per fare musica elettronica. Quelli a Roma ospitano, sono fatti per la produzione rapida, non vogliono essere macchine di ricerca. Sono pensati per la produzione cinematografica. Stretta relazione con il mondo anglosassone e statunitense. Tendenza all'aggregazione/ sodalizi di natura piu o meno commerciale o progettuale, come MEV, Musica ex machina, R7 ecc.. Carattere multiforme delle sue produzioni, a milano nastro magnetico come campo di lavoro (strumento principale), a Roma tutte le caratteristiche elencate prima producono musica ibrida per teatro, musica improvvisata, musica con apporti strutturali diversi (mista, acustica, elettronica, live electronics(non corrisponde alla nostra visione contemporanea))

**Live electronics:** definizione difficile. Significato contestuale ci é chiaro. Non é la stessa cosa in UK o in Italia. UK: tutta la musica dal vivo che comprende musica elettronica (anche regia). In Italia: musica mista, interazione in tempo reale con musica acustica. Perché dobbiamo definirlo? Per fare classi di concorso? Per taggare produzioni discografiche su un database? Risposta piu sensata é: non esiste, perché non serve definirlo in quanto essa cambia in base alla pertinenza con la performance. A Roma il live electronics é: microfono a conttato, sonorizzazione onde celebrali, esecuzione dal vivo con sintetizzatori, elaborazione di strumenti musicali in tempo reale. Questa é la definizione piu ampia. Se escludo il synth dal l.e. perché non c'é elaborazione dal vivo é un'affermazione faziosa. Perché non deve esserci lo strumento acustico? L'unica cosa che non deve escludere é l'elettronica. Mancata definizione omologata di strumento elettronico é la base del problema.

**Acusmatico:** oggetti sonori e non eventi sonori.

![oggetto_evento.jpg](oggetto_evento.jpg)
_____________

**_Appunti miei_**

## Punti chiave della musica elettronica romana
Riassunto dei 5 punti della musica elettronica romana:
1. Policentricità e assenza di un centro egemonico
2. Contiguità con la musica applicata, contiguità con il cinema
4. stretta relazione con il mondo anglosassone e statunitense dovuta a un flusso di persone attraverso il canale della'Accademia americana
5. tendendza all'aggregazione come MEV, R7 e Musica Ex Machina
6. Carattere multiforme delle produzioni(non solo su nastro magnetico come a Milano) a Roma,

Tutte le caratteristiche elencate prima portano a questa constatazione, realizzando
:
1. musica per teatro
2. musica improvvisata
3. musica con apporti strumentali diversi (acustica ed elettronica)
4. Live Electronics

### Ma cosa è Live Elctronics?

Diverso in Inghilterra ed in Italia
- UK ascolto di qualsiasi cosa con regia del suono
- IT elaborazione in tempo reale di qualcosa di acustico

Ma è veramente cosí?

Perchè abbiamo bisogno di definire il Live Electronics?

"Il Live Electronics non è niente, perchè non esiste un'autentica necessità di definirlo, e muta a seconda di ciò che noi vogliamo intendere"

### Oggetto Sonoro e acusmatico
![oggetto_evento.jpg](oggetto_evento.jpg)

Non possiamo prescindere dalla causa e dal significato.

(A Trevor Wishart non interessa dell'ascolto ridotto)

A Roma si fa anche il Live Electronics:
- microfoni a contatto
- sonorizzazione onde cerebrali
- escuzione dal vivo di strumenti amplificati
- esecuzione con sintetizzatori

Ovvero elettronica prodotta in un contesto live.(questione affrontata da Emmerson in Living Electronic Music che allude alla musica viva e dal vivo)


(Discorso sull'italiano e l'inglese e l'utilizzo dello stesso nell'educazione! Inglese usato dove non necessario...)

Dove vi è una situazione performativa e dove vi è l'elettronica siamo puntati a dire che si tratti di Live Electronics.

La questione del Live Electronics è particolare e perchè devo indossare l'armatura per definire cosa sia il Live Electronics?

### Inizio del postmoderno
- La condizione postmoderna di Lyotard(in vui vengono affrontate le grandi narrazioni: narrazione cristiana ultraterrena e narrazione illuminista che postula un progresso continuo (la fine di queste narrazioni è la fine dell'idea del finalismo))
- Intertestualità di Julia Kristeva

Quando verrà superato il postmodernismo?

La concezione di Live Electronics ci sfugge nei suoi dettagli ma non nella sua accezione più ampia e meno polemica!
________
(Il brano nell'esecuzione dal vivo prende una vita, un carattere, come se da un film in bianco e nero ci si ritrova in mezzo alla scena.)


Negli anni '80 e '90 il Live Electronics era il MIDI, e potevi fare le mani.

Il lavoro di Curran, come tutto il sonoro rientra nelle possbilità dell'esecutore(MEV):
- campanello
- Moog (Richard Tautelbaum)
- Armamentario per sonorizzazione onde celebrali
- microfoni a contatto
- tromba rotta

È sospeso ogni pregiudizio su ciò che produce suono.

La soundpool invitata a "buttare il suono nella piscina".

Vi è l'high tech, e il low tech e nulla esclude nulla.

(Alvin Curran con il Giardino Magnetico, che era la sua performance solitaria)

## Pagina delle composizioni scritte nell'ambito della musica romana

Chi ha fatto cosa e dove?

Nella lista riportata nel libro!

Lo specchio temporale va dal 1957 al 1953, con il laboratorio.

Marinuzzi è un esempio particolare di compositore di avanguardia che è impegnato non solo nell'avanguardia e nell'elettronica.

Berio invece a Milano è un post-seriale che si affaccia all'elettronica poichè in Germania vi era l'elettronica.

Caso particolare è Franco Evangelisti che è compositore romano ma anche tedesco e scrive Incontri di Fasce Sonore, emblema dell'estetica tedesca. Compositore che si da all'improvvisazione e non scrive per molti anni, che girava molto in Germania.

### Marinuzzi
Molti compositori romani arrivano all'elettronica per strade diverse(diversamente da Berio e Maderna), come Marinuzzi compositore molto abile che scrive però una musica tradizionale cinematografica.

Se ascoltiamo i _Dialoghi nell'infinito_(radio dramma con musica, polpettone spiritule, letto da attori straordinari) di Marinuzzi e Fellegara, sentiamo le parti elettroniche coerenti degli anni '50 di Marinuzzi, mentre le parti strumentali scritte da Fellegara sono coerenti con le parti strumentali dell'epoca.

Mancando una correlazione con un'accademia istituzzionalizata dell'avanguardia, come Milano(Guaccero è nel periodo bartokiano, Petrassi ancora non è agli ultimi sviluppi), su un piano puramente neutro.

Sentendo quello che dice Petrassi della musica elettronica capiamo che lo studio dell'Accademia della Filarmonica era pressochè uno scantinato.

Abbiamo alcuni studi di Marinuzzi realizzati alla Filarmonica Romana.

- Terrore nello spazio -> Mario Bava

### Fono Roma
Studio di sonorizzazione per le colonne effetti e commenti sonori dei film

#### Carlo Franci

3  invenzioni su nastro

### Accademia america


La musica seria era altra cosa in America, che era tradizionale come Copland e Barber (compositori piú classici, che non hanno fatto passi da giganti per una rottura del paradigma, non sono dei Maverick, ovvero pazzoidi come Nancarrow che aveva capito che il rullo bucherrellato poteva far fare cose che un essere umano non poteva fare; Cowell scriveva musica per la cordiera)

I compositori della scuola di Princeton, come Babbit che è un compositore colto, seriale e che si occupa di musica elettronica, e come Babbit abbiamo Elliot Carter, maestro di Alvin Curran.

#### Otto Luening

Esso faceva parte dell'avanguardia americana accademica opposto a Cage, il quale era in patria marginalizzato, considerato anche un compositore di musica applicata.

Era l'anima ed il direttore dell'Accademia Americana a Roma faceva parte della scuola accademica riformista americana filo-Babbitiano, ed erano presenti delle attrezzature all'accademia americana proprio grazie a Luening. Il laboratorio verrà poi ristrutturato nel 1962.

Luening scrisse i brani piú importanti a Princeton.

### Roman Vlad

(Muovesi l'amante vita leonardo ornella vanoni nel teatro Straederino, come Fiorenzo Carpi girava in quel teatro con la Vanoni, Carpi era autore delle musiche di Pinocchio)

Roman Vlad fu direttore dell'Accademia filarmonica romana, con grandi poteri e noto al grande pubblico per le registrazioni di Benedetti Michelangeli (sabato sera sul tardi). Egli scrisse radiodrammi come il Requiem per una Monaca, Requiem for a Nun, che si pensa che abbia scritto questo radiodramma diversamente dall'Accademia Filarmonica Romana.

Vlad è un compositore accademico scisso tra la musica applicata e la musica colta.

Le cose per la radio e la televisione le realizza a Roma, ma quando scrive il suo pezzo di vetrina di musica elettronica, Vlad lo va a scrivere a Milano.

Vi è una schizofrenia con un'operatività alla buona che si faceva a Roma, e una operatività che si realizzava a Milano per la musica colta e d'avanguardia.

- Il dottore di vetro
- la ragazza in vetrina

### Antonio di Blasio

Studio I

### Vittorio Gelmetti
Misure I

Gelmetti e De Blasio confluiscono in un disco che fu pubblicato dalla discoteca di stato, con anche Studio II - Energia e Tempo.

Questi compositori gravitano tra ISPT e la Discoteca di Stato.

Queste composizioni hanno molto del catalogo e poco della composizione, e sono una testimonianza di interesse precoce.

Evangelisti parla della musica elettronica come studio di Colonia ai Martedí dell'eliseo, e i compositori romani iniziano ad interessarsi alle possibilità estetiche della musica elettronica.

"La musica elettronica a Roma nasce come già applicata", e dunque la musica elettronica a Roma come musica pura è solo una transizione.

A Roma la musica elettronica non diverrà mai un elemento completamente autonomo, è ancora un linguaggio ibridato, non si arriva mai a composizioni elettroacustiche con nastro magnetico.

(Antonio De Blasio è uno dei fondatori di Nuova consonaza)

- Modulazioni per Michelangelo(Musica per mostra di Michelangelo) realizzato all'IPST -> importante l'idea e Gelmetti accompagna delle riflessioni a queste musiche -> musica che diviene un elemento di un linguaggio piú complesso in cui la dominante non deve essere soltanto la musica, ma essa puó anche essere da servizio. (anche la musica per danza) ibridazione dei codici.

- Deserto Rosso di Antonioni fu scritta da IPST

#### William O. Smith (Bill Smith)

- Esso è stato un compositore attivo all'accademia americana di Roma.

- Clarinettista d'avanguardia di Free Jazz.

- clarinettista di riferimento di Luigi Nono(prima esecuzione della Floresta di Nono)

Ha contatti ravvicinati con la musica europea oltre quella romana.

Egli fu uno degli unici acquirenti del Synket di Ketoff.

E la moglie Jane Schoonover  fu una delle uniche virtuose del Synket.

(Vi sono istituti nazionali che fanno promozione della nazione e ospitano stagisti e borsisti in vari campi)

### Maderna

Curiosa interpretazione di RAI con un concerto per Oboe che molto probabilmente venne realizzato a Milano.

### John Eaton

Concert music for tape and jazz ensemble, che era l'uomo dell'accademia americana interessato al Synket.

Il brano del 1964 è un prodromo della stagione del Synket.

- songs for RPB, prima composizione importante e non dimostrativa(come piece for Synket) che ebbe varie esecuzioni, il soprano era la Koraiama

### Egisto Macchi
Problema non risolto -> studio proprio

### Domenico Guaccero
Problema non risolto -> studio proprio

Ma questi studi da cosa fossero caratterizzati non lo sappiamo.

- claviatura

### Larry Austin

Pezzo teatrale prodotto presso l'Accademia americana a Roma. Austin è uno degli animatori dell'avanguardia americana e fondatore della rivista Source.

### Gruppo di Improvvisazione Nuova Consonanza

Walter Branchi sostiene che l'elettronica non abbia mai avuto spazio nel GINC perchè la ricerca acustica sugli strumenti, estraendo sonorità particolari dallo strumentario tradizionale.

Esiste un disco della Detusche Gramophone con Jewsky che suonava elettronica, e vi è stato un periodo del GINC in cui venne prodotto qualcosa di elettronico. Dove venivano realizzate queste cose? Molte erano realizzate direttamente in studio.

### Stephen Albert

Fu un altro di quelli che si comprò il Synket, Albert è noto come un compositore pre-romantico.
Che ci fa intendere l'estensione della lista.

### Allan Bryant

Quadruple Play realizzato allo studio MEV.
1966 per il festival Avanguardia musicale, messo su da Musica Elettronica Viva.

La sede principale era la chiesa americana di St.Paul's a via Nazionale.

Bryant scriveva le cose per elastici amplificati, hackerava gli organi elettrici.

Egli era un pazzoide che si era inventato anche una sua riforma ortografica dell'inglese.

Con gli strumenti faceva musica tonale, mentre con gli elastici o altro faceva altra musica.

Questa mentalità è molto interessante e sta dicendo che ogni strumento ha la sua musica.

Non vi è una differenza di gerarchia, etica, estetica e storia e vi è una differenza puramente funzionale.
_____________
Come è stata compilata la lista di composizioni oltre al testo di Davies? Osservando il  catalogo di Davies!
