# Appunti dell'incontro con Giorgio Netti di mercoledì 13 ottobre 2021

## Il ciclo dell'assedio
![1](1.png)

Iniziare dalla fine arrivando alle ragioni poetiche partendo da un'analisi sommaria di quelle che sono le caratteristiche principali, in modo da ritrovare con più consapevolezza ciò che è all'origine di tutto.

![2](2.png)

Vi sono 3 aspetti del ciclo:
- interno
- esterno
- medio

Vi sono 4 aspetti _)place(_ nella prima parte vi sono 4 elementi, che assediano il pubblico.

Poi in _rinascere sirena_, si hanno poi 3 elementi.

In _inoltre_ i due violini sono il soggetto agente.

Vi è nella quarte parte il vocello, ovvero e da monitor che circondano il pubblico come la voce dentro il violoncello.


### Il livello tematico mediano:le preparazioni

La qualità del suono, cambia in base a dove mettiamo le preparazioni:
![3](3.png)

Tutti gli strumenti oltre il ponticello hanno varie preparazioni a fare un'estremità mediana.

Il livello tematico mediano contiene sottozoone, con relazioni delle due preparazioni dello stesso strumento.

Si vede una prima lamaella metallica, grezza e tutta a sinistra, ovvero le lamelle a stucco, ovvero in acciao armonico.

Nel caso del quartetto d'archi nell'ultimo movimento denominato _apparizioni_ con il si risonante.

### Trio

Nel trio si abbandandonano in parte le preparazioni con lamelle...

Vi sono ora delle lamelle plastiche e rimangono solo quelle metalliche lasciando la quarta corda libera.

![4](4.png)

Da un punto di vista strumentale, se ho una corda libera:
- ho accesso a un suono preparato
- ho accesso ad un suono di uno strumento normale

Le preparazioni plastiche che erano sul lato del ponticello, ora sono sul lato dove si suona.

Dove vediamo A, a, B e b etc... Vediamo le descrizioni delle preparazioni.

Nel violino la lunghezza della tastiera e delle corde può cambiare molto, e nella viola cambia molto...

L'unica maniera con cui misurare in maniera precisa è misurare dal ponte.

La lettera grande significa lamella grande e la lettera piccola significa lamella piccola.

### Duo
Con i due violini scompaiono anche le preparazioni plastiche, facendo sempre piú spazio verso lo strumento tradizionale.

![5](5.png)

Possiamo suonare ora sia sullo stesso strumento preparato, sia sullo strumento libero con un suono naturale sulla quarta corda, sia sulla terza corda dell'altro violino.

### Vocello

È presente solo la lamella metallica non tagliata:
![6](6.png)

La lamella per il violino e la viola era troppo grande e la misura va centrata in un giusto equilibro fra qualità del suono e smorzamento che rimane comunque collegato alla presenza di un elemento estraneo sulle corde, e vi è comunque una sorta di effetto sordina, ma non amplifica il suono di uno strumento ad arco l'avere una lamella nelle corde.

Anche le preparazioni morbide sono state lavorate.

Dopo aver associato l'associazione di materiali non si puó prescindere dal lavoro per ottenere risultati.

Nel vocello vi è solo la lamella metallica tra la prima e terza corda e questo solo finale è veramente ridotto all'osso, come una testa senza corpo e non vi sono archi, ma solo pizzicati.

La preparazione serve per donare elementi timbri in piú...

Grazie alle preparazioni si sono affinate le caratteristiche senza bisogno di preparazione.

Vi è in questo brano una mediazione tra suono alterato e naturale.

In questo comportamento è stato necessario riconoscere la tradizione nella novità e la novità nella tradizione.

La possibilità di integrare la nuova musica nella tradizione da uno spessore maggiore della nuova musica.

### La forma

#### Primo brano
Sette parti di 27 minuti senza interruzioni:
![7](7.png)

Ciascuno dei 7 temi ha uno degli aspetti mediati dalla tradizioni.

A destra vi è un primo livello di sviluppo di ciò che vi è a sinista.

A sinistra vediamo i sottomovimenti relativi alle sette grandi parti.

L'ultima parte del primo movimento ostinato è O.c quindi ostinato-cuore.

A questi movimenti corrispondono delle qualità di azione, piú che delle qualità sonore, anche se non si possono scindere le une dalle altre.

>Vi è un lavoro che dal macro entra sempre in profondità per sapere a che cosa di esterno ci si sta relazionando.

#### Secondo brano

La durata si accorcia e vi sono 5 parti con una costruzione ricorsiva e ipnotica:
- mare
- vortice
- onda
- risacca
- volto

![8](8.png)

Essi sono gli operatori di azione e sono per ogni movimento il carattere principale e hanno dei sottoperatori che danno in maniera globale un'idea.

Abbiamo un sentimento spiraliforme e dove si torna nella stessa zona, si è a un livello differente, per incontrare qualcosa di nuovo ma che in qualche misura si conosce già, facendo divenire ciò linguaggio.

Rinascere sirena, per ripetere un discorsi...

Far parte delle sirene per ripetere lo stesso...

Il risultato è la musica in fuga, come Ulisse che si fa legare per ascoltare, ma non si lascia andare.

La musica abitata da una fortissima coerenza interna.

#### Inoltre

Il violoncello è per Netti la base su cui appoggia il quartetto. La viola è l'orizzonte e l'anima ed i violini sono il canto appartenente ai due emisferi cerebarali.

![9](9.png)

Fuggire dall'abbraccio delle sirene si è andati verso la continuità, brano fluido e non frammentato. Un po' come aver inteso una frequenza interna dello strumento, immediatamente mediate dallo strumento stesso.

Questo brano è l'area del ponte dello strumento stesso.

E vi è qualcos'altro nel brano, con l'ultima parte con pizzicati, riferimento per il solo di violoncello finale.

#### Solo finale, ultimo brano con tete

Il brano finale una sorta di assedio di Troia...

Alberto Giacometti è l'antenato del ciclo e Giacometti rifletto sul senso del suo lavoro.

Giacometti è interessato all'arte come strumento di trasformazione.

Sentiamo un pezzo dell'intervista di Giacometti per come si presenta in Tete.

12 passaggi di ascolto e registrazione dall'interno del violoncello, con l'ascolto delle formanti del violoncello.

Non si usa nel pezzo in maniera consequenziale l'ascolto dal violoncello, come se si modificassero le stanze intorno a lui da un passaggio ad un altro. Vi è quindi un cammino interno alla risonanza costruito appositamente.

>Dice di non sapere niente ed il fatto che non sappia niente non è importante e se facendo arte capisco ciò che mi circonda ed il fatto che ci sia un quadro o una figura, non ha importanze e ciò che è importante è trovare qualcosa che non ci fosse prima.

![10](10.png)

Abbiamo una suddivisione per qualità vocalica in relazione alla linea ritmica superiore con la qualità vocalica suddivisa per uno strumento preparato.

Avevamo prima infatti tutte le possibilità di pizzicato...

Poco a poco vediamo che cola qualcosa in paritura.

Vi è un click per il violoncellista altrimenti vi sarebbe una difficoltà insormontabile.

### Preparazione dello strumento

![11](11.png)

Più ci si allontana dalla preparazione, più scompare la standardizzazione necessaria ad una grande orchestra e con essa il modello di uno strumento perfetto.

Una caratteristica dello strumento, suggerisce al compositore una nuova musica.

La preparazione serve per:
- ampliare l'area del ponticello
- ampliare le possibilità dello sfregamento

Si sono moltiplicate le zone delle corde per lo sfregamento, le aree devono essere in continuità con le altre aree che sono collegate fra loro.

Compositivamente si può integrare la diversità dei suoni in una gradazione della differenza verso una trasformazione organica dei parametri della materia sonora.

>Lo strumento ad archi serve per esplorare il continuum.

### Scrittura del Ciclo

Come scrivere su carta i suoni del ciclo?
![12](12.png)

Una volta preparato il pianoforte, non si modifica la preparazione, si decide quindi una scrittura d'azione: cosa suonare, dove e quando.

Vi sono due righi superiori che rappresentano le corde sopra la preparazione.

1, 2 e 3 sono gli spazi per la preparazione.

Vi è poi un tetragramma per indicare su  quale corda sto suonando e poi vi è un pentagramma per indicare la diteggiatura, e indicare le altezze la dove si hanno in evidenza delle altezze precise.

Nel trio e nel duo vi sono altezze precise...

Il tipo di notazione è piuttosto complesso e capire come suonerà, ma il processo di apprendimento di come suonerà una partitura.

Si da agli strumentisti la registrazione di come suona, si da agli strumentisti un riferimento diretto suonando quei segni.

>Si praticano gli strumenti per cui si scrive, pensando alla vibrazione come un qualcosa di fisico e non qualcosa di mentale.

Attraverso le registrazione e il montaggio si riesce a definire una sorta di robot musicale per gli strumentisi, e la realtà fisica del suono nello spazio è qualcosa di straordinario:
![13](13.png)

Come suonare la musica negli spartiti tradizionali, non vi è scritto, mentre per il ciclo si!

Ogni strumentista riceve la propria parte e le altre parti, in modo da avere un insieme degli elementi utili per poter eseguire l'esecuzione.

![14](14.png)

Dal punto di vista di studio vi sono problemi sulle sovrapposizione di modalità che si contraddicono fra loro.

Vi è quindi la scomposizione di unico gesto musicale per poter agire in maniera piú efficace per capire un gesto piú complesso.

La scrittura cambia e ciò che è stato capito o assimilato viene eliminato e la scrittura del violoncello ha un solo pentagramma.

Overpression e grattato cosa vuol dire?

Un rumore è semplicemente un suono non contestualizzato, come un suono incompreso.

Dove vi è un suono non contestualizzato, vi è quindi un rumore.

Nel trio la presenza di diteggiature è costante.

Nel duo vi è solo il pentagramma e là dove serve viene aggiunto un pentagramma superiore per specificare un qualcosa.

La scrittura a due è quasi una scrittura tradizionale.

Nel cello solo vi è solo la linea di basso del pizzicato con teste delle note diverse per quanto riguarda la caratterizzazione del pizzicato.

_La novità di un brano è inseparabile dalla scrittura che ne assicura la riproducibilità._

Più ci allontaniamo dalle convenzioni, piú il segno dovrà essere ricalibrato.

Il percorso contenuto in un brano è anche il percorso che vi è

### Il titolo

Il ciclo nasce dopo la scrittura dei primi due brani.

Come associare il brano _place_ a dei testi dell'iliade e l'assedio è principale modalità di ricerca con 4 aree tematiche e 4 strategie d'assedio attraverso i quali questo ciclo si è creato.

Ogni pezzo nasce da un'attrazione, da uno spazio acustico che a poco a poco mette insieme ciò che Netti vive ogni giorno.

Ogni brano è un qualcoa che cresce attorno a se.

Ogni brano deve crescere e divenire indipendente da se.

Assedio è un derivato del verbo _Obsidere_...

L'oggetto dell'assedio è il suono. Si preferisce frequentare il suono di volta in volta incontrando la complessità...

### Il quartetto d'archi

Esso è una sorta di rocca forte che impone i compositori e di stare fuori e ciò che caratterizza l'assedio è un vuoto.

Si deve lasciarsi al quartetto, attraversando e cercare l'altrove a cui il vuoto indicava.

Cercare di capire cosa il vuoto ha generato, cosa mi chiama e a cosa conduce.

Quando arriva il vuoto lascia un segno preciso e si rinnova.

Il quartetto è per Netti un luogo acustico speciale, ricordando il concerto di Xenakis per quartetto...

### Verso il ponte
Vuoto colmato da un'esplorazione diretta spostando l'arco verso il ponte dello strumento.

Si va verso il ponticello...

E se il ponte fosse piú largo, con più di un ponte?

#### Preparazione

Suoni complessi grazie alla scheda telefonica che mantengono un rapporto adiacente alla grana dello strumento.

Si può articolare un linguaggio fatto di suoni nuovi.

Ciò che rimane è il ciclo dell'assedio e il ciclo del ritorno per viola.

Ci si è concentrati verso il suono al ponte e verso una soglia verso un acustico altrove.

Semplificando potremo dire che la porzione di corda vicino al ponte è decentrata e periferica...

Era naturale avere il centro della vibrazione libero, grazie all'accordatura tradizionale forma una sorta di ponte aereo e le corde risuonano per simpatica con un'architettura immateriale.

Continuamente abbiamo elementi attraversati da correnti diverse.

Allargando e moltiplicando i ponti si è affidato a architetture immateriali con il decentramento del suono perchè l'architettura storica è senza riferimenti:
![15](15.png)

Come l'abbazzia di San Galgano e tutti i suoni compresi quelli esterni sono relativi dai limiti.

Con i limiti della sala da concerto per estendersi.

#### Scultura di Giacometti

![16](16.jpeg)

Scultura con la base come se sprofondasse dalle 4 presenze.

La forza di un'opera può estendersi verso uno spazio che si estende attorno a noi.

La piccola piazza è divenuta il quartetto d'archi in grado di far convergere e comporre molto più acusticamente di quanto si guarda per strada.

Quando si iniziano ad ascoltare i suoni in quanto suoni, ci rendiamo conto della polifonia in cui siamo immersi.

La polifonia ha forse un senso in quanto non verbalizzabile ma vi è un senso.

### Conclusioni

Si ritorna periodicamente a qualcosa con una nostra interpretazione alla vita che arricchisce la vita stessa e l'ascolto è un'elegante improvvissazione e interpretazione.

L'ascolto coincide sempre con una trasformazione percettiva e con la condivisione del proprio lavoro con gli altri.

___

>Cercare di mostrare come le ragioni poetiche non sono un pretesto, ma lo strumento più prezioso che permette di osservare anche ciò che già si conosce con orecchie e occhi differenti.

>Il comporre stesso è l'altro strumento che abbiamo in mano, lavoriamo sul senso delle vibrazioni e della qualità delle vibrazioni fra loro. Vi è una relazione fra comporre-pensiero...

Pollini disse che abbiamo la fortuna di lavora con la qualità, dove in genere si lavora sulla quantità, lo strumento compositivo è pontentissimo di messa in relazione delle cose tra loro.

___

## Domande

Pasquale:
Dal suono tradizionale, si va verso qualcos'altro nel ciclo dell'assedio, mentre nel ciclo del ritorno si parte da una parte preparata e amplificata portandosi a un qualcosa di acustico... Come partire dal rumore di fondo del Big Bang per andare fino alla finestra di Bach che stava scrivendo le partite. La viola di Bach è qualcosa di diverso, in virtù del percorso che si è attraversato. La dimostrazione che l'ascolto è trasformato è data dall'esistenza di brani stessi...
Un volto può essere banale o un magnete e la qualità delle relazioni interne che ci dall'esterno percepisce è totalmente differente.

Gli strumenti sono degli enti e degli organismi con un deposito di memoria secolare.

UR come originario...

>Joyce, siamo noi che facciamo i nostri antenati...

Vi sono negli ultimi quartetti di Schubert e Beethoven vi sono delle cose inquietanti...

Quanto della percezione che noi abbiamo è il frutto di un cammino percettivo...

Andrea:
Analisi del quartetto del ciclo dell'assedio... dal sito di Netti

Luca Spanedda:
Nuova musica e lo sparitacque tra la musica scritta e improvvisata. Nell'improvvisazione vi sono punte eccezionali e ciarlatani.

Discorso interessante è l'interpretazione per far crescere l'interpretazione di quello che si vede e sente.

Il valore dell'arte è creare dei riferimenti che attraverso il tempo vengono interpretati differentemente. Parliamo dal punto di vista dell'opera di sostenere moltissime interpretazioni che fanno la differenza.

Prendendo la stragrande maggioranza di composizioni contemporanee, non si possono realizzare brani molto profondi...

Creare dei luoghi per aggiungere qualcos'altro da parte di altre persone.

Fra gente che fa, un segno importante è che ascoltando, si dia voglia di lavorare.

Si viene nutriti da ciò che si ascolta.

Pasquale:
Musica Lo-Fi e Hi-Fi, giudizi non qualitativi, ma puramente descrittivi per quanto riguarda la qualità del suono.

La qualità del suono nasce in studio...

La cura della qualità del suono, e l'ambiente classico ha imparato dall'ambiente pop, la cura e la qualità della registrazione.

Coltrane si distingue al primo suono che si fa, mentre Parker ha bisogno di una semifrase per distinguerlo.

Tutti i soprani sono diversi fra loro con una cura e qualità del suono diversi fra loro.

Me:

Nel momento in cui ci si pone la domanda, siamo fuori strada per trovare la novità nella tradizione, ma nel cercarla abbiamo già sbagliato.

Il respiro degli strumentisti non si può fare a meno del respiro e di far apprezzare come l'arco appoggia sulla corda prima di vederlo.

Questa attenzione ai suoni e alla contestualizzazione dei suoni e dei rumori.

La novità nella tradizione è relativa alla crescita nell'ascolto.

>Noi vediamo, sentiamo nell'altro, un qualcosa che è già presente in noi e nell'altro per qualche motivo estemporaneo si attiva, ma è un qualcosa che è già presente. Non vi è niente che ci muove dall'esterno.

La tradizione è mantenere vivo il fuoco e non le ceneri, la tradizione è un grande bacino di memoria e le risposte alle domande che ci continuiamo a fare, risposte che sono andate bene in una zona, in un contesto etc...

Il contesto è cambiato, ho bisogno di qualcos'altro, il vuoto è ciò che rimane rispetto alla tradizione, entro in dialogo...

Tradizione come memorie che mi hanno toccato...

Pasquale:

Come da idea di percorso come si passa alla formalizzazione di percorsi.

Niente di cui si è parlato era chiaro prima, e si chiariscono le cose strada facendo!

Si ha un processo che si muove su piani paralleli.

Da vedere:
[Il ciclo dell'assedio prima e dopo il concerto ad Avidi Lumi.](https://www.giorgionetti.com/resources/)

Presentazione ad Harvard ed al conservatorio di Parigi...

Andrea:

Fascinazione particolare per la voce che non canta, estraendo il fattore vocale nella prosodia.

La prosodia e il linguaggio di vista della prosodia ha una certa rigidità.

Come lavorare con la prosodia superando i vincoli e le rigidità?

Vi è in una trasformazione progressiva con una trasformazione e strutturazione progressiva, ma all'interno il contrappunto è musicale.

Una prima possibile soluzione è quella di sempre escludere le relazioni dirette e tutto è sempre mediato e dove è il nostro spazio, esattamente nella mediazione e nella modalità di mediare ed interpretare.

Sono quindi associati dei frammenti di articolazione che via evolvono.

Tutto ha ovviamente un suo senso musicale e il sonogramma è l'ossatura.

>Lo strumento è il mediatore per introdurre la complessità di cui si ha bisogno.
