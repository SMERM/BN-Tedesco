# Dal segno al suono, il lavoro del musicista

In collaborazione con i Dipartimenti di Nuove Tecnologie e Linguaggi Musicali dei Conservatori di Roma, Latina, L’Aquila e Benevento.

Rivolto agli studenti dei suddetti Dipartimenti e tenuto dal Compositore Michelangelo Lupone, dal Direttore del PMCE, Tonino Battista, con il contributo del CRM - Centro Ricerche Musicali.

La partecipazione al seminario è prevista nella veste di uditore.

**22 novembre ore 11:00 - Teatro Studio**

La prima parte del Seminario prevede l’analisi delle partiture e dell’interazione tra strumenti ed elettronica e verranno illustrati i criteri di elaborazione del suono previsti nei due brani.

I partecipanti al Seminario assisteranno alla Prova di
allestimento e al Sound Check.

**22 novembre ore 15:00 - Teatro Studio Prova Generale del programma**
**22 novembre ore 21 - Teatro Studio Concerto**

## Appunti

![1](1.png)
