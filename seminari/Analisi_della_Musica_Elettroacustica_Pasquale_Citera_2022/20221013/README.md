# Appunti della lezione del 13 ottobre 2022

## Composizione multimediale

### Cos'è la composizione multimediale e a cosa si riferisce?

Idea di composizione sviluppata su più media e più persone.

Quando l'opera perde un media, il peso di un media è fondamentale.

### Definizioni

Ci troviamo spesso con tre termini utilizzati in modo indistinto:
- multi-mediale
- trans-mediale
- cross-mediale

>E intermediale?

Vi sono differenze tra i termini, sappiamo che multimediale e transmediale, in genere vengono confusi, ma il termine più antico multimediale, con aspetto giornalistico di ricerca, un modo di fare giornalismo su diversi media, come la costruzione di un ambiente multi mediale, ovvero su diversi media, con diversi codici oltre che su diverse arti.

Il modo con cui comunicano i diversi mondi pone la differenza tra multimediale e transmediale:
- multimediale -> comunicare la stessa cosa in modi diversi
- transmediale -> comunicare cose diversi con media diversi con obiettivo comune

>La differenza tra essi è sostanziale:
1. nella prima definizione abbiamo un unico processo declinato in diversi modi
2. ogni media ha un carattere indipendente accomunato da uno stesso obbiettivo


Il teatro musicale e l'opera tradizionale sono quindi multimediali con testo concetto di narrazione, quindi la storia viene declinata in diversi media.

Abbiamo esempi nella contemporaneità di trans-medialità come An Index Of Metals, con video e audio diversi che non hanno lo stesso tempo, e gli elementi di audio e video vanno in percorsi completamente indipendenti.

>Sappiamo che nel video succedono elementi e nell'audio altri e non abbiamo sincronizzazione.

La sovrapposizione di più arti crea degli elementi che crea punti di contatto fra le varie arti.

>Il film può essere inteso come un elemento multimediale ma ciò è molto dipendente dalla tipologia di media.

Griglia comune per il descrittivo e per la composizione in genere.

Il connubio di diverse arti è stato sempre presente ed ogni arte si porta un senso appresso.

### Esempi di multimedialità

Esso è Luigi Nono in Intolleranza con proiezioni che si integrano alla recitazione, alla musica ed alla coreografia, proiezioni realizzate con la _lanterna magica_.

### Poeme Electronique

Esso è uno dei primi studi-installazione transmediali.

Varese realizzò il brano a tre canali e Xenakis progetto e mise in piedi il sistema audio, mentre Le Corbusier firmò il progetto architettonico.

>Esso fu un paradigma di confronto per una sorta di orchestra di altoparlanti.


### Cross-mediale

Qualcosa che nasce in un media e passa di arte in arte, come ad esempio Spider Man, con fumetto,  film, poi la serie, etc...

>Molte idee sono cross-mediali, ma nel momento della performance il primo media non lo è...

Quando una cosa esiste in una determinata arte e la si rifà in opera, diviene un qualcosa di cross-mediale.

### Come fare?

Far coincidere le arti in diversi percorsi o dare altri percorsi.

1. Bisogna riuscire a capire cosa sia il rapporto fra diverse arti.
2. Perchè vi è uno sviluppo di attenzione più pronunciata negli ultimi anni.

Stiamo parlando del presente, quindi non sappiamo se la multimedialità sarà un qualcosa che si svilupperà molto o meno.

### Oggi

Nei compositori moderni o contemporanei vi è una grande parte di repertorio multimediale. Oggi vi è quindi un rinnovato interesse.

Molti dei compositori di oggi realizzano composizioni multimediali oggi, perchè?

>Sappiamo che la società dell'immagine nasce dall'alfabeto.

Notiamo che l'arte multimediale può difficilmente essere realizzata dalla stessa persona, per poter seguire un'unico percorso preciso, e spesso il compositore fa tutto...

Mentre nella composizione trans-mediale in genere vi sono figure diverse per realizzare elementi mediali diversi.

>È sbagliato riportare ad unico processo solo.

Tutti i compositori arrivano a realizzare una composizione multimediale, dipende tutto dalla moda?

Tutti i compositori hanno raggiunto la multimedialità partendo da percorsi didattici. Partendo quindi partendo da percorsi sperimentali.

Chi ha studiato in accademia evolve in maniera diversa, rispetto a chi non è cresciuto in maniera di cognizione musicale.

Alexander Schubert ad esempio realizza ogni volta un qualcosa di completamente diverso, con una sorta di evoluzione normale del linguaggio contemporaneo.

Sappiamo che costruendo poco alla volta un percorso, avremo qualcosa di incedentale per andare da qualche altra parte.


### Installazioni

Si hanno stessi obbiettivi ma percorsi diversi nelle installazioni, quindi qualcosa di transmediale.

### Mettere in relazione diverse arti

Essa è la cosa più difficile, in cui si possono fare degli errori banali di sincronizzazione e considerazione, non possiamo pensare che i codici della musica come frequenza e ritmo, siano gli stessi di un'altra arte.

Dare parametri musicali ad un'altra arte, in genere un'altra arte ha suoi concetti di piani, processi, piani etc...

Sappiamo che ad esempio la profondità in musica, è diversa da quella delle luci o dell'arte pittorica.

Sapendo ad esempio di avere un attore che recita in base musicale che non ha base attoriale ma base musicale, spostando l'accento sul ritmo, quindi facendo il rap o il musical.

Inquadriamo il parlato in un ritmo quadrato, comanda la musica.

Se invece alla parola, colleghiamo indissolubilmente la musica, realizziamo mickey mousing.

La difficoltà sta nel capire quale sia il legame tra un'arte e l'altra.

Sappiamo ad esempio che il concetto di ritmo musicale sia il più matematico tra tutte le altre arti, cerchiamo discretizzazioni a parti uguali a determinata cose che sono un continuum, ponendo dei rapporti matematici, per far quadrare delle cose...

Il teatro non suddivide in frazioni ed il ritmo teatrale è dato dall'interpretazione della battuta e il modo di passarsi la battuta tra un attore e l'altro ad esempio con l'accelerazione e la decelerazione di un'altra.

Avremo quindi un tempo diverso a livello teatrale, che ragionando musicalmente non fa capire e comprendere nulla di ciò...

Sappiamo che per i ballerini si conta in levare, ed il loro levare è quindi il battere, per capire come realizzare i gesti, con un fattore di latenza del corpo.

Ragionare con un solo tempo non è possibile.

>La bellezza dell'arte multi mediale è il cercare di armonizzare le differenze e non unificare sotto un'unica caratteristica.

Interpretando un fattore altro con il filtro della musica, si sbaglia la lettura, se va bene il discorso avremo sound design, non si deve fare un discorso associando a una cosa quello che si conosce, perchè si farà mickey mousing.

![1](1.png)

>Il testo significa _tu sei donna_...

I due sensi devono essere compenetrati.

Si riesce a realizzare questo processo, perchè si riesce a far combaciare contesti diversi elementi simili.

(Artisti che realizzano parte registica che compositiva Carpenter e Gabriele Mainetti)

### Arte multimediale come arte collettiva

Chi fa arte multimediale, fa sempre più arte collettiva ovvero fa parte di un collettivo.

### Operazione multimediale

_Visione e ascolto del concerto di Simon Steen-Andersen_

Secondo la teoria della multimedialità, la tecnologia funziona quando è invisibile, in cui rimane un'aura di mistero.

>La tecnologia va bene quando si lascia un margine di ambiguità in cui non si capisce qualcosa come avviene.

Del 2012...

### Opera di teatro musicale

Del 2021 con una forte connotazione di cambiamento climatico.
Con scultura, recita con parole intellegibili, suono...

_Visione di The force of things di Ashley Fure_

Lo spazio diviene un meta strumento che si può sunare in vari modi.

Notare l'aspetto rituale della composizione oltre che la complessità...

### Opera per voce, foschia, luci e elettronica

_Visione di supramodal parser di Alexander Schubert_

### Difficoltà nel recepire la partitura

È difficile trovare le partiture di brani multimediali e transmediali.

Inoltre come scrivere la partitura per tutti i media.

Sappiamo dal teatro d'opera che la partitura e il copione sono separati.

Sappiamo ad esempio che per alcune opere la partitura è obsoleta, come risolvere?

___

Perchè le opere partono da un'autore musicale come un compositore?
