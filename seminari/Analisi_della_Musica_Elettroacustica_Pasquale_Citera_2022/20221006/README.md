# Appunti della lezione del 6 ottobre 2022

## Argomenti trattati nel corso

1. Focus sulle compositrici femminili (con scelta personale del docente)
2. Composizione post-internet (post-digitale)
3. Panorama vario sulla composizione trans-mediale

## Disclaimer al corso

Parleremo di qualcosa di troppo recente per avere molta documentazione, abbiamo infatti documentazione parziale.

Vi sarà materiale e composizioni degli ultimi tre anni.

I musicologi non prendono in considerazione in genere tali materiali perchè abbiamo un qualcosa di troppo recente.

>Si limita il corso alle informazioni che vi sono senza opinioni.

## Struttura del corso

Proposizioni di materiali ed estetiche con conseguente approfondimento singolare degli elementi.

## Le compositrici contemporanee

### Premessa

Sulla lezione del giorno abbiamo domande ma non è detto che si abbiano risposte corrette.

Oltre la scelta aprioristica delle compositrici che vedremo, avremo una sorta di collezione di dubbi nati dai brani, abbiamo inoltre non una lezione politica femminista.

### Peculiarità di arte nelle varie culture

>Esistono le differenze di natura culturale per quanto riguarda l'arte...

All'interno di una stessa nazione possiamo dire che abbiamo avuto diverse scuole di pensiero e di composizione. Vi sono delle caratteristiche su base culturale.

Se le differenze sono possibili a livello culturale, la cultura ed il sistema in cui cresciamo, può portare delle differenze su base biologica e di pensiero?

### Tre parole su cui intenderci
Intendere tre parole su cui dobbiamo intenderci:
- biologico -> quando si tratta di un qualche cosa che viene dalla natura, ovvero come qualcosa nasce naturalmente parlando -> dimorfismo sessuale
- strutturale -> come si è rappresentati nella società
- sistematico  -> come si è posti nella società, che crea sistema, che produce un sistema

>Essi sono uno dei sensi e non la definzione del vocabolario.

Oggi sappiamo che una società rende una persona per struttura sociale in un certo modo...

>La composizione femminile è stata strutturalmente considerata come inferiore.

Sappiamo che Alma Malher viene chiamata sempre con il cognome del marito Gustav, e quindi la società mette in secondo piano la compositrice femmina.

Nelle altre arti è trattata in modo diverso la presenza di compositrici femminili in musica.

### Ildegarda di Bingen
>Alcune compositrici sono importanti e hanno rivoluzionato dei certi elementi compositivi.

Essa rivoluzionò degli elementi compositivi portati avanti da molti compositori maschi successivamente.

### Diverse sezioni della composizione non sono trattate

Vi è un paradosso e il fatto che avere approccio a qualsiasi tipo di informazione divenga altrimenti dispersivo.

### Scelta delle compositrici di oggi

Abbiamo quindi una lista di compositrici di cui non si approfondirà il discorso poichè non vi sarà il tempo per farlo.

### Quota rosa e presenza femminile per la direzione

Tenere un equilibrio tra quota maschile e quota femminile.

### Realizzare una playlist

Cercare di fare il connubio tra un pezzo di riferimento, inserendo lo stesso numero di compositrici e compositori.

In un fattore strutturale, la capacità comunicativa dell'opera culturale ha una mira diversa inserendo nelle prossime ricerche delle compositrici femminili.

### Cultura e società nell'influenza culturale

Sappiamo che con tutte le caratteristiche diverse dal punto di vista di genere, abbiamo molte conseguenze dal punto di vista della creazione.

### Esempi di transizione e avvicinamento ad altro genere

#### Fratelli e sorelle Wachowski

Esempio nella storia del cinema sulla trilogia di Matrix e il cambiamento nella narrazione.

#### Wendy Carlos

Fece la transizione mentre stava facendo la colonna sonora di Arancia Meccanica.

#### Salvador Dalì

La cui sensibilità cambiò rispetto alla sua conoscenza del lato omosessuale.

### Considerazioni generali sulle sfumature e la sensibilità

Potremmo avere molte sfumature di una sensibilità rispetto ad un'altra, e potremmo infondere curiosità e potremo non considerare la composizione monolitica.

### Razione e istinto

Quando un essere umano non riesce a gestire l'istinto bisognerebbe porre un qualcosa di razionale.

Non si fa qualcosa per una serie di motivi il cui motivo estremo potrebbe essere l'esclusione dalla società.

### La diversità ha pari diritto

La diversità è un qualcosa che dovrebbe portare a far eccellere e portare avanti le differenze di pensiero.

### Éliane Radigue

![1](1.jpg)

Sappiamo che lei proviene dall'RTF dove aveva lavorato anche Pierre Schaeffer, nacque nel 1932. Ha avuto diversi passaggi tra musica concreta, feedback e nastri magnetici, fino ad avvicinarsi al buddismo. Il buddismo le ha dato una capacità di ascolto e relazione con il tempo completamente differente rispetto a prima e la trilogia indicata è la denominata Trilogia della morte _Trilogie de la Mort_ del 1998, in cui si ha pura elettronica anche molto semplice e pochi materiali concreti. L'opera in totale dura quasi tre ore e vi sono tre brani che rappresentano tre stadi della coscienza buddista.
Tale opera con i tre brani:
- Kyema
- Kailasha
- Koume

#### Durata di una composizione, perchè articolazioni temporali lunghe?
Si ha più o meno un rapporto simile al concerto di Turazzi a Palazzo Altemps.

>Il risultato finale è che la durata del brano diviene in secondo piano, ed infatti all'inizio si sente molto la pesantezza del brano entrandovici temporalmente, ed in realtà poi concentrandoci si realizza che si debba entrare nel brano.

Il primo momento sembra che non faccia succedere nulla, ed i concetti sono espressi in maniera molto dilatata e si diviene consapevoli poco alla volta e tutte le attese sono disattese. Quando si inizia a capire il codice, il tempo si annulla.

Metabolizzare una pratica e fenomeno fisico della musica con le parole utili per far comprendere l'ascolto.

#### Radigue e i processi di meditazione Buddista

Radigue si rifà ai processi di meditazione Buddista proprio per la percezione della durata temporale.

Quando la musica diviene un qualcosa di fenomeno-acustico ed il tempo discretizzato arriva ad un certo punto e diviene una gabbia che racchiude poco di ciò che succede.

L'opera della Trilogia della Morte fa si che sembri che non succeda nulla, ma in realtà accadono moltissimi processi.

__Ascoltare la Trilogia__

### Sofia Asgatovna Gubajdulina

È famosa per la musica strumentale ed era molto sponsorizzata da Shostakovic.

Shostakovic fu un suo protettore, perchè era donna, compositrice, interessata alla musica contemporanea in Russia.

>Egli cercava quasi di autoimporsi per via delle imposizioni che lo stato applicava e cercava quindi di mantenere dei canoni tradizionali facili per il popolo socialista.

#### Il Bayan e la fisarmonica per la Gubajdulina

Lei ha spinto molto la musica per Bayan e fisarmonica, esso è uno strumento delle caratterstiche importanti.

Tanti fisarmonicisti hanno il _De profundis_  come brano di repertorio(parte del requiem originariamente).

Anche il brano _In crucem_ è uno dei brani per Bayan e contrabbasso.

#### On the Edge of Abyss

Per 7 violoncelli e due waterphones esso è un brano con un equilibrio di tradizione ed innovazione.

La Gubajdulina aveva un animo quasi di auto-censurarsi.

#### Autocensurarsi

Esso definì sistematicamente un suo stile scrittura.

#### Compositrice religiosa

Essa ha un profondo senso di religione cristiana nella sua scrittura.

### Pauline Oliveros

Egli è un'altra delle compositrici agli albori della musica elettronica nel gruppo americano di San Francisco agli inizi della tape music.

Una delle sue composizioni più interessanti è quella della performance ed interpretazione con un'attenzione all'ascolto con interesse all'ascolto e legame con musica e spazio.

Pauline ha suonato nel trio _Deep Listening_ la fisarmonica, e vi era prima la ricerca dello spazio nel quale fare una certa registrazione, con una ricerca di luoghi non tradizionali.

Deep Listening è il nome di un disco e il nome della prassi ed il nome del trio, esso diviene una musica di relazione con l'attenzione alla creazione del fenomeno musicale estemporanea.

Deep Listening è una serie di performance con diverso materiale di specifico.

_Da sentire [questo](https://www.youtube.com/watch?v=U__lpPDTUS4)_

### Kaija Saariaho

Ha avuto molto riconoscimento a livello contemporaneo della musica, con moltissime composizioni.

La Saariaho è diventata tradizionale contemporanea. Abbiamo una fabbrica di composizioni sempre simili tra loro.

_L'amour de loin_ è una composizione tra le più votate degli ultimi vent'anni (in cui In Veins di Haas risulta come la più rilevante).

### Rebecca Saunders

Compositrice del 1967 è molto riconosciuta e dovrebbe essere musicologicamente una giovane compositrice, e la considerazione musicologica e sociale la possiamo trovare...

(Teresa Rampazzi prima cattedra di musica elettronica in Italia)

Crescita dal punto di vista dell'interesse per quanto riguarda la musica elettroacustica molto recente.

I brani consigliati sono:
- Still (2011)
- Void (2013)
- [Myriad](https://www.rebeccasaunders.net/myriad-ii) (2022)

### Chiyoko Szlavnics

Compositrice canadese del 1967 che ha composto il brano _Inner voicings_ nel 2014, e l'opera di questa compositrice ha un'esperienza molto vicina a quella di Alvin Lucier.

>Attenzione verso la relazione spaziale tra suono puro elettronico e suono complesso acustico.

Ascoltare un'altra versione dell'esperienza acustica potrebbe essere utile.

Affinità e divergenze tra Szlavnics e Lucier.

## Considerazioni sulle compositrici affrontate fino ad ora
>Fino ad ora abbiamo affrontato compositrici che dal punto di vista musicale sono abbastanza simili come stile musicale ai corrispettivi compositori maschili.

Le compositrici seguono più lo stile dell'epoca che quello personale. Le compositrici successive vogliono quasi rimarcare delle differenze di carattere puramente femminile.

(Alcune compositrici sono allieve di Chaya Czernowyn)

### Jennifer Walshe

Alcune sue composizioni sono al limite tra la performance e la musica.

Alcune delle sue composizioni:
- _XXX_LIVE_NUDE_GIRLS_ del 2018 atto musicale, esso è una sorta di teatro con delle bambole; video camera per virtualizzare qualcosa che ti accade davanti
- _Physics for the Girl in the Street_, cosa una donna deve sopportare camminare per strada

>Nella sua produzione ci mette sempre il corpo ed il corpo è sempre presente nelle sue opere.

### Ashley Fure

Americana di classe 1982 con gran parte di composizione multimediale.

Con alcune composizioni:
- Interior Listening Protocol 01 -> Composizione performance a solo, qualcosa che si può ascoltare solo se lo fai, vi è un video in cui viene spiegato che cosa fare -> nato dall'esperienza dell'isolamento
- A library on Lightning -> per orchestra

>La musica live, in loco ha un appeal completamente diverso rispetto alla musica creata per il live.

### Clara Iannotta

Di classe 1983, romana.

Composizioni citate:
- Moult -> concetto della muta degli animali, ovvero con la muta della pelle, con periodi stabiliti della vita gli animali cambiano la pelle, cosa succederebbe se si avesse la possibilità di avere la pelle di se stessi negli anni -> cosa si può rappresentare come seconda pelle? composizioni proprie del passato e metodo di riproduzione passato, si creano strati elettronici con poi un'esecuzione acustica - è molto interessante
- M/R del 2022 -> opera multimediale, sono dei collaborative piece, in cui quando un brano è multimediale come opera intera, o opera completa che è intesa come opera multimediale con diverse dimensioni artistiche

>Si possono avere deduzioni e collaborare inserendo del nostro e avere qualcosa di audio video.

__Bisogna sapere che la composizione multimediale è una composizione collaborativa__.

### Kelly Sheehan

Compositrice nata nel 1989 con composizioni relative a ciò che facciamo in Europa ma riportato in America.

Composizione consigliata:
- Eyes on

### Giulia Lorusso

Classe del 1990

- Fabrica brano del (2021) brano per ensemble e ed ambiente virtuale tridimensionale

## Altre compositrici

- Natasha Barrett
- Caroline Shaw
- Egidija Medeksaite

## Conclusione

In tutto il percorso che abbiamo sempre considerato è costellato dalla stessa quantità di compositrici femminili.

Come le persone vengono indirizzate e crescono, ovvero come educhiamo prima.
