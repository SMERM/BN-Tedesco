# Il pensiero scientifico e gli strumenti della musica

>La costruzione e l’evoluzione degli strumenti musicali, le tecniche di esecuzione e di scrittura musicale si avvalgono sempre più di metodi e di processi di calcolo digitale, di apparecchiature elettroniche per l’analisi e la generazione del suono. I domini dell’Acustica e dell’Elettronica collaborano alla definizione dei nuovi paradigmi musicali.

Michele Dall'ongaro: Scherzo della nona sinfonia di beethoven -> timbro come parametro fondamentale -> Gesang der Jungelinge centrale nel percorso.

## Renato Meucci: tappe fondamentali che hanno portato ad uso dell'elettronica diffuso

### Strumenti elettronici ed elettrici, cosa è successo?

Tradizione di automatismi, con cilindri o carta perforata.

Nome pianola -> nome usato in maniera piú variegata ed a proposito

![pianola](pianola.png)

Charles Stroh inventa un violino con una campana esterna che riesce ad essere uno strumento tradizionale.

Lev Theremin -> unico strumento al mondo inventato che funziona senza suonarlo

![theremin](theremin.png)

Poi il Wurlitzer e poi l'elettrificazione applicata alla chitarra.
![hendrix](hendrix.png)

_Ascolto canzone Emerson Lake e Palmer_

L'elettricità tiene insieme la ricerca il pop, l'avanguardia, l'elettricità e l'elettronica cambia le cose, con una società hifi...

Michele Dall'ongaro: introduzione di Luigi Pizzaleo

Paolo Ketoff si sapeva che era stato l'epicentro di un qualcosa di importante.

## Luigi Pizzaleo: il Synket

### Perchè a Roma?
Il Synket è una macchina che compare sulla scena romana nel 1965, anno in cui vi fu la prima dimostrazione americana.

Il Synket è un oggetto che non poteva non nascere a Roma, ciò lo vediamo dal catalogo compilato da Huge Davies...

Vi erano 14 studi di musica elettronica attivi a Roma. Vi è una realtà molto variegata che portano il Synket come realtà culturale...

- Vi sono molti compositori che danno una loro fisionomia alla musica di Roma
- Industria cinematografica
- invocazioni a forme musicali ibride

C'è quindi la presenza di forme intermediarie come sonorizzare le mostre. Forme multi artistiche, con un allargamento del fatto musicale.

Il Synket nasce grazie alla commissione dell'accademia americana del 1964, che aveva un precedente ovvero il Phonosynth, che serviva per velocizzare e convogliare il montaggio di tracce audio.

Il Fonosynth è il precursore del Synket.

### Cos'è il Phonosynth

Sintesi sottrattiva con 3 moduli, sintesi simile a quella che sentiamo uscire dal Moog...

I primi moduli li portò Taitelbaum dall'America...

Questa macchina venne usata negli studi, nella sonorizzazione.

Ad esempio venne usato nello studio R7, che sonorizzo tantissimi cinegiornali.

Vi è una grande attività sommersa del Fonosynth.

### Eaton

John Eaton inserí il Synket in un panorama di musica colta (musica dei Serious Composers)...

Eaton deve sperimentare il Synket, esso sa fare bene alcune cose molto particolari...

Integrazione sociale, poichè sembrava un centralino...

Il grande sogno di Paolo Ketoff era vedere il Synket in orchestra.

_Ascolto del brano per due pianoforti scordati al quarto di tono e Synket di Eaton_

Melange di timbri

Il grande brano per il Synket fu il concerto per Synket ed orchestra.

_Ascolto del Concert Piece per Synket & simphony orchestra_

È chiaro che il grosso problema è far dialogare il Synket, e Chadabe lo affronta...

Un ultimo esempio ci serve per capire che vi fu un tentativo di rendere appetibile su un piano commerciale questo Synket.

Bob Moog voleva Paolo Ketoff nella sua azienda, ma ci fu anche chi lavorò per espandere il Synket in un ambito piú commerciale

_Ascolto di Blues machine_

Si sente in questo brano l'intento commerciale.

Il controllo della tastiera è su due moduli di sintesi, mentre il terzo modulo di sintesi è realizzato da un potenziometro.

(uno degli obbiettivi di questo studi)

bibliomedioteca@santacecilia.it

>La tradizione è una concatenazione di rivoluzioni

### Michelangelo Lupone: dal Synket a...

percorso velocissimo che ci ha nutrito di aspetti storici della tradizione all'interno di uno strumentario elettronico di tantissimi generi diversi

Walter Branchi, Guido Bagiani, fino ad arrivare alla SIM, per arrivare al CRM, che mantiene fede ad un'ipotesi in cui le realizzazioni tecnlogiche e di ricerca.

Sviluppo delle competenze per la realizzazione dello studio Paolo Ketoff.

Dall'Ongaro sta concretizzando un'idea che negli anni '60 venne manifestata da Evangelisti, e da Berio negli anni '90. Ciò da luogo alla concretizzazione di un'ambizione bellissima che sentiamo come musicisti e compositori di supporto fondamentale per tutto lo scenario musicale che fa uso della tecnologia elettronica e grazie allo Studio Paolo Ketoff, dovrebbe garantire anche la condizione di divulgazione e formazione culturale che il dominio digitale propone.

Lo studio Paolo Ketoff, ha davanti a se un percorso didattico, e vi è da un lato lo stimolo essenziale di Ivan Fedele, con una serie di aspetti che danno sostanza.

Supporto artistico, didattico e tecnico con una competenza con modi e tecnologie per la registrazione e post-produzione dell'orchestra, per un lavoro sostanziale.

In questa ambizione vi è la possibilità di connettere le competenze con esponenti di ambito musicale.

- integrazione con lo strumentario acustico -> supporto e tutt'uno nell'attuazione del loro utilizzo
- spazio, analizzato virtualmente e realizzato attraverso strumentazione che permette virtualità ed indagine fisica con uno strumento adeguato per lo stimolo creativo dei compositori d'oggi
- installazione sonora d'arte -> sistemi complessi che ci permettono di dare supporto al musicista per poter soddisfare attraverso la creazione musicale, un'etica con cui la tradizione musicale...

Produzione e post-produzione saranno centrali nello studio!

Lo studio sarà veicolo di natura culturale, attraverso gli strumenti critici che ci permettono di elaborare un reale avanzamento su aspetti creativi e modalità di fruizione e realizzazione di questa musica.

>Tensione che Roma ha dagli anni '50 in poi con stimolo creativo etc...

Il prossimo incontro -> Mercoledí 24 Marzo con le Tecniche estese

Data di apertura della mostra!

Michele Dall'Ongaro: Aggiungere agli spazi per la musica elettronica, anche altri spazi con confronto tra suono orchestrale ed elettronico!
