# Jean-Claude Risset - Music is meant to be heard: Perception is central in (my) computer music

[Link](https://www.youtube.com/watch?v=nOv30rHV7Ds&list=WL&index=30&ab_channel=CIRMMT)

## Introduction
- CIRMMT
- Norman MacLaren synthesis
- Huge LaCaine
- Perception at center of the speech for the school started by Albert Bregman
- Steve MacAdams

## Summary

The computer is powerful, is like a workshop with which you can build whatever we want.

Techniques for applying computer synthesis.

Nel 1875 si conosceva l'origine e la causa di un suono, e quando venne introdotta la registrazione, la causalità divenne diversa e potemmo studiare i suoni come oggetti riportati nel tempo.

Il telefono usava un microfono ed uno speaker tradotti in elettricità.

Nel 1897 Thaddeus Cahill realizzò un brevetto costruendo un dinamo sonoro, che poteva propagare l'armonia attraverso la linea telefonica.
![i1](i1.png)

L'era elettronica iniziò con Lee de Forest che realizzò nel 1921 il Theremin, venne usato per trasporre canzoni a parte Varèse che venne stimolato all'utilizzo come un manifesto in Ecuatorial, con dei glissandi, realizzati con un Theremin che puntano verso il futuro.
![i2](i2.png)

(Imaginary Landscape 1931 Cage)
La musica veniva composta in un medium registrato dal 1948, quando Schaeffer a Parigi assemblava i suoni, realizzando ad esempio _Etude aux chemins de fer_.

![i3](i3.png)

Nello stesso tempo la musica elettronica venne realizzata diversamente a Colonia e Monaco, realizzato precisi eventi complessi, e l'idea era opposta a quella concreta.

![i4](i4.png)

L'era digitale della musica venne introdotta da Lejaren Hiller con la Illiac Suite nel 1956 e l'anno successivo nel 1957 Max Mathews realizzò la prima sintesi digitale e la prima registrazione digitale. La tecnologia era molto costosa allora.

![i5](i5.png)

Scrisse in quell'anno un articolo.

Risset lesse quell'articolo ed essendo interessato al timbro, e facendo le sue considerazioni, cercando nuovi modi di rinnovare il vocabolario musicale e non la sintassi, rinnovando dunque i suoni, nuovi materiali e nuove architetture e strutture.

Lesse l'articolo e andò a lavorare con Max Mathews usando le risorse informatiche.

Inoltre John Chowning provó a realizzare Computer Music.
![i6](i6.png)

Chowning non aveva però una base scientifica come Risset inizialmente.

![i7](i7.png)

Varèse mise nel programma il brano pitch Variations.
![i8](i8.png)
Realizzato con il programma MUSIC, in cui bisognava indicare precisamente come realizzare il suono.

(Risset non era attratto dalla musica concreta o dalla musica elettronica, perchè bisognava attuare trasformazioni rudimentali su suoni pieni di significato)
![i9](i9.png)

I ricercatori dovevano trarre i vantaggi della Computer Music in qualche modo...

Quelle rappresentate in figura sono delle forme d'onda diverse, ma che suonano molto similmente che hanno fasi diverse, ma sonoricamente
![i10](i10.png)

Schaeffer disse: "music is meant to be heard", è il risultato aurale che conta.
![i11](i11.png)

Ciò dipende anche dagli intervalli individuati da pitagora preferiti (intervalli 3/2), e la musica è nell'orecchio e non nella matematica.

_Ascoltiamo un esempió di ciò_
![i12](i12.png)

La capacità di apprezzare gli intervalli è diversa dunque a frequenze diverse.

Con queste possibilità

(fino a 18 minuti)
