# Appunti della lezione di lunedì 14 marzo 2022

## Informazioni sui 24 CFA


Lunedì 11 aprile ci vedremo in presenza in Sala Accademica.

>Come si acquisiscono le conoscenze: "Psicologia della musica" è specifica per studenti e futuri docenti di Musica.

## Arredamento del corso

- drive del corso
- gruppi che alla fine della lezione possano vedersi e segnare alcuni concetti chiave della lezione

## Cos'è la cultura?

Psicologia culturale (Bruner), nel testo "La cultura dell'educazione", in cosa consiste la propria cultura, vi è il ruolo cruciale della negoziazione tra pari.

Vi sono quindi alcuni contenuti chiave che si sono negoziati tra diversi individui.

>Spesso anche lo scontro mette a fuoco per contrasto alcuni elementi chiave.

L'incontro/scontro costituisce a realizzare la mappa mentale, grazie all'incontro tra pari.

## Concorsi per gli strumentisti con 24 CFA

- Musica per la secondaria di primo grado
- Strumento per la secondaria di primo grado
- Strumento per la secondaria di secondo grado

## Logica delle competenze

Un'insegnate di sostegno musicista, se entrato di ruolo, può proporre di realizzare un progetto musicale.

Con il titolo che si ha, cosa si sa fare? e cosa sa saper fare?

>Sono gli insegnanti stessi che devono organizzare la competenza musicale, in funzione di quanti più contesti problematici possibile.

## Cos'è la psicologia della musica?

Disciplina giovane che produce moltissimi testi e documenti.

Fondazione Mariani, sulla musica e le neuroscienze.

In questo momento c'è una grande attenzione sulla psicologia della musica.

Dal libro "Psycology of music" di Carl Seashore, che pubblicò il primo libro con tale titolo.

Seashore si mosse alla ricerca della caratteristiche della percezione dei musicisti.

La cosa importante e interessante è il test per la verifica acustica e musicale, esso è usato ancora oggi come test per iscriversi al corso di indirizzo musicale. Tale test è importantissimo, ma oggi cosa significa essere musicali?

## Di cosa ci occupiamo in questo corso?

Intervistiamo la letteratura esistente, vedendo se esistano delle ricerche che affrontano o provano a risolvere un problema che noi abbiamo (questo è quindi un corso deduttivo).

Individuiamo alcuni problemi importanti di cui tratteremo.

>La cosa fondamentale è che ci si ponga un problema che ci interessa, "How do we think?", cosa ci fa pensare?

Sappiamo che la nostra mente è attiva quando ci troviamo davanti ad un problema.

### Cosa significa essere musicali?

Siamo esseri musicali, ma cosa vuol dire essere musicale? Da dove viene la musicalità? Si nasce con la musicalità? Si vede da qualche parte la musicalità?

La musicalità non è una ed unica, vari individui hanno musicalità diverse, si deve quindi lavorare con la musicalità degli altri, quindi cos'è la musicalità?

La musicalità ha basi biologiche? È suscettibile di sviluppo?

>Il metodo è chiederci se esistano dei filoni di ricerca che hanno affrontato questo problema.

La domanda può essere affrontata da percezioni e prospettive diverse, come si trasforma l'uomo, avremo dei filoni diversi per poter sapere e conoscere.

### La musicalità è una forma di intelligenza?

Gardner (1967) in "Frames of mind" descrive che il filone di ricerca cognitivista, che riconduce le attività della psiche ad un modo di organizzare la conoscenza, le rappresentazioni del mondo.

>Conta solo l'esperienza?

L'esperienza è la "Conditio sin equa non", ma ciò che ci resta non è l'esperienza, che prima o poi sparisce, quello che resta sono i modi in cui si ripensa un'esperienza.

Vi sono i tanti modi, le organizzazioni, le soluzioni espressive con cui si rappresentano le esperienze.

Le rappresentazioni che si sono realizzate servono per potersi ricordare delle esperienze.

Il tipo di cineprese con cui ci si fanno i film.

>Non esiste un'unica intelligenza.

#### Esempio: la mer, il mare

Per un'esperienza complessa ognuno di noi sviluppa degli strumenti per poter portar via un'esperienza complessa.

##### Intelligenza visiva

Si pensa di un'intelligenza visiva con un film che sono delle immagini, immaginazione può essere quindi solo visiva?

Si conservano e catturano delle immagini.

##### Intelligenza propriocettiva (cinetica)

Abbiamo uno strumento propriocettiva per evocare alla mente anche se non è in questo momento ciò che stai facendo.

##### Intelligenza uditiva

Il suono del mare, i suoni.

Si può avere un'intelligenza uditiva come Verga, nell'ultima pagina dei Malavoglia.

##### Intelligenza uditiva/visiva

La memoria dei pianisti è una memoria visiva o uditiva?

Essa è a metà tra uditiva e visiva.

##### Intelligenza verbale

Come le parole di Proust dedicate al mare.

##### Intelligenza olfattiva

Dare valore all'olfatto è un qualcosa in più rispetto allo schema di Gardner.

### L'idea di intelligenza di Gardner

Perchè fare una fatica per riorganizzare e prendere le informazioni e categorizzarle.

Nella nostra cultura, molto cognitivista, da Cartesio sappiamo che "Penso quindi sono", ma ciò è vero?

>La parola di intelligenza deriva da "intell-igere" scegliere tra.

Scegliendo quindi le parole, i suoni, gli elementi che possono far ricordare e sviluppare degli elementi.

Gardner ha cominciato scrivendo 7 tipologie di Intelligenza che posi sono divenute 9, con una intellegenza relazionale o emotiva, istigato da un altro studioso (Goldner).

Anche il modo di gestire le relazioni ha bisogno di un'intelligenza specifica.

#### Dominio cognitivo

Ognuno delle intelligenze sviluppa un dominio cognitivo, apposito per sviluppare un'intelligenza.

Si studiarono i casi limite per capire ed intendere.

Delle intelligenze più o meno ne abbiamo alcune più sviluppate di altre.

Un autore che pensa al mare, riesce ad evocare memorie ma ad elaborare suoni nuovi che ripropongono il mare (come Debussy).

Si sviluppano quindi zone del cervello diverse, ovvero zone del cervello che si attivano quando si chiama uno specifico strumento (Vigorskij).

### Cosa significa "pensare bambino"?

Tutti abbiamo un'intelligenza musicale adulta, ma cosa vuol dire avere un'intelligenza adulta o bambina.

Cosa significa pensare da bambini o pensare da adulti?

Un bambino piccolo ad esempio pensa ad un cane, se si ricorda dei cani, e per un bambino pensare ad un cane significa collegare ad un cane che si conosce (il cane del vicino, il mio cane, etc...).

Un bambino pensa ricordando, e quindi per una lunga parte della vita il pensiero è legato al saper ricordare.

Viene poi un momento in cui si elabora, si compone un cane nuovo che mescola diverse componenti canine...

Il pensiero è quindi legato alla memoria, ed alle memorie.

Piaget chiama questo pensiero, come un pensiero concreto; il pensiero è legato a memorie per lungo tempo.

Con il database di memorie accumulato si incomincia ad elaborare pensieri nuovi.

Il pensiero adulto è che per prove ed errori si riesce a realizzare un qualcosa di nuovo creativamente.

Vigorskij da "Il pensiero cognitivo":
![1](1.png)

>Bisogna dotarsi di segni, di promemoria e dispositivi per dotarsi di memorie.

I dispositivi allargano l'azione della memoria e le permetto di assorbire stimoli artificali che chiamiamo segni.

Vigotskij direbbe che: "la musicalità, ovvero l'intelligenza musicale, ovvero privilegiare il mondo con dati sonori uditivi, tanto più ci si è dotati di segni e strumenti linguistici, con cui organizzare le proprie memorie."

![2](2.png)

Si costringe un oggetto esterno al ricordare qualcosa.

Si ricorda attivamente con l'aiuto di segni!

Gli esseri umani influenzano i loro rapporti con l'ambiente!

>Si costruiscono monumenti musicali per ricordare.

Come si può portar via un'esperienza?

>Nel corso dello sviluppo avviene una trasformazione, per l'adolescente ricordare significa pensare.

Il pensiero di un bambino adulto significa ricordare, mentre per bambini piccoli, ricordare significa pensare.

>L'operatore musicale incide molto sulle memorie per un bambino, e quindi un repertorio di memoria che cresce.

### Pensare da adulto

Arriva quindi un atteggiamento ed un pensiero adulto che elabora.

Ci si pongono prima delle domande e dopo l'elaborazione.

### Adolescenza

L'adolescenza è un periodo in cui vi sono dei musicisti a scuola e in questo periodo vi è un estremo, l'elaborare conta più del ricordare.

L'intelligenza prevalente in questo periodo della vita, vi è una intelligenza relazionale e personale.

Si soffre perchè non si riesce a gestire ne l'intelligenza personale ne quella relazionale.

Si decide intuitivamente che ciò che serve per campare bene sono i fattori che servono a proiettare in modo competitivo, con importante valore con l'intelligenza personale e l'intelligenza relazionale.

Bisogna creare sinergia tra intelligenze diverse.


___
## Riepilogo del discorso fino ad ora

Cos'è la musicalità -> modo di intelligere il mondo -> intelligenza come dotarsi di strumenti linguistici per ripensare l'esperienza -> dare ai propri alunni (e a se stessi) per ripensare l'esperienza -> la musicalità è uno di questi linguaggi.
___

### Gardner

Egli ci tiene a dire che ogni intelligenza conltiva un dominio cognitivo, ovvero determinate sinapsi, l'intelligenza non è una sola (il test di Binet non è sempre corretto).

### Tipologie di intelligenze fra bambini diversi

(vi sono bambini che dicono e parlano ed altri che sono cinetici)

Le intelligenze sono delle strategie di sopravvivenza.

>Rispetto alle concezioni monolitiche ed il fatto che l'unica intelligenza che conta è la logico-deduttiva, ci si sta pian piano distaccando.

### Piaget: crescita e diversi tipi di intelligenza

Nel corso della crescita si sviluppano diversi tipi di intelligenza, selezionando quello che conta nei diversi momenti.

Quando Piaget parla di un pensiero senso-motorio, ci si chiede quanti gesti esso usa? I pediatri hanno delle tabelle di schemi di azione in base a quanti schemi di azione si hanno per pensare il mondo.

Un'intelligenza senso-motoria: "se la natura non fa salti", l'intelligenza senso-motoria è avere un archivio ed un repertorio crescente di schemi di azione per far esprimere la realtà.

Lo smontare un qualcosa è uno stadio successivo al pensiero senso-motorio o meno?

Si scopre un qualcosa di trovato, ovvero una trovata per capire cosa succede quando cade il cucchiaino, si ripete quindi la trovata con una ripetizione di reazioni circolari.

Si cerca l'invenzione che nasce come trovata attraverso prove ed errori.

### Delalande: interpretazione musicale

Si cerca la vera interpretazione attraverso il provare ed esplorare, cercando schemi di azione efficaci.

Si trova quindi un qualcosa e ce lo si segna, e quindi l'errore fa parte dell'apprendimento.

L'esplorazione serve apposta per stimolare delle trovate e cercare quindi più soluzioni.

### Cos'è la creatività?

La creatività è cercare varie soluzioni per risolvere un problema.

La creatività è trovare delle soluzioni per risolvere dei problemi.

Si prepara quindi un terreno in cui le soluzioni sono considerate.

(Tipico dell'intelligenza motoria di Piaget sono le trovate e ripetizioni circolari.)

### Quanto rimane l'intelligenza senso-motoria?

Nelle intelligenze in cui siamo più sguarniti dobbiamo avere delle esperienze senso-motorie.

### Il tempo lontano e la concretezza delle esperienze

Il tempo lontano dal pensiero concreto è sempre molto lontano, e i ragazzi devono avere un'esperienza.

### Il tempo

Cosa si pensa, pensando il tempo.

- modi visivi
- modi ritmici

Si può ripensare a due musiche nate nello stesso posto in due momenti diversi Beethoven e Mozart.

Intelligere e pensare il tempo.

Per spiegare il Romanticismo, si devono far ascoltare degli esempi per poter generare questo stravolgimento dell'intelligenza.

>La ritualità e la ripetizione con un senso crea delle memorie organizzate e rappresentabili.

### Dopo Piaget: dal simbolo all'oggetto concreto

La penna verrà un momento con cui dirigerò, canterò o ci fumerò.

Si cerca di imitare qualche cosa, ed il bambino piccolo ha delle azioni imitative.

Un bambino che ha 3 anni e non usa un telefono ma un libro, facendo finta che è un telefono.

Grazie al contatto con un qualcosa si fa venire alla mente un qualcosa di astratto che fa organizzare una sere di concetti ed astrazioni, imitando quindi il gesto di un adulto.

>Il simbolo è potente perchè si può avere sotto le dita, e catalizza una serie di pensieri che sono astratti.

Quindi con l'età del pensiero simbolico (da 3 anni in poi), si fa finta che un qualcosa abbia concettualmente un senso avendo quindi pensiero astratto.

>L'evocazione mentale (e non immaginazione) è una capacità che viene limitata alla manipolazione sensoriale, il pensiero dura quanto dura il contatto sensoriale motorio.

Viene poi un momento con un oggetto che assomiglia a quello veritiero che ha qualcosa di simile che caratterizza un qualcosa con cui senza di esso non si può far nulla.

Ognuno ha il suo finto microfono, ovvero un oggetto simbolo che scatena una serie di rappresentazioni mentali, ovvero costruzioni mentali che hanno ha che fare con uno specifico oggetto della realtà.

Mentre nell'intelligenza senso-motoria, si deve usare un oggetto specifico, successivamente si può utilizzare un qualsiasi oggetto per far si che ad un oggetto si dia un'importanza diversa.

Un simbolo ha qualche cosa in comune con la realtà!

I bambini sono più degli adulti disposti ad utilizzare degli oggetti diversi.

Gli adulti sono quindi più rigidi a fare delle associazioni simboliche rispetto ai bambini.
Gli adulti sono più funzionali e più creativi, e i bambini sono piú abituati a realizzare libere associazioni.

(Intelligenza senso-motoria, intelligenza simbolica, intelligenza del gioco con regole)

### Intelligenza del gioco con regole (o del gioco d'esercizio)

Risolvere problemi avendo delle regole.

Sappiamo che l'investimento del tipo simbolico è ancora presente a 2/3 anni.

Mentre vi è un'età della meccanica, ovvero il piacere del gioco con regole.

Si scopre quindi come fare, con un'intelligenza con regole.


### Le diverse intelligenze in momenti diverse

In genere l'intelligenza fa degli zoom facendo alcuni focus, mettendo in moto e facendo un gioco senso-motorio piuttosto che un altro.

>Concezione dell'intelligenza diversificata in base alla rappresentazione mentale (Gardner), essa è diversificata a seconda del tipo di rappresentazione mentale.

>Concezione dell'intelligenza a seconda del tipo di gioco o momento che si fa(Piaget).

Anche adulti possono riniziare ad avere un interesse ed utilizzare un'intelligenza senso-motorio (come degli adulti che riniziano a suonare a 50 anni).

Per ognuna delle intelligenze bisogna trovare un peso diverso e bisogna modificarle in base all'età.

### Modelli di rappresentazione del mondo (Bruner da "Verso una teroria dell'istruzione")

I modi di rappresentazione della realtà cambiano:

![3](3.png)

Vi è sempre:
1. il fare delle __esperienze concrete__ come primo stadio (fase senso-motoria), l'esperienza della musica con il corpo, con la voce si ha un'esperienza __senso-motoria__ -> coinvolgere i bambini in esperienze senso-motorie -> qui ed ora
2. il maestro che fa fare qualcosa con la musica c'è poco bisogna quindi __far uscire__ le esperienze (bisogna creare una categoria di permanenza come il sapere dove è il cibo a casa; la categoria di permanenza è una delle prime categorie che si pensano, sapere che c'è anche se non si vede), __fase iconica__ realizzare delle esperienze costruendo delle cose che ci portiamo via che ci permetteranno di richiamare le esperienze alla mente, problema delle notazioni in senso ampio (Sheyla Nelson con cartine con giochi ed altezze, schema analogico per richiamare una cosa fatta in classe) usare dei dispositivi che creano un legame, ovvero una rappresentazione iconica, facendosi un'idea per rappresentare delle immagini, bisogna avere tracce per poter evocare qualcosa quando il maestro non c'è più
3. fas

___
## Parole chiave: gruppo 7

Dedicare un quarto d'ora a raccogliere parole e nomi chiave per questo incontro.

Scrivendosi ciò che abbiamo fatto.

- Psicologia Culturale (Bruner) -> che cos'è la cultura? negoziazione dei concetti chiave tra pari
- Logica delle competenze
- Psicologia della musica (Seashore)

[drive](https://drive.google.com/drive/folders/1LiBMus8dcqK3kVwKBBYrRK7MjVFqwzRM?usp=sharing)
