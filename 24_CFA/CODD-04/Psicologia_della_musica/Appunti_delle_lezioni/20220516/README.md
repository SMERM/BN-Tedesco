# Appunti della lezione di lunedì 16 maggio 2022

## Data d'esame

Giovedì 26 maggio

## L'esame

Obbligarci a scrivere. Preparando una serie di temi, per cui il giorno dell'esame avremo la mattinata per elaborare dei file sintetici.

>Più si è consapevoli del proprio ruolo, più allora chi è intorno a noi crederà in quello che stiamo facendo, anche un bambino.

## Psicologia della musica

Il corso di psicologia serve per riconoscere dei modelli di autori riconosciuti, come Piaget, Bruner, e gli studi di neuroscienze.

Le affermazioni che facciamo devono essere riferite

## Aggiornamenti sul corso e temi d'esame

>Faremo 9 incontri e dedicheremo la prossima lezione per delle sintesi ed aiutarci a fare delle scalette con i
contenuti dei temi.

La prossima volta realizzeremo riferimenti ad argomenti con i gruppi che potremo utilizzare per scrivere il testo per la risposta d'esame.

![1](1.png)

Esse sono parole chiave che servono per vedere che vi sono alcuni concetti per vedere tutto legato alla disciplina musicale.

## Identità musicale: un'introduzione

### Visione di parte del convegno ["La musica che unisce"](https://youtu.be/ofqDPqDCBX0?t=1862)
 tenutosi il 10 maggio 2022

Vediamo l'intervento di Daniela Lucangeli:

- Alba del sentire
- le cellule del meccanismo umano si sincronizzano al battito della mamma
- sintetizzare l'intelligere
- nei primi 1000 giorni di vita, la __voce e il tatto sono i più importanti organizzatori pro-sociali__
- il piú grande organizzatore di emisfero destro e sinistro, sono i suoni
- il suono sono le culture su cui l'intelligenza umana costruisce le culture
- suono del padre calma il piccolo, la nostra filogenesi ha fatto sviluppare strutture _I care_
- nelle nostre lacrime è scritta la storia del nostro sentire (marcatore somatico di Antonio Damasio):
  - la biochimica delle lacrime di dolore è diversa da quelle della felicità
- la scuola dovrà essere differenziale e di sviluppo, portando ad una scuola che nutra le funzioni e non le alfabetizzo solamente
- voce, suono e musica, ci consentono di star bene con gli altri e con nei stessi

#### Considerazioni del video visto
>Un marcatore somatico realizza una connessione tra sistema fisico e sistema emotivo.

Il sistema scolastico iniziò con il Seicento...

Ad esempio l'intonazione non è ON/OFF, ma va in base alla motivazione, che è un fattore affettivo che conduce le proprie conoscenze.

Il tatto e l'udito sono i primi sensi del noi, per mezzo del tatto e dell'udito e poi dopo 3 mesi di vita anche lo sguardo...

Prestissimo si usano il tatto e l'udito per creare le connessioni creando un noi.

In due si parla attraverso una modalità imitativa, rispecchiandosi nell'altro come con il baby talk.

È importante il gioco del rispecchiamento con una modalità affettiva che stimola e mi fa capire cosa io debba fare, e cosa io faccia.

>Creare delle modalità di sintonizzazione affettiva.

Bisogna pensare che la situazione in cui si hanno lezioni uno a uno...

### "L'involucro sonoro del se"

Ansieur e "l'involucro sonoro del se", con una figura che ha contenuto per un'interazione dialogica a due che consente un rispecchiamento.

Ansieur parla di "involucro sonoro del se", come la Guerra Lisi che descrive il liquido amniotico, il tatto e l'udito, ci plasma come umani, come se fossimo delle piccole sculture:
- Guerra Lisi ce lo dice per esperienza
- Lucangeli ce lo dice osservando le radiografie

Vi sono tre fattori nei testi di Stern, Threvarten, Malloch e Imberty che sono:
- il timing, che consente di realizzare un rispecchiamento
- prosodia, movimento della voce
- dinamica, intensità della voce, equalizzare e rispecchiare

Questi sono i profili di attivazione, che non possono essere separati, e quindi vi è una transmodalità. La comunicazione è fino a un certo punto tattile e visiva.

#### Vi è una prevalenza dell'aspetto ritmico?

Vi è una differenza tra il figlio di una mamma cantante o meno, vi è comunque una vibrazione ritmica dei cicli interni del battito della madre.

Abbiamo dentro di noi una quantità di ritmici periodici legati al cuore ed alla respirazione che hanno tutti la caratteristica di essere ritmi periodici.

È molto difficile trovare qualcuno aritmico, e il ritmo è una delle prime manifestazioni attive di musicalità.

Il parlare e l'eloquio è uno dei primi momenti che fa comprendere se un bambino abbia qualche diversità.

### Cosa pensiamo del futuro della psiche umana?

Una serie di umani potranno permettersi di fare palestra e di essere belli e senza rughe, e siamo nel transumanesimo e ciò che conta sono le infinite estensioni della mente.

Al centro della conoscenza vi è la capacità di:
- esprimere la propria persona e la propria intelligenza?
- esprimere la propria intelligenza relazionandosi con gli altri?

La componente di motivazione all'ascolto è molto importante...

### Feto e stimoli

Qualsiasi feto percepisce qualsiasi elemento sonoro, ma anche la sola voce, non conta quindi il contenuto, ma è importante la voce umana.

#### Mamma muta e stimoli

Come si può sfruttare il liquido amniotico per portare stimoli al bambino?

Questi stimoli arrivano comunque al bambino, anche se la mamma fosse muta.

>Il ritmo, come organizzatore di azione, è molto importante ed è collegato a tutto.

Gli elementi base del ritmo: beat e misura; sono fattori biologici che riguardano moltissime performance. Il ritmo è quindi un connettore di molte sinapsi.

#### Psicologia e ascolto del feto

Il feto sente sempre un qualcosa come figura-sfondo nella sua crescita già all'intenro della pancia della mamma.

## Identità musicale

Pagina 57 del curricolo del ciclo primario, vi sono i traguardi di competenza per la musica:
![2](2.png)

Esse sono uscite la prima volta nel 2008 e nel 2012 sono state aggiornate.

### Musica come mission

Bisogna orientare la costruzione della proprià identità.

L'identità musicale per farla costruire, serve un musicista vero per insegnare nella scuola secondaria di primo grado.

### Messa a punto dell'identità e sviluppo socio-affettivo

L'idea che si ha di se stessi, cambia e più si è sicuri delle risposte alla domanda ("chi sono") meglio si riesce a negoziare la propria identità con gli altri.

Se si ha qualche tranquillità, forse ci si potrà anche aprire a identità diverse.

È molto forte l'idea che si ha di se stessi e la disponibilità a trattare con la propria identità.

![3](3.png)

Osserviamo la tabella che viene da un libro del 1950 di Erikson _Infanzia e società_.

Citiamo questo testo perchè dopo il 1950 si è cercato più per l'ambito del cognitivismo.

Oggi stiamo tornando al capire il legame dell'apprendimento con la socializzazione.

Erikson afferma che:
>in ogni età della vita lo sviluppo socio-affettivo è trovare un equilibrio tra due poli.

Per un adulto vi sono due poli:
- generatività o stagnazione

Nell'età di giovane adulto si è tra:
- intimità o isolamento

Erikson dice che in ogni età i poli sono diversi.

Per una maturazione, ovvero per un anziano:
- integrità dell'io o della disperazione (inutilità sociale e vicinanza alla morte)
  - l'io che si integra per attivare altre componenti

Osserviamo che per l'__adolescenza__ che si sovrappone con l'età della scuola secondaria da 11 a 19 anni:
un pre-adolescente ha bisogno di modelli di _identificazione_, modelli con cui confrontarsi e servono quindi dei modelli di orientamento. Attraverso alcune esperienze significative si saggia un'abilità, una risorsa che indica che il proprio orientamento sia in una precisa direzione.

La scuola media dovrebbe portare a sicurezze orientative oppure si disperde, avviene quindi una _dispersione_ di un qualcosa.

Vi sono una serie di fattori che aiutano a riconoscere degli orientamenti identitari, ovvero fattori del contesto che aiutino lo sviluppo dell'identità.

L'autostima ha a che fare con l'identità, e l'idea che si ha di se stessi è in continuo movimento.

L'età della vita in cui vi è qualcosa di piú labile è l'età dell'adolescenza.

>L'insegnante di musica deve aiutare a realizzare un'identità musicale, e tale insegnante può essere un modello di identità più del professore di geografia e storia.

La mission n.1 della scuola, bisogna fornire:
- identità
- dispersione

Nell'adolescenza gli educatori, devono fornire dei modelli di identità, per costruire situazioni per il ragazzino di riconoscere la propria identità musicale.

### Il modello matrioshka

![4](4.png)

L'identità musicale, e quella dei propri alunni ha già qualcosa di acquisito precedentemente come afferma Welch.

Vi sono delle sintonizzazioni che hanno realizzato un disegno neuro-psico-biologico.

Il contesto parentale con la quale si pone la base sicura musicalmente, l'inculturazione ha avuto una spinta verso la maturazione musicale.

Lo _schooling_ lo si trova come marcatore per lo sviluppo della specie.

Alcune specie sono suscettibili alla prima matrioshka, ma lo _schooling_ è un contesto organizzato.

L'intelligenza degli animali è diversa ed alcuni animali realizzano dei giochi senso-motorio (come per i delfini), ma i giochi simbolici, con i giochi che stanno per qualcos'altro, come dei giochi simbolici.

testo Francois Delalande, La musica è un gioco da bambini

[Elefanti e pittura](https://www.youtube.com/watch?v=foahTqz7On4&ab_channel=FSchleyhahn)

### Autismo e il riconoscimento

A 2/3 anni i sintomi dell'autismo si vedono, e ciò può essere comportato dallo stile di vita della mamma.

L'autismo può esser comportata dalla denutrizione.

### L'abitudine all'attenzione

Essa Stern la fa risalire al baby talk ed in situazioni in cui manca il compiacimento e le prime forme di interazione, generano un disinteresse del bambino.

La teoria della mente si sviluppa guardando anche la faccia dell'altro.

La teoria della mente è funzionale alla sopravvivenza.

Finchè si è piccoli ci si sincornizza con modelli.

### Cos'è l'identità musicale e come si sviluppa

![5](5.png)

Duccio Demetrio realizzò studi su depressione, schizzofrenia e in generale studi sui disturbi dell'identità.

Il proprio ruolo da insegnante di musica è fornire un orientamento alla realizzazione di qualcosa di musicale.

>Fornire strumenti per ricostruire la propria identità.

Bisogna quindi costruirsi la propria mappa dell'identità musicale con vari elementi:
1. imprinting: involucro sonoro del se, che è nella prima matrioshka di Welch, ci plasmiamo per mezzo dell'involucro sonoro ![6](6.png)
2. autobiografia(è utile aiutare a scoprire cosa si è fatto di musicale): essa è una __linea del tempo__ delle proprie esperienze, musiche paletto che permettono di ricostruire la propria linea del tempo, quanto della propria autobiografia è affidato a paletti sonori, elementi musicali, tutti hanno un curriculum musicale e gli adolescenti vivendo un momento di difficoltà nel riconoscimento dell'identità, lo sviluppo socio-affettivo si dispiega insieme ai pari che si frequentano, gli adolescenti sanno quello che non sono (Montale "siamo quello che non siamo"), non è ancora il tempo delle affinità elettive e spesso ci si sente pesce fuor d'acqua; se in terza media d'accordo con un altro insegnante si chiede di disegnare e raccontare la propria autobiografia musicale (ricerca di una narrazione della propria storia strumentale) gli adolescenti sentono il bisogno di capirsi _i bravi educatori e genitori devono aiutare i ragazzi a lasciare dei monumenti delle esperienze significative_ (rappresentandolo e disegnandolo); aiutare i ragazzi a raccogliere un book per rappresentare delle esperienze musicali significative ![7](7.png) è importante notare:
  - __i momenti di svolta__ (Bartok, Kodaly, realizzano e scrivono melodie di quando si era piccoli)
    - la depressione è un qualcosa che si avvicina molto all'identità, che fa creare una crisi di identità se non ci si riconosce nel progetto che si voleva inizialmente realizzare
  - __i momenti di crisi a livello musica__
  - __i personaggi chiave__ a livello musicale (l'inizio della propria intelligenza è un _noi_), nella propria storia i dialoghi chiave sono molto importanti, tali personaggi chiave fanno rinforzare la volontà di realizzazione musicale
  - __comunità di pratica, i gruppi__, in quante comunità di pratica si è fatto parte? Le comunità di pratica segnano la propria storia
    - il bullismo è una patologia dell'identità e vi sono degli aspetti problematici sia nel bullo che nella vittima, e si dice che alle vittime bisogna fargli un clan, il bullo ha dei problemi di identità e deve riconoscersi in qualcosa di forte, il bullo si sceglie i deboli e i soli, esso è legato a una patologia dell'identità (studi e ricerche contro il bullismo per mezzo di ASL intelligenti per finanziare pratiche di comunità di teatro e musica e vittima e bullo trovandosi in un ruolo di una comunità di pratica, danno un'identità, una forma al ragazzo)
    - laboratori e situazioni per le pratiche musicali di comunità: cori, orchestre(orchestra giovanile di Mirandola), etc... Oggi vi sono problemi di identità sociale che pesano molto sulla scuola. -> credere nelle comunità di pratica musicale, _la musica serve per costruire una riflessione collettiva_
3. Competenze
  - sapere
  - saper fare
  - saper essere
  - saper far fare
4. Gusti
  - valori
  - modelli
5. condotte musicali, ovvero le motivazioni profonode, ovvero i giochi con la musica che si fa (la parola _condotta_ è presa dalla psicologia condizionalista), la condotta è una motivazione profonda che modella i propri atti nella realizzazione della musica; la musica ci rappresenta come esseri

Si potrebbe chiedere ai ragazzi 15 minuti di repertorio che ci rappresenta, una sorta di playlist che rappresenti le esperienze musicali:
![8](8.png)

>La mappa fa riferimento al testo di Franca Ferrari, il concetto di autobiografia è stata sviluppata da Duccio Demetrio, mentre il concetto di identità musicale è stato sviluppato [Mario Piatti e Maurizio Disoteo](https://www.musicheria.net/progetti/collana-editoriale/5030-specchi-sonori-disoteo-piatti)

Tenere i contenuti del corso come piattaforma d'appoggio.

[Strategie per la prevenzione del bullismo](https://journals.uniurb.it/index.php/studi-B/article/view/175)

## Temi d'esame

![1](1.png)
