# Appunti della lezione di lunedì 4 aprile 2022

## Riepilogo della scorsa lezione

Torneremo successivamente sulle risonanze magnetiche e le PET.

## Cos'è la musicalità?

Quali sono le prospettive di ricerca psicologica generale?
- ha basi biologiche?
- è suscettibile di sviluppo?

### Musicalità come funzionamento della propria mente (ipotesi cognitivista)

Abbiamo espresso la musicalità come un modo di esprimere il mondo.

Pensieri musicali sviluppano dei collegamenti, delle sinapsi, che tracciano dei collegamenti neuronali che se ripetuti divengono dei binari referenziali.

Avremo quindi iper-sensibilità al musicale che ciascuno di noi ha sviluppato in modo diverso.

Le iper-sensibilità ci dimostrano oggi le risonanze magnetiche funzionali, sparano nel cervello in certe zone piuttosto che in altre zone, ed il cervello ha un'economia ed un'ecologia ed il cervello è concentrato in certi livelli.

Ognuno di noi mentre suona ha un pensiero attivo sul controllo di alcuni aspetti che suona, mentre tanti altri aspetti sono automatizzati.

Le tecniche diagnostiche contemporanee confermano la serie di ipotesi della musicalità come una delle possibili Frames of mind.

>Studiare la presentazione di Welch.

### Le correnti psicologiche e la psicologia della musica
Prendiamo spunto dalla domanda che ci interessa sulla musicalità, prendendo dalle varie correnti psicologiche.

Vi sono spesso anche dei paradigmi che non riescono a conciliare.

## Sigla per la lezione di oggi

Ascolto della traccia 3 di Nascere Musicali dal testo di Johanella Tafuri:
- introduzione della madre
  - melodia della tonalità
  - scala discendente
  - silenzio
- dialogo tra bambino e mamma che lo imita
- sviluppo del dialogo
- domande e poi risposte

>Il bambino è rimasto in maniera costante dietro al dialogo realizzato.

- Cos'è la musicalità in questa traccia?
- quando si inizia a dialogare?
- in che modo la capacità umana realizza una sorta di dialogo?

Abbiamo un bambino di 8 mesi che non sa parlare, a dispetto di una capacità verbale.

Quanto il sonoro sta all'origine della componente umana che è la capacità di interazione.

>Il baby talk è un comportamento molto frequente. Esso è realizzato in genere da una persona riconosciuta, ovvero con un adulto di riferimento si sviluppa un __rispecchiamento__ ovvero un'interazione che si manifesta in modo sonoro.

Si riconosce una pratica Gordoniana()!

### Sintonizzazione affettiva

Oggi leggeremo insieme il saggio di Michel Imberty, dedicato a questo tipo di interazione, ovvero un'interazione adulto-bambino con cui vi è un legame che Daniel Stern ha chiamato __sintonizzazione affettiva_.

>Come imprinting della nostra capacità di esprimere emozioni vi è uno scambio di affetti musicali con un adulto.

Si rispecchia quindi l'involucro sonoro del se, ovvero l'identità che si chiarisce e si mette a punto con una persona di riferimento.

Vi è una pratica di interazione che è diventata una metodologia didattica, e quindi più si stimola un bimbo lattante(con meno di 12 mesi) e più li si stimola al piacere di questa interazione e più si sviluppa la musicalità.

Se la musicalità è una capacità di interazione con i suoni che più la si pratica e più si sviluppa, si avrebbe un grande sviluppo della musicalità del bambino.

(La Tafuri realizza corsi che poi portano a registrazioni delle conseguenze del canto da -3 mesi a 6 anni, con mamme normali.)

Un operatore Gordoniano svilupperebbe a differenza delle mamme.

### Come è fatto un dialogo?

Riascolto della traccia 3 di Nascere Musicali, testo di Johanella Tafuri:

1. Introduzione
2. soluzione
3. risposta del bambino
4. inizio del dialogo tra madre e bambino
5. cambio delle altezze e delle espressioni

### Teoria dell'attaccamento di Bowlby

[What is attachment theory?](https://www.verywellmind.com/what-is-attachment-theory-2795337)

Essa è una teoria secondo la quale all'inizio della nostra capacità di relazioni, vi è un'esperienza di attaccamento sviluppata con un adulto significativo. Essa è un'esperienza di sicurezza che mi permette da un certo punto in poi di poter riconoscere e trasferire da un certo punto in poi, chi sono.

Freud parla di una relazione forte che mi permette da un certo punto di tenere l'immagine della madre o della figura adulta, ovvero una sicurezza con qualcuno che sappiamo che tornerà.

>Sappiamo che ciò che ci caratterizza come esseri umani sono:
- capacità cognitiva -> penso dunque sono
- capacità affettiva -> uomo come animale sociale

Queste due capacità si fondono e questi due vettori di sviluppo, con lo sviluppo congitivo e lo sviluppo socio-affettivo, vanno insieme.

### Composizione del baby talk

Vi è una conversazione e la mamma corrisponde agli stati emotivi del bambino.

Si capisce che vi è un dialogo, perchè la voce materna si modula in funzione delle risposte, con un elemento narrativo, che cambia in funzione di entrambe le parti.

L'elemento narrativo si realizza quando si sente la condivisione.

Le voci si imitano fra loro, e l'imitazione reciproca delle frequenze e della prosodia del bambino e della madre.

Vediamo che si scambiano degli elementi, come la pulsazione condivisa, vi è come un beat, e vi è una lunga attesa che è voluta.

Il fatto che vi sia un timing è indice di un qualcosa di musicale.

### Communicative musicality

Stevene Mallock e Colin Tremarte

La musicalità è il paradigma primario dell'interazione degli umani. In base alla musicalità si capisce quanto due umani stanno praticando.

Per ora abbiamo trovato:
- ambito tonale condiviso (input, ovvero stimolo musicale con musica di accoglienza)
- indice formale (rispecchio dei versetti)
- gamma dinamica
- esser con ritmico-articolatorio

Essi dicono che si può capire che vi è un dialogo. Lo scambio ed il rispecchiamento non è solo una gamma di altezze.

Le persone che stanno insieme parlano in modo simile, scambiando i contorni prosodici.

### Gestualità corporea e dialogo con il bambino

Con un video avremo la comunicazione anche dei movimenti, ovvero dei gesti e l'interazione visiva è anche importante.

Non avremo solo suono, ma anche gesti, ovvero un gesto con la stessa energia.

Non avremo solo frequenze, ma anche dinamiche, ovvero articolazione vocale che ha una presa tattile isomorfa ad un tocco.

Imberty dice che sono sovrapponibili la modalità di tocco e carezza e la modalità di voce.

### Imberty e la nota pivot

Analizzando i lallati dei bambini piccoli, capiamo che il gorgeggio di un bambino è tutto in una quarta o quinta, tutta dentro un'altezza tenor, ovvero una nota pivot che è una corda di recita biologica.

La mamma saprà fotografare con una proposta iniziale una gamma del bambino e il bambino farà proposte dentro la proposta.

Lezioni su youtube:
- [Lezione 1](https://www.youtube.com/watch?v=56EwNFR37DU)
- [Lezione 2](https://www.youtube.com/watch?v=H8JlnBkm4Fw&ab_channel=ScuolaRiflessiva)

### Macroforma del dialogo

1. avvio
2. climax
3. fase di decadimento in cui gli interscambi divengono più radi con una dinamica a forchetta

### Regolazione affettiva e relazione tra madre e bambino

Si percepiscono gli affetti vitali, ovvero il modo di essere della mamma.

E ne parlerà Stefania Guerralisi, ovvero parlando delle azioni del sangue.


## Lettura del saggio di Imberty: "Interazioni vocali adulto/bambino e sintonizzazione affettiva"

La musica come un modo per pensare il tempo e come incidano i rapporti temporali affettivi.

![1](1.png)

Il suono della voce, ovvero il filo vocale, e la musica di una voce che parla, crea un legame che attraverso i contenuti sonori trasmette un qualcosa, ovvero __l'involucro sonoro del se__


![2](2.png)
- segmentazione
- ripetizione
- semplicità

![3](3.png)

Sappiamo che l'occhio inizia ad essere formato solo 15 giorni dopo la nascita, quindi il neonato già conosce la voce della mamma prima di nascere.

![4](4.png)

L'adulto rispecchia il bambino e vi è un desiderio-intutito che porta ad una sintonia nel senso di utilizzo delle stesse frequenze.

Vi è un rapporto parentale intutivo tra adulto e bambino.

>Tutte le pratiche vanno avanti se vengono sviluppate.

![5](5.png)

Un bambino apprende, sintonizzando con sua madre.

![6](6.png)

>Vi è una struttura tipica antropologica per imitare variando.

![7](7.png)

![8](8.png)

![9](9.png)

I mille modi di sorridere o di aggrottare gli occhi, servono per riconoscere ragionando e riconoscendo il profilo dinamico temporale, e ciò condivide con un adulto di riferimento con cui realizza uno scambio ed impara a riconoscere cosa sia il mondo:
![10](10.png)

Si parla cercando di toccare un bambino anche con la voce, abbiamo quindi un'aspetto transmediale, con una percezione olistica globale.

Abbiamo all'inizio dell'emotività della musica un qualcosa che ci collega alle carenze che si hanno nel proprio imprinting. Si riconosce un contorno dinamico temporale che si riconosce.

>Vi è un'idea di riconoscere in un suono ovvero in un tipo di fraseggio, qualcosa che si conosce già.

Stern direbbe "che si ha nel proprio vocabolario espressivo, alcuni prototipi sonori antichissimi". Vi sono dei modi di _essere con_ precoci. Ma tutti i modi che riguardano la relazione intima, sono qualcosa di antico.

Ciò lo sostiene anche Nattiez (semiologo), che lavora sulla narratività musicale.

Ci chiediamo:
- come mai vi sono individui più espressivi di altri musicalmente
- quanto intervengono gli stimoli sulla musicalità dell'adulto

(Dario Spampinato e la semiologia musicale degli animali)

Si riconosce una sinestesia della musica.

![11](11.png)

![12](12.png)

Assenza di interazioni vocali, porta a qualcosa di maggiormente profondo:
![15](15.png)

L'intenzione e la disponibilità a co-costruire il tempo della madre influisce.

![13](13.png)

![14](14.png)

Presenza o assenza dialogo e legame con la teoria della mente.

### Teoria della mente

Il bambino non ha la gamma per conoscere tra arrabbiato e pensante e allora il bambino chiede se l'adulto sia arrabbiato o meno. Il bambino guardando un'espressione, si interroga sull'espressione e gli studi ci comunicano che queste espressioni sono legate ai gesti, al tocco, alla voce ed alle sfumature dinamiche, di articolazione e contorno melodico della voce e del gesto della persona che si occupa di qualcun altro.

Si capisce perchè un bambino impari a riconoscere i tipi di pianto per le determinate reazioni.

Ricaduta sul linguaggio del corpo e sullo sviluppo fine del linguaggio del corpo.

La sensibilità alle sfumature dell'interazione si stabilisce in un contesto relazionale in cui si hanno interazioni.

___
### Progetto con applicazione: continuator

Essa con una registrazione realizza una minima variazione del suono.

[Continuator di Sony](https://www.francoispachet.fr/continuator/)

Cosa succede quando il tipo di dialogo, imita variando, quanto tale gioco stimola la produzione musicale e l'improvvisazione.

Con _music interaction relive on reflection_...

Vi sono degli esperimenti con una tastiera per terra, si innesta un gioco di imitazione e ripetizione variata, e vi sono dialoghi con i bambini di 1 anno e mezzo che improvvisano per 5 minuti.

[MIROR project](https://www.researchgate.net/publication/276205132_MIROR-Musical_Interaction_Relying_On_Reflexion_PROJECT_FINAL_REPORT)

È importante avere stimoli alla comunicazione e la musicalità e l'avere interazioni con la mente e l'interazione musicale va sviluppata.
___

Lo scambio della forma del dialogo:

![16](16.png)

La musica non narra nulla, ma proto-racconta:
![17](17.png)

![18](18.png)

![19](19.png)

![20](20.png)

La musica ha un potere:
- biologico
- sociale

>Saggio da leggere e studiare per poterne citare dei concetti chiave.

- come si sviluppa l'approccio affettivo in relazione alla musica?
- come la musica si lega allo sviluppo del bambino?

### Tafuri e lo sviluppo dell'intonazione

Osservando dei bambini di scuola dell'infazia si notava che i bambini, prima intonavano le parole e poi a cantarli con la melodia e lo sviluppo della melodia era molto tardo e solo verso la scuola dell'infanzia si cerca di realizzare un'intonazione fino agli anni '70.

Nella ricerca della Tafuri, con comportamenti canori anche prima della nascita, si cerca di capire se si impari prima a parlare o a fare musica.

___

### Mente umane e relazione musicale

_Ascolto della canzone dell'anatroccolo_

La musica sviluppa un modo in cui la mente umana mette appunto delle strutture mentali a cui poi vengono appoggiate altre funzioni secondarie, tra cui la musica.

La disponibilità all'interazione si sviluppa prima di tutto sul sonoro, e sui codici sonori dell'interazione.

La bambina ha un'alfabetizzazione tonale e si capiscono gli intervalli tonali e la tonalità.

Vi è quindi una precocità dell'intervallo musicale, della Tafuri e di Sound Minds di Nina Krauz, che spiegano le strutture mentali musicali a cui poi si appoggiano altri elementi.

Ci potrebbe bastare riconoscere che l'essere umano nasce per realizzare interazione, dopodichè la capacità di interazione può essere sviluppata o meno.

### La natura umana della musica

Dall'[articolo](https://www.frontiersin.org/articles/10.3389/fpsyg.2018.01680/full) di Malloch e Threvarten.

Utilizzo di pattern come la innata musicalità degli umani.

![21](21.png)

Intervista a Malloch e Threvarten di Manuela Filippa: da [Musica Domani volume 170](https://www.musicadomani.it/wp-content/uploads/2020/11/MD_170.pdf) a pagina 10.

Osserviamo che il tempo non è piatto, vi è una narratività temporale condivisa. I parametri che tutti gli umani sviluppano sono:
- pulsazione
- qualità
- narratività

>Le conseguenze sull'espressività e la capacità di relazione di assenza di relazione e insufficenti interazioni della fase precoce dello sviluppo.

![22](22.png)

Alla base della communicative musicality, vi è lo sviluppo di una conferenza e sintonizzazione affettiva di affetti vitali che vengono espressi come gesti e suoni che hanno profili dinamici-temporali comprensibili.

>Le intuizioni scientifiche vengono sempre riprese e poi confermate e la ricerca neurologica con la scoperta dei neuroni specchio che mostrano la predisposizione degli esseri umani all'interazione, con un'imitazione variata, cominciando rispecchiando e poi variando di nuovo.

## Musica e aspetti musicali anche prenatali

La voce è lo strumento letterale per amplificare delle matrici motori che si sviluppa.

Se il neonato mostra una particolare sensibilità alla voce umana, si può spiegare che in un primo contesto in cui si sviluppa il canto, si sviluppano prima i contorni melodici della voce, che quelli verbali.

La differenza fondamentale sono i contesti in cui si canta e quelli in cui non si canta.

Giocando con la capacità degli schemi di imitazione, si impara subito a riprodurre e plasmare i suoni.

La modalità di interazione, con una voce che canta dal vivo con un bambino aiuta la musicalità di un bambino.

## Stimolo di alcuni aspetti nel musicale

Il discorso delle intelligenze multiple vale anche per aspetti diversi di ascolto della musica.

Ad esempio in certi casi si preferiscono melodie senza parole (approccio Gordoniano) per poter tenere l'attenzione sugli elementi musicali e non solo verbali.

Si differenziano le melodie per il tipo di impianto melodico e fisico e non per le parole per Gordon...

Tra i 3 e i 5 anni avvengono nello sviluppo linguistico tanti avvenimenti.

## Cercare tracce del dialogo tra bambino e mamma

Ovvero osservare lo scambio di profili dinamico-temporali tra due adulti che realizzano un'improvvisazione.

___

## Da fare a casa

Visione di link con 4 orchestre ed esecutori diversi dal concerto n.4 per pianoforte e orchestra e il rapporto tra orchestra e piano generano un coinvolgimento.

4 link in cui confrontare 4 situazioni di grandi esecutori che traducono lo scambio di sfumature fra i vari esecutori.

Questo esprime quando il tipo di interazione musicale ha conseguenze diverse sull'esecuzione della musica.

Si fa sentire il dialogo che si trasforma grazie a Beethooven.

___

Lunedì prossimo in presenza vi sarà un foglio firme.

Stefania Guerralisi, grazie all'idea della base sinestesica della musica.

___

Giulia Cremaschi ha sviluppato il protocollo di Nordoff e Robbins, traducendo le sfumature minime di tocco e melodia delle persone che hanno davanti.
