# Appunti della lezione di lunedì 9 maggio 2022

(Legge 107 2015, legge Buona Scuola)

La valutazione viene data per i corsi dei 24 CFA.

Inoltre oltre ai 24 crediti dei corsi che stiamo seguendo, mentre serviranno 6 crediti per i tirocini.

___

## Concetti chiave definiti nel corso

- teoria delle intelligenze:
  - intelligenza musicale
  - intelligenze multiple -> Gardner ("teoria delle intelligenze multiple"), Piaget, Bruner("intelligenze mentali del suono, del timbro e del ritmo", teoria dei vari stadi con _pensiero iconico_ e _pensiero simbolico_)

- pensiero armonico
- body-mind -> Welch, Dalcroze("applicazione operativa" confermata da studi), Stefania Guerra Lisi,
- embodied cognition

>In questo corso diversamente dal corso di pedagogia, studiamo le ricerche collegate a un panorama di concetti chiave.

- baby talk -> saggio di Imberty, voce come prima interazione vocale con il bambino, prototipo di tutte le sintonizzazioni adulto-bambino
- memorie dei sensi
- narratività
- pensiero cognitivo
- musicalità nelle varie aree del cervello ->
- percezione sinestesica
- emo-tono-fonia

>Estrarre concetti chiave che tornano e si collegano ad altri studi.

Importante la connessione body-mind, in cui la mente non è separata dal corpo, partendo dalle memorie fetali che in tutta la storia del corpo che lasciano delle scie.

Il nostro cervello nasce dei kit di base, con l'organizzazione del nostro cervello, le esperienze ripetute divengono delle piste centrali rispetto a dei solchi.

Tutto il mondo neurologico riconosce l'embodied cognition, che non passa per l'esercizio delle aree semantiche della corteccia, ma per livelli più profondi.

La ricerca è finanziata dai parenti di persone con Alzheimer e Parkinsono. Cosa sono le memorie procedurali, ovvero quelle che rimangono quando non vi è più nulla.

>Alcune cose sono delle memorie del corpo e tutto quello che riguarda la propriocezione si portano dietro.

[Esempio di embodied cognition](https://www.classicfm.com/discover-music/periods-genres/ballet/new-york-dancer-alzheimers-remembers-swan-lake/)

### Visione del dialogo papà-bambina

Interazione con il suono della bottiglia, cercando di imitare il suono.

Abbiamo una bambina di 8 mesi, e guardando la faccia della bambina, essa mette le labbra verso l'emissione del suono e dell'imitazione.

La bambina cerca di incamerare nel suo imprinting attraverso una modalità che è imitazione nel senso di rispecchiamento.

Ci si rispecchia incamerando il gesto...

Conta molto la ripetizione perchè si creiino le memorie procedurali, quindi tutte le azioni ripetute vanno bene?

>La musica o l'arte si prestano facilmente, perchè hanno movimenti e realizzazione di un qualcosa di base.

Parola chiave: sintonizzazione affettiva

La sintonizzazione affettiva che abbiamo espresso in modo tipico con cui abbiamo cercato il rispecchiamento, non si sperimenta una sola volta ma volte volte, cercando sempre insegnanti con cui rispecchiarsi, dei modelli.

>Si impara per ripetizione, esperienza ed imitazione.

La capacità di automatizzazione senza far più sparare.

>Quali sono le memorie che rimangono?

Serve un desiderio di sintonizzazione, ovvero un desiderio che da bambino si ha, mentre da adulto bisogna avere la motivazione.

_Vi sono molti studi sulla motivazione._

Alcuni elementi scattano per complicità affettiva, creando un'affezione.

L'unica cosa che può stimolare al rispecchiamento, ovvero alla sintonizzazione è la stima per una maestria.

La memoria e gli studi di [Damasio (l'errore di Cartesio)](https://www.adelphi.it/libro/9788845911811).

Nella musica vi sono delle potenzialità in più dell'abbassare la pialla, ed il dialogo sonoro per imitazione di gesti funzionali del dialogo musicale.

Perchè dal baby talk in poi bisogna coltivare la sintonizzazione affettiva, intuendo che creando dei contesti di interazione sonora diviene un contesto ideale per realizzare degli automatismi.

Vedremo il _dialogo sonoro_ dal baby talk all'improvvisazione.

Mamma e bambino improvvisano nel baby talk e la modalità di improvvisazione è un qualcosa.

Con il progetto Miror della SONY vi sono interazioni di improvvisazione per 5 minuti.

#### Il dialogo sonoro
>Quanto il dialogo sonoro aiuta nella lezione?

[Mauro Scardovelli - Il dialogo sonoro](https://shop.tuttosteopatia.it/libri-di-comunicazione/17071-il-dialogo-sonoro.html)

Rispecchiare in ogni lezione per mezzo di un dialogo sonoro.


#### I neuroni specchio e l'intenzionalità

I neuroni specchio sparano riconoscendo l'intenzionalità dell'interazione, bisogna quindi la volontà di fare qualcosa, senza ragionare, realizzando un gesto che ha già tutto un modello con cui voglio rispecchiarmi.

#### Sintonizzaizone affettiva e Imberty

Ricerca del prototipo di sintonizzazione che ricerca i marcatori somatici e cognitivi di una relazione stretta di ascolto reciproco.

Nel baby talk la svolta si ha quando la mamma ascolta il bambino dopo esso è stato ad ascoltare la mamma.

L'affettività sarà la molla.

Secondo Stern, bisogna stimolare l'ascolto reciproco.

Le relazioni che ti cambiano, sono quelle più strette con ascolto...

#### Proust e le esperienze della mamma che gli leggeva le storie

Si ha un modello di relazione con il mondo, fortemente segnato dalla ricerca del bello.

### Multidisciplinarietà e musicalità

![1](1.png)

La musicalità appartiene a tutti gli umani.
- Musicalità per __esprimere la propria esperienza del mondo__ con un filone di studi legati a Gardner: distinzione attraverso il tatto e l'udito, interleggere e imparare a crescere dagli stimoli; un musicista per mezzo di una rappresentazione mentale cerca di evocare alla mente e si sarà tanto più musicisti, quante più memorie si avranno e si saranno elaborate
- __Modalità ottimale per comunicare__ con Stern, Threvarten e Addesi (vi è un collegamento tra essa e l'espressione dela propria esperienza)
- __Giocare con i suoni__ esso significa concentrarsi e trarre piacere dal gioco, per questo il gioco è collegato all'apprendimento e all'intelligenza

>Abbiamo quindi una rilettura dello schema di Piaget, osservata per mezzo dei giochi, parafrasata da DeLalande (Libro - [la musica è un gioco da bambini](https://www.francoangeli.it/Ricerca/scheda_libro.aspx?Id=9377)), più ci si concentra apprendendo cose diverse, più si avranno diversi tipi di sensibilità.

### Abilità come catena di sinapsi

Riconoscere degli elementi collegati ad altri in maniere complesse.

## Ricerche sulla capacità distinta di linguaggio e musica

Isabelle Peretz si pone una domanda _fino a che punto la musicalità è una capacità distinta dal linguaggio?_ rispondendo alla domanda dicendo che ciò è molto complesso.

![2](2.png)
Si sono quindi sviluppate due correnti:
1. modularità specialistica -> Gardner
2. ipotesi del parassitismo

### Peretz e gli studi su afasia e amusia

- caso di Sebalin
- caso di Isabelle

>Ogni minima area della corteccia parietale(zona della testa sopra alle orecchie) ovvero dove vi sono l'area di Wernicke e Broca, in cui vi sono le reti di sinapsi dedicate alla comprensione del linguaggio ed alla produzione del linguaggio.

Il linguaggio non è solo comprensione e produzione, ma è anche timbro, spazialità, fraseggio, staccato, legato, etc...

Oggi sappiamo che la modularità è fondamentale nella produzione artistica e musicale.

Alcuni attori usano dei microcervelli di cui è specialista il musicista.

>Vi sono delle localizzazioni in cui vi sono dei microcervelli che si usano per la sola musica, altri per il linguaggio ed altri che si usano per entrambi.

![3](3.png)

Bisogna far interagire la parte sinistra e destra del cervello.

Parti del cervello:
- parte sinistra -> elementi di ragionamento
- parte destra -> colore del suono

È importante anche la rappresentazione dei movimenti delle dita della mano sinistra.

## Ipotesi sulla demenza di Ravel

[Articolo](https://www.lescienze.it/news/2002/01/23/news/nuove_ipotesi_sulla_demenza_di_maurice_ravel-590068/)

Molto probabilimente egli subì due disturbi diversi:
- afasia progressiva primaria
- degenerazione corticobasale

Le abilità musicali sono distribuite su tutto il cervello.

### Il coinvolgemento del nuclues accumbens

![4](4.png)

## Ricerche sul dialogo sonoro

Da sempre sull'uomo vi sono prospettive filo-genetiche e onto-genetiche.

Si possono osservre nell'uomo delle situazioni che poi dopo ritornano, infatti la natura non fa salti, ma l'apprendimento è ciclico.

Ci interessa quindi il filone di studi filo-genetici. Da tempo sappiamo che la nostra mente non acquisce per cassetti e l'apprendimento è a spirale, e l'apprendimento è significativo quando si riesce ad agganciarsi qualcosina a qualcosa che già c'è.

L'apprendimento è ritornare sempre sulle stesse esperienze ritornando in modi diversi su elementi a spirale.

Diventano interessanti i contesti per possibili apprendimenti.

Scardovelli come anche nel [Mondo intrapresonale Adulto-bambino di Stern](https://www.ibs.it/mondo-interpersonale-del-bambino-libro-daniel-n-stern/e/9788833954127), affrontano e si chiedono: è possibile che essendo il contesto della scuola primaria, un qualcosa di difficile con relazioni non attive.

Addessi e il suo [libro sul progetto MIROR](https://www.lim.it/it/opere-collettive/5186-le-tecnologie-riflessive-per-lo-sviluppo-della-creativita-musicale-e-motoria-dei-bambini-il-progetto-miror-9788870969009.html#/1-tipo_prodotto-pdf_lim)

### Progetto finanziato dalla provincia di Pisa supervisionata da Delalande

Il progetto è stato realizzato da Andrea Vitali.

>Quanto conta nella prima infanzia l'incrementare la pratica del dialogo sonoro?

Nel nido si è fatto realizzare un angolo insonorizzato intorno al quale si possono sviluppare delle interazioni adulto-bambino, l'adulto non è un genitore, ma qualcun altro che si sa porre come un dialogo sintonizzato uno a uno lo strumento farà quindi da mediatore di questa interazione.

[Video del libro di Vitali](https://www.musicheria.net/suoni-dai-libri/5060-suoni-con-me): DS 4 - Lorenzo e Manuela.

_[Visione del video](https://www.youtube.com/watch?v=5tuSb517yIg&feature=youtu.be)_

>Rispecchiare e sostenere ciò che realizza il bambino.

#### Cronologia
1. il bambino tocca le corde per primo
2. la maestra tocca nuovamente le corde
3. la maestra tocca la cassa armonica
4. la maestra percuote la cassa
5. il bambino la percuote
6. inizia un dialogo con le percussioni
7. dialogo sulle corde
8. dialogo sulle corde con velocità diverse
9. analisi degli elementi sotto al ponte da parte di bambino e maestra
10. il bambino accarezza con gesti leggeri le chiavi e lo segue la maestra
11. il bambino torna nuovamente sulle corde con una velocità costante

#### Osservazioni

- strumento posto su un telo nero al centro della stanza, ovvero preparando il setting
- bisogna creare un ambiente didattico in cui creare e muoversi ed eliminare i distrattori
- esplorazione dello strumento (fino alla realizzazione e all'utilizzo dello strumento)
- importante è la prossemica
- esplorazione di zone nuove
- scoprire lo spazio
- ricerca della diversità dei timbri
- piacere dell'interazione: il bambino viene stimolato dall'azione dell'educatore
- nel video non si parla mai, ma vi è un'attenzione sul suono

### Progetto che colloca alla fine di ogni lezione, un dialogo sonoro improvvisato

>Quanto conta dedicare sistematicamente un angolo alla pratica del dialogo sonoro.

(Dal libro di Scardovelli osserviamo che vi si allena)
L'I.C. del rione Monti, ha realizzato un tirocinio con un dialogo sonoro:
- sui tamburi

_Visione del video_

Ascoltare le improvvisazioni dei compagni.

Realizzazione in coppia con ecosistemi equilibrati tramite la modalità del rispecchiamento.

È mancata forse la ricerca sistematica su varie modalità di linguaggio, ad esempio con un gioco di ruolo.

Nel dialogo sonoro, l'interazione deve andare oltre le modalità intuitive dell'interazione.

L'interazione è problematica ed il dialogo sonoro è utile insieme all'oservazione dell'altro.

>L'insegnante di musica ha fatto realizzare il laboratorio.
