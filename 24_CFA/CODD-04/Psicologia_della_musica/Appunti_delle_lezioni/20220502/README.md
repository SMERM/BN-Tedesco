# Appunti della lezione di lunedì 2 maggio 2022

- Concentrazione simultanea/armonica solo con elementi

## Parole chiave dell'incontro con Stefania Guerralisi

- Globalità dei linguaggi
- Reazione emo-tono-fonica
- Integrazione possibile:
  - l'handicap è relativo?
  - Diritto a essere come si è
  - Principio di piacere
  - Siamo come alberi con foglie diverse
- memoria evolutiva
- associazioni sinestesiche
- Contenimento/Compassione
  - non viene dal protagonismo, ma dalla condivisione: com-piacimento
- l'insegnante è alchimista
  - deve saper aspettare i tempi della materia che plasma distillandone i potenziali, aiutandoli a svilupparsi
- Pinocchio - occhio di Pino [libro](https://www.amazon.it/Occhio-pino-Pinocchio-Francesco-Calvino/dp/8826314640)
- homus musicus
- Placenta (placet)

![1b](1b.png)

[Stefania Guerr Lisi su Youtube](https://www.amazon.it/Occhio-pino-Pinocchio-Francesco-Calvino/dp/8826314640)

## Intervento di Tyna Maria

- diritto a essere come si è
- siamo come alberi, ciascuno con foglie diverse

[Globalità dei linguaggi](https://www.disciplinaglobalitadeilinguaggi.it/paradigma/#:~:text='Con%20tutti%20i%20linguaggi'%20o,non%20verbali%2C%20senza%20previe%20esclusioni.)


## Quando inizia la percezione?

Sono in quanto sento e più sento, più percepisco più sono...

>Penso quindi sono - Cartesio

Recuperiamo un salto significativo che lo sviluppo dell'essere umano, e veniamo da una fase cognitivista e l'essere umano è in quanto conosce, poichè si crea delle rappresentazioni mentali del mondo.

La musicalità è intelligenza musicale.

Io sono, in quanto conosco.

Abbiamo una prospettiva sull'uomo fortemente ancorata sull'uomo, alla mente.

La prospettiva sull'uomo inizia con Cartesio: "Penso dunque sono".

Quindi se l'uomo non pensa? Esso è solo un peso?

Gli studi di neuroscienze confermano che le sinapsi nel cervello sono articolate e organizzate in base ai tipi di pensieri che si sono sviluppati.

Ravel negli ultimi anni della sua vita ebbe una malattia mentale, anche se continuò a scrivere fino alla fine.

### Neuroni specchio e _embodied cognition_
Le sinapsi nel cervello si organizzano in base a quello che si fa.

Le connessioni tra corpo e mente, in cui cervello-sistema endocrino-sistema emotivo sono connesse, da quando scienziati italiani con la teoria dei _neuroni specchio_ che riconoscono azioni in quanto azioni e non perchè ci ritornano sopra.

Oggi abbiamo l'_embodied cognition_ ovvero la cognizione integrata. Tutto il corpo conosce anche prima di nominarle, dirle e spiegarle.

Vi sono _memorie del corpo_, quindi il corpo ha delle memorie, esse sono delle memorie corporee che si sono iniziate a raccogliere.

Gli ecografi che studiano l'orecchio, vedono che le vibrazioni tattili, in cui il liquido in cui si è immersi, raccoglie delle vibrazioni di varia natura.

>Anche una vibrazione tattile che si propaga nell'aria arriva nel liquido amniotico.

Arriva la vibrazione da parte del liquido amniotico su tutto il corpo, dato che il feto ne è immerso all'interno dell'utero.

Il feto assorbe le vibrazioni tattili e le vibrazioni tattili-uditive.

Stefania Guerra Lisi afferma che tutti noi, si portano dietro l'archivio di memorie corporee, ovvero una sorta di vocabolario potenziale che le esperienze successive che si vivono nel corso della vita, possono sviluppare.

>Lo sviluppo avviene in un grembo, denominato _grembo sociale_.

### Schemi cognitivi

L'intelligenza è un qualcosa che l'umano ha per sopravvivere, come ad esempio lo schema: figura-sfondo.

### Lingua madre

Ci si sincronizza con la lingua madre

### Teoria pre-natale

Si hanno 9 mesi potenziali.

### Utero come MusiCavernoTeca

Sensazioni piacevoli e preoccupazioni inesistenti.

Le discoteche come cavernoteche, un po' regressive.

Il suono di ambienti piacevoli o spiacevoli ricevuti si cerca di ritrovare.

### Raccolta di profili temporali dinamico-temporali

![2](2.png)

Il paradigma della musicalità come capacità dii interazione espressiva ovvero la _communicative musicality_.

![3](3.png)

Vi è un imprinting fondamentale, con una sintonizzazione affettiva con una figura parentale per mezzo di tante condivisioni.

>La sintonizzazione comincia prestissimo, scambiando affetti vitali come gesti e come suoni che hanno profili temporali riconoscibili.

Vi sono mille modi per comunicare affetti vitali.

>Ciucciare è il primo schema motorio di tutti gli umani.

### Insegnare a scrivere e leggere recuperando i materiali emo-tono-foniche

Ciò potrebbe aiutare a realizzare e sviluppare la parte della fonia.

Come il recuperare alcune parole e mimarle:
- in italiano: goccia
- in inglese: drop

Abbiamo due gesti molto diversi.

Cercando le radici emotoniche di una parola, mimando e disegnando come si dice una parola.

Come anche la parola albero, recuperando le parole emo-tono-simboliche:
![4](4.png)

### Quale è la prima cosa utile rispetto a un corso di psicologia della musica?

Leggere il saggio di Imberty sulla sintonizzazione affettiva.

Tutti gli studiosi dicono a modo loro con il paradigma della _communicative musicality_ che tratta di memorie musicali tattili che incomincia a sviluppare in una fase precocissima della vita.

[Scienza con coscienza di Edgar Morin](https://www.amazon.it/Scienza-coscienza-Edgar-Morin/dp/8820424258)

## Tratti distinitivi di un evento e distinzione di alcuni elementi

Secondo la gestalt...

## Legge 517 del 1977

Essa afferma che una qualunque difficoltà si puó gestire meglio dentro un gruppo integrato.

Legge sull'integrazione, unicum del nostro paese. Gli italiani erano gli unici a dire le parole.

Lezioni importanti sull'inclusioni, possibilità di accesso agli spazi.

>In Italia le norme vanno molto più avanti dei fatti.

Un insegnante straniero non sa cosa si insegna in Italia e come si insegna ai bambini con differenze psicofisiche e socio-culturali.

## Pratiche musicali di comunità

Laboratori esperienziali attraverso i quali stiamo meglio inisieme per via di pandemia e guerra.

Come poter riscoprire l'identità collettiva?

Decreto 517 del 2017, ed il decreto 60 è il decreto sulle arti che promuove e finanzia il progetto sulle arti che prevede dei laboratori artistici che aiutino i ragazzi che aiutino a riscoprire e a ritessere la loro identità collettiva.

Come quelle realizzate dalle comunità intorno a Tivoli.

Vi è una ricerca di artisti per promuovere pratiche comunitarie.

## Addio cognitivismo: l'embodied cognition

La cognizione riguarda prima di tutto il corpo.

- Guerra Lisi -> le memorie del corpo
- gli scienziati la chiamano -> embodied cognition (conoscenza incarnata)

### I neuroni specchio

![5](5.png)

Richiamo all'origine ed al nucleo delle scienze neurologiche.

>Cosa accade se un macaco mangia una banana davanti ad un altro macaco?

Quali parti del cervello si attivano?

Si muove la corteccia motoria e non quella visiva e si muovono gli stessi neuroni del movimento, di quello che mangia la banana. La risonanza magnetica spara gli stessi neuroni del movimento di quello che mangia la banana.

Esiste una particolare classe di cellule nervose che si attiva quando mangia un pezzo di cibo, sia quando un'altra scimmia o un altro umano effettua lo stesso movimento.

>Vi è una predisposizione a catturare il gesto che si guarda. Si cattura con i neuroni specchio mentre qualcun altro fa qualcosa davanti a noi.

I neuroni specchio sono particolari aree dell'area motoria e non rispondono alla sola presentazione del cibo.

#### Apprendimento per imitazione

Due soggetti in modo ravvicinato in cui due soggetti uno esperto ed uno no, realizzano un collegamento per lo studio.

Ma bisogna realizzare una sintonizzazione affettiva.

Ci vuole un contesto di motivazione allertata che Stern chiama sintonizzazione affettiva.

Il macaco rispecchia il gesto perchè capische dove vada la banana.

Insegnando gesti strumentali all'allievo bisogna far capire all'allievo a cosa serva il gesto.

>Bisogna imitare per far capire che serve un gesto per far legare ad un effetto.

![6](6.png)

#### Comprensione del senso di un'azione

Essa non è solo conseguenza di un procedimento logico-inferienziale, ma è il risultato di una simulazione globale del modello.

L'atto motorio è in se intellegibile.

![7](7.png)

#### I neuroni e l'atto osservato

Il fatto di aver visto serve ad apprendere per imitazione.

![8](8.png)

La modalità di comprensione altrui più rapida, diretta ed intuitiva.

>Ci si capisce subito quando nel repertorio si capisce già cosa si sta realizzando, ovvero in base agli schemi d'azione.

![9](9.png)

#### Studi sull'autismo e deficit dei neuroni specchio

Si sta studiando e si è scoperto che molto probabilmente gli autistici hanno problemi nei neuroni specchio.

#### Empatia e neuroni specchio

![10](10.png)

### Autismo e gestualità mimica del riconoscimento

Si deriva dal ragionamento o si realizza un'altra tipologia di rituali per poterli far accedere all'interno della società.

Cio chè si apprende per potersi relazionare con gli altri.

### Il Mirror System è plastico

Questo gruppo di neuroni si può sviluppare molto o meno, esso è quindi plastico.

#### Esperimento con la Capoeira

Abbiamo:
- danzatori che danzano
- ballerini di Capoeira
- ballerini dell'opera
- principianti

Vedremo quando i danzatori danzano, quanto verranno attivate le zone dei due gruppi di ballerini che osservano gli altri osservatori.

I neuroni specchio si attivano maggiormente nell'osservare la specialità in cui si è esperti.

### Riconoscimento delle azioni

Anche per il riconoscimento delle azioni vi sono dei meccanismi distinti:
![11](11.png)

Vi sono due modi di spiegare l'espressività strumentale:
1. Patrick Jusslin -> Il feedback cognitivo (come si agisce per discriminare in modo fine per rendere più o meno morbide o fini gli elementi)
2. Utilizzo dei neuroni specchio con attivazione di attivatori che gli fa discriminare subito (toccare il tasto prendendo al volo)

## Conclusioni

Si capisce quanto divenga interessante quando si inizi ad iniziare un vocabolario.

Il repertorio di memorie motorie incarnate lo abbiamo già iniziato a raccogliere quand eravamo piccolissimi.

>Memorie pre-natali come la vibrazione tattile, all'inizio abbiamo un qualcosa di emo-tonico, che poi diviene fonico.

Abbiamo quindi suoni nella pancia filtrati sulle frequenze acute con la percezione pre-natale che costruisce gli schemi che ci porteremo durante la nostra vita.

____

## Per l'esame

28 maggio o prima settimana di giugno

forse il 23 maggio ci sarà l'ultima lezione

2 esami sessione estiva, 1 ad ottobre, 1 in autunno.

### Cosa vi sarà all'esame?

Possibilità:
- domande
- concetti chiave

>Prepararsi alla verifica rivedendo i contenuti delle lezioni, utili ad articolare i concetti chiave.

Di tutti i temi (8/10) il giorno della verifica si chiede di mandare online l'elaborazione con le parole chiave in base ai contenuti del corso.

Utilazzere citazioni di autori trattati a lezione.

Possiamo citare autori con anno e titolo tipo:
- Welch (1989)
- Stern

### Tipologia di elaborato

L'elaborato dovrà essere un testo per elaborare l'embodied cognition.

____

Cognizione -> Cognizione incarnata -> memorie corporee dell'Homo Musicus

>Dobbiamo riconoscere che la psicologia è un ambito di studi in cui ci incominciamo ad orientare.
