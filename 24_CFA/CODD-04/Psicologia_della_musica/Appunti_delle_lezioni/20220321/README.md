# Appunti della lezione di lunedì 21 marzo 2022

## Affrontare le cose insensate

Un modo per affrontare le cose insensate della realtà è fare in modo che le nostre scelte non siano assurde.

Non subire gli eventi e re-indirizzare quello che facciamo, tenere in mano e gestendo il senso di quello che facciamo.

Discipline come quelle della psicologia sono nate proprio per questo motivo (Platone e Socrate facevano psicologia ma era tutto racchiuso nel sofismo).

## La nascita della psicologia

Si cercò di capire e sapere il motivo delle nostre scelte.

Cercando di capire e non essere vittime delle emozioni.

La psicologia è nata come disciplina per cercare di capire il senso di quello che facciamo.

È interessante utilizzare questi strumenti per riflettere sopratutto su quello che facciamo noi per capire le nostre caratteristiche.

Il musicale è un fenomeno espressivo variegato e multiforme; i modelli per capire quanto un essere è musicale, sono moltissimi.

## Come misurare la musicalità?

Siamo partiti con la prima lezione chiedendoci: quanto sono musicale?

Ci poniamo questa domanda, per avere avere l'occasione di percorrere i vari tipi di correnti di studio, ovvero gli ambiti della letteratura psicologica che si sono occupati di questo problema.

Le neuroscienze hanno sfondato una porta cognitivista cercando di capire la musicalità.

La musicalità è solo uno dei modi per cercare di capire cosa sia la musicalità.

I musicisti hanno un tipo di intelligenza musicale per cercare di costruire rappresentazioni dall'esperienza.

Vediamo uno schema realizzato per mezzo delle parole chiave poste sul drive.

Tutti i gruppi hanno realizzato un'esperienza condividendola tutti.

Ma cosa resta dell'esperienza della scorsa volta? Assumiamo che ciò che resta della memoria è la significatività dell'esperienza, con esperienze, belle e interessanti.

## Curva di Hebbing-House

Dicono gli specialisti della memoria, che anche un'esperienza la si ricorda, ma con una memoria a breve termine.

## Vygotskij dice

![1](1.png)

- il pensiero bambino è ricordare, memorie e pensieri
- pensare per un adulto è invece elaborare

>Il problema del pensiero bambino essendo basato sui ricordi, vive delle realtà fantastiche, ma alcuni elementi si perdono.

Nella memoria restano degli elementi contrastanti, il compito dell'educatore è fare in modo che avvenga quello che Vygotskij ha studiato, ovvero il ruolo dei segni.

Vygotskij disse:
-  quanto contano i segni

Non limitarsi passivamente a vivere le esperienze ma esperire.

Il pensiero adulto è fermare le memorie con dei segni e proprio con i segni, cominciare ad elaborare quel pensiero.

>Spesso si sprecano le esperienze.

Dentro di noi, pensiamo che basti fare delle esperienze, ma dal punto di vista della corrente cognitivista, bisogna sempre dotarsi di segni.

## Gruppi

### Gruppo 9

Segnato con una lista temporalmente distribuiti:
![2](2.png)

### Gruppo 7

La differenziazione con il segno (maiuscolo e minuscolo), dotandosi di segni per spiegare i concetti chiave:
![3](3.png)

Elabrorare il segno e lo scheletro di pensiero che vi è dietro.

La scrittura del linguaggio verbale è uno strumento potente.

### Tipologie di report della lezione
- cronaca -> esso è realizzato in genere nella scuola primaria -> si impara a realizzare uno storyboard, restituendo parole chiave in un ordine temporale
- mappa concettuale -> organizzare le parole non con ordine temporale, ma ipotizzando delle gerarchie

Baremboim, nel libro "La musica sveglia il tempo", (citando Spinoza) quando deve dirigere un qualcosa di lungo, si richiama alla mente lo schema e le relazioni d'ordine che si pensa con dei suoni, un'intelligenza musicale che richiama alla mente lo schema di relazioni d'ordine.

>Una caratteristica della musicalità è che centrali sono le composizioni uditive.

## Schema fino ad ora

![4](4.png)

- la __ricerca è messa in moto da problemi e domande__
- __che cos'è la musicalità?__
  - _essa ha basi biologiche?_ (i cadaveri dei musicisti hanno delle caratteristiche particolari come studiato da Goettfried Schlaug che si occupa di bioneurologia)
  - _è suscettibile di sviluppo?_


![5](5.png)

![6](6.png)

Abbiamo trattato:
- Seashore -> relazioni tra fenomeni fisici dei suoni e la percezione degli stessi
- Gardner -> rappresentazioni fondamnteli uditive
- Piaget -> entomologo degli anni '30 studiando i coleotteri -> egli scoprì che non vi è una forma unica di intelligenza, si scelgono i modi di intelligere, ovvero dello scegliere
  - gioco nello sviluppo dell'intelligenza, "dimmi come giochi e ti dirò chi sei"
  ![7](7.png)
  - età in cui i bambini giocano con la scopa, il pensiero divene legato al pensiero d'azione (come il moemnto della scopa con il pensiero senso-motorio, con schemi di azione della scopa) -> a un livello elevato il pensiero senso-motorio ti appartiene -> graduando lo schema senso-motorio in modo molto fine, realizzando discriminazioni motorie sempre più fini -> _dentro un tipo di intelligenza, si può divenire estremamente fine e raffinati_ -> __il piacere del gioco senso motorio__
    - la musicalità è un modo di giocare con i suoni e si vede nel modo di giocare, mettendo insieme intelligenza e gioco
  - tre modi di pensare una scopa che si sviluppano in 3 momenti diversi dell'età evolutiva:
    - __pensiero senso-motorio__ -> pulire con la scopa
    - __pensiero del gioco simbolico__ -> realizzazione di astrazioni -> oggetto concreto per "fare finta che sono" -> suonare è sognare -> ciò che tocco è la cosa concreta, la cosa che sogno, i suoni stanno al posto di un sogno musicale che faccio assieme a Schumann
    - __pensiero del gioco con regole__ -> dal piacere senso-motorio, all'organizzazione di incastri e gesti complessi con le sole scope
- H. Witzinga -> storico e antropologo che scrisse negli anni '40 -> il libro Homo Ludens, e studiò che una delle caratteristiche dell'umano è che quando si gioca, sempre bisogna concentrarsi; per quanto semplice sia il gioco bisogna concentrarsi,(senza concentrazione e senza piacere non si impara) le componenti che spiegano perchè i giochi sono importanti, sono:
  - concentrazione
  - piacere

  ![8](8.png)
- Delalande -> teoria delle condotte musicali (con 3 opere di Mondrian diverse) osserviamo che cambia il modo di intelligere qualcosa (le età riportate non sono più rilevanti per Delalande, ma egli pensa che esse siano intelligenza diverse):
  - gioco senso-motorio (dalla nascita)
  - gioco simbolico (dai 3 anni)
  - gioco con regole (dai 7 anni)

>Le musiche che si conoscono meglio sono quelle su cui si sanno fare più giochi. Dal testo ["Giochi d'ascolto" di Franca Ferrari](https://books.google.it/books?id=ycoq2hdRX9QC&printsec=frontcover&hl=it&source=gbs_ge_summary_r&cad=0#v=onepage&q&f=false).


### Montessori e il senso motorio

Maria Montessori occupò di costruire i materiali Montessori. Campanelli Montessori per lo sviluppo della percezione del suono.

Edgard Willehms pensò a 100 campanelle uguali che divergono per cents.

### Problemi DSA
>Il problema dei DSA è un problema di rapporto con i segni scritti e rapporto con i segni scritti, essa è una neuro-diversità.

### Bruner
![9](9.png)
- Bruner -> un conto è l'esperienza ed un altro è come ci rappresentiamo l'esperienza -> come si formano le rappresentazioni mentali? -> Bruner si poneva il problema di come far capire il codice (per la lingua), quello che conta è far realizzare esercizi, ma quanto conta è realizzare un qualcosa per fare ricordare delle esperienze:
  - __fare esperienze con il proprio corpo__ -> hic et nunc -> per crearsi pensieri sulla musica -> avendo delle rappresentazioni e memorie sulla musica -> esse devono essere esperienze senso-motorie significative (Pasquale Bona inventò il metodo Bona e insegnava il solfeggio negli anni '70 dell'Ottecento l'esperienza delle note lunghe deriva dal fatto che egli era un cornista)
  - __rappresentare e "far uscire" le esperienze__ -> dispositivi che aiutano e costringono l'allievo far venire alla mente le esperienze -> si apre l'esperienza del sonoro nella mente? -> in questa fase azzurra centrale, ovvero la __fase iconica__ non si è abbastanza pronti per dare il senso di ciò che esca
  ![10](10.png)
    - ad esempio si può far uscire gli allievi con dei grafici che si leggono per analogia come gli esempi grafici realizzati con le flash cards -> cercando di farli ritornare a pensare -> __un'insegnante bravo ha una serie di notazioni mirate per ricordarsi l'esperienza per stimolare a ritornare sull'esperienza__, nel caso dell'alfabetizzazione ritmica e melodica vengono seguiti pattern neurologici diversi (codice ritmico e melodico seguono strade diverse)
    - far lavorare sull'associazioni grafico-uditive
    - dando per un po' di volte giochi da fare per creare compiti diversi, usando la letto-scrittura per motivare una composizione
    - il __ruolo della notazione analogica__ -> essa serve a realizzare un _passaggio alla discriminazione di unità fonologiche_, bisogna imparare a passare dall'esperienza del continuum, ma avendo bisogno di imparare a riconoscere nel continuum delle unità fonologiche, ovvero imparare a riconoscere la composizione sintattico-lessicale -> per l'acculturazione
    - anche con problemi di ritmo, i bambini hanno una rappresentazione di ritmo grafico-mentale -> la discriminazione di unità fonologiche è il capire
    - le carte servono fino a quando non si arriva ad una fase dell'esperienza per poter imparare a scrivere la musica
  - __dalle esperienze nostre a quelle degli altri__ essa è la __fase simbolica__ in cui si sostituiscono i mazzetti delle carte con delle scritture come quella riportata in figura, lo scopò dell'insegnante di musica è mettere in moto l'intelligenza musicale, riuscendo a discriminare unità di senso ritmico
    - giochi che aiutano a mediare i passaggi

### Cos'è il codice per noi musicisti

Esso è un simbolo e deve evocare alla mente un'esperienza musicale.

Se quando si arriva al codice non si riconosce l'esperienza ritmica allora vi è stato un grande problema nell'insegnamento.

### Cosa motiva i bambini

I bambini si stanca molto e il maestro si deve dotare di una serie di strumenti per collobarare anche con i genitori.

### Applichiamo lo schema di bruner alla melodia

![11](11.png)


Il cantare bene, aiuta ad articolare bene anche il linguaggio, l'articoloazione ritmica si lega alla melodia.

Il dispositivo più efficace per consolidare e rappresentare consolidazioni mentali della melodia, è il canto.

>Il feedback audio-vocale, consolida molto di più di quello strumentale.

(Dagli studi di Matisse e Krauz)

Il canto:
- mi fa ascoltare con il corpo

Il feedback audio-vocale è la conditio sin equa non per parlare bene, suonare bene.

Più si esercita il feedback audio-vocale, meglio sarà per capire la melodia.

#### Progetto incanto
Una delle ricerche in questo senso è ["Progetto inCanto" di Johannella Tafuri](https://www.centroincanto.it/il-progetto-di-ricerca/) pubblicata da EDT.

Avviso per le mamme in attesa a frequentare un corso di musica per mamme in attesa.

La ricerca longitudinale è durata dai 6 mesi nella pancia ai 6 anni dei bambini.

Si sono raccolte le testimonianze delle registrazioni.

>L'organo uditivo nella sua struttura è pronto fisiologicamente in tutti gli umani normali al sesto mese di gravidanza.

In alcuni feti, l'orecchio è pronto al quinto mese di gravidanza, mentre la struttura dell'occhio cominica a funzionare bene dopo 2 settimane che si è nati.

Vi è quindi un apprendimento che inizia prima di nascere.

Il musicoterapeuta in Neonatologia (Barbara Sgobbi) .

##### Nascere a Parco Leonardo

Se si è incinta e si va ad abitare a Parco Leonardo, al bambino danno fastidio i rumori degli aerei.

E dopo 15 giorni si abitua.

Il bambino sente tutto dal punto di vista tattile e propriocettivo, sentendo e capendo tutto.

>Ci vedremo in presenza l'11 aprile in Sala Accademica.

#### Zoltan Kodaly e le funzioni educative della musica

Sappiamo che più i bambini conoscono i linguaggi e più la loro capacità di pensare si sviluppa.

Più linguaggi -> più pensiero -> più elaboro

Egli sviluppò il cerchio azzurro, cantando e non solamente consumando, realizzando un repertorio di memorie melodiche.

Il metodo ungherese parte da un repertorio di melodie (come il metodo Gordon) che impariamo giocando insieme con l'esperienza.

Con le melodie dovremo partire con la fase iconica, senza cantare sempre per imitazione, arrivando ad una fase iconica, con delle tracce visive che aiutano a portar via l'esperienza di canto.

Imparare con la chironomia, realizzando una danza in cerchio con una canzoncina cantata in cerchio che parla la zanzara.

Realizzare uno schema visivo non scritto, ma con una posizione delle mani con cui possiamo cantare molte cose.

Imparate le posizioni, avremo il codice, il lessico, ovvero l'alfabeto con cui cantare anche un'altra serie di cose.

Si collegano ad uno schema visivo dei passaggi fonologici.

Chiamare con i nomi delle note i movimenti, per aiutare a far sentire la spazialità.

Si può cantare pensando ad una campana con le note scritte.

Si possono portare via tracce visive, ovvero schemi da cui portarsi via l'esperienza.

>Realizzare un gioco in cui è collegato il continuum-senso-motorio ad una traccia ritmica.

Leggere vuol dire coordinare il corpo e l'occhio per dirgli dove si vada poi a finire.

Camminare gli intervalli e misurarlo con il corpo, aiuta a misurare l'ampiezza maggiore o minore degli intervalli.

Realizzare un corredo di melodie pentatoniche e pentafoniche ed una campana su cui si possono suonare e seguire con i movimenti tutte le melodie.

>Intervalli come unità fonologiche e movimenti spaziali da acquisire con un movimento visivo.

Dopo aver realizzato il movimento si realizzano le composizioni, scrivendosi tesserine con anche i pezzi di pentagramma.

Senza scrivere la musica, si scrivono dei pezzetti e poi s cerca di capire la musica.

>Dare senso alle scritture degli intervalli con la scrittura da fase iconica, estraendo e ricomponendo da una fase senso-motoria.

Le esperienze melodiche sono quindi importantissime.

Gli ungheresi e in Italia Roberto Goitre, hanno realizzato libri con repertori di canzoni e nell'indice vi sono i titoli e le note che usa una canzone.

Lo strumento compensativo richiede che si organizzino le melodie avendo presente la gamma in cui si muovono e i movimenti che si fanno fare.

#### Bruner e le tre fasi

Bruner descrive come solamente tutte e tre le fasi insieme sono efficaci.

Essi sono 3 livelli di rappresentazione dell'esperienza per arrivare al possesso del codice.

## Come capire dalla fase iconica cosa si sta capendo?

![12](12.png)

Jean Bamberger ha studiato come i bambini realizzano rappresentazioni mentali, cercando di far disegnare la tecnica in diversi momenti dell'età evolutiva.

Bamberger cerca di aprire finestre per capire.

Ma ci si chiede: come funziona l'intelligenza?

Si cerca di raccogliere e capire cosa si ha raccolto per costruirsi una data rappresentazione mentale?

Che tipo di intelligenza si sta usando?

![13](13.png)

### Scarabocchi ritmici
Gli scarabocchi ritmici sono la rappresentazione di un qualcosa di continuo.

### Gocce

Cosa rappresenta il bambino con le gocce? Esso rappresenta un bambino già con il senso simbolico, il suono fa finta di fare le gocce.

### Mani

Essa è una rappresentazione della causa per l'effetto, usando un simbolo.

Abbiamo in questo caso un simbolismo diverso.
