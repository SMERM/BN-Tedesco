# Appunti della lezione di giovedì 3 marzo 2022

[link per le lezioni](https://zoom.us/j/3651882838?pwd=MXhxZnpWYWNyODJCZ1ZGbHdKaStOUT09)

## I corsi dei 24 CFA

Corso di 10 lunedí consecutivi, esso durerà 30 ore, con una verifica scritta.

Questo corso vale per l'area elettiva, mettendola nell'area elettiva.

Questo corso non è pensato per l'area elettiva, ma per il pacchetto dei 24 crediti.

### Il pacchetto 24 crediti

Dalla legge 107, è previsto che per l'accesso a qualsiasi posto di docenza per insegnare è necessario superare un concorso che richiede:
- diploma di secondo livello
- 24 CFA

I 24 CFA si possono acquisire in una quantità di modi:
- iscrizione al corso accademico
- da esterno

Ci sarà la possibilità di acquisire i 24 crediti.

Leggere da soli le pagine del DM 616/17

I 24 crediti hanno una tassa di 500 euro e sono proporzionali al numero dei crediti.

Il decreto precisa che si possono conseguire i crediti fino a 6 mesi dopo il diploma di Biennio.

Non sentirsi obbligati a fare tutto velocemente e male.

Distribuire gli esami per i 24 CFA.

### Perchè seguire i corsi

Seguire i corsi servirà forse per insegnare, e l'unica preparazione per un contesto pubblico è questa dei 24 CFA.

L'esame del conseguimento è difficile.

I 24 CFA si possono prendere anche nelle università telematiche.

### Perchè seguire al Conservatorio
Per la formazione orientata alla musica, per i 24 CFA è il conservatorio.

Per i crediti si può chiedere il riconoscimento ovunque.

### Allegati DM 616/17

Vedere gli allegati del Decreto.

### Nuovo corso dei crediti

Conta che si arrivi all'esame nel nuovo modo...

### Attestato

Per la terminazione dei 24 CFA viene rilasciato un attestato (p.fraioli@conservatoriosantacecila.it).

Il pacchetto dei 24 CFA deve avere almeno 6 crediti in almeno 3 ambiti diversi.

Quarto ambito antropologico che non è trattato.
___

## Presentazione del corso

Stare al gioco per fare le cose.

Tutti devono essere degli insegnanti, è interessante che qualcuno abbia chiesto di insegnare qualcosa.

Si è scoperto che le cose che si sanno insegnare meglio, non sono tutto, e ci si chiede cosa sia una competenza.

>E ci si chiede in cosa consiste una competenza musicale?

In cosa consiste essere competenti?

Vi è un sapere ed un saper fare.

Le abilità tecniche e le capacità strumentali sono cose diverse.

_Musicorum et cantorm magna est distantia_

Essere dei musici e dei cantori è molto diverso.

La competenza musicale passa da una serie di conoscenze, ovvero da un sapere e da un saper fare, ed essi si devono sostenere.

Bisogna inventarsi un saper far fare, ovvero un saper comunciare in cui questi due aspetti si sostengono l'un l'altro, altrimenti si disperdono.

Visto che "Divide et Impera", trionfa.

Una serie di abilità che si sono automatizzate non si riescono ad insegnare.

La competenza musicale:
- sapere
- saper fare
- saper far fare

Lavoriamo sul saper far fare.

### Su cosa lavoriamo

"Il candidato possiede modelli teorici e operativi per condividere la condivisione del suo sapere".

Per il sapere musicale, esso deve essere giustificato da un saper fare.

Trovare dei modelli teorici e operativi per condividire il sapere ed il saper far fare.

Tutti sono portatori sani di:
- sapere
- saper fare
- saper far fare

Serve una progettualità per il sapere.

### Come si fa ad insegnare la musica?

- fare degli esempi -> sembra scontato
- realizzare un esperienza

L'unico insegnante che fa un po' di differenza è l'insegnante di educazione fisica.

Un insegnante musicista entra in classe ed è una risorsa per la scuola, che è rigida, fare esempi live è una risorsa pazzesca.

Controindicazioni...

### Classi di concorso

Le classi di musica hanno tre prove e non due.

Qualcuno ha detto che i musicisti insegnanti sono anche dei musicisti e per tutte le classi di concorso.

La prova telematica fa media con una prova pratica, ovvero prova di allestimento di una lezione.

In questo concorso tutte le prove fanno media.

>Il concetto è che entrano insegnanti musicisti.

I musicisti sono in grado di portare la loro disciplina live.

### Modelli educativi

Essi sono tanti e ognuno può decidere i propri.

#### Modello deduttivo
Viviamo in una traduzione di insegnamento che realizza un discorso con metodo deduttivo.

Alla fine si fa un esempio, ovvero si inserisce l'esempio live alla fine del discorso.

Si spiega prima un qualcosa e poi si risolve il problema.

Quello che succede è che quando arriva l'esempio, nessuno arriva all'esempio.

Il documento che sostiene e regola l'insegnamento musicale di tutte le discipline "Indicazioni nazionali per il curricolo del ciclo primario", in una premessa c'è scritto più volte che il modello educativo che più si addice ai giovani è un modello induttivo e laboratoriale, euristico, esperienziale.

#### Modello esperienziale

Gli apprendimenti nell'età del pensiero concreto si realizzano in un contesto laboratoriale.

Si farà di tutto per organizzare il corso con un impianto laboratoriale, inserendo dentro una componente laboratoriale.

Assumendo un modello educativo che dice che "la musica si impara facendola".

Si lavorerà a capire come si fa a "saper far fare".

Si punterà quindi a realizzare un modello laboratoriale.

Alcune componenti si erogano anche con una didattica frontale, ma esse dovrebbero essere una singola parte.

Euristico, da trovare euristo...

Dobbiamo realizzare un contesto in cui gli allievi realizzano qualcosa.

Nella riflessione pedagogica si contrappongono un modello euristico e un modello depositario.


#### Modello depositario

Modello che fa uso del libro come prima fonte

### Come funziona questo corso?

C'è una struttura, avremo gli incontri, vi è una struttura parallela, ed avremo i materiali del drive con organizzazioni a gruppi.

Ognuno di noi sarà in un gruppo.

L'idea sarà di cercare e sforzarsi e organizzare la scoperta dei concetti chiave attraverso una doppia faccia di sapere e saper fare, propondendo un'esperienza e riascoltando le esperienze e organizzare quindi il sapere.

Per seguire il corso bisogna avere il link del drive e anche essere in un gruppo con il quale condividere delle esperienze durante la lezione e al di fuori della lezione.

### Parole chiave

Segnare in chat le parole chiave del discorso per poter organizzare lo schema della lezione.

### Il ruolo dell'educatore

Esso è realizzare un ambiente uno spazio in modo da attirare l'attenzione e la sua abilità, confezionandosi degli arredi per il suo ambiente di apprendimento.

Il tempo della lezione non è casuale ma è da organizzare.

Bisogna organizzare lo spazio e il tempo, realizzando il _setting_, ovvero "l'ambiente di apprendimento" secondo Maria Montessori.

#### Il nostro ambiente di apprendimento

- Drive
- gruppo -> per imparare a condividere il sapere con tutti


### Cosa fare quando l'ambiente non è musico-orientato o artistico-orientato

Essa è una mission da eroi.

### Cos'è la creatività?

Essere dei musicisti creativi.

Dipartimento di Creative Learning del MIT, il guru del dipartimento, Michael Resnyk, "Give Peace a chance".

#### Le 4 P dell'apprendimento creativo.

- Play
- Project -> guardare avanti
- Passion
- Peer -> lavorare tra pari, il gioco è da far fare tra noi, ciò vale per i giovani

>Si scopre tra 10 e 15 anni la personalità, scontrandosi con i propri pari, e quindi l'insegnante deve incontrare l'incontro-scontro tra pari.

### Far musica, come?


Pensare che la musica è un'attività trasversale, ed almeno una di queste 4 attività gli allievi la possono fare.

Partendo dalle aree performative di base, realizzare qualcosa partendo da quello che già c'è:

![1](1.png)

Si può fare:
- parlando
- cantando
- ascoltando
- suonando

Realizzeremo delle simulazioni per ciascuna delle 4 cose.

Ci si inventa qualcosa su questa abilità del parlare:
![2](2.png)

#### Gruppi 1,2 e 3
12 stanze zoom e si dirà che i gruppi 1, 2 e 3 sono invitate ad assumere questa frase stimolo come materiale per organizzare qualche cosa per imparare il concetto di "timbro", ovvero "sound".

Inventare dei modi per spiegare cosa sia il __timbro__, ovvero il sound, facendo capire cosa sia il timbro.

>Sonor

#### Gruppi 4,5,6

Lavoreranno su __ritmo__, articolazione per far capire il concetto di ritmo.

Cercare di far capire e condividere qualcosa con il ritmo.

#### Gruppi 7, 8 e 9

Lavorano su una __melodia__.

Costruire degli esempi, ovvero delle domande melodiche da completare.

Una scala è una lingua è un sapore, ovvero un modo di organizzare una melodia.

Registrare le prove che servono per far capire cosa sia una melodia.

#### Gruppi 10, 11 e 12

Lavorano sulla __forma__, ovvero lavorare sull'organizzazione della forma.

Molto è orientato alla forma.

Dare una forma sonora, una forma musicale.

Organizzare la lettura come una forma musicale.

### Aspetto musicale

Entrare con una frase per realizzare un aspetto musicale.


### Imparare la musica: un castello

La musica è come un castello a 5 porte e si può entrare in essa in 5 modi diversi, con zone neurali diverse, e nella musica vi è un ingresso che è il sound.

#### Iperaudizione del sound
Michel Imberty dice:
"I giovani consumano la musica in genere essendo attenti al sound" e si cerca prima dei contenuti lessicali, un certo timbro che arriva come messaggio musicale.

Il sound engineer prende in genere più dell'artista e tutto si gioca sulla qualità del suono che fa presa dell'oggetto.

## Strutture e forme della musica come processi simbolici

![4](4.png)

Qualsisasi aspetto della musica è un processo simbolico.

Marco de Natale...

>Preoccuparsi dell'emissione vocale, la fonazione e la ricerca della modalità di suono.

Quando suoniamo alcuni aspetti anche se siamo concentrati, vi è un focus, ovvero uno zoom su alcune zone specifiche del cervello.

---
20 minuti

scambiarsi qualche idea per registrare audio prova anche singoli.

preparare e confezionare la frase.

Scrivere: Gruppo 1 - Cognome - Aspetto del suono su cui si lavora come (gr1_FERRARI_timbro)

[Drive](https://drive.google.com/drive/folders/1MbLSsfVAPItC2fnLiGiUu_RoQ1l0FNJ_)

Lasciare delle tracce, ovvero delle briciole...


## Gruppi
### Gruppo 1



- Considerate la vostra semenza -> un timbro
- fatti non foste a viver come bruti -> un timbro
- ma per seguire virtute e conoscenza -> un timbro

Idea: ognuno fa il suo timbro, organizzare il paesaggio sonoro e organizzare lo spazio in funzione dei ruoli.

Ognuno fa il suo timbro...

I progetti servono ad insegnare il timbro a partire da una frase che si possa leggere e poi capire.

Non facilità di integrazione tra il progetto e la comunicazione sonora.

Cercare dei suoni sonori con 3 passaggi diversi.

Insegnare ai ragazzi lo strumento della voce, ad esempio essendo cantanti.

### Gruppo 2

Duetti, amici dello stesso sesso con cui fare le cose e per la prima volta si sperimenta la complicità con un pari. Ovvero con qualcuno con cui si fanno le cose.

Dipendentemente dal modello educativo si cerca un amico del cuore.

Ad una certa età si ha bisogno di cercare dei giochi, delle attività e dei progetti per creare un abilità di coppia.

>Il prof di musica dice: in due creare dei progetti.

Si ha un tipo di ascolto reciproco fra pari.

Progetto semplice e replicabile

Vicinia, voce/strumento

### Gruppo 3

Struttura per l'organizzazione di una sequenza, il passaggio del testimone, la staffetta.

Il lavoro sull'identità è fondamentale.

---
## Parole chiave del giorno

- modelli educativi (deduttivo, esperienziale, depositario)
- creazione di un ambiente di apprendimento
- le 4 P dell’apprendimento creativo
- Multitraccia come possibilità di raccogliere/editare il suono di un'identità collettiva di cui si fa parte (Gruppo 1 - Sound)
- Creazione di raggruppamenti con ruoli ben definiti con lo scopo di sviluppare l'ascolto reciproco tra pari
- struttura per l'organizzazione di una sequenza
---

Libro consigliato: Raymond Murray Schafer - il paesaggio sonoro

___
Su moodle vi è tutta la sezione degli annunci con i materiali.

___
## Esame finale

- Prova di gruppo
- Prova personale

Una parte si confezionerà con il gruppo, ed un altra singolarmente.

___

## Testi di riferimento
