# Appunti della lezione di giovedì 28 aprile 2022

## Informazioni sull'esame
Dopo la fine delle lezioni.

Un appello prima del 31 maggio (in genere ad inizio giugno), poi ad inizio luglio, a ottobre ed a febbraio.

Vi sarà una lista di domande, ovvero una serie di temi di cui ciascuno ha una parte di risposta operativa e una parte teorica.

Vi è per ogni tema, una parte di lavoro realizzata con il gruppo, mentre la parte teorica è molto più personale.

---

## Far incontrare la teoria e la prassi: cosa fa la differenza nell'insegnamento

Cerchiamo di convogliare la situazione particolare del nostro paese e noi. I ragazzi che seguono il corso devono andare in una situazione particolare e generare un cambiamento.

Ad esempio le verifiche della EBE, che osservano quando un progetto educativo funziona, le condizioni perchè vi sia una condizione di insegnamento migliore, è che il docente debba credere in ciò che fa.

Studieremo un saggio sulla scelta politica e culturale che la scuola italiana ha realizzato, con la legge 517 del 1977.

Il ministro dell'istruzione dell'epoca, Falcucci, decise di abolire le _scuole differenziali_, ovvero le scuole frequentate da tutti i portatori di disabilità.

Oggi quante scuole speciali vi siano, dipende dall'organizzazione del paese.

In Germania vi sono 2 tipi di scuole differenziali a Baden Burtenberg (dove vi è Stoccarda):
- svantaggi psicofisici
- svantaggi sociali/culturali

Se non si supera la scuola alla quarta e alla quinta primaria in Germania, non si può andare al Gymnasium, e vi è un paradigma e quindi un modello diverso da quello italiano.

### Italia: pedagogia e scuole all'avanguardia

Nel 1977 in Italia, studi pedagogici molto seri (l'Italia dal 1950 in poi ha scuole a Reggio Emilia con il progetto Reggio Children famose ed all'avanguardia in tutto il mondo).

Le scuole italiane in alcuni casi costituiscono dei prototipi all'avanguardia.

In questa situazione di studi sulla pedagogia, ci si chiede:
- che senso ha dividere i disabili in una scuola?

I ragazzi con disabilità richiedono modelli diversissimi, ed ogni diversità ha il suo modello terapeutico.

Una serie di studi oltre alla chiusura dei manicomi portò anche al cambiamento di insegnamento.

>La soluzione migliore per qualunque malattia psichica, è il gruppo integrato. I ragazzi con un problema più serio hanno delle modalità di interazione che permettono al ragazzo con problemi di partecipare al lavoro in classe. Questa tipologia di contesto serve per sviluppare il potenziale, per far regredire alcune patologie.

Si cerca di sviluppare i potenziali all'interno di un contesto integrato per mezzo di un facilitatore.

Si cerca di realizzare, come ideato da Andrea Canevaro, con l'introduzione di uno _sfondo integratore_ in cui si cerca di far armonizzare nei gruppi integrati e integrare tutti all'interno dello _sfondo_.

>L'educazione diviene quindi un arte del design, perchè fa progettare un tipo di contesto di apprendimento.

Maria Montessori ci aiutò a pensare all'insegnamento come un compito da regista per progettare un ambiente di apprendimento.

La disposizione dell'aula fa scegliere la tipologia di didattica, ovvero la didattica frontale se disponiamo la classe a battaglione.

>Didattica frontale a dinamica interattiva.

La modalità didattica frontale, diventa tragica, se è sempre e solo l'unica tipologia di didattica, ciò rispetto alle differenze di apprendimento porta a qualcos'altro.

### Insegnamento ed apprendimento

Pensare ed inventare delle modalità di insegnamento in base a ciò che i ragazzi apprendono da ciò che insegno.

Molto di quello che ha sviluppato la Montessori è stato realizzato organizzando l'aula, dividendo il gruppo, ovvero un'organizzazione dello spazio, del gruppo e del tempo.

La lezione come è realizzata, l'allievo si aspetta qualcosa quando si arriva a lezione. Come si organizza la lezione? Come si congeda un alunno da una lezione per farlo tornare?

Si realizza quindi temporalmente una struttura della lezione.

>Mettersi alla pari dell'allievo, scrivendo insieme all'allievo, facendo qualcosa insieme, calandosi entrambi nella stessa dimensione.

Vi sono varie modalità di insegnamento e apprendimento.

### Didattica laboratoriale

Al contrario della didattica frontale, la didattica laboratoriale include tutti a fare qualcosa per far capire ciò facciamo.

Nella tipologia di didattica con imitazione vi possono essere imitazioni:
- simultanea -> essa è un'imitazione simultanea a raccogliere, questa modalità consente una vicinanza, ovvero una prossemica che può essere da lontano o da vicino; si può imparare in maniera contemporanea all'insegnante
- successiva -> per l'insegnamento dei moduli melodici, piú difficile e complessa, perchè richiede stare in un tempo ed il controllo melodico

>Bisogna costruire l'agio per il gruppo di ragazzi.

L'insegnante se realizza qualcosa con i ragazzi deve far sperimentare il momento educativo come contatto tra insegnante ed allievo.

>Saggio di Baldacci sul laboratorio come metodologio didattica.

È importante la prossemica all'interno di un discorso.

Il primo modo di stabilire una distanza è la voce.

>L'uso delle dinamiche e delle altezze, i registri vocali stabiliscono una distanza con l'interlocutore.

La dinamica laboratoriale consente di organizzare schemi di distanza diversi, in questo senso si favorisce l'inclusione.

>La metafora dello sfondo integratore, nasce anche da cosa si possa chiedere a tutti.

#### Modello dell'orchestra esagramma

Vi è un esempio limite ovvero quello dell'orchestra esagramma, di individui che non riescono a gestire la manualità fine.

Nelle scuole medie musciali questi ragazzi non li inseriamo, perchè non possono gestire la manualità fine. Le disabilità fisiche emergono moltissimo dallo sfondo, e bisogna disegnare una modalità musicale, che consenta di armonizzare le differenze.

#### [Diverso non stona](https://www.youtube.com/watch?v=BbLw1dOz2rc)

>Ricordando il video _Diverso non stona_, eseguendo una versione di insieme dell'Allegretto di Diabelli.

Si costruisce un arrangiamento, ovvero una riscrittura del brano con un gruppo che non richiedono la manualità fine, ed una gamma di altri interventi di tipo percussivo, in cui ciò che conta per suonare insieme, non sono importanti le note, ma capire il gesto strumentale adeguato, al momento giusto.

Il tipo di gesto e di qualità di suono che vada bene in un dato momento, ricercando nelle risorse di tutti, cercando una risorsa strumentale per tutti, studiando e trovando una gamma di interventi per tutti.

Si devono quindi stimolare i ragazzi per educare 2 o più modalità alternative.

Con ogni soggetto umano non è fare o non fare, ma è gestire le alternative.

Dopo aver proposto le alternative, creo un arrangiamento del brano.

Creare degli arrangiamento per creare con rispetto della forma un intervento di tutti i ragazzi come nell'[Orchestra Esagramma](https://esagramma.net/eoi-e-orchestra/), inserndo una gamma di ruoli per armonizzare l'inclusione dei ragazzi senza uccidere il compositore.

>La chiave per l'inclusione è un arrangiamento ben fatto.

I ragazzi l'unica cosa che sono capaci di fare da solo è andare tutti insieme.

### Il problema della didattica frontale

Il problema della didattica frontale è che essa è in genere sempre l'unico modello didattico, che non favorisce diverse situazioni prossemiche.

### Didattica e differenza di età
>La distanza di età e i modelli di identificazione, ci fanno portare a chiedere, quando si riesce ad imparare?

Si impara quando si identifica quello in cui si sta insegnando.

>Conta molto avere una mamma o una maestra/maestro che canta bene.

L'identificazione nell'età di chi si ha davanti è molto forte nelle prime età dell'apprendimento.

Saper creare delle risonanze che mettano in moto le antenne dell'allievo.

La stima per la maestria del maestro, fa scattare un legame con il maestro.

(10 maggio convegno al ministero sulle comunicazioni di pratica artistica)

>Per un adolescente creare dei modelli di insegnamento di gruppo è molto più semplice farlo con la musica.

## Il testo di Lucia Chiappetta

Lucia Chiappetta Cajola è una delle più importanti personalità per la realizzazione e lo studio della pedagogia per l'inclusione.

I diritti dei BES, tutti hanno diritto alla partecipazione ed all'apprendimento.

Quali sono le risorse e i potenziali del ragazzo, cercando di capire i potenziali e le risorse di ogni individuo, come visto con i ragazzi del Liceo di Viterbo.

Cercando di scoprire dei nuovi potenziali espressivi.

I ragazzi che potenziali espressivi hanno?

Bisogna cercare di capire con occhi ed orecchie cosa possano fare a livello musicale.

>Una scuola con inclusione per le diversità è presente in Italia e in pochissimi altri paesi.

È possibile che in Italia si trovino situazioni difficili, qualsiasi materia si vada ad insegnare.

### Chi è Lucia Chiappetta?

Essa è un docente all'Università di Roma Tre.

[CV](https://www.univaq.it/include/utilities/blob.php?table=collaboratori_per_struttura&id=1903&item=curriculum)

È una specialista di didattica che esprime la necessità di laboratori con i giochi e laboratori musicali.

### Lettura del testo (testo all'interno di un [progetto più ampio](https://www.siem-online.it/siem/ferrari-santini-musiche-inclusive-download-saggi-e-materiali/))

![1](1.png)

Le leggi riportate, sono importanti perchè nel nostro paese si va da una fase di esclusione iniziale dei disabili e la correlata istruzione separata, fino all'inserimento e l'integrazione, l'integrazione e la partecipazione.

Il percorso può essere realizzato solo in situazioni di buone prassi che riescono a far fare solamente se vi è una rete tra insegnanti, collaboratori didattici, genitori etc...

>È faticoso tessere la rete se si è da soli.

![2](2.png)

Stefania Guerralisi ha aiutato a facilitare l'integrazione e l'inclusione dei ragazzi nelle classi.

Tutto il mondo occidentale è stato investito dal vento per l'inclusione, rimuovendo le barriere architettoniche.

La parola nei paesi anglofoni è stata inclusione oltre all'integrazione.

>Se non cambia la didattica è inutile parlare di inclusione.

Si ha un riferimento all'In-Clausum, ovvero al chiostro, entrare all'interno di un chiostro, condividendo dei rituali.

L'inclusione ha a che fare con il consentire a tutti di partecipare ed apprendere.

Le maestre hanno più tempo per organizzare le lezioni.

Utilizzare un cartellone per realizzare un programma di ciò che si farà a lezione, ovvero un diagramma, una mappa specificando cosa si farà nell'ora.

Tutti gli alunni in alcune situazioni non riescono a pensare al dopo, ma solo ad adesso. Nella scaletta della lezione che si realizza, si lancia una nuova sfida, lavoro a coppie etc...

>È difficile avere un approccio al tempo senza forma.

Il caso ideale visto, è quello di Marina Penso, pensando di organizzare un arrangiamento, per mezzo dell'insegnante di sostegno, di facilitare e realizzare un arrangiamento ideale per tutti.

Un insegnante di sostengo per mezzo dell'interpretazione corrente sta sulla classe e non sull'unico ragazzo.

L'esclusione è divenuta quindi costante...

Ci si ricorda a scuola qualcosa di speciale in maniera positiva o negativa.

Si armonizzano ruoli diversi all'interno della classe.

>L'esperienza con normo-dotati e disabili vi sarà solo a scuola.

__Il paradigma culturale inclusivo__, esso è un paradigma di:
 - omogeneità
 - diversità

 >Le musiche che si conoscono meglio, sono quelle su cui si sanno fare più giochi.

![3](3.png)

Ciò vuol dire che oggi esiste uno strumento (index for inclusion) che stabilisce che tutte le aziende hanno una pagella corredata in base a questo indice, che descrive come ci si è organizzato in base a questo indice:
- barriere architettoniche
- realizzazione organizzativa

>Il problema non è trovare dispositivi

Gli strumenti compensativi servono per rimuovere le barriere che ostacolano l'apprendimento, creando ad esempio uno schema educativo (come diverso non stona) per far capire dove si è.

Per Enrico Strobino, in Butulumani, si lavora sulla pronuncia ritmica del testo, catturando le frasi ritmiche con strumenti.

>Organizzare il progetto per permettere la partecipazione e l'apprendimento di ciascun alunno alla lezione.

Tutti gli individui sono complessi e bisogna pensare alla differenza come il valore fondante:
- cercando di valutare le differenze
- cercando di capire cosa un individuo sappia fare

È importante la direttiva del MIUR del 2012 che parla dei bisogni educativi speciali:
![4](4.png)

- disabilità
- disturbi evolutivi specifici
  - complesso
  - vi sono all'interno delle patologie dello sviluppo che hanno un nome -> patologia dello spettro autistico (sindrome di Asperger)
  - DSA, come dislessia, disgrafia, discalculia, ADHD(spesso questo ragazzo ha un insegnante di sostegno per comorbilità)
- svantaggi culturali

>Tutti noi abbiamo sviluppato una teoria della mente diversa e riusciamo ad infierire qualcosa solamente in base a qualcosa di rituale che qualcuno mi ha insegnato a riconoscere.

Si cerca di creare un frame in cui la modalità non verbale ed esperienziale consenta degli incontri.

Gli studi di pedegagia in Italia, sono stati realizzati sugli autistici.

I disturbi evolutivi specifici, sono diversi da quelli dell'apprendimento.

>Trovare degli ostinati ritmici da trovare suonando e cantando.

Si deve coinvolgere in un qualcosa che dia forma, ovvero contenga l'ipercinetismo. Come ad esempio un ostinato ritmico. Si è dentro a una cosa complessa in cui siamo dentro a qualcosa di ripetibile.

>La direttiva del 2012 da la facoltà di richiedere BES per ciascun individuo che abbia gli svantaggi trattati.

È importante citare parti del testo come ad esempio la didattica laboratoriale:
![5](5.png)

Laboratorio musicale:
![6](6.png)

### Considerazioni sul testo

Bisogna combinare il tempo con almeno alcuni moduli di didattica laboratoriale che coinvolgano i ragazzi nella didattica.

### Modello EAS (Episodi di Apprendimento Situato)

Uno dei modelli più importanti è quello di Pier Cesare Rivoltella, denominato EAS.

L'apprendimento situatato deve portare da un'esperienza laboratoriale situata, quello che c'è da capire.

## Colloquio con la collaboratrice di Rivoltella (Federica Pilotti)

Essa è la gestrice di una comunità su Facebook di insegnanti.

Il modello di episodi di apprendimento situato è un modello di realizzazione di situazioni.

Bisogna far pensare avanti e non al qui ed ora, bisogna aiutare a far progettare e il modello EAS esso ci può aiutare molto.

Questi modelli sono utili all'inclusione.

### Episodi di Apprendimento Situato

![7](7.png)

Gli EAS partono analizzando come si apprende e mettendo come base teorica dove si forma l'apprendimento, per intervenire sui tre modelli di apprendimento.

![8](8.png)

Si apprende sempre con gli stessi modelli...

#### Apprendere per ripetizione

![9](9.png)

Kandel ha potuto sperimentare che si apprende con una proteina ripetendo, e alcuni automatismi mnemonici vanno sviluppati.

![10](10.png)

>Il problema didattico in classe è che la classe sia noiosa.

![11](11.png)

L'importante è uscire stanchi ma non accorgersi di aver fatto fatica.

È importante sfruttare le nuove tecnologie che ci facilitano togliendo un po' la fatica:
![12](12.png)

Applicativi per togliere parte della fatica.

#### Apprendere per esperienza

![13](13.png)

Si apprende avendo l'esperienza e sviluppando una competenza con esperienze molto personali:
![14](14.png)

La competenza è ciò che rimane quando si ha studiato.

![15](15.png)

Se impariamo con fatica e con ansia, allora avremo sempre un approccio negativo allo studio.

![16](16.png)

L'aspetto esperienziale ci serve per anticipare un qualcosa che sta per accadere.

Ed il fare ci consente di anticipare e ci da la previsione.

![17](17.png)

La tecnologia ci toglie tanto tempo:
![18](18.png)

Dietro l'approccio laboratoriale vi è tanto lavoro ed una buona progettazione.

Vi è un costo per portare quello che si è fatto e la tecnologia abbassa di molto i costi, ed un device è uno strumento che pone lo studente in azione.

### Apprendere per imitazione

Sappiamo che vedendo si impara e si apprende qualcosa, scoperto grazie agli studi di Rizzolati sui neuroni specchio.

![19](19.png)

Si impara addirittura ripensando a ciò che si è visto.

![20](20.png)

Dai 0 ai 12 anni il collegamento tra neuroni e sinapsi si conclude con un'accellerazione poderosa dalla fascia primaria, fino al primo ciclo e realizzare la matematica e l'italiano attraverso il corpo ci permette di fare di più.

Per mezzo del pruning si rimuove una parte di informazioni che non si usano.

### Lo studente cambia

Lo studente cambia uscendo dalla classe in cui si fa lezione ed un insegnante modella i propri studenti.

>Insegnare è scrivere nell'anima del discepolo -Platone

![21](21.png)


### EAS

Essi sono legati alle tre parole:
- Episodio
- Episodio di apprendimento -> obbiettivo: l'apprendimento significativo, un qualcosa fa parte dei mattoni della conoscenza e competenza di un individuo
- situato

#### Perchè episodi?

Si è iniziato ad inseriere dei vocali di 3/4 minuti vanno a lavorare meglio di una lezione di 3 ore.

Si concentra una lezione con un inizio ed una fine, lavorando su piccoli contenuti e unità di lavoro.

![22](22.png)

#### Perchè situato?

Si tratta del compito autentico, legato alla vita di ogni studente:
![23](23.png)

Quando si ritrova qualcosa che è utile nella quotidianità, ciò è spendibile subito.

#### La semplessità

Essa significa utilizzare in classe qualche strumento che realizzi strategie per rendere semplesso un compito (come una mappa contestuale):
![24](24.png)

#### Flipped teaching

![25](25.png)

Si da il compito al ragazzo per poter lavorare insieme ai ragazzi, producendo in classe.

### Struttura dell'EAS

1. fase preparatoria
2. fase operatoria
3. fase riflessiva

#### Fase preparatoria

Il docente da un compito:
![26](26.png)

L'azione dello studente è inizialmente di ascolto. e comprensione.

Questa fase può essere data anche a casa, chiedendo di vedere un video.


#### Fase operatoria

L'insegnante da dei tempi per schede di osservazione...

Essa realizza un compito per gruppi ed il docente non interviene facendo lezione, ma da uno stimolo, una guida.

La fase operatoria richiede di lavorare in gruppo.

![27](27.png)

#### Fase riflessiva o costruttiva

![28](28.png)

Deve essere dato del tempo per sedimentare, dialogare ed ascoltare e qui il docente, da guida fa lezione, ragionando su ciò che è stato fatto.

Si passa ad un capovolgimento dell'insegnamento.

>In questa fase i ragazzi il docente ascolta gli studenti, permettendo di intervenire su ciò che non si è capito.

![29](29.png)

>Sfruttando la tecnologia per la parte riflessiva.

### Considerazioni

Non suddividere un compito di questo tipo in ruoli, perchè ognuno troverà il suo ruolo senza semplificare, perchè abbiamo uno strumento Semplesso!

Riducendo il carico cognitivo per chi ha alcune difficoltà nell'apprendimento!

### Domande

---
>Relazione tra le 4 P di Resnick e l'EAS?

Si, anche se l'EAS è l'unità minima di apprendimento.

Un EAS può durare poco, ma può arrivare anche fino a due settimane.

Possiamo legare l'EAS ai laboratori visti come Butulumani:
- fase motivazionale
- fase di laboratorio guidato di gruppo
- fase di laboratorio a coppie
- fase di debriefing con suddivisione dei ruoli

Osserviamo in questa organizzazione:
- un project
- lavoro fra peer
- vi è un play
- vi è anche una passion

Per noi musicisti è molto più facile pensare a fasi motivazionale, di laboratorio e di riflessione.

Ogni azione didattica delle unità di apprendimento deve avere un modello EAS.
---
>Ricordarsi che ciò che trasmettiamo lascia qualcosa.

La verifica deve essere un momento di riflessione.
___

### Osserviamo il [progetto](https://zoomiamoilmondo.blogspot.com/2018/12/compito-autentico-progetto-di-una-sedia.html?q=sedia) di Federica Pilotti per poterlo riportare in maniera musicale

![30](30.png)

Con un apprendimento sul legno. Progettare una sedia da giardino con un [blog preciso](https://zoomiamoilmondo.blogspot.com/).

>Realizzare un repertorio solo da osservare, ovvero con aspetti caratteristici ed a cosa essi devono servire.

Dopo aver visto come sono fatte le sedie, il segno diviene una loro guida per realizzare la sedia.

![31](31.png)

Si sente la modalità di realizzare le sedie, ovvero realizzare dei prototipi, lavorando con l'originalità.

Costruendo delle semplici sedie.

Si lavora quindi al fatto di come deve essere fatta la sedia perchè deve essere posta in un giardino.

Bisogna cercare di vendere la sedia pensando agli errori progettuali.

___

>Disabilità motoria e realizzazione del progetto.

La difficoltà motoria e la tecnologia come si accosta alla materia di Tecnologia.

Utilizzando il portale [SIVA](http://portale.siva.it/it-IT/home/default)
___

## Realizzazione di un compito da proporre secondo il modello EAS

Da pensare e proporre per la prossima lezione.

___

Festival online INDIRE

La musica unisce la scuola dal 9 maggio al 14 maggio...

Calendario di webinar
