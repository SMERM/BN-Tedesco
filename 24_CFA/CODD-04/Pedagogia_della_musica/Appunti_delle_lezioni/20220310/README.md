# Appunti della lezione di giovedì 10 marzo 2022

## Dimensione gruppale e possibilità di concertare
>Importanza dell'interscambio, della relazione e dello scambio. Dobbiamo inventare un arredamento dell'ambiente che metta in conto la dimensione del corso.

La dimensione gruppale è una dimensione importnate.

È importante concertare e lavorare con la musica sempre condividendo dei modelli di concertazione, con modi di arredare lo spazio e il tempo.

La parola concertare proviene forse da "far combattere insieme le parti" oppure da "far andare insieme le parti".

La dimensione gruppale e relazionale è l'unica arma che ci conviene ora affinare.

(segnare nella chat delle parole chiave per ricostruire la traccia della lezione).

## Vetrina: condividere e comunicare

Avere una vetrina in cui conservare gli esiti del proprio lavoro, ed ogni progetto deve finire con il piacere di condividere e comunicare quello che si è realizzato.

Bisogna condividere e comunicare l'apprendimento per essere in grado di salvare le cose migliori.

A volte con alunni di età minore bisogna interfacciarsi per forza di cose con i genitori.


## Riepilogo della scorsa lezione e metodo induttivo

- __metodo euristico__ -> da Euristo -> si spiega tutta la teoria e poi si da un esempio al termine (capire ad esempio cosa ci sia di primo Ottocento in un brano di Beethoven)
- __metodo deduttivo__ è un metodo per trovare
- il contrario del metodo euristico è il __metodo induttivo__
Il curricolo per l'insegnamento del ciclo primario(partire dal metodo induttivo) -> insiemi di occorrenze significative -> dall'ascolto delle musiche si cercano dei tratti caratteristici di un'epoca, con delle parole chiave

Vi è un insegnante che guida, ma l'acquisizione della conoscenza avviene induttivamente attraverso il confronto di una bacheca con esperienze induttive.

Trovandoci dentro un database di esperienze in cui vi è un'occorrenza significative che poi mi fa intendere perchè le cose vadano in un certo modo.

Si raccolgono dei casi e confrontandole si raccolgono dei casi significativi.

- altra tipologie di conoscenze sono trasmesse con un __metodo depositario__

>È interessante che la premessa pedagogica alle "Indicazioni per il ciclo primario..." con consiglio di svolgere con un metodo euristico, che presuppone una motivazione che non è detto che vi sia.

Per attivare un po' un metodo euristico, bisogna pensare alla mia aula di lezione come un laboratorio, ovvero come un contesto di laboratorio.

__Lettura sul drive sull'attività laboratoriale.__

In questo momento gli straschi psicologici identitari della pandemia e della guerra, danno indici verso il cinismo,lo stare da soli il chiudersi. Si indicano quindi elementi con attività laboratoriali per recuperare la dimensione dell'interazione.

Il laboratorio facendo delle cose fa connettere le tre componenti della competenza musicale:
- Sapere
- Saper fare
- Saper far fare -> esso lo si scopre quando si insegna

Abbiamo poi visto gli ingredienti dell'apprendimento creativo:

![1](1.png)

- Play
- Project -> vi deve essere un progetto
- Passion -> la passione è contagiosa
- Peer -> relazioni tra pari -> dispositivi per far lavorare i ragazzi fra loro

Vi sono 4 aree performative che in parte tutti i bambini e i ragazzi possiedono per poter avvicinarsi al far musica:
- parlando
- suonando
- cantando
- ascoltando

Tutti i bambini che si hanno davanti hanno quindi almeno una di queste areee.

Abbiamo sondato le conoscenze de gruppo con un lavoro sul testo che di per se è solo parlato.

__La musica come un castello a 5 porte__ esso è legato ad ognuno di noi in un certo modo. Ora tutti i neuroscienziati parlando per tipologie di orecchi, ovvero di ascolti.

Si attivano posti diversi del cervello, a seconda di cosa ci si sta concentrando.

Specializziamo l'accesso a una delle porte al castello.

Se interessa svegliare un po' tutti gli orecchi, ognuno ha i suoi dispositivi didattici, e vi sono delle strade apposite per ognuna delle porte.

Lavorare su 3 versi sulla Divina Commedia, per sviluppare competenze su:
- timbro
- ritmo
- ecc...

## Attivazione di un laboratorio sul timbro/sound

Abbiamo messo in moto un rapporto di un database, raccogliendo e osservando dei dispositivi ovvero dei modelli.

![2](2.png)

### Dispositivo: coppie(gruppo 2)

Ci mettiamo a coppie e costruiamo un paesaggio sonoro adeguato, con una sorta di _reading_ poetico-musicale.
Il ruolo dello strumentista accompagnatore è la creazione di un ambiente sonoro che favorisca la comprensione di questo testo poetico. Facile dal punto di vista del dispositivo didattico mettendo a disposizione degli elementi sonori.

Attivare delle scoperte semplici sulla qualità del processo sonoro.

### Dispositivo: sequenza(gruppo 3)

Lettura a mille timbri, ovvero una lettura multicolor, in cui ogni verso avrà un suono diverso, in questo gruppo le prime due persone leggono, mentre la terza persona lo intona, con un'idea di sequenza non stop.

### Dispositivo: key words - key sounds (gruppo 1)

Individuare una parola chiave e suoni chiave.
Realizzando una musica a tre blocchi con una scansione dei tre blocchi con un multitraccia che fondono le tracce relative ad ogni elemento.

### Cosa si è fatto e cosa si farà?

3 gruppi hanno trovato 3 modelli, ed abbiamo bisogno di passare da una fase 1 di esplorazione, ad una fase 2, tirandosi fuori, provando qualcos'altro con qualcosa che mi costringa ad andare avanti.

>Nello sviluppo tutto può avvenire.

Se oggi si può improvvisare è perchè si ha una bae sicura, si ha un nido, una casa, con amici, maestri, un gruppo. Si sono provate delle prime interazioni di gruppo.

Abbiamo realizzato una fase dell'"Hic et nunc" della fase sicura.

L'e-ducare, "ex-ducere" spinge a fare qualcosa per poi andare oltre e fare qualcosa di nuovo.

Spesso abbiamo il confronto con altre musiche, altri elementi.

#### Cosa fare per dare delle conferme per incuriosire un allievo con il reading-poetico o paessaggio sonoro

>Prendendo il modello in giallo.

Gli allievi tornano avendo scoperto delle cose, con una liason tra i bisogni dell'allievo e quelli della nostra tipologia di insegmanto.

- __trovare una musica di repertorio__ che può funzionare come ambientazione sonora di questi versi -> costringerlo a pensare a ripensare il repertorio in una funzione applicata con sostegno applicato
- __improvvisare con elementi musicali diversi__
- __condivisione di maestria__ -> rinnovando lo stupore, prendere l'idea e far costruire paesaggi sonori

#### L'orecchio timbrico

È la capacità di distinguere paesaggi sonori diversi che la mia voce che parla, può fare.

Lavorare sulla costruzione timbrica con differenziazioni sonore.

Provare e organizzare, costruendo.

Il reading può diventare un trucco per costringerti a pensare al suono.

Fase 1 -> cercare
Fase 2 -> mettere a disposizione un database, una bacheca

#### Cosa fare per andare oltre con il gruppo 3, la sequenza

Sequenza letta e ad un certo punto cantata.

Cosa succede se si impone una modalità fisca con i bambini.

Simulazione e ricreazione del timbro di un altro individuo.

Applicazione del fonosimbolismo, ovvero dell'applicazione e realizzazione di dispositivi per costringere gli individui ad andare oltre.

Utilizzare un dispositivo fisico che costringa tutti a cambiare il suono.

1. rifare lo stesso gioco, ma passandosi un dispositivo, con cambiamento di timbro che tutti noteranno
2. lanciare un tema che può far cambiare suono

Lo scopo di questo modello è compiacerci delle nostre differenze timbriche.

Se in questo cerchio vi è qualcuno che ha uno strumento portatile, far pronunciare un verso dallo strumento, dando al verso un suono nuovo, senza parlare ma solo suonando.

>Si capisce la logica progettuale, preparare per ogni gruppo la fase 2 per andare oltre a ciò veniva.

## Laboratorio sul ritmo

### Gruppo 4

Figure ritimiche applicate a delle sillabe.

Essendo un ragazzo di prima o seconda media si può spiegare il ritmo con la mediazione tra ritmo e musica.

Come si fa a trovare il metro?

Raggruppare delle terzine, mettendo degli accenti diversi facendo scoprire il tempo, prima di arrivare a sapere cosa sia il tempo.

"Dire delle parole", poi utilizzare delle "parole non parole".

Nella fase 2 costringere a passare alle sillabe cercando gli accenti delle parole.

Il ritmo ha a che fare con forme e raggruppamenti.

Ricondurre il testo a un forma di movimento, una forma gestuale.

1. tradurre in parole non parole
2. cercare gli accenti
3. scoprire se gli accenti hanno una periodicità (metro libero o metro misurato?)

Per la fase 2 autodirigendosi o tramite un direttore, facendo scoprire la metrica.

Svincolarsi da un accento che riguarda dalla parola specifica. Svincolandosi da una forma del linguaggio di Dante.

Giocare con le scansioni sillabiche, con una dimensione ritmica che focalizza ...

### Gruppo 5
![4](4.png)

Varie soluzioni per leggere il testo:

![5](5.png)

Queste configurazioni servono per capire cosa si capisce con le varie articolazioni ritmiche.

Abbiamo un dispositivo elementare che condiziona l'articolazione metrica.

![6](6.png)

#### Fase 2

Si sceglie una delle soluzioni, accostandola ad un suono, allenandoci a sostenere la voce.

Si prende una soluzione e la si prova più volte, avremo il ritmo realizzato come articolazione delle durate.

Si usa il dispositivo della scrittura pausata in modi diversi, respirando in modi diversi.

Ogni gruppo confezioni e realizzi la soluzione, marcandola con un sostegno strumentale per sottolineare lo schema di fraseggio, con una forma di sostegno del suono.

Far discriminare e comporre il parametro ritmico, gestendo un flusso sonoro parlato. Tagliando la stringa di flusso capiamo come si parla.

>Cos'è il ritmo, con cosa ha a che fare? Con durate e accenti.

Usare gli accenti di durate, allungando le sillabe per evidenziare l'importanza di una parte delle frasi.

Preparare e far ascoltare andando a capo, segnando dove si sposta l'accento o come si riscrive.

>Consegna: il gruppo costruisca un messaggio efficace(messaggio che si fa ascoltare e capire). L'importante è che sia come è scritto.

### Gruppo 6

Costruzione di una notazione.

![7](7.png)

Metrica lenta e legata.

Ruolo periodici, ciclici.

#### Fase 2

Body percussion, per condividere con loro questo sapere, si determina quello che si farà.

Si condivide quindi uno schema motorio che condivida il nostro 4/4, con un gesto in 4 che condivida la pulsazione.

Per interiorizzare il 4/4 che ci fa articolare la pulsazione e il metro.

Avremo 2 ruoli:
- uno che tiene il beat
- uno che tiene la pulsazione

Avremo una sequenza ritmica:
- schema di body percussion (gesto per ottavi e gesto per sedicesimi)

Non imporre una figura ritmica.

Articolare la sequenza ritmica sviluppando una consapevolezza di dove sono le durate.

>Il metodo Kodaly, definisce due sillabe nonsense, una per gli ottavi ed una per i sedicesimi.

__Più soluzioni per la classe__ e per formalizzare con una stringa da condividere, bisogna trovare una metodologia integrata che metta insieme (per prendere i visivi, i cinetici e gli uditivi).

Ricostruire quindi la sequenza in modi diversi:
- dire e far vedere con le flashcards -> suono con la scrittura
- dire e realizzare movimenti con i vari pattern -> suono con il movimento
- suonare con lo stesso strumento in maniera diversa -> suono con strumento

Lavorare sulla memoria, vuol dire ritornare sullo stesso oggetto con più modalità di rappresentazione dell'oggetto.

Wor-Ton-Drama -> Orff e Wagner, esprimere con più linguaggi la cosa.

>Differenza tra suono e mettilo in scena, è importante capire movimenti grandi e movimenti piccoli.

Ocetus con distribuzione in gruppi:
- qualcuno fa l'accento di frase
- pulsazioni
- accenti forti
- ottavi
- sedicesimi
- tutta la sequenza ritmica

Con assegnazione di ruoli diversi.

>Inizialmente tutti devono saper fare tutto.

Se è misurato:
- in 4
- in 3

Realizzare lo schema in parole con la descrizione dei dispositivi.

(La fase 2 del progetto è realizzazione.)
___

## Metodologie per lo sviluppo ritmico

1. ritmo e movimento corporeo
2. coordinare la forma con dei gesti
3. trovare l'unità di tempo
4. mettere in una forma ritmica accentuativa che si possono focalizzare con dispositivi come i piedi ritmici

>Cercare di far capire e dedurre senza dover spiegare qualcossa.

Il lessico deve essere collegato all'induzione.

>Avere una metodologia permette di passare dal semplice al complesso con procedure e materiali, in cui se lavoriamo sul ritmo, avremo oggetti che servono a fare delle esperienze ritmiche.

## Dispositivo possibile per arrivare a seguire la pronuncia di tutte le sillabe: flashcard

In queste flashcard facciamo gustare tutta la composizione sillabica.

![3](3.png)

Nella metrica classica si chiamano piedi, per poter declamare camminando avanti e indietro, con un gruppo di sillabe intorno ad un accento.

Forma visiva che aiuta a ritrovare le corrispondenze.

Il prerequisito sono i nomi, con una Gestalt ritmico uditiva.

### Baccheo

Quello in verde è un baccheo come: Giovanni, Andrea, Giuseppe

Piede ritmico anche per le verdure: verdure, spinaci, carote

### Peone

#### Peone terzo

Benedetto, Emiliano, Adriano

Melanzana, peperone

### Tradurre in nome

Traduzione di parole non parole, costringendo a tradurre con parole imposte un ritmo.

- Considerate la vostra semenza -> Elisabetta-Nicola-Roberta
- fatti non foste -> Luca-Roberto (bisillabo piano e poi baccheo)
- a viver come bruti -> Nicola-Elisabbeta

Dispositivo per costringere a passare ad un altro livello della struttura ritmica metrica.

### Jean Bamberger

Far rappresentare graficamente
___

## Parole chiave

- Piacere di condividere e comunicare
- Cognizione - Affettività
- Contrario del metodo Euristico Modello Induttivo
- Impianto, Contesto Laboratoriale
- Pensare a soluzioni interattive, laboratoriali
- Sapere, Saper fare e saper far fare
- Quattro Aree Performative
- reading-poetico
  - 1) Scegliere dal repertorio
  - 2a) Suggerire dei suoni particolari, degli effetti
  - 2b) Creare dei paesaggi sonori come suggerimento per l'alunno
- sequenza
  -
