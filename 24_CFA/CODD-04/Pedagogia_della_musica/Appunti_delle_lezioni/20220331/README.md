# Appunti della lezione di giovedì 31 marzo 2022

## Riepilogo ed introduzione

Oggi siamo a metà corso.

Sappiamo che il primo punto si basa sui modelli teorici educativi, e tali modelli sono rappresentati sul PDF dal nome traccia lezione 1-4.

Prossimamente dovremo realizzare la fase 2 dei gruppi.

La prima fase del progetto prevedeva: esplorazione, motivazione e coinvolgimento.

La seconda fase del progetto prevede: l'elaborazone, la comprensione e la composizione. La seconda fase prevede di portare avanti degli elaborati musicali e scritti anche vedeendo cosa succede nelle scuole.

Oltre ai modelli teorici, dovremo approfondire questi aspetti per poter raccogliere i modelli, e cambiando modalità di lavoro, senza più ragionare tra di noi, e vedremo una fase osservativa.

- oggi vedremo dei video(scuola elementare e scuola secondaria di primo grado)
- la settimana prossima avremo degli adolescenti (Liceo musicale Santa Rosa di Viterbo)

## I video

### Introduzione

Essi saranno da commentare e osservare.

Oggi sappiamo che vi sono gli istituti comprensivi con (da 3 a 14 anni), curricolo(piccola corsa) del ciclo primario:
- scuola dell'infanzia ed elementare
- scuola secondaria di primo grado (scuola media)

>Altra cosa è il ciclo secondario di istruzione, in cui la musica non c'è ed esiste la possibilità di attivare attività extra-curricolari, ed alcuni licei dedicano l'attività alle attività musicali, con i licei che hanno il coro. Vi è il liceo musicale che è un qualcosa di diverso.

Un dirigente scolastico coordina una serie di concerti che vengono assunti con competenze diverse.

Negli istituti comprensivi vi sono scuole dell'infanzia elementari e medie.

Grazie al DM.8 il docente del ciclo secondario può preparare ed essere utilizzato per il ciclo primario, grazie alla decisione del dirigente scolastico.

Vi sono anche casi più particolari, come un insegnante di sostegno musicista, può andare ad insegnare.

### Primo video: scuola primaria

Il primo video è registrato in una scuola primaria e per insegnare in una scuola primaria, bisogna avere una laurea in scienze della formazione primaria.

Se si è in grado di proporre un progetto per la scuola primaria si può essere utilizzati per la scuola primaria (anche se assunti per la scuola secondaria).

Il video è realizzato dalla regione Piemonte, con una delle maestre della scuola primaria, che ha ottenuto un invito dal dirigente a attuare elementi di musica nella scuola primaria.

#### Cosa ci interessa osservare nel video?

- dobbiamo capire cosa faccia l'insegnante?
- cosa fanno invece i bambini?

Quando l'insegnante insegna e gli altri guardano, osservano e non fanno qualcosa di pratico, allora essa si chiama __didattica frontale__.

Ci interessa sapere se c'è una varietà di mettere in relazione il corpo e la mente (oggi qualunque neurologo direbbe che è importante il bodymind), nel caso dell'apprendimento, bisogna mettere in moto il corpo per mezzo della mente.

Vedremo come l'azione didattica coinvolge in azioni diverse gli eventi.

>Spesso l'insegnante non si vede nel video, ma anche se non si vede l'insegnante si può indurre l'azione dell'ambiente di apprendimento che l'insegnante ha previsto.

L'ambiente di apprendimento deve essere progettato in un modo preciso anche in base al tempo a disposizione, e la consegna che si da, da una forma al gruppo al tempo ed allo spazio.

Anche se l'insegnante non si vede, si possono ricavare delle consegne che il gruppo ha pensato.

>La tabella serve per costringere a fare acquisizioni in più, per costringere a fare acquisizione.

#### Le 3 fasi del progetto

Il progetto ha sempre 3 fasi:
1. cosa si inventa l'insegnate per motivare e per coinvolgere, quali sono le azioni che mettono in moto il progetto, un qualcosa che abbia a che fare con la motivazione, la curiosità, bisogna impegnare gli alunni a motivarsi, inventandosi qualche cosa, come una modalità per avviare gli studenti, con qualche dispositivo che intrighi con questi contenuti
2. vari modi di articolazione del progetto, in quali ambiti avviene il progetto
3. è importante che ogni progetto, preveda cosa si è imparato, ovvero una fase che esprima il piacere della comunicazione, che è molto più di un saggio; alla fine di un percorso di apprendimento, prevedere un'esposizione, ovvero un evento che ci motivi ad andare avanti, con il piacere dell'apprendimento (con quali strumenti viene condiviso il progetto):
 - libro bianco
 - lezione aperta
 - evento in piazza

#### Indicatori di qualità del progetto

Il progetto presentato vale?

Bisogna trovare quale sia l'indicatore di qualità che permette di dare valore al progetto.

Vedendo un progetto ci chiediamo, su cosa si è lavorato? Su cosa la musica ha senso?

#### Competenze trasversali sviluppate

Sappiamo che il far musica, coinvolgersi come gruppo, etc... Ha delle competenze comunicative, sappiamo che il parlamento di Strasburgo dell'Unione Europea.

Esse sono [8 competenze chiave per l'apprendimento permanente](https://www.orizzontescuola.it/competenze-chiave-per-lapprendimento-permanente-orizzonte-di-riferimento-per-tutti-gli-insegnanti-precisazioni/).

Bisogna diventare consapevoli della propria cultura, facendo proprio qualcosa.

#### Competenze musicali sviluppate

Qui entriamo in una questione complessa difficile da analizzare, ma l'obiettivo vasto, come può essere articolato in modo da poterlo gestire e realizzare?

Vi sono dei gradi intermedi per progettare le competenze musicale, e nel [portale musicascuola](https://musicascuola.indire.it/quadro_didattico.pdf), abbiamo uno specchio di ciò che avvenga nelle squadre italiane oggi, in cui vi è un quadro didattico che spiega meglio le voci della competenza musicale, con le macro-aree specifiche.

##### Ascolto
Ad esempio aevamo indicato l'ascolto come:
- ascolto come ear-training
- dare senso allo stimolo uditivo
- ascolto come comprensione
- ascolto come analisi

Possiamo sviluppare in un progetto per l'ascolto solo alcuni dei punti chiave.

Gli ascolti qui descritti sono molto legati fra loro, ma è importante individuare un'area.

>È tutto legato, ma molto diverso...

##### Produzione

Essa è una macro-competenza e ci caratterizza, parafrasando anche i colleghi che insegnano lingue.

È importante:
- produrre come comporre
- produrre come improvvisazione
- produrre come esecuzione

>I musicisti che fanno musica si proietta nelle capacità dell'interprete e del compositore che si proietta negli ambiti di: esecuzione, improvvisazione e composizione.

Ci si chiede come il progetto attivi la sezione della produzione, indicando verbo e complemento oggetto.

##### Letto-scrittura

Questa area è importante come dice Vigotskij, e deve lasciare segni ed il pensiero musicale si deve sviluppare e tuttal'più si può scegliere di lasciare tracce analogiche o non convenzionali.

È importante che il progetto lasci delle tracce scritte.

Nel progetto a qualche livello vi sono sviluppi delle competenze della letto-scrittura.

___
In Italia per insegnare nella scuola primaria bisogna seguire il corso di Scienze della formazione.

Siamo figli di Cicerone e l'importante è creare l'istituto giuridico, vuol dire che nelle indicazioni nazionali per il curricolo, c'è musica, ma il problema sarà: chi deve insegnare la musica nella scuola primaria?

Si sono fusi gli istituti comprensivi perchè se vi è un insegnante di musica per la secondaria di primo grado esso viene chiamato anche nella scuola primaria.

>In questo momento il modo più veloce per entrare nell'istruzione è realizzare un corso per insegnanti di sostegno.
___

Vedremo dei laboratori dell'insegnante di sostegno che si mettono a lavorare con l'alunno problematico, insieme agli altri ragazzi, in una modalità laboratoriale che si può mettere a lavorare in qualche modo.

Un'insegnante di sostegno musicista è una risorsa per la scuola.

*__Visione del [video](https://www.youtube.com/watch?v=n7ZLFgYSYsQ)__* dalla pagina della [Scuola IC Pino Torinese](http://pinoscuola.edu.it/index.php/indirizzo-musicale/191-video)

Il monile peruviano è preso dal testo: Musica dal corpo. Percorsi didattici con la body percussion di di Alberto Conrado e Ciro Paduano

___

## Con il gruppo

[file realizzato con il gruppo](https://docs.google.com/document/d/1sNStFR6OG8uagh2dWSne46c4kFfecV8y/edit#heading=h.lkkxtgu7hivc)

## Analisi del lavoro realizzato con il gruppo

### Gruppo 9

#### Fase 1

Tattica di problem solving, con un immagine si scatena il problem solving.

#### Fase 2

- esecuzione in classe
- realizzazione di un labirinto
- concertare

#### Fase 3


### Altri commenti

Riconoscimento di ciò che sia una partitura o meno.

Diversi stimoli grafici proposti dall'insegnante:
- visualizzazione con oggetti di vita quotidiana
- visualizzazione [MusAnim](https://www.musanim.com/) (Malinowski) -> schema grafico diverso dando un senso musicale ad un oggetto

>Fare un progetto didattico è un'organizzazione molto chiara.

- Interpretare l'immagine (Vedere dei segni e pensare dei suoni)
- ascolto
- ricomporre l'immagine
- realizzazione con parti ritmiche sovrapposte

>È abbastanza facile guidare il percorso nello spazio ritmico, mentre l'immaginazione melodica richiede altre forme di immaginazione e di modi di funzionamento del cervello.

#### Competenze trasversali sviluppate
Imparare ad imparare, si realizza per mezzo dell'utilizzo di una forma, riuscendo a trasferire il concetto formale a diversi ambiti esperienziali.

>l'apprendimento non è a compartimenti stagni.

Utilizzare uno stimolo culturale per generare nuovi elementi culturali, stando dentro la cultura in un modo generativo.

I bambini imparano e svolgono il compito di dare un senso sequenziale ad un oggetto grafico ed attraverso una serie di esperienze si capisce che un oggetto può essere ripensato attraverso diverse discipline e l'interdisciplinarietà è un approccio che si impara, dando molte applicazioni disciplinarie.

Nell'età del pensiero concreto (da 3 a 8 anni) si cerca di comprendere come si impara ad imparare.

### Indicazioni per le competenze musicali sviluppate

Un progetto è un'unità di apprendimento (UDA) e si possono sviluppare in parte delle competenze.

## Considerazioni finali

>I bambini imparano a lavorare insieme come vedono i maestri che lavorano insieme.

L'insegnante realizza 3 modelli diversi nel gruppo:
- realizziamo tutti insieme
- realizziamo qualcosa a gruppo
- realizziamo qualcosa con ciascuno la sua entrata all'interno del progetto

---

## I criteri di valutazione?

Nella griglia proposta non mancano i criteri di valutazione?

Si hanno nella scuola primaria, 4 livelli di valutazione con cui osservare l'azione:
- 0 -> si rifiuta o non fa nulla
- 1 -> ci riesce con l'aiuto di qualcuno
- 2 -> ci riesce da solo
- 3 -> ci riesce e arriva con uno strumento suo o un'altra sua idea, ovvero un contributo personale originale

>Si invita a valutare il compito con chiarificazione di azioni.

## Schema

- valutazione dell'ascolto
- valutazione della lettura
- valutazione della composizione ed esecuzione di tasselli geometrici colorati con un gruppo di 3 strumenti(3 timbri diversi)

## Scuola senza voti

[Davide Tamagnini](https://www.youtube.com/watch?v=_hPyqCKmTqI), la valutazione con colori.

La realizzazione di un'autovalutazione è utile per far capire all'allievo.

Esiste anche la Docimologia, ovvero la scienza della valutazione.

I traguardi di competenza spiegati nelle indiczioni nazionali del curriculum serviranno per valutare.

___

Dalla prossima volta potremo inserire una tabella di valutazione e sarà difficile valutare quando non si sa cosa valutare, bisogna quindi avere degli indicatori di qualità.

>Se si vuole, sistemare lo [schema](https://docs.google.com/document/d/1sNStFR6OG8uagh2dWSne46c4kFfecV8y/edit#heading=h.lkkxtgu7hivc) realizzato per la prossima volta!
