# Appunti della lezione di giovedì 24 marzo 2022

## Riepilogo delle lezioni fino ad ora

Abbiamo preso vari aspetti della musica per poter entrare nella musica:
- ritmo
- melodia
- timbro/sound
- forma
- (armonia)

![1](1.png)

Stiamo arrivando alla fase 2 che mette in gioco la parte di rielaborazione.

Da un lato abbiamo una dimensione euristica: "imparare la musica per mezzo di un laboratorio"
- il laboratorio come spazio per accogliere e valorizzare ambienti e spazi diversi
- laboratorio per far acquisire competenze musicali


>Il problema del laboratorio è che spesso nelle scuole italiane non esiste, e il laboratorio esiste solo se un insegnante è in classe da 3/4 anni. Solo dopo un tot di tempo si riesce ad arredare un luogo.

Dobbiamo quindi arredare un laboratorio, ipotizzando la partenza di un corso da delle terzine di Dante, quindi portando un elemento, un'idea musicologica per il laboratorio.

![2](2.png)

Ogni volta si cercano di realizzare delle zommate verso alcuni aspetti.

>Per delle lezioni di musica bisogna realizzare delle lezioni in forma laboratoriale per cercare ed esplorare, lavorando insieme coinvolgendosi nell'obbietivo di apprendimento.

Sui 3 versi danteschi lavoriamo mirando ad elementi o aspetti diversi:
- timbro o sound divisi in due fasi ![3](3.png)

__Bisogna organizzare in modo induttivo, ovvero laboratoriale, cercando di far emergere un aspetto del castello__, realizzando un obiettivo mirato e consapevole.

Provare in una classe di 25 alunni divisi in gruppi di 4/5 ragazzi massimo con un compito mirato, chiedendo di lavorare con un testo mirato.

Il laboratorio servirà comunque
- perchè uno dei gruppi sicuramente non realizzerà niente perchè avrà discusso
- uno avrà prodotto molto

>Tutti quanti i gruppi avranno comunque fatto qualcosa.

Vi saranno quindi aspetti diversi:
- cercare di fare qualcosa a gruppi porta a chiedersi cosa fare una seconda volta
- gli altri gruppi ascoltano i gruppi

È importante lavorare in team.

### Le fasi

#### Fase 1

È importante che ascoltando i vari gruppi l'insegnante ponga l'attenzione su un dispositivo trasferibile che il gruppo è riuscito a realizzare con idea organizzativa del gruppo e delle idee.

#### Fase 2

Essa servirà invece per elaborare.

### Cosa sono le intelligenze ritmiche e melodiche?

Da bambini sono dei ricordi, da adulti esse saranno relazionate ad elaborazioni le intelligenze, come affrontato nel corso di Psicologia della musica.

E-ducere:
- spronare a far qualcosa
- spingere successivamente a fare qualcos'altro.

### Gruppi 7,8,9

![4](4.png)

#### Gruppo 7

##### Fase 1

Dato un gruppo di persone, persone diverse realizzano un qualcosa di diverso ad esempio:
- prima persona del gruppo realizza un discorso sulla prosodia (guardare l'etimologia)
- seconda persona realizza una scala per terze
- cantare la scala con tutti i suoni

##### Fase 2

1. trasferire lo schema ad altre scale
2. dopo aver intuito che la scala è uno degli scheletri possibili per dare forma ad una melodia, cercare melodie di repertorio costruite nello stesso modo, in cui la scala faccia da ossatura della melodia -> cantando seguendo la grammatica scalare
3. cantando le melodie con i nomi delle note
4. utilizzare anche la chironomia registrando dei video con i gesti delle mani per i nomi delle note, rendendo consapevoli per mezzo dei gesti
5. tappetini note spostandosi da un tappetino al lato
6. cantare canzoni che già i ragazzi conoscono


#### Gruppo 8

##### Fase 1

Far sviluppare l'intelligenza melodica facendo cantare.

Il prof inventa una melodia e per far ricordare una terzina di Dante, il prof inventa una melodia.

Per variare l'interesse accompagno ogni volta la terzina in stili diversi.

##### Fase 2

La prof canta e la pronuncia ritmica della melodia è la stessa dell'accompanamento.

Far sentire i punti d'appoggio armonico, fermando il canto subito prima delle note d'appoggio.

Cercando l'appoggio armonico con una melodia bucata.

- preparare e registrare diverse melodie interrotte su diversi schemi ritmici

>Il dispositivo sarà la melodia interrotta.

[Robert Frances scrisse la "Perception de la musique"](https://books.google.it/books/about/La_perception_de_la_musique.html?id=vu17V-73ezkC&redir_esc=y) spiegando cosa sia il senso tonale ("Perception du ritme" altro libro), capacità di riconoscere forme chiuse e non aperte, chiudere con una nota casa.

L'orecchio ha un senso tonale per poter andare a chiudere.

Prosodica che ritorna con la modalità di partenza.

Software [praat](https://www.fon.hum.uva.nl/praat/).

L'orecchio è formato per un bambino già -3 mesi.

- organizzare dei piccolo ocetus realizzando una collaborazione, ovvero organizzando un duetto coinvolgendo e suonando con il ragazzo per obbligare ad:
  - ascoltare
  - ascoltarsi

#### Gruppo 9

##### Fase 1
Invenzione melodica, intonando cercando una melodia che abbia un mood.

Partendo da un gioco di intonazione libera, si lasci, ci si liberi.


##### Fase 2

- Ruotare nel ruolo di cantore del testo.
- avviare realizzazioni melodiche in stili diversi

Vi sono stadi diversi:
- rituale
- da stadio
- infantile
- lirico
- cerimoniale solenne

>Se possibile cercare i ["Chants d'Italie"](https://www.placedeslibraires.fr/livre/9782914147552-chants-d-italie-serena-facci-gabriella-santini/) pubblicato dalla Citè de la musique.
___

## Collegamento a Bruner e preparazione alla fase simbolica
![5](5.png)

Per la rappresentazione di codici melodici bisogna passare per 3 standi:
1. cantare con una fase di esperienze
2. fase iconica -> contorno analogico per rievocare alla mente delle melodie, ad esempio con segni come dei neumi; utilizzo della chironomia per essere costretti a ricavare lo schema della melodia, non solo cantando la melodia
3. fase simbolica -> intonazione per lettura, cantare melodie facili cantando i nomi delle note
___

## Gruppi 10, 11, 12

Intelligenza logico-formale, lavorare sulla forma, come musicisti, a qualsiasi livello, significa mettersi in una mission psico-evolutiva del pensiero umano specifica.

Il pensiero concreto ha suddivisioni di esso, ma il momento di un qualcosa che riguarda solo forme astratte, con un passaggio logico formale che parte da 8/9 anni e arriva a 15/16 anni, lavorando con le forme e comporre forme.

>Ad esempio dall'ultimo stadio, con la costruzione, realizziamo astrazioni che gli sono consentite dalla manipolazione, quindi dei livelli di pensiero concreto, ma viene un

Al pensiero logico formale si arrivano a riconoscere delle forme trasferibili.

### Gruppo 10

#### Fase 1

Impressione di una forma chiusa.

La parola Gestalt vuol dire forma, essa è una corrente psicologica che indica che si può iniziare ad agire sull'esperienza agendo sulle forme.

- quali sono le prime forme?
- cosa ci fa capire che abbiamo una forma chiusa?

>Tra gli schemi gestaltici vi è anche lo schema della buona musica!

La forma è definita dai limiti, è importante riconoscere l'inizio e la fine.

L'incipit e la cadenza sono molte chiari.

Abbiamo ascoltato accenti, scale, direzioni melodiche.

>Nel melodizzare occidentale da mille anni ad oggi, gli scheletri accordali (da Rameau in poi) e scalari sono impianti scalari che danno forma alla melodia, arrivando sulla tonica si chiude la melodia.

In tante metodolgie, si fa cantare facendo capire è il gioco della melodia interrotta.
#### Fase 2

Realizzare melodie in la minore per poterci suonare. Facendo scoprire lo scheletro scalare

Confrontare degli elementi diversi.

(bando di laboratorio di songwriting su un problema che coinvolge la comunità)

### Gruppo 11

![6](6.png)

#### Fase 1

Emilio ha costruito un'introduzione e una forma strumentale.

Si possono cantare tutte le terzine, subito dopo aver realizzato uno schema ed aver posto in una forma.

Si è utilizzato uno stampo armonico ripetendo dei versi, senza ripeter l'ultimo verso che abbia una differenza.

Si può utilizzare quindi uno stampo stilistico e formale del tutto conosciuto.

Si ha uno schema facile da condividere.

#### Fase 2

Cercare di trascrivere la melodia, trasferendola ad altri ruoli esecutivi.

Con la realizzazione di flashcards e poi di partiture.

>Il meccanismo compositivo più usato al mondo è la parodia.

##### Concetto di concertare

Il senso della forma dal punto di vista musicale è legato al concertare. Distribuire le parti.

Piacere di concertare ruoli diversi, suddividendo le parti affidandogli ruoli diversi.

Realizzazione di un diagramma di flusso che rappresenta uno schema temporale che raffigura una forma.

Il piacere del lavorare sulla forma, ovvero l'organizzazione del tempo lavorando sul materiale.

Partendo da un materiale dato, dare al materiale, una forma che organizza il tempo con una logica.

### Gruppo 12

#### Fase 1

Realizzazione visiva e richiesta di realizzazione di scatoline del brano con schemi logici formali.

Ritrovare brani conosciuti in forma ternaria.

Evidenziare dei blocchi che ritornano e che vanno per contrasto.

Idea di porre attenzione alla rima, realizzando un'attenzione diversa a fasi della forma diverse.

Il primo schema formale che impariamo a descrivere è il contrasto che dura per tutta la scuola dell'infanzia.

Leggere la terzina come A-B-A, gestire una lettura con uno schema di contrasto.

Attenzione all'effetto sonoro e per la realizzazione estemporanea.

>Interessante è l'analogia tra forma temporale e forma visiva.

Si possono costruire collage di paesaggi sonori che hanno componenti differenti.

#### Fase 2

Si possono anche cercare delle forme ternarie come questa celebre immagine (trasfigurazione di Raffaello con l'articolazione del dipinto in 3 livelli):
![7](7.png)

- flagellazione di Piero della Francesca
- uovo di Piero della Francesca -> la pala di Brera ![8](8.png)
- enigma dell'arrivo di Chirico

>Lettura della forma in una quantità di situazioni e linguaggi e più si capisce e più si generano esecuzioni chiare.

Paul Klee - Teoria della forma e della figurazione

Dare forma ad un gruppo, ovvero organizzare dei ruoli e delle disposizioni, organizzando uno spazio scenico.

Dare un ruolo a sezioni di un gruppo, cosa bisogna fare?

Cosa deve fare un gruppo? Che forma deve avere il gruppo?

Organizzare il gruppo in funzione della forma.

Realizzare degli schemi per capire dove siamo e perchè.

>La forma dello spazio, del tempo e dei contenuti sono intrecciate.

La forma è connessa ai ruoli.

>Quanti ruoli si devono dare.

___

Dalla prossima lezione vedremo dei progetti di altri insegnanti.
