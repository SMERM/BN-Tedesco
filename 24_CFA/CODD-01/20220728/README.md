# Appunti della lezione di giovedì 28 luglio 2022

## Progettazione
Cosa faremo oggi:
- Progettualità che include percorsi corali/strumentali.
- Esposizione delle consegne per il lavoro di gruppo

### Introduzione ai progetti

Essi hanno la stessa traccia ma realizzano qualcosa di diverso.
Avremo documentazione di un progetto.

>Parlando di minori, servono le liberatorie per la presentazione di progetti con video o audio.

Osserveremo due progetti diversi fra loro, uno didattico ed uno artistico.
Con un progetto artistico non si deve dar conto di cosa si fa, ma si ha qualcosa di stampo politico.

Proponendo delle cose non dovremo avere la pretesa che siamo i primi a fare qualcosa, ma bisognerebbe dare informazioni sullo stato dell'arte di dove e come si è.

__Bisogna sottolineare la nostra progettualità__ avendo la cognizione delle ipotesi con i vantaggi ed i limiti che si hanno. Ovviamente siamo in una scia di docenti che apportano novità e vogliono trasformare e sottolineare novità.

>Non considerare ogni testo come sacro, bisogna critacare ed eventualmente indicare i punti di forza del progetto che stiamo realizzando.

### Distinguere bambini dai ragazzi

Essi hanno età evolutive diverse, e i progetti devono avere target ben identificati.

#### Struttura della scuola italiana
Sappiamo che nella scuola abbiamo:
- primo ciclo (dirigente scolastico)
  - scuola primaria
  - scuola secondaria di primo grado
- secondo ciclo
  - secondaria di secondo grado

### Come cercare bandi

Cercare su siti dei conservatori specifici.

### Esempi di progetti

#### Lettura dell'articolo di Maria Grazia Bellia sul progetto _COROSCENICO : APPRENDIMENTO COOPERATIVO DEL CANTO CORALE_

Il brano utilizzato è di Visioli.

Punti chiave:
- creazione dei cori che rappresentassero l'istituto di appartenenza.
- ben differenziare bambini e ragazzi

>È importante la scelta di:
- target
- tempi (con suddivisione trimestrale, semestrale, annuale)

Comprensione della convenzione del fanciullo per i diritti dei minori (non firmata da USA e Somalia).

Il coro deve essere un diritto e non deve essere un qualcosa di obbligatorio.

##### Il coroscenico
- utilizzo dello spazio con il coro
- intervento di rielaborazione
- progetto con gruppo

##### Struttura del progetto

- ricerca delle parole chiave
- ricerca di suoni per le parole chiave
- costruzione di un paesaggio sonoro (luogo come il mercato)

#### Progetto per il _Rinascimento napoletano_ con la seconda legislatura Bassolino

Opera per le feste natalizie, di Rebecca Horn legata alla musica:

![rebecca-horn-il_culto_delle_anime_del_purgatorio.jpg](rebecca-horn-il_culto_delle_anime_del_purgatorio.jpg)

![Spirit_di_Madreperla-rebecca-horn.jpg](Spirit_di_Madreperla-rebecca-horn.jpg)

Musicista neozelandese Daniel Schiscolm...

Scuola Custra a Certola.

Fiabe ironiche:
- melodie con testo realizzato a casa (con musica di Kodaly)
- cluster di varie persone

### Lavoro dei gruppi

5 gruppi:
- 3 gruppi da 4 persone
- 2 gruppi da 3 persone

#### Il progetto

- progetto da svolgere annualmente
- nome di un progetto
- titolo didascalico
- responsabile di un progetto
- descrizione del progetto
- cosa si propone
- obiettivi culturali
- contenuti
  - aggiungere metodologie sui contenuti
- durata (arco temporale e fasi)
- specificare i profili necessari
- numero di alunni e le classi coinvolte (progetto sostanzioso con collaborazione di molti ragazzi)
- beni e servizi  (a cosa servono?)
- verifiche e valutazioni, a medio e lungo termine (abbandono dei ragazzi, verifiche qualitative)
  - quali competenze, con quali capacità
  - aspetti quantitativi
- griglie per la valutazione
- collegamento con altri enti e/o istituzioni


---
Fine prima parte
---

## Creazione e presentazione dei progetti

### Progetti presentazioni

1. Musecal
2. Paesi sonori
3. Stornelli in piazza
4. Pierino e il coro
5. Fiabazione
