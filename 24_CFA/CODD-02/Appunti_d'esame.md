# Appunti d'esame

Tipologie di tecniche compositive affrontate:

- tecniche polifoniche
	- voci come linee orizzontali organizzate
	- voci indipendenti fra loro
	- compare nel manuale di Joseph Fux del 1715 (sistematizzazione degli eventi che danno luogo alla partitura musicale)
	- indicazioni della teoria desunte dalla pratica
	- _contrappunto_ (testo di riferimento Salzer e Schachter)
	- utilizzo del _cantus firmus_, con i suoi criteri costruttivi (usato per essere trasformato e contrappuntato)
		- direzione (inzio, culmine, fine)
		- continuità
		- varietà
		- equilibrio
		- completezza
	- imitazione fra parti, tali imitazioni appaiono anche come:
		- aggravazioni
		- diminuzioni
		- retrogradi
		- inversi		
- tecniche omofoniche
	- voci non indipendenti fra loro
	- vi è una sola voce con le altre di contorno ad essa(le altre voci fanno da accompagnamento)
	- forma sonata(presenza di un tema che si rapporta a tutta la composizione)
		- forma ternaria -> A-B-A'
- tecniche del XX secolo
	- Satie e le Vexations
	- composizione come attenzione al suono -> le composizioni di Cage
	- tecniche estese per lo strumento -> scrittura di Henry Cowell per Pianoforte
	- comporre con l'alea -> scrittura di Maderna nella Serenata per un satellite
	- comporre con la notazione analogica(differenza tra notazione astratta e notazione analogica) -> Volumina di Ligeti

Tecniche compositive affrontate:

- contrappunto
	- incontro fra le note
	- sequenza orizzontale di note (conseguente sovrapposizione di linee melodiche con realizzazione di intervalli armonici)
	- gradi di stabilità di un intervallo (verticale) rispetto ad un altro
		- In ordine per stabilità degli intervalli avremo:

			1. unisono
			2. ottava
			3. quinta
			4. terza (intervallo dinamico)
			5. sesta (intervallo fortemente dinamico)
	- nota contro nota
	- sistematizzazione delle durate in 4/5 tipologie, definite _specie_ (tale sistematizzazione è usata per portare l'allievo gradualemente alla consapevolezza del contrappunto, circoscrivendo l'intervento didattico)
		- prima specie -> due note su voci diverse hanno la stessa durata
		- seconda specie -> due note su voci diverse hanno uno la durata dimezzata rispetto all'altra - due note contro una
		- terza specie -> due note su voci diverse hanno uno la durata il quadruplo rispetto all'altra - due note contro una
		- quarta specie -> due voci si rapportano fra loro per mezzo di un comportamento di sincope
		- quinta specie -> utilizo di tutte quante le specie (mescolanza)
	- moto delle parti
		- moto parallelo (quinte e ottave parallele possono offscare l'indipendenza delle voci)
		- moto simile
		- moto obliquo
		- moto contrario
- dodecafonia
	- metodo di comporre con dodici suoni
	- ricerca nel campo dell'organizzazione dei suoni
	- utilizzo di una serie dodecafonica con il totale cromatico dei dodici suoni(tali utilizzi delle serie sono tratti dall'imitazione fra parti dello stile classico)
		- serie originale
		- serie retrograda
		- serie inversa
		- serie inversa retrograda
	- osserviamo che si dà molta importanza alla dinamica
	- il metodo dodecafonico tratta la serializzazione delle sole altezze, rimanendo carente nella trattazione di altri profili musicali
- serialismo
	- metodo di comporre che sfrutta la serializzazione di ogni parametro musicale
	- Messiaen e la serializzazione di ogni parametro
	- Boulez e la critica a Schönberg
	- Boulez e la ricerca della perfezione nel serialismo
	- assenza di equivoci tematici o melodici


Temi trattati:

- comporre → mettere insieme(mettere assieme eventi sonori)
- indicazioni nazionali
	- D.M. 176/2022
	- D.M. 254/2012
	- Indicazioni nazionali riguardanti gli obiettivi specifici di apprendimento concernenti le attività e gli insegnamenti compresi nei piani degli studi previsti per i percorsi liceali
- spostare conoscenza sugli altri
- far fare la musica e non farla solo ascoltare ai ragazzi
- a cosa serve l’arte
- puntare all'armonizzazione della filiera musicale
- partire dal materiale sonoro conosciuto dall'allievo
- organizzazione formale della musica(necessaria con l'avvento della musica puramente strumentale, perchè nella musica vocale era il testo che dettava la forma)
	- suddivisione della forma
		- inizio
		- sviluppo
		- fine(chiusa) _in genere quinto grado seguito dal primo grado_
	- descrizione dell'organizzazione formale della musica(dal testo di Schönberg - Elementi di composizione musicale
		- proposizione(durata di due battute) _in genere sul primo grado_
		- tema(durata convenzionale di 8 battute, quindi quattro proposizioni)
			- esso è distinguibile in:
				- frase: proposizione, contrasto, ripetizione identica, chiusa
				- periodo: proposizione, ripetizione, contrasto, chiusa
			- i concetti fondamentali(simili al contrappunto) sono
				- ripetizione _in genere sul quinto grado_
					- ripetizione identica
					- ripetizione variata (ripetzione con un solo parametro modificato)
					- contrasto (ripetizione con più parametri modificati simultaneamente) _in genere sul primo grado_ e realizzato generalmente sul piano armonico
				- periodo
- cosa significa comporre oggi
	- Vexations di Satie
	- ASLSP di Cage
	- 4'33" di Cage (provocazione al modo di fruire e affrontare la musica, con un'operazione concettuale.)
- educazione musicale -> Trarre fuori, quello che lo studente ha dentro di se, per valorizzare, indagare e sviluppare quello che egli ha al suo interno.
	- spostare conoscenza sugli altri
- che cos'è la musica?
- nell'arte l'importanza dell'idea è superiore a ciò che l'idea realizza
- a cosa serve l'arte?

Idee originali da trattare:

- le tecniche compositive ed analitiche come elementi per fornire una comprensione maggiore agli allievi dei brani suonati nella musica individualmente e nella musica d'insieme
- suddivisione in cinque famiglie e spiegazione delle tecniche polifoniche in base a tale suddivisione strumentale
- Cantus Firmus come elemento su cui basare la comprensione della musica per i bambini (far comprendere ai ragazzi cosa sia la musica e la composizione musicale per mezzo della creazione di un Cantus Firmus) per la scuola dell'infanzia, la scuola primaria e la scuola secondaria di primo grado -> utilizzo di notazione analogica(più diretta ed interpretabile dai ragazzi di età inferiore)
- Contrappunto come elemento come elemento su cui basare la comprensione della musica per i ragazzi (far comprendere cosa sia la musica e la composizione musicale per mezzo della creazione di uno o più contrappunti) per la scuola secondaria di secondo grado -> il massimo dello sviluppo porta ad un pensiero astratto -> utilizzo di notazione astratta(astrarre significa capire senza vedere)
