# Appunti della lezione di lunedì 12 settembre 2022

## Argomenti della scorsa lezione

Lettura e commento del decreto 382.

## Indicazioni nazionali

Guardare l'allegato E, per le indicazioni della sezione musicale pagina 304.

>Importante è l'autonoma capacità di autovalutazione.

## Obiettivi specifici del corso di "Elementi di composizione per la didattica"

Avevamo osservato le tecniche compositive:
- polifoniche
- monofoniche
- del XX e XXI secolo

Il docente deve conoscere tali tecniche ma non deve per forza di cose esercitarle.

>Dobbiamo noi conoscere gli elementi fondamentali per poter vedere se riusciamo a combinare un'azione didattica utilizzando tale conoscenza.

Di tutte queste tecniche ne esiste una manualistica, i testi teorici seguono l'osservazione della pratica ed il teorico scriveva il manuale dopo aver osservato praticato, quindi il teorico dopo l'osservazione del repertorio mette insieme una serie di regole che dovrebbero aiutare lo studente a creare una composizione nello stile e nella tecnica di un periodo.

### Tecnica polifonica

Tale tecnica si basa sullo studio del contrappunto.

Il primo manuale per il contrappunto compare nel 1715, ovvero in un periodo storico in cui la tecnica del contrappunto era arrivata alla sua massima espressione (considerando che Bach morì nel 1750).
Nel manuale le indicazioni comportamentali sono desunte dalla scrittura e tale manuale fu scritto da Joseph Fux.

Tale primo manuale è intitolato _Gradus ad Parnassum_.

>Tale manuale è importante, perchè esso è l'occasione in cui per la prima volta si sistematizzano gli eventi che danno luogo in una partitura musicale.

#### Il significato di contrappunto

_Punctum_ definiva la nota, e contra punctum indicava, nota contro, abbreviazione di _incontro_ con nota che icontra un'altra nota. Tale incontro fra le note presuppone che le note siano inserite in una sequenza orizzontale, con l'incontro di una nota con una sequenza orizzontale con un'altra nota di una sequenza orizzontale.

Le note vengono sistematizzate nel momento in cui due o più linee orizzontali si sovrappongono l'una sull'altra, il contrappunto è infatti un pensiero orizzontale.

Si presuppongono linee orizzontali che stanno le une sulle altre e si sovrappongo, e nel contrappunto si indica questa sovrapposizione con la frase _nota contro nota_.

#### Sistematizzazione delle durate

Il modo di incontrarsi fra le note è sistematizzato con riferimento alle durate, e la nota di una voce si rapporta con la nota di un'altra voce e la sistematizzazione è il rapporto di una nota di una voce, con la nota di un'altra voce.

Fux ha sistematizzato le durate in 4/5 tipologie, e tali tipologie sono definite da Fux in __specie__.

Prendendo un qualunque brano di repertorio potremo ricondurre il rapporto di una voce con l'altra secondo una delle quattro specie che Fux ha individuato, e questa sistematizzazione ce la siamo portata avanti sino ad ora, ed i nostri studenti che studiano il contrappunto la studiano ancora oggi.

>La sistematizzazione viene definita per circoscrivere l'intervento didattico ad una gradualità, per portare l'allievo alla padronanza della tecnica per mezzo di processi graduali.

### Specie

#### Rapporto di prima specie

Esso prevede che le __due note__ abbiano la __stessa durata__, avendo due voci che procedo insieme ed hanno la stessa durata, si avrà un rapporto di prima specie perchè la durata delle due è uguale.

Questo rapporto di prima specie può essere a 2 o più voci.

Quando tre voci si muovo tra loro con la stessa durata, tra loro vi è la prima specie.

>Gli esempi sono stati improvvisati nel sistema armonico tonale. Ed un rapporto che si fonda sulle durate prescinde dal sistema armonico. Possiamo rapportare in prima specie qualunque nota con qualunque altra nota, purchè il rapporto sia regolato dalla durata.

Abbiamo modo di vedere che i manuali prevedono delle esercitazioni basate sull'uso del sistema armonico-tonale, che prevede la conoscenza da parte dello studente del sistema, ma limitando lo studio alle sole durate, potremo azzardare un'attività didattica che sovrappone qualsiasi nota ad un'altra purchè esse abbiano la stessa durata.

>Il ragionamento dell'applicazione delle specie può esser realizzato anche con suoni senza altezza, basta che essi abbiano stessa durata. L'applicazione delle specie oltre il sistema armonico tonale deve fornire un suggerimento importante.

#### Rapporto di seconda specie

Tale rapporto prevede __due note contro una__.

Ad una nota al basso ad esempio ne corrispondono due che sono sulla parte superiore o inferiore.

#### Rapporto di terza specie

Tale rapporto prevede __quattro note contro una__ (anche tre contro uno).

Considerando che qualunque voce possa prendere la frammentazione di durata richiesta dalla specie.

#### Rapporto di quarta specie

Tale rapporto prevede __due voci si rapportano tra loro per mezzo di un procedimento di sincope__, una risuona successivamente all'altra.

Una voce si muove in sincope rispetto all'altra.

#### Rapporto di quinta specie

Tale rapporto prevede l'__utilizzo di tutte quante le specie__ ed in essa vengono ad aggiungersi note più veloci, ornamenti ed abbellimenti che precedentemente non erano utilizzate (_contrappunto fiorito_).

### Il metodo di scrittura con le specie

Il metodo prevede la scrittura con le singole specie per poi passare alla mescolanza delle varie specie.

### Il testo di riferimento per il contrappunto

Su questo argomento utilizziamo il testo di Salzer e Schachter _Contrappunto e composizione ridotto_.

Alcuni metodi utilizzati sono quelli di Schanker (autore dell'_Analisi Schenkeriana_), di cui gli autori del libro sono allievi.

>Gli argomenti affrontati oggi li troviamo nel testo.

Il testo è completo e scorrevole nella lettura e si distingue dai soli testi di contrappunto che sono in genere un'elencazione di regole, mentre il testo è discorsivo e le regole tenta sempre di motivarle, spiegando proibizioni ed eccezioni delle regole.

>Nulla è per caso...

### Considerazioni sul contrappunto

Notare che i manuali dell'Ottocento prevedono uno studio e una comprensione dell'armonia anche se ciò è contrasto con il decorso storico, in cui venne prima il contrappunto e poi l'armonia.

Le prime altezze utilizzate erano quelle degli armonici.

### Ascolto dell'Organum duplum sobre "Immola" di Leonino

[Link al video](https://www.youtube.com/watch?v=2tM8KIKnYqY)

### Ascolto Organum duplum sobre «Alleluia pascha no[-strum]» di Leonino

[Link al video](https://www.youtube.com/watch?v=8DS4wWWKa9Y&ab_channel=M%C3%BAsicasHist%C3%B3ricas)

I melismi sono quelli che il testo citato prima, indica come ornamenti.

Vediamo criteri di durata e intervalli con consonanze(ovvero dei suoni armonici):
![1](1.png)

Le voci si rapportano tra loro secondo consonanze e questi contrappunti.

### Il cantus firmus

Essa è una voce che viene resa ferma, e nell'essere reso fermo perde le sue caratteristiche melodiche ed è strumentale ad essere contrappuntanto. Un canto si ferma allungando i valori di ogni singolo suono.

Il cantus viene trasformato e reso ferme per essere contrappuntato.

### Cosa sa l'allievo?

Ricordarsi che l'allievo non sa nulla...

Attraverso il parlato si possono far intendere delle nozioni di contrappunto, leggendo in maniera sfalsata.

>Utilizziamo il linguaggio verbale, perchè sappiamo che esso è conosciuto dagli studenti.

La parola sappiamo che ha accenti che possono essere in concomitanza con la musica.

Sappiamo che un ritmo è un ritmo è un'organizzazione delle durate.

### Psicologia e pedagogia

Psicologia -> Studio del cosa

Pedagogia -> Studio del come e delle modalità di intervento.

Bisogna differenziare le tipologie di insegnamento.

### Superare la concezione della musica come un sentimento

Essa è una concezione superata ed era figlia di un certo periodo culturale.

>Scrivere musica è un'attività del tutto artigianale. Infatti Beethoven scrisse musica anche da sordo.

Schonberg definiva i compositori che suonavano al pianoforte erano definiti _Compositori del tastino_.

>Un compositore è un artigiano che ha sviluppato un orecchio interno e che riesce a scrivere musica anche non sentendolo.

### Composizione e committenza musicale

La committenza musicale ha cessato di esistere con la caduta di alcune monarchie e con l'avvento e la nascita degli stati nazionali, svincolandoli dalla committenza.

Beethoven a volte scriveva anche senza commesse, elemosinando oboli alla società con lezioni ed altro.

Ad esempio le 32 sonate di Beethoven hanno dedicatari diversi, ovvero coloro i quali forniva un sostegno di qualche tipo a Beethoven.

I compositori lavoravano per portare avanti una ricerca che porti piacere ai compositori e realizzi un'operazione di ricerca e sviluppo sul linguaggio.

Notare che tutti i compositori tesi a sviluppare il linguaggio erano in genere persone che non avevano necessità quotidiane come Stravinsky o gli americani che andavano a studiare a Parigi.

#### Schonberg fece molta ricerca, ma come viveva?

Schonberg dirigeva le orchestrine dei caffè viennesi per le quali faceva trascrizioni dei delle canzoni napoletane.

### Suoni temperati e non

Il suono è temperato e l'ottava è quindi un artifizio, ascoltando i cori si potrebbe sentire qualcosa di strano con le componenti del gruppo che tendono ad accordarsi in altro modo.

>Il contrappunto è quindi una prima organizzazione delle linee melodiche.

### La serie di Fibonacci

Successione di numeri interi che hanno tra loro sempre lo stesso rapporto, ovvero 0,618 ovvero la sezione aurea.

La proporzione aurea è utilizzata in genere per la costruzione di monumenti nell'antica grecia, per le proporzioni degli elementi compositivi in musica etc...

>La sezione aurea è stata definita anche la proporzione divina.

Tale proporzione porta un'idea di bello e equilibratura.

Utilizzando la serie di Fibonacci abbiamo delle altezze per realizzare un cantus firmus.

### Esempio di contrappunto elaborato da Brahms dal testo di Salzer e Schachter ed analisi di brani

Vediamo nell'esempio la coincidenza sui tempi forti di alcune note.
![2](2.png)

Sappiamo che l'analisi Schenkeriana prevede linee fondamentali e parti meno fondamentali.

Osserviamo ad esempio quali note definiscono le linee più importanti o quelle che hanno un'importanza secondaria:
![3](3.png)

Gli esempi sono indicati in questo modo:
![4](4.png)

Ogni pezzo si riduce alla dicotomia riposo-slancio-riposo realizzata da 3 note:
![5](5.png)

### Cantus firmus e libro di testo

Si indica come scrivere un cantus firmus circoscrivendolo alla tonalità, ovvero con con scale tonali.

I criteri costruttivi del canto indicano:
- direzione
- continuità (come essa viene raggiunta)

Bisogna rispettare varietà (non solo grado congiunto o solo salti) e la continuità, insieme alla direzione.

___
>Leggere il primo capitolo (pagina 3-11) del tezto di Salzer e Schachter.

È importante la linea per l'insegnamento ai nostri studenti.
