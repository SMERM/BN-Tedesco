# Appunti della lezione di lunedì 28 novembre 2022

Continuiamo con la trattazione della parte teorica...

## Ripilogo

La scorsa volta abbiamo osservato diverse situazioni e ritmi misti.

Abbiamo osservato che il 5/4 di Cajcoskij era un 5/4 costituito da diverse pulsazioni.

Mentre il 5/4 di Mussorsgky è un vero 5/4 quindi non sempre il numero frazionario corrisponde al significato della musica.

Il numero frazionario è la tradizione che lo impone.

Ricordarsi anche la sinfonia di Beethoven in 2/4 che in realtà è un 4/8...

## Flussi superiori

### Esempio 10

Da Schubert da ErlKonig, abbiamo scritto in 4/4, ma abbiamo un qualcosa di scritto in terzina.

Perchè non ha scritto in 12/8?

![1](1.png)

Per non abbandonare nella parte pianistica i 12/8, Schubert scrive in 4/4 per non creare una poliritmia, ma sappiamo che non era di moda realizzare poliritmie.

Anche Stravinski

### Cosa sono i Flussi superiori

I Flussi superiori sono ritmi che vanno aldilà della battuta.

Vediamo dalla nona Sinfonia di Beethoven che vi sono flussi superiori.

Abbiamo una minima col punto come tempo, mentre semimina col punto come tempo primo.

Il flusso superiore ce lo indica Beethoven, ed a pagina 7 dopo la corona dell'esempio 11 vediamo un ritmo di tre battute. Quindi Beethoven ci sta indicando i ritmi superiori ed ogni tre vi è un'entrata. Il ritmo precedente è quindi dato per scontato che debba essere raggruppato a tre battute.

![2](2.png)

>Anche la quinta di Beethoven è da accorpare quattro a quattro.

Sappiamo che i conti a volte non tornano, quindi la frase a volte deve andare con divisioni diverse.

>Haydn realizza periodi con 16 battute+16 battute, etc...

Beethoven realizza i periodi con una battuta in meno o in più...

>Sulla Nona dice Beethoven di raggruppare per tre...

### Schumann Traumerei

![3](3.png)

In rosso osserviamo come un fraseggio dovrebbe andare, senza far sentire l'accento del 4/4.

Il 4/4 serve quindi per convenzione e l'idea viene ingabbiata nel 4/4.

Osserviamo che vi è un'irregolarità che deve essere cercata, e il flusso superiore deve essere cercato e trovato, e non bisogna osservare la sola scansione metrica.

>Bisogna capire dov'è il vero ritmo/metro.

Osserviamo che si può studiare anche dove e come prendere fiato...

Bisogna cercare di capire dove respirerebbe anche un cantante.

>La direzione d'orchestra è una sorta di strumento molto vasto...

Talberg (che gareggiava con Liszt) diceva di prendere lezioni di canto per poter permettere di cantare la linea immaginaria.

Bisogna quindi capire quale sia la linea che passa tra i vari strumenti e stabilita questa linea, bisogna cantarla.

Ricordandosi nei flussi superiori che rispetto al ritmo imposto vi è un flusso superiore.

### Flussi secondari

Oltre i flussi ritmici vi sono i flussi secondari che generano una polimetria.

La multiritmia è un cambiamento di tempo orizzontale (come Rolle).

La poliritmia è la polimetria sono la stessa cosa...

Osserviamo nell'esempio 13 di Chopin che possiamo dividere al basso in minime mentre sopra abbiamo diviso in 4...

![4](4.png)

Abbiamo quindi due flussi, ed abbiamo quindi alla chiave di violino quattro semiminime il cui tempo primo è una croma, in basso abbiamo 2 minime e il tempo primo è la semiminima.

Quello visto è un primo esempio di polimetria.

La cellula metrica basilare sotto è la minima...

### Differenza tra multiritmia e poliritmia

Abbiamo una dissonanza ritmica con una fase, ovvero un inizio ed una fine della fase con inizio sincrono e fine sincrona.

![5](5.png)

La fase sincronica è la distanza in battute che separa l'allineamento degli ictus.

#### Esempio di Mozart dal Don Giovanni

Più è lunga la fase sincronica più abbiamo dissonanza ritmica.
Non vi è infatti solo la dissonanza armonica, ma anche quella ritmica.

>Nell'esempio di Chopin vi era polimetria ma senza dissonanza ritmica.

Nel caso di Don Giovanni vi è proprio una dissonanza ritmica, poichè vi sono tempi diversi.

![6](6.png)

Abbiamo la sovrapposizione di 3/4 (minuetto), 3/8 e 2/4...

Abbiamo che la fase sincronica tra 2/4 e 3/4 per i 2/4 che è di tre battute mentre quella...

### Poliritmia nello stesso metro

La poliritmia si può instaurare anche all'interno di stessi metri.

Osserviamo all'interno dello stesso tempo una poliritmia. Chopin come in questo esempio crea poliritmia all'interno dello stesso metro.

Abbiamo qui un tre contro due...

Il tre contro due di per sè è una poliritmia che crea una dissonanza ritmica.

![7](7.png)

E la fase sincronica è qui ogni battuta.

>La fase sincronica è così chiamata perchè permette di far sincronizzare i metri.

### Multiritmia in Bartok

Osserviamo i continui cambiamenti e nella sezione di 7/8 si possono suddividere in maniera diversa le battute:

![8](8.png)

### Fasi sincroniche e il direttore

Come nei brani di Stockhausen come Gruppen dove si usano tre direttori d'orchestra. Anche Charles Ives utilizza due direttori d'orchestra.

### Stravinski e Petruska

Stravinski fece diverse versioni delle composizioni, anche con Petruska.

Si nota nella versione del 1910 il 3/4 e poi vi sono i 7/8:

![9](9.png)

Versione del 1948:

![10](10.png)

Inserisce la settimina ovviando al 7/8...

Non abbiamo quindi una poliritmia ma abbiamo una dissonanza ritmica.

### Conflitti ritmici

Il primo conflitto che ci viene è la sincope in cui si inverte l'ordine degli ictus.

#### Emiola
Vi è un altro modo storico per invertire l'ordine degli ictus ovvero la sostituzione di due note in tre note.

L'emiola è la sostituzione di due note con tre note.

##### Esempi di emiola

Incontriamo l'emiola in Strauss nel valzer in cui abbiamo ogni due battute...

![11](11.png)

Incontrando l'emiola bisogna enfatizzare l'emiola.

Anche nella terza sinfonia di Beethoven vi è l'emiola e Karajan lascia il tre ed enfatizza l'emiola, andando in due e tutte le arcate vanno verso il basso.

Abbiamo nel concerto di Schumann l'emiola segnata in rosso in cui due minime col punto vengono sostituite da 3 minime.

![12](12.png)

#### Sincope su tempo ternario

Vi è un tipo di sincope molto particolare, e la sincope crea conflittualità.

È interessante la sincope sul tempo ternario.

La sincope è il prolungamento della parte debole verso la parte forte del tempo successivo.

Nel 3/4 possiamo avere la sincope quando prolungiamo la parte debole sull'altra parte debole. Oppure possiamo legare il terzo tempo con il tempo successivo.

In caso di sincope bisogna accentare la nota sincopata.

Il classico esempio di sincopi ternarie vi è nella sinfonia 5 Cajcoskij.

![13](13.png)

>Azio Corghi diceva che la forma più difficile per andare insieme è pianoforte ed archi.

---

Da studiare l'esercizio tre oltre al due per la prossima settimana.

Per studiare la croma col punto usare l'alberello.
