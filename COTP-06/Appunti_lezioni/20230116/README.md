# Appunti della lezione di lunedì 16 gennaio 2023

>Ferruccio Busoni, diceva per i pianisti di cercare di riprendere Cramer, Clementi e Czerny e suonarli perfezionandoli.

## Questione crediti ed esame

Attendere una comunicazione dal Maestro dopo il dialogo con i direttore.

## Modalità d'esame
1. un argomento proposto da noi a piacere
2. un argomento proposto dal docente
3. solfeggi dal Rolle

## Messiaen

Proseguiamo affrontando l'autore...

### Tecniche del mio linguaggio musicale

Messiaen fece un trattato sul ritmo con 7 volumi ma di 8 tomi.

Studiò gli uccelli, infatti era anche un ornitologo.

>Nel testo _Tecniche del mio linguaggio musicale_ esprime che si possa variare un ritmo aggiungendo semplicemente un valore in più con:
- una nota (di qualsiasi valor) ovvero con valore aggiunto
- con una pausa

### Quarto capitolo: Tempi diminuiti o aumentati

La tradizione raddoppiava le note, e se voleva fare la diminuizione di tre semiminime, diminuiva con tre crome.

Realizzò aumentazioni e diminuizioni anche con valori frazionari.

![1](1.jpg)

Osserviamo che venga aumentata o diminuita una figurazione.

Vediamo che possiamo aggiungere un punto, una croma, etc...

Per poter dei valori frazionari, vi sono i primi tre esempi di diminuzione.

Messiaen era alla radice dello sviluppo della musica nuova.

Messiaen insegnò a:
- Boulez
- Berio
- Stockhausen
- Nono

### Quinto capitolo: ritmi retrogradabili e non

Vi sono ritmi che si possono o non si possono leggere al contrario.

I ritmi palindromi, come le parole palindrome hanno lo stesso significato lette al contrario.

I ritmi palindromi non cambiano di significato se letti al contrario, essi sono non retrogradabili.

Altrimenti si hanno ritmi retrogradabili se non palindromi.


![2](2.jpg)

Nel ritmo 32 non retrogradabile, osserviamo che i ritmi con valore centrale comune sono non retrogradabili.

Avendo il ritmo A retrogradabile, che letto al contrario cambia di significato, osserviamo che leggendo

>Si realizza un modo per rendere non retrogradabile un qualcosa di retrogradabile.

Il palindromo crea ritmi non retrogradabili, mentre il non palindromo crea ritmi retrogradabili.

### Capitolo sesto: poliritmia

Avevamo visto un brano dal Don Giovanni di Mozart, in cui vi era la fase sincronica con delle danze.

Messiaen afferma che per ottenere la poliritmia servirà sovrapporre due ritmi non diversi, ma di lunghezza diversa.

![3](3.jpg)

Osserviamo chela parte superiore ha 10 semicrome, mentre la parte sotto ha 9 semicrome.

>Osserviamo che la poliritmia si crea con ritmi di lunghezze differenti, non di ritmi differenti...

Avevamo definito come poliritmia due ritmi di lunghezza differenti, con l'esempio di Mozart in cui alcuni personaggi ballano un 3/8, altri ballano 3/4 ed ogni tanto ci si ritrova con al fase sincronica, creando dissonanza ritmica.

Messiaen invece afferma che si possono sovrappore ritmi di lunghezza differente, come in Le Verbe, in cui sovrappone un ritmo con 10 semicrome sopra e 9 semicrome sotto.

Un brano del genere si suona contando i valori più piccoli.

La battuta da un orientamento, ma l'ictus non vi è.

La poliritmia secondo la nuova ritmica senza tempi, e quindi si ottiene poliritmia sovrapponendo due ritmi di lunghezza differente.

![4](4.jpg)

Osserviamo la sovrapposizione di un ritmo alla sua regradazione.

Realizzando un pezzo per organo.

![5](5.jpg)

Osserviamo nel 46 un ritmo retrogrado.

Il pedale è un ritmo non retrogradabile con un'unità centrale.

### Canoni ritmici

I canoni ritmici sono dei ritmi che partono da una semiminima di distanza, solamente la ritmica è la stessa, non le altezze.

![6](6.jpg)

### Pedale ritmico

Esso è una specie di ritmo che i ripete in modo ostinato.

>Karajan dice che l'ostinato ha tutte le figurazioni ritmiche con la stessa forza.

Il pedale ostinato del pianoforte

![7](7.png)

![8](8.png)

## I quattro metodi di scrittura

1. multiritmia
2. ritmica senza tempo
3. scrittura del ritmo con un tempo fisso che si serve di accenti, legature e sincopi per poter ingabbiare il ritmo all'interno di elementi ritmici
4. ritmica ingabbiata come Roger Desormiet con figurazioni triangolo, etc...
![9](9.jpg)

---

Lavorare sugli esercizi
