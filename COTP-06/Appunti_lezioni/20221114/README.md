# Appunti della lezione di lunedì 14 novembre 2022

## Riepilogo della scorsa lezione

Avevamo trattato della teoria del ritmo, e ci eravamo soffermati sull'idea di Platone di ritmo.

Sappiamo che vi è diversità tra chi trasmette un ritmo e chi lo percepisce.

L'ascoltatore realizza il processo inverso da chi lo emette e per arrivare alla capacità di analisi deve arrivare ad una sintesi.

Ricordarsi dell'esempio dell'apprendista stregone di Dukas.

>Chi suona e emette ritmo deve avere chiaro tutto quello che deve trasmettere.

Il trasmettitore ha l'analisi in mente e opera una sintesi.

L'ascoltatore deve decifrare quello che gli viene inviato.

Alla fine di un brano bisogna realizzare un'analisi e poi realizzare una sintesi (contrario del trasmettitore).

Avevamo anche detto che la cellula ritmica più piccola è realizzata da arsi e tesi.

_Quante volte si suona senza dare importanza all'arsi?_

Bisogna sempre pensare in levare ed il direttore d'orchestra deve per forza pensare in levare, perchè il comando dell'orchestra viene trasmesso in levare.

## Parte teorica

### Cosa deve comunicare il direttore con il levare

Sappiamo che il direttore i  levare deve comunicare:
- tempo
- intensità
- espressività

### Il motivo e Riemann

La cellula viene chiamata motivo da un teorico tedesco, ovvero colui che da grande impulso all'analisi con le teorie di Riemann.

>L'analisi deve essere realizzata solo se vi è un motivo per fare l'analisi.

Un'analisi deve avere un motivo fondamentale per cui si debba realizzare.

### Analisi Schenkeriana

Riemann si è perso un po' e ora è molto utilizzata l'analisi Schenkeriana.

Sanguinetti esperto di analisi Schenkeriana.

### Tempo primo

Esso è il valore medio di un'arsi.

Ad esempio nella figura 1 abbiamo un levare di un tempo ternario.
![1](1.png)

Il tempo primo qui è la croma.

Osserviamo che le unità ritmiche fondamentali sono formate da arsi e tesi.

Mentre le unità metriche definiscono la pulsazione.

Le unità metriche hanno il prncipale appoggio sul battere. L'appoggio è sempre sulla figura ritmica più grande che acquista il suo ictus.

L'ictus è il primo accento che cade nella battuata.

Vi è una grande differenza tra metro e ritmo.

### Canto gregoriano, metro e ritmo

Il canto gregoriano sappiamo che è una forma libera e potremo dire che il metro può essere ritmico, ma il ritmo non è mai metro.

Il ritmo è un qualcosa di più ampio del metro.

Il metro è un qualcosa di imposto, mentre il ritmo vive anche senza metro.

Sappiamo che mentre il metro è ritmo, il ritmo può non essere metro come il canto gregoriano che un canto ritmico libero.

Il metro è la distanza fra due ictus. In una distanza tra due ictus abbiamo un metro.

L'unica ritmica dipende dalla durata delle figure ritmiche, mentre l'unità metrica non varia.

### Differenza tra danze

Ad esempio la differenza tra un valzer, una polacca e una passacaglia.

Abbiamo tre danze inquadrate sempre in tre quarti.

Abbiamo nel ritmo differenze anche con metri uguali.

Variando le figure interne avremo ritmi diversi, mentre il metro rimane sempre invariato.

Il metro è invariabile mentre il ritmo varia in base alle figure ritmiche.

### Unità ritmica

Essa è l'unione di un'arsi e di una tesi

### I quattro parametri che influenzano il suono

Abbiamo visto che il metro è differente dal ritmo e abbiamo quattro parametri del suono:
- altezza
- intensità
- timbro
- durata

I primi tre non influiscono sul ritmo. L'unico parametro che intacca il suono è la durata.

L'organo e il clavicembalo sono importanti perchè il clavicembalista non può variare l'intensità del suono e il clavicembalista fa durare un suono maggiormente per ottenere un'importanza diversa di un elemento.

La durata è importante per uno sviluppo ritmico, il ritmo dipende quindi dalla durata.

### Metrica quantitativa e qualitativa
La lingua italiana si basa sulla metrica qualitativa, mentre la lingua greca si basa sulla metrica quantitativa.

### Sincope

Sappiamo che mentre la metrica detta gli accenti, dato che il ritmo può essere in accordo e in disaccordo con il metro, fa creare un contrasto tra la ritmica imposta dal metro e la ritmica imposta dal ritmo.

La sincope serve per quindi per creare un disaccordo fra metro imposto e ritmo.

Creando sincopi il compositore crea quindi interesse.

La sincope è un momento in cui l'accento ritmico s ribella alle accentuazioni date dal metro. Essa si ottiene prolungando la parte debole di un tempo, su un forte successivo.

Succederà che l'accento libero salterà e andrà indietro sul tempo precedentemente debole.

La sincope è un processo che manda in conflitto i due accenti.

>Il compositore per creare effetti, crea conflitti ritmici.

### Metro e pause
>Il ritmo dipende dal valore delle note, mentre il metro è insensibile ai valori musicali ed alle pause.

Inserendo delle pause vediamo che non specifichiamo molto musicalmente.

### Ritmo e metro concordante o meno

![2](2.png)

Osserviamo un episema verticale, in cui ritmi concordanti cadono su un accento metrico.

- ictus è l'accento ritmico
- metro è l'accento metrico

Osserviamo che vi sono sopra gli accenti rimtici e sotto gli accenti metrici.

Vediamo che nell'esempio a non vi sono contrasti.

Osserviamo che gli ictus sono posti sulle note con maggiore durata e sappiamo che il ritmo dipende dalla durata delle note.

Osserviamo nella seconda battuta non casca l'ictus ed abbiamo un qualcosa di neutrale.

Osserviamo che abbiamo nella prima battuta un ictus concordante con l'accento metrico, mentre abbiamo nella terza battuta uno spostamento dell'accento metrico...

Abbiamo un andamento sincopato composto e semplice.

Abbiamo una continuazione di contrasti.

### Flusso ritmico

L'insieme di cellule ritmiche elementari forma una cellula composta, denominata flusso ritmico.

Essi possiamo distinguerli in flussi:
- ciclici
- aciclici

Un esempio di __flusso aciclico__ che non si ripete è il canto gregoriano. Avremo un continuo variare di ritmo (v. studi benedettini di Solems). Abbiamo nel canto gregoriano tutti tempi primi in cui alla fine di ogni voce vi è la mora vocis, ovvero ritmo doppio. Non ha un ciclo e si ripete.

Sappiamo invece che la musica ordinaria ha un __flusso ritmico ciclico isometrico__, ovvero si ripete ed è con metro costante.

Vi sono anche ritmi ciclici con metri che cambiano. Ovvero ritmi ciclici misti con metro che ogni tanto cambia.

Bartok (vi sono alcune sezioni che si ripetono), Stavinskij possono essere inseriti nei flussi ritmici ciclici misti, in cui il metro cambia sempre e si ha quindi:
- multiritmia -> ritmo su un rigo che cambia, ovvero vi sono più ritmi
- poliritmia -> sovrapposizione di ritmi (definizione che proviene da polifonia)

## Parte pratica

Ci occuperemo dei ritmi di Bartok che non fanno parte della nostra cultura, in cui vi sono isometri.

### Gestualità

Essa è la gestualità che utilizzano i direttori.
Sappiamo che il movimento deve essere piccolo.

>Silvetre Reultas, lo Stravinskij messicano Ascoltare _Ocho por radio_.

### Rubato e senso ritmico

Senza senso ritmico non si può fare rubato.

### Tecnica italiana e gesto

È molto importante il penultimo movimento prima del battere, se il penultimo movimento non è chiaro manda fuori, ed il penultimo è esempre esterno.

È importante l'apertura, ricordarsi che Gesù non può fare il direttore.

Ricordarsi che il movimento sul due deve essere laterale e bisogna essere più scattosi e meno morbidi.

![3](3.png)

## Per la prossima volta

Studiare per la prossima volta l'esercizio

(Il gesto con 1 è indicazione di 3/8)
