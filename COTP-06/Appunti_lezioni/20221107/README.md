# Appunti della lezione di lunedì 7 novembre 2022

30 ore se studente consenziente si possono ridurre alle 18 ore del nuovo piano di studi.

## Organizzazione del corso

- parte teorica -> ritmo, elementi su Messiaen e il ritmo, esercitazioni pratiche per trascrivere nella ritmica con notazione spaziale o proporzionale
- parte pratica -> lettura della ritmica che può servire nel repertorio (con ad esempio la ritmica bartokiana), come 8/8, e moduli di suddivisione dello stesso

La ritmica va imbrigliata in una gestualità e chironomia, tale controllo ritmico va inserito all'interno del contesto della tecnica direttoriale.

L'esame sarà svolto sulla parte di primo violino di un brano contemporaneo.

Abbiamo dal volume di Rolle problemi di ritmica contemporanea.

>Sappiamo che gli studi del solfeggio non arrivano a toccare la parte ritmica che analizzeremo e studieremo.

Con la ritmica che vedremo usciremo dal 4/4...

Osserveremo degli articoli interessanti sui ritmi particolari.

Osserveremo una parte teorico e una parte pratica.

>Se non si riescono a dire le note, si usa il metodo tedesco.

Vedremo che l'__Histoire du soldat__ di Stravinskji è la bibbia del ritmo.

## Materiale utile

Portare un tablet o un computer.

Gli esempi saranno caricati su google drive, e il materiale sarà tutto online.

Apriremo i vari esempi e commenteremo gli esempi mentre li guardiamo.


## Parte teorica: approfondimento sul ritmo

Sappiamo che in molte partiture vi sono tematiche ritmiche a livello superiore, ma sappiamo che vi sono diverse notazioni ritmiche oltre 3/4 e 4/4.

>Sappiamo infatti che bisogna controllare dei ritmi oltre la singola battuta.

Dobbiamo riuscire a controllare ritmi oltre la sola battuta.

Ci servirà la trascrizione della quinta di Beethoven per pianoforte.

Sappiamo che essa è scritta in tre quarti e il direttore d'orchestra suddivide e inserisce i cosiddetti raggruppamenti di frase, ovvero raggruppamenti di battute.

Quando vi è un tipo di ritmo che si muove a livello superiore, ed il direttore d'orchestra si ferma e realizza pensieri a gruppi di un tot di battute.

Sapremo che vi sono pensieri musicali raggruppati per 3,4 etc...

Sappiamo ad esempio che un recitativo secco è sempre in 3/4 e in base alla tradizione tale ritmo viene mantenuto.

>Sappiamo che i recitativi erano a volte fatti dagli allievi per Mozart, ed a volte i recitativi erano realizzati dai compisti. Come per Monteverdi e Palestrina si inserivano le parole sotto la musica.

### Definizione di ritmo

Ordine nel movimento, o movimento ordinato nel tempo.

Tale definizione classica è stata realizzata da Platone.

Righini, musicologo e acustico ha dato più di 60 definizoni sul ritmo(cfr. al testo sul ritmo).

Il suono è il movimento del suono ordinato nel tempo, come l'architettura è il movimento ordinato di linee architetturali.

Sappiamo ad esempio che Sant'Agostino diceva che chi canta prega due volte...

La musica era per Sant'Agostino l'ars modulandi.

>Sappiamo ovviamente che vi è il ritmo e il ritmo modulato.

### Ritmo come elemento soggettivo

(cfr. All'apprendista stregone di Dukas)

Se il compositore ha in mente tutta l'arcata ritmica e l'ascoltatore sente questo brano ed avrà l'idea che rappresenta il brano solamente alla fine dello stesso.

>Questo concetto si descrive come concetto di analisi e di sintesi.

Compositore: dalla sintesi all'analisi

Ascoltatore: dall'analisi alla sintesi

Gli esecutori sono dei trasmettitori che devono far percepire la sintesi attraverso l'analisi.
L'esecutore deve avere un pensiero, tanto forte, tanto vicino a quello della sintesi che si è realizzata.

>Quarto anno di composizione del Maestro, qual'è l'unità ritmica più piccola della musica? Essa è l'insieme di arsi e tesi.

La teoria Rimaniana chiama Beethoven ciò che faceva Beethoven: teoria del movimento.

Pensando al Preludio in Re Maggiore di Beethoven, sappiamo che l'accento non casca sulla scaletta iniziale, ma dopo...

Settima Sinfonia di Beethoven, secondo movimento, sappiamo che le orchestre danno sempre un appoggio, ma bisogna farglielo pensare in levare agli orchestrali, e cambierà il brano totalmente ed il battere (l'ictus) del primo movimento è dannoso.

>Bisogna ricordarsi che la musica parte in levare.

Il direttore d'orchestra pensa sempre in levare.

Pensare anche al Clavicembalo ben temperato che si inizia a suonare in levare.

Sapremo che l'unità più piccola del movimento è il motivo che è composto da:
- arsi
- tesi

## Parte pratica

Avremo nove lezioni e all'esame bisognerà eseguire un esercizio estratto a sorte del Rolle ed essi vanno eseguiti come un piccolo direttore.

Osservare anche la gestualità che serve per tracciare i tipi di ritmi.

### Multiritmia

Il ritmo Stravinskjiano o bartokiana viene introdotto dalla musica ungherese o balcana.
Si usano in tali tradizioni i balli popolari, e tale movimento è stato introdotto da Bartok.
Tutta la musica del Novecento ha seguito tali ritmi.

Sappiamo che il Bona si basa sul metodo di divisione musicale, mentre quella che vediamo è una tecnica di accorpamento, ovvero unità che viene accorpata, tutta la musica è basata su un qualcosa di unitario con raggruppamenti di due e di tre.

>Quando si realizzano ritmi con unità e suddivisione bisogna sempre essere all'interno della pulsazione, poichè uscendo da tale pulsazione si perde la concezione.

Gavazzeni, direttore d'orchestra e uomo di cultura, tagliò un ottavo perchè l'orchestra non riusciva a suonarla a tempo.

### Tempi base

- 2/8
- 3/8
- 4/8
- 5/8
  - 3+2
  - 2*3
- 6/8
  - 1,2,3
  - 1,2,1,2,1,2
- 7/8
  - 2+2+3
  - ...
- 8/8
  - 3+3+2
- 9/8
  -3+3+3
- 10/8
  - 3+3+3+2

>Ciò che stiamo raccontando dipende dal fraseggio o dal fatto che è indicata da battuta con linea tratteggiata.

La croma è sempre fissa e riuscendo a controllare tale ritmo fisso si riesce a realizzare qualsiasi combinazione.

>Questo corso riuscirà a far comprendere anche come realizzare il _rubato_.

### Esercizio 1 del Rolle

![1](1.png)

Osserviamo che sono indicate le suddivisioni.

Per quanto concerne la gestualità:
- non muovere la braccia
- muovere solo il polso (più è piccolo il movimento e più si controlla il ritmo)

Il movimento si dice:
- morto quando la mano è giù
- vivo quando la mano è su

Con quattro quarti sappiamo:
- forte
- debole
- mezzo forte
- debole

>Si utilizza un movimento diagonale perchè più debole del movimento dritto.

Sappiamo ad esempio che Liszt studiava la partitura in carrozza.

Volendo si può mettere un metronomo a 60 o 180.

_Leggere ritmicamente il primo esercizio_.
