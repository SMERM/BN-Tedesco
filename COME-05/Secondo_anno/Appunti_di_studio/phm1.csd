<CsoundSynthesizer>
<CsOptions>
-n
</CsOptions>
<CsInstruments>
; Initialize the global variables. 
;ksmps = 32
;nchnls = 2
;0dbfs = 1

sr = 44100
kr = 5

instr StampaOscillatore
kstep init 0
k0 init 0
k1 init 0.5
kc init 0.4

k2 = k1+(k1-k0)-kc*k1
printks "Sample=%d: k0=%.3f, k1=%.3f,  k2=%.3f\n", 0, kstep, k0, k1, k2
kstep= kstep+1
k0=k1
k1=k2
endin

</CsInstruments>
<CsScore>
i "StampaOscillatore" 0 10
</CsScore>
</CsoundSynthesizer>