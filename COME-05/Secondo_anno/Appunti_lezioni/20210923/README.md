# Appunti della lezione di giovedì 23 settembre 2021

Vediamo una carrellata di trasformate oltre ad approfondimenti sulla trasformata di Fourier.

## Le trasformate

>L'idea generale nelle trasformate è di passare da un dominio ad un altro.

### Prodotto scalare

![ps](ps.png)

Partiamo dal concetto di prodotto scalare in cui partiamo dalle lunghezze di due vettori, moltiplicata per il coseno dell'angolo fra i due vettori.

>Quando un vettore possa influenzare l'altro vettore...

Quanto un vettore sia all'interno di un altro vettore.

Due vettori possono essere coincidenti(cos=1) o ortogonali(prodotto scalare di vettori ortogonali=0 poichè il coseno di 90º = 0).

Il prodotto scalare è anche detto prodotto interno e definisce anche le basi di uno spazio.

Le basi di un vettore sono ortogonali fra loro, ovvero hanno sempre un angolo di 90º e facendo il prodotto scalare tra vettori ortogonali, avremo un prodotto scalare nullo.

Potremmo quindi definire uno spazio ortogonale.

![ps1](ps1.png)

Il prodotto scalare è dato dalla somma dei componenti, presi componente per componente.

Abbiamo la stima quindi dei vari valori...

>Ogni vettore è rappresentato da terne o coppie e definiamo il prodotto interno, ovvero del prodotto scalare.

Il prodotto scalare è la somma dei prodotti delle singole componenti.

Il prodotto scalare viene descritto da questi simboli:
![b](braket.png)

#### Prodotto scalare in Max

![psm](psm.png)

Di esso ne vediamo la rappresentazione cartesiana...

#### Prodotto scalare in Python

Implementando la libreria `numpy`, definiamo due array e utilizziamo il metodo `inner()`.
![psp](psp.png)

>E` importante sottolineare che il prodotto vettoriale invece rimane un vettore:
![pspp](pspp.png)

### Prodotto scalare in coordinate polari e cartesiane

![pspc](pspc.png)

Abbiamo un punto rappresentabile in vari modi:
- cartesiano
- polare

Avremo il prodotto scalare calcolabile con il teorema di Pitagora, e l'angolo sarà calcolabile come tangente.

Se volessi passare da coordinate cartesiane a coordinate polari, potrò avere una formula che mi porterà ad ottenere modulo e fase:
![m](m.png)

![f](f.png)

Avendo un vettore (1,1) avremo il modulo pari a √(2), mentre la fase sarà pari all'arcotangente di phi ovvero π/4.

Possiamo fare tutto ciò con l'oggetto `cartopol` in Max.

Vedremo molto la formula detta rappresentazione di Eulero.

Possiamo quindi passare dalle coordinate polari alle coordinate cartesiane e viceversa.

Avendo x e y posso passare alle coordinate polari, altrimenti il contrario.

Abbiamo gli oggetti `poltocar` e `cartopol` sia come controlli che come segnali.

#### Spazio di funzioni

Abbiamo fino ad ora parlato di spazi vettoriali, ma esistono anche spazi di funzioni in cui invece di avere vettori abbiamo delle funzioni.

Il prodotto scalare in questo spazio di funzioni è definito secondo questa formula che di fatto è un integrale del prodotto fra le due funzioni(o piú) in tutti gli istanti del tempo:
![psi](psi.png)

Facendo il prodotto scalare tra due funzioni:
![int](int.png)

Le funzioni seno di t e coseno di t sono ortogonali.

Possiamo quindi come inventare una nuova base con due vettori ortogonali, ma nello spazio di funzioni e non in uno spazio vettoriali.

>Potremmo vedere quanta ombra una funzione esercita su di un'altra.

##### Prodotto scalare nello spazio di funzioni

![pssf](pssf.png)

Per realizzare prodotti usiamo gli integrali, ma con rappresentazione polare.

Un prodotto scalare tra una funzione e la sua proiezione sulle basi e^jwt, vedo quindi quanta componente seno e quanta componente coseno vi è in un segnale.

Abbiamo quindi un discorso complesso, abbiamo quindi un discorso su uno spazio di funzioni e degli assi immaginari.

Siccome il prodotto scalare genera un numero, avremo quindi un numero reale ed un numero immaginario, ovvero due coefficienti di Fourier che si integrano come integrale.

## FFT
![fft](fft.png)

Troviamo una strategia di smussare gli spigoli di volta in volta ovvero con la STFT.

![fft1](fft1.png)

Se effettuo l'analisi su un campione di 0,023 secondi, la frequenza piú piccola che posso cercare in questo suono è di 43.06Hz e non potrò andare piú in basso e dovrò aumentare il numero di campioni.

![fft2](fft2.png)

Avremo in questo caso 512 bande e non potrò esprimere ogni frequenza possibile, ma degli intorni di 43 Hz e potrò esprimere a(k) e b(k), le altre frequenze da 22050 a 44100 non ci interessano.

![fft3](fft3.png)

Le bande si chiamano bin, esse sono le bande in cui si suddividono le window size.

Nell'analisi di Fourier terremo conto dei primi 512 bin.

### Trasformata di Fourier Discreta

![dft](dft.png)

La DFT converte una collezione finita di campioni e divengono una collezione di coefficienti di ampiezze di una collezione di sinusoidi che vanno a comporre il segnale.

La trasformata di Fourier sarà un modo di rappresentare delle funzioni.

La trasformata sarà quindi un prodotto scalare di funzioni.

_k_ sono i campioni da 0 a n-1, _n_ è il numero totale di campioni che ho.

I numeri complessi rappresenteranno l'ampiezza e la fase delle sinusoidi.

Per il calcolo della DFT, invece di fare il prodotto, si preferisce realizzare degli algoritmi più veloci.

Invece di fare: prodotti e somme, che avrebbe tempo di di calcolo N^2, mentre con la DFT avremo N(logN)

#### FFT in Max
![fftm](fftm.png)

Avremo 3 uscite nell'oggetto:
- valori immaginari
- valori reali
- indice delle bande(bin)

![fftm1](fftm1.png)

Se analizzo una frequenza che non è multipla di 86 Hz o 43 come prima, analizzando un sengale non troveremo quella frequenza con un periodo completo:
![d](d.png)

>Quindi usando una frequenza che non è multipla di 86Hz avremo un'analisi effettuata su una porzione di periodo che non è pari al segnale originale.

Realizziamo quindi una serie di funzioni denominate di Schwartz per smussare le ampiezze dei campioni piú laterali e dargli meno importanza.

Lo spostamento(hopSize) da un punto ad un altro è una quantità minore della WindowSize, per sovrapporre varie parti di finestre.

Avendo parti di finestre sovrapposte potremmo trovare anche frequenze non multiple di 86.

![fftmax](fftmax.png)

L'operazione di estrarre e riportarsi al segnale dopo una FFT si chiama antitrasaformata con il segno + su jwt:
![+](+.png)

Facciamo quindi prima un prodotto scalare e poi una sorta di "divisione scalare"...

>Possiamo realizzare operazioni spettrali...

![po](po.png)

mettiamo nel buffer le ampiezze del sengale che sta arrivando.

![v](v.png)

![vis](vis.png)

Avremo quindi spettro, diagrammi e fase:
![vv](vv.png)

## Convoluzione

Prendendo due segnali nel dominio della frequenza, realizziamo un'operazione nel dominio del tempo, lineare e tempo-invariante, conoscendo una risposta all'impulso.

![conv](conv.png)

Dovremmo conoscere la sequenza del segnale di ingresso...

Una sequenza digitale è una sequenza di singoli campioni con una certa ampiezza.

Una Convoluzione nel dominio del tempo è equivalente a moltiplicare le ampiezze degli spettri e sommarne le relative fasi.

Abbiamo quindi un'operazione equivalente al prodotto di due spettri e il tutto risponderà in un certo modo.

Fare la convuluzione significa moltiplicare le ampiezze degli spettri dei due segnali, come prendere un violino pizzicato e faccio l'impulso, prendo l'archetto lo metto sul violino e genero un onda a dente di sega.

Con la convoluzione possiamo passare nel dominio della frequenza e poi fare il prodotto, moltiplicare due spettri sarà come fare la convoluzione.

![cc](cc.png)

La convoluzione sarà tra due spettri nel dominio della frequenza moltiplico le energie dei rispettivi bin:
![ccc](ccc.png)

Quando facciamo l'FFT abbiamo una componente reale ed una immaginaria, e avremo 4 moltiplicazioni, 2 somme e una differenza:
![diff](diff.png)

Passando alla rappresentazione polare basterà fare il prodotto dei moduli e la somma delle due fasi e potremmo avere 2 moltiplicazioni ed una somma, è per questo piú comodo passare alla rappresentazione polare.

Conviene quindi passare alle coordinate polari:
![dfre](dfre.png)

#### Esempio

![es](es.png)

![es1](es1.png)

Avremmo quindi fatto la convoluzione...

#### Cross-synthesis

Prendendo le ampiezze di un segnale e le fasi di un altro strumento realizziamo questa tipologia di sintesi.

#### Filtro FFT attraverso gen~

Avremmo quindi un filtraggio che va per bins

![filtro](filtro.png)

Trasformiamo i coefficienti di Fourier in coordinate polari e poi effettuiamo una moltiplicazione di coefficienti per le ampiezze dei bin, riuscendo cosí a realizzare un filtraggio.

In pfft abbiamo:
![pfft](pfft.png)

![filtro1](filtro1.png)

## Trasformata di Hilbert

![hilbert](hilbert.png)

Abbiamo una trasformata che rimane nello stesso ambito, non passiamo dal dominio del tempo al dominio della frequenza, ma rimaniamo sempre nel dominio del tempo.

Trasformiamo quindi il segnale da reale a complesso.

![phs](phs.png)

Potremmo scrivere l'uscita come una parte reale, ovvero detta in fase e la lettera Q, ovvero in quadratura.

![phase](phase.png)

Vedremo che dovremmo moltiplicare il segnale complesso sarà praticamente dividere le basi con le potenze separate rispetto a prima.

Vediamo che la trasformata di Hilbert puó essere comoda perchè possiamo avere un theta che trasli questo argomento.

Il prodotto dato dalla trasfromazione di Hilbert, sarà:
![hil](hil.png)

In grassetto la parte reale.

I e Q sono le componenti di parte reale e parte immaginaria.

![fres](fres.png)

La frequenza è una derivata della fase...

![hmax](hmax.png)

Vediamo che abbiamo realizzato uno shifter di 200 Hz e non abbiamo bisogno di filtri o altro e lo shifting me lo realizza la formula stessa.

>Posso quindi traslare il dominio della frequenza senza far accadere nulla di strano come succederebbe ad esempio con una RM.

Abbiamo una sorta di modulazione con una distorsione dello spettro ma con stessa quantità di distorsione differente per le parziali.

(multifonico e trasformata di Hilbert)

Un Phasor realizza un dente di sega dall'alto verso il basso, ma utilizziamo il faso da 0 ad 1 e quindi moltiplichiamo per -1 e sommiamo 1.

>Nella RM abbiamo le bande laterali, mentre in questo caso no!

![tr](tr.png)

Possiamo passare dal dominio complesso a quello discreto ed avremmo quindi:
![rt](rt.png)

Sommando questi valori, ottengo la parte immaginaria Q e la parte reale è prelevata.

## Time stretching

![ts](ts.png)

Invece di prendere le fasi di ogni bin faccio la derivata fra due fasi, ed osservo se vi sia stata in due bin la variazione e registro le ampiezze e non le fasi.

Quando risintetizzo, prendo i vari bins e ricalcolo le ampiezze.

Quando ho un segnale che varia nel tempo, mi accorgo che varia nel tempo se la fase cambia nel tempo.

Se riesco a decorrelare la fase dall'ampiezza posso ottenere lo stesso tipo di frequenza ma leggerla in modo differente, pur mantenendo la stessa ampiezza.

Posso ottenere lo stesso segnale in modo differente, ma non falsandone la forma.

Cambiando le frequenze cambierà il contenuto armonico e cambierà completamente lo spettro, e non mi cambierà il timbro.

Nel Phase Vocoder cambio la velocità di lettura, ma mantenendo lo stesso timbro.

## Predizione lineare

Predire il valore di una sequenza a partire dai valori precedenti.

![lpc](lpc.png)

Simile all'indice di correlazione che abbiamo osservato la scorsa settimana.

Possiamo prendere tutti i campioni precedenti del segnale, moltiplicandoli per un certo peso per predire il campione successivo.

![lpc1](lpc1.png)

Faccio la differenza tra valori reali e stimati, cercando il minimo per la funzione epsilon(n), dobbiamo risolvere un sistema lineare di p incognite:
![lpc2](lpc2.png)

Trascurando l'errore, riusciamo a fare una trasformazione...

Prendendo un timbro riusciamo a trovare consonanti e vocali facendo predizione lineare...

Vediamo alcune implementazioni in CSound...

## Trasformata di Walsh-Hadamard

Essa nasce per ottimizzare la trasformata di Fourier che si applica alle frequenze digitali e moltiplica i valori per 1 e -1:

![wh](wh.png)

Vediamo generiche funzioni di Walsh con i numeri che indicano numeri di attraversamento dello zero:
![wh1](wh1.png)

Abbiamo tante funzioni di Walsh moltiplicate per dei coefficienti con dei peso:
![wh2](wh2.png)

![wh3](wh3.png)

Prendiamo una funzione e la moltiplichiamo per 1 e -1 e pi realizzo un'antitrasaformata.

Posso utilizzare lo stesso algoritmo per ottimizzare il codice.

Le matrici con soli 1 e -1 vengono dette matrici di Hadamard.

![wh4](wh4.png)

### Applicazioni

![wh5](wh5.png)

## Trasformata Wavelet

Facendo l'analisi di Fourier pensiamo alle sinusoidi di lunghezza infinita, mentre nella Wavelet, prendiamo un'onda madre e scaliamo le ondicelle per farle adattare ad un segnale in ingresso.

Le ondicelle sono definite nel dominio del tempo e non nel dominio della frequenza, vedremo il segnale e come esse si adattano(vedere i vieo su youtube).

Questa trasformata è molto rapida e meno onerosa ed ha un costo computazionale O(N).

## Conclusioni

Queste trasformate sono degli elementi utili per poter trasformare e utilizzare i segnali:
- Fourier
- Walsh Hadamard
- Wavelet

(Spazio L2 è lo spazio delle funzioni che abbiamo trattato)

## Per l'esame

Portare uno degli argomenti approfonditi con un esempio pratico.

Sentire il maestro via email.

## Links

[Esempio di FFT in c](http://www.iowahills.com/FFTCode.html)
