# Appunti della lezione di mercoledì 30 giugno 2021

## Sommario

Vediamo una panoramica sulle guide d'onda in Max, e vediamo la scanned synthesis, legata alla sintesi per modelli fisici.

La scanned synthesis realizzabile per mezzo di matrici.

Oscillatore di Van Der Pol...

## Guide d'onda

Avevamo visto le guide d'onda ovvero l'idea della simulazione della risonanza per mezzo di linee di ritardo.

Realizziamo guide d'onda per mezzo della sequenza originaria ritardata in vari modi.

Avevamo parlato di filtri FIR e IIR, in cui avevamo guardato la sequenza in ingresso e la stessa sequenza era ritardata sia in feedforward che in feedback:
![sch](sch.png)

Vediamo come viene realizzata la guida d'onda alla base dell'algoritmo di Karplus-Strong.

### Guida d'onda in Max

La cosa piú semplice da simulare in Max è realizzarlo in `gen~` poichè possiamo scendere al ritardo di specifici campioni.

![go](go.png)

Ed abbiamo poi il blocco di eccitazione smussato:
![go1](go1.png)

Il decadimento è naturale perchè non vi è nessuno inviluppo.

Ora è come avessimo preso una corda e l'avessimo eccitata in un punto qualsiasi, ma sappiamo che la corda ha un'onda progressiva e regressiva che intervengono sul suono.

#### Guida d'onda con punto d'eccitazione differente

Sappiamo che utilizzando un filtro comb, possiamo realizzare un filtraggio con n riflessioni dell'impulso, ovvero il suono invece di arrivare all'istante x, esso arriverà all'istante x+delta, x+delta2 etc...

Mettendo un filtro comb tra l'eccitazione e `gen~` otteniamo un suono diverso, cambiando il tempo di ritardo del comb è come se io spostassi la sorgente di eccitazione sulla corda.

Cambiando la frequenza sarebbe come se allungassi o accorciassi la corda.

![com](com.png)

>Problema se cambio la corda ovvero la frequenza, cambia il rapporto tra comb e guida d'onda.

![c](c.png)

#### Spostamento d'eccitazione

Posso dividere la guida d'onda in varie parti e posso prendere il segnale sul punto in cui voglio che venga prelevato il segnale:
![ss](ss.png)

![spo](spo.png)

Abbiamo quindi altre linee di ritardo...

>Ciò ci è utile per capire dove prelevare il segnale di uscita.

![n](n.png)

Spezzando quindi lo spostamento di eccitazione possiamo avere risposte differenti...

Abbiamo un nuovo parametro `pos`:
![p](p.png)

### Disadattamento di impedenza-scattering junctions

Quando un'onda di energia si puó propagare da un mezzo ad un altro e si hanno due sezione di tubi diversi, di corda diversa etc... si crea disadattamento di impedenza.

L'impedenza è un'onda ha per passare da una parte di corda ad un'altra o da una parte di tubo ad un altro.

![di](di.png)

Possiamo simulare una giunzione e quindi un disadattamento di impedenza...

Il coefficiente di riflessione si puó esprimere come la differenza delle aree:
![a](a.png)

#### Giunzione della guida d'onda
![giu](giu.png)

Avevamo visto l'analogia di Maxwell...

La giunzione sulla guida d'onda possiamo osservarla per mezzo dell'analogia di Maxwell.

![ggo](ggo.png)

Se volessi capire la forza o la pressione in un punto specifico dovrei prendere il contributo di Vf e sommarci Vr, esse hanno fase opposta in generale.

In ogni punto del tubo posso andare a cercare il contributo di pressione diretta e di ritorno.

Se Vr=0 abbiamo Vx=Vf.

Avremmo quindi che:
![ggg](ggg.png)

Avendo onde stazionarie, vi saranno dei punti lungo x di interferenza costruttiva e di interferenza distruttiva.

Possiamo esprimere i punti di massimo ed i punti di minimo...

Possiamo quindi simulare la giunzione.

Sapendo che ingresso ed uscita presentano quindi Vf abbiamo una simulazione del disadattamento di impedenza:
![qu](qu.png)

Se volessi sapere quant'è la Vmax di una giunzione, prendo Vf, la moltiplico per 1+r e analgoamente ma con 1-r ottengo la pressione minima.

Questa simulazione della giunzione mi da l'idea di come allargare e stringere l'area di questo tubo per mandare avanti ed indietro una parte di energia.

>È importante partire dalla definizione del coefficiente di riflessione ed avere delle onde stazionarie.

#### Realizzazione della giunzione di guida d'onda in Max

![m](m.png)

Volta per volta prendiamo il coefficiente e quello all'istante precedente ed avremo tutte le formule, con K=R.

![ma](ma.png)

Vediamo una realizzazione di due giunzioni.

La tangente iperbolica non fa altro che normalizzare i valori.

Le tangenti iperboliche riescono a simulare un neurone, poichè il neurone scatta quando la somma dei pesi dei dendridi supera i valori di una tangente iperbolica.

Una sorta di condensatori a piú ingressi con autoregolazioni.

![sca](sca.png)

Dalle sole due giunzioni possiamo realizzare piú di due...

## Scanned synthesis

Essa è una sorta di deviazione della sintesi per modelli fisici.

Essa fu sviluppata molto da Max Mathews all'inizio degli anni 2000.

Essa modellizza un sistema complesso di masse e molle, è un'applicazione di modelli fisici tramite oscillatori:
![scn](scn.png)

Ad esempio potremmo avere 4 molle e 4 masse:
![scnd](scnd.png)

![mo](mo.png)

Se voglio collegare la massa 1 con la massa 2 la inserisco nella matrice:

![mmm](mmm.png)

Maggiore è il numero di 1, piú il sistema è complesso.

Basterà poco che il sistema oscilli tutto e porti a esplodere piú velocemente la matrice di connessione saranno quindi:
![cc](cc.png)

### Scanned synthesis in CSound

Per mezzo dell'opcode `scanu` e `scans`...

Per mezzo di questa scanned synthesis possiamo simulare un oscillatore ed avremo una serie i parametri legati al singolo oscillatore.

Vi è anche il punto in cui eccito la massa, con piú masse ciò diviene un qualcosa di complesso.

Su CSound si parla di colpo di eccitazione per mezzo di un martello, se abbiamo il colpo di eccitazione <0 sarebbe come avere la membrana deformata.

La GEN23 serve per riprodurre la matrice:
![scaa](scaa.png)

Leggeremo tutti i valori e sapremo a quale oscillatore è connesso il sistema massa molla.

Gli opcode con la parola chiave `scan` vengono usati per la scanned synthesis.

`scanu` legge la matrice, `scans` genera il suono.

#### `scanu`

![scanu](scanu.png)

Ogni massa puó avere piú di una molla attaccata.

Sia la velocità, sia la massa, i numeri vanno dati per righe successive.

![scanu1](scanu1.png)

![scanu2](scanu2.png)

![scanu3](scanu3.png)

`id` è l'identificativo della tabella generata da `scanu`...

[`scanu` in CSound](https://www.csounds.com/manual/html/scanu.html)

#### `scans`

Abbiamo tutti i parametri che descrivono le connessioni...

![scans](scans.png)

Abbiamo quindi una definizione:
- del sistema di connessione
- materiale con cui è realizzato il sistema

![scsc](scsc.png)

[`scans` in CSound](https://www.csounds.com/manual/html/scans.html)

#### Esempio

![es](es.png)

Nell'esempio `a0=0` indica che non vi è eccitazione oltre le bacchette.

Abbiamo le matrici per descrivere tutti i valori:
![va](va.png)

`-23` significa leggo da file valori non riscalati.

La stringa connette una molla all'altra in serie.

### Variazioni su tema

![geca](geca.png)

Possiamo riempire la matrice in modo intelligente:
![ge](ge.png)

## Sintesi modale

Opcode di Csound `mode` che integra i sistemi di massa e molla simili a quelli introdotti a mano.

[`mode` in CSound](http://www.csounds.com/manual/html/mode.html)

Abbiamo realizzato le equazioni precedentemente mentre questo opcode semplifica i modelli.

## Oscillatori a dinamica non lineare

L'oscillatore di Van Der Pol descrive un comportamento come una valvola.

Van Der Pol scrive un'equazione differenziale:
![vdp](vdp.png)

Mu è un termine di riscalamento...

Al posto di mettere il termine che abbiamo visto prima degli oscillatori, viene associato il termine di Van Der Pol...

![vdpp](vdpp.png)

Abbiamo che dal punto di vista spettrale succede qualcosa con questo oscillatore e cambiando la frequenza di eccitazione cambiano le fasi e quindi cambierà il suono prodotto.

Si puó andare da un comportamento sincrono ad asincrono.

>L'oscillatore di Van Der Pol è autosostenuto e si auto alimenta rispetto ad altre tipologie di oscillatori...
____

Integrare fra loro i vari modelli fisici e provare quelli visti fino ad ora.
