# Appunti della lezione di mercoledì 9 giugno 2021


Avevamo visto sostanzialmente dei modelli come l'oscillatore armonico e l'aspetto legato all'attrito ovvero il damping legato alla velocità, usando la modellizzazione di Eulero, mettendo l'accellerazione calcoliamo la velocità.

Pensando che l'accellerazione sia l'incremento o deceremento della velocità nel tempo.

Abbiamo visto dei risuonatori che decadono in modo lineare, abbiamo realizzato degli esperimenti con delle oscillazioni e inserendo un treno di impulsi.

## Pianeta in un sistema binario
Simuliamo un sistema di stelle con 2 pianeti...

In un sistema del genere do le due coordinate:
![bi](bi.png)

![2s](2s.png)

Ora vediamo come dobbiamo considerare la forza e l'accellerazione":

![te](te.png)

![t](t.png)

### Implementazione in CSound

![s](s.png)

![s1](s1.png)
![s2](s2.png)

Un sistema del genere puó essere utile per regolare ad esempio il vibrato di un sintetizzatore, mettendolo sul controllo di un FM per l'indice di modulazione e controllare il rapporto portante/modulante.

![fm](fm.png)

Cambiando le masse e le distanze cambia di molto...

![1](1.png)

## Guide d'onda

Esse sono un modello che descrive il comportamento di un'onda in un mezzo di propagazione:
![go](go.png)

D'Alambert scrive un modello matematico per scrive questa modellizzazione.

La variazione di perturbazione lungo l'asse delle x ha una relazione con il tempo e con l'ampiezza:
![go1](go1.png)

Abbiamo quindi una fotografia della corda all'istante t.

La variazione della funzione lungo x e lungo t è uguale alla variazione rispetto al tempo.

Con un coefficiente 1/c^2...

Normalmente nella corda abbiamo un'equazione con asse x e tempo.

Per una membrana dovremmo avere 3 incognite.

![n](n.png)

Abbiamo dei punti fissi che sono i nodi e gli altri punti si muovono sempre.

Nel caso delle membrane abbiamo un comportamento che genera una serie di frequenze:
![m](m.png)

[Guide d'onda bidimensionali in Falstad](https://www.falstad.com/circosc/)

Il risultato sarà l'interferenza fra le armoniche con configurazioni risolte dalla formula:
![fo](fo.png)

La frequenza f(0,1) è dovuta a una circonferenza nodale e 0 diametri nodali.

Abbiamo le funzioni di Bessel che servivano per identificare per mezzo di un certo indice di modulazione che forma d'onda realizzavamo.

I nodi della membrana sono gli zeri delle funzioni di Bessel, ovvero dove la funzione incontra l'asse delle x.

![fb](fb.png)

Abbiamo tutti valori non multipli e non avremo mai armonici.

Ciò che succede è la frequenza che ci da il risuonatore al di sotto della membrana con alcuni modi legati alla membrana.

![tim](tim.png)

Abbiamo l'altoparlante che funzionerà allo stesso modo con 2 frequenze:
- frequenza del cestello -> risuonatore
- frequenza di brekup -> frequenza con cui eccitiamo il cono dell'altoparlante è maggiore di j(0,1)

***

## Simulazione delle guide d'onda

Risolvere un'equazione differenziale è un qualcosa di molto complesso...

Usiamo una guida d'onda con uno o piú ritardi ed uno e piú filtri.

Una guida d'onda elementare e1 composta da:
- delay
- filtro
- feedback

![el](el.png)

I vincoli meccanici fanno in modo che una parte dell'energia viene trasferita nel mezzo e un'altra parte torna indietro realizzando le onde stazionarie.

Il filtro modellizza la dispersione di energia per rifrazione dell'energia della perturbazione.

Usiamo il filtro passa basso come per il modello di Moorer:
![ele](ele.png)

Abbiamo ogni tanto modelli con moltiplicazione sull'anello di retroazione:
![ele1](ele1.png)

Anche il delay contribuisce e dobbiamo tenere conto che facciamo un ritardo ma non di un solo campione, ma di più campioni.

![sim](sim.png)

### Eccitazione

![sim1](sim1.png)

Eccitiamo la guida d'onda con un'onda triangolare, come se modificassi in qualche modo un'eccitazione...

### DC OFFSET

![dc](dc.png)

I filtri usati potrebbero generare una componente in continua, ovvero un valore costante dal punto di vista della sequenza numerica.

Se tutti i bit vanno ad 1, la guida d'onda esploderà.

Sarà utile usare dispositivi che bloccano la corrente continua.

### Implementazione

![impl](impl.png)

Osserviamo che riduciamo di 15 campioni per compensare il ritardo di 15 secondi del campione.

![impl1](impl1.png)

Usiamo 3 dc blocker perchè ogni componente deve realizzare :
![impl2](impl2.png)

Abbiamo visto una guida d'onda che ricorda molto il modello fisico di un basso.

***

La prossima volta realizzeremo il disadattamento di impedenza e modello del flauto...
