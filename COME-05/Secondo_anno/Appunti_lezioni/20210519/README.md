# Appunti della lezione di mercoledì 19 maggio 2021

Andiamo avanti con i modelli fisici con l'implementazione su `gen~`.

E poi vedremo gli attrattori di Lorenz etc...

***

## Sistema massa-molla

Abbiamo iniziato ad implementare i sistemi massa-molla, vedendo che le posizioni relative della particella, se sottoposte ad una forza o accellerazione, rispetto ad un sistema di riferimento, con un vincolo, la massa oscillava.

Abbiamo implementato poi su CSound questo aspetto.

Abbiamo preso questa implementazione e lo implementiamo sia in CSound che in Max.

Un sistema di questo tipo possiamo pensarlo anche come un risuonatore possibile.

Le casse armoniche sono realizzate da molti risonatori, come i risuonatori di Helmoltoz, ovvero un filtro del secondo ordine, in cui abbiamo però una massa ed una molla.

![vo](vo.png)

### Oscillatore armonico

Consideriamo il sistema massa-molla come possibile modello a cui ricondurre tutti i modelli che oscillano:
![ar](ar.png)

### Come studiamo un sistema massa-molla?

Per mezzo della seconda legge della dinamica e la forza che sposta la molla è proporzionale a quanto io sposto il sistema nella posizione di equilibrio, quindi la somma delle forze è uguale a -kx, perchè la molla si oppone alla forza che lo sta perturbando.

Per questo mettiamo il segno - per sottolineare il verso differente fra la forza che agisce...

Abbiamo una formula particolare in cui abbiamo la forza _f_ descritta come massa per accellerazione(dipenderà la forza dal peso e dalla accellerazione nel tempo).

Abbiamo la grandezza x ed il suo spostamento orizzontale, ed avremo un equazione differenziale del secondo ordine realizzate in questo modo, provocano un'oscillazione:
![mm](mm.png)

Invece di contarla come tempo continuo, ma come tempo discreto, allora se esprimo _a_ in termini di velocità, mi chiedo che tempo sia passato da _a_ a _v2-v1_, e posso ricavare la velocità nell'istante subito successivo a quello in cui sono ovvero _v2_.

Potremmo analogamente calcolarci la seconda posizione nel sistema massa molla _x2_ utile per calcolarci la posizione del sistema massa-molla successiva alla prima posizione:
![pos](pos.png)

![se](se.png)

Quindi avremo che:

![ra](ra.png)

Sapremo che omega=2pigrecof
![ar](ar.png)


>L'oscillatore armonico è direttamente proporzionale alla costante di elasticità(rigidità della molla) e inversamente proporzionale alla massa.

#### Cedevolezza

Piú una molla è cedevole, piú è lenta la vibrazione, prendendo una lastra molto lunga, piú essa è rigida più vibra a frequenze piú acute.

La rigidità è legata alla cedevolezza:
![c](c.png)


>Ma è importante ricordarsi quindi che la velocità all'istante successivo è uguale all'accellerazione piú la velocità all'istante precedente.

![acc](acc.png)

Posso prevedere all'istante successivo quanto avremo...

### Implementazione su CSound

![cc](cc.png)

Vediamo le condizioni al contorno secondo la forma vista precedentemente...

Memorizzo l'istante precedente con init, abbiamo la velocità iniziale e il punto da cui partiamo, mandiamo x in uscita e vedo l'accellerazione...

Abbiamo una sorta di fattore _c_ utile per capire il pendolo:

![fa](fa.png)

Avremo quindi una formula che tiene conto di questo fattore ed una frequenza normalizzata alla frequenza di campionamento:

![di](di.png)

### Implementazione in `gen~`

Avevamo visto che v2 è uguale ad una costante piú v1, e su max possiamo realizzare uno schema di questo tipo:

Abbiamo un ingresso sommato a se stesso nella posizione successiva, avremo quindi:
![max](max.png)

Realizzo in Max ciò che abbiamo visto con l'oggetto `history`, con il ritardo di un campione, se volessimo più di un campione potremmo realizzarlo con `delay`...

Abbiamo `history` che si ricorda del campione e avremo l'oggetto `feedback` che serve per controllare la retroazione.

#### Integrazione in matematica

In analisi abbiamo quindi un integrazione, ovvero per sommare dei contributi di funzione.

![int](int.png)

Vediamo che abbiamo sopra una costante e sotto una sorta di parabola con t^2.

Se nel mio feedback cambiassi il valore di feedback cambierei l'ampiezza della parabola.

La componente in continua si può eliminare con `svf~` o con `dcblock~`.

Integrare significa smussare le freuquenze acute:
![smu](smu.png)

![inte](inte.png)

Vediamo che moltiplichiamo per una costante per non andare fuori range.

![cb](cb.png)

Vediamo che possiamo realizzarlo anche in CodeBox, sommando insieme l'ingresso piú il prodotto...

#### Implementiamo il sistema massa-molla come visto in CSound

Avevamo visto prima però la velocità e lo spazio, quindi:
- prima integrazione per trovare la velocità
- secondo integrazione che dalla velocità mi torna alla posizione

![intd](intd.png)

Avremmo quindi:

![gru](gru.png)

Non avremo altre forze esterne, e avremo che continuerà ad oscillare in base al coefficiente, e metteremo:
- valore uguale alla Forza
- massa
- k
- coefficiente

Implementiamo in Max
![coe](coe.png)

Realizziamo il primo integratore e poi il secondo come descritto, e vediamo cambiando i parametri cosa succede...

![filtr](filtr.png)

Possiamo pensare ciò, anche come un risuonatore...

Cambiando l'elasticità cambieremo la frequenza...

***
Cambiamo in questo caso il k, per poterlo esprimere secondo la frequneza di campionamento:
![sr](sr.png)
***

Provare a realizzarlo senza guardare le slides.

### Smorzamento in CSound

Lo Smorzamento in CSound è legato all'attrito, e dovremmo simularlo diminuendo la velocità di un fattore 1-d:

![smo](smo.png)

Ogni volta avremo uno spostamento e un'uscita sempre piú piccola, smorzandosi fino a smussarsi.

### Riepilogo sul modello fisico dell'oscillatore armonico

Il modello fisico di un oscillatore armonico si comporta come un filtro del secondo ordine:
![rie](rie.png)

Potremmo ad esempio cambiare la massa della molla nel tempo...

## Sistema Karplus-Strong

Vi sono modelli fisici risuonatore->oscillatore e modelli risuonatore->eccitatore.

Essi sono quindi modelli che funzioano:
- in feed forward
- se vi è una qualche energia intorno all'eccitatore abbiamo un feedback

![modelli](modelli.png)

Ad esempio suonando un violino con un archetto, l'arco avrà un certo attrito sulla corda, ed allora trascinando la corda avremo un'interazione continua tra corda ed archetto, invece di eccitare la corda con un impulso, la ecciterò con un dente di sega...

![violino](violino.png)

Potrei inventarmi un arpa virtuale con 100 corde e avremo quindi come la cordiera di un pianoforte che simula un arpa.

Il modello Karplus-Strong è un modello eccitatore-risuonatore:
![ks](ks.png)

Vediamo che MSP non permette il loop, e dovremo quindi realizzare lo strumento in `gen~`:
![kss](kss.png)

Posso simulare lo smorzamento della frequenza per mezzo di un filtro passa basso per far smorzare prima le frequenze più acute rispetto a quelle piú gravi:
![mix](mix.png)

## Modello fisico di uno strumento ad arco e modellizzazione di un semplice arco...

![arco](arco.png)

Oggetto reson scritto con gen:
![reson](reson.png)

Realizziamo quindi un'implementazione di `gen~`:
![risuo](risuo.png)

Vediamo l'implementazione di un onepole realizzato in questo modo:
![onepole](onepole.png)

Avremo quindi la parte della corda in alto, quindi nella sezione sottostante abbiamo il risuonatore:
![re](re.png)

Un oggetto `pass` serve a collezionare degli ingressi ed a distribuirli.

Possiamo quindi implementare lo strumento con una sorta di arco realizzato:
![cona](cona.png)

***
### Links della lezione

https://drive.google.com/file/d/1KXeU1-z7l47RlGCdQP6236FOMpa12bup/view

https://drive.google.com/file/d/1lAvu7OsPLafeu3MMBrjCRiZF_TH_Qeib/view



La dispensa è già nel drive::

https://drive.google.com/drive/u/0/folders/1xdDBv3WiJQxgkpC7G-ojqZDc4yyGJ-IX



***

La settimana prossima passiamo a parlare delle derivate e dei sistemi di attrattori.

Software che si chiama [PRAAT](https://www.fon.hum.uva.nl/praat/)

Vediamo il software Praat insieme a lezione!
