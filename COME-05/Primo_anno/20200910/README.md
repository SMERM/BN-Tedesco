# Appunti della lezione del 10 Settembre 2020

con segnali statici e semplici riesci a capire sull'allpass se c'è qualche spostamento di fase, in base al ritardo che diamo all'allpass

## Modelli di riverbero

Come fare una densità di echi per arrivare ad una certa densità di echi.
Cercavano un modo di moltiplicare gli echi(per arrivare a circa 1000 echi al secondo), l'introduzione di altri comb, avrebbe aumentato l'oscillazione armonica, con picchi che sono divisi per frequenza di risonanza e sue formanti.

Un comb in se è armonico, più insieme divengono inarmonici, nella risposta in frequenza non dovrei avere mai dei picchi piú alti.
Ci troviamo comunque in una situazione di estrema oscillazione risonante, ovvero una risposta in frequenza di questo tipo:
![risp_freq](risp_freq.png)

1. L'allpass permette di aumentare la densità in frequenza, poichè ha una risposta flat, come un micro delay che aumenta la densità in frequenza dei comb, le fasi sono molto mobili.
2. Ció confonde, amalgama e sparpaglia le fasi, bande frequenziali verranno mosse in modo randomico.
3. Prima cosa smussata da allpass, sono i transienti(anche dei picchi). Avendo picchi piú dolci: ![picchidolci](picchidolci.png)

## Allpass
Unione di 2 tipologie di comb, un comb feedback e comb feedforward, le risposte dei
![allpass](allpass.png)

La risposta del FIR comb ha solo valli, poichè ha diversi 0, disposti nel piano Z
![valli](valli.png)

IIR comb ha solo poli.
![poli](poli.png)
Unione di IIR comb, e FIR comb, ha in teoria una risposta flat, ovviamente peró avendo una retroazione ho una modifica delle fasi.

## Universal Comb Filter
Se mettiamo in modo adeguato due comb realizziamo 4 tipologie di filtri diversi, questa volta abbiamo però 3 coefficenti da poter modificare.

Nomi dei coefficienti: a, fb, ff
![ucf](ucf.png)
Costruiamo con una sola architettura 4 tipologie di filtri
- FIR COMB:
a=x, fb=0, ff=x
- IIR COMB:
a=1, fb=x, ff=0
- ALLPASS
a=x, fb,=-x, ff=1
- DELAY
a=0, fb=1, ff=1

### UCF in codebox

Algoritmo solo per poter realizzare 4 tipologie
```
Delay zeta(samplerate);
Xn = in1;
deltime= in2*(samplerate/1000);
fb= in3;
ff= in4;
alfa= in5;

//=============================

del1=zeta.read(deltime);

feedf= del1*ff;
feedb= del1*fb;

sum1=feedb+Xn;

alfac=sum1*alfa;

Yn= feedf+alfac;

out1= Yn;

zeta.write(sum1);
```

### Allpass in codebox

```
Delay zeta(samplerate);
Xn= in1;
deltime= in2*(samplerate/1000);
g= clamp(in3, 0.,0.999);
menog= -1*g;

//-----------------------

del1= zeta.read(deltime);
feedback= del1*menog;
sum1=feedback+Xn;
mul1= sum1*g;
Yn= del1+mul1;

	out1= Yn;

zeta.write(sum1);
```



[Playlist lezioni Pasquale](https://www.youtube.com/playlist?list=PLxcqYDjKDVLRVfkGh4HpOirz2w46nol8q )



## Riverberi
 Ci sono vari modelli di riverbero.

### Riverbero primordiale

![IMG_0535](IMG_0535.png)

Riverbero con 3 allpass, in serie e due in parallelo per destra e sinistra, il gain è unitario a tutti.

Riverbero allpass in codebox
```
require "filtrini";

//SERIE
allp1= allpaxx(in1, 49.96, in3);
allp2= allpaxx(allp1, 54.65, in3);
allp3= allpaxx(allp2, 24.18, in3);

//PARALLELO
allpL= allpaxx(allp3, 17.85, in3);
allpR= allpaxx(allp3, 17.95, in3);

out1= allpL;
out2= allpR;
```

## Riverbero primordiale di Schroeder
![img724_2x.png](img724_2x.png)


## Variante di Chowning

![img711_2x.png](img711_2x.png)

Matrici per la variante di Chowning

## Riverbero realizzato da noi
Con schema per ogni canale

Calcolo del primo comb

20 ms per 7 metri

![riv_arb1](riv_arb1.png)

![riv_arb2](riv_arb2.png)

Inziare a fare delle prove per trovare alternatice interessanti

## Calcolo del T60

Determinare un T60 in secondi e non da 0 a 1

La perdita del T60 è logaritmica, e dunque il tempo necessario per raggiungere il T60

La formula per calcolare g è:![formula_g](formula_g.png)

Quanto dobbiamo dare a g se abbiamo 20 ms al primo comb e un T60:
![T60](T60.png)

Dobbiamo mettere nel codice del comb ciò che ci calcola mettendo il T60

Dopo aver calcolato dal T60 la g la applichiamo al riverbero arbitrario
