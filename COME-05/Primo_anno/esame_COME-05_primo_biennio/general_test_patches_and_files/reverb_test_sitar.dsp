import("stdfaust.lib");

fb1 = hslider("fb1",0.8,.001,0.9998,0.001);
fb2 = hslider("fb2",0.8,.001,0.9998,0.001);
damp = hslider("damp",0.8,.001,0.9998,0.001);
spread = hslider("spread",128,0,4096,1.);
drive = hslider("drive",0.1,.001,0.9998,0.001);
offset = 0;
maxdel = 2048;
intdel = 1024;
aN = 0;

process = _ : ef.cubicnl(drive,offset)<: _,_ : re.stereo_freeverb(fb1, fb2, damp, spread) : fi.allpass_comb(maxdel,intdel,aN),fi.allpass_comb(maxdel,intdel,aN) : _,_;
