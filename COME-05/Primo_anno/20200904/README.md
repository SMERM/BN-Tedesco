# Appunti della lezione di Venerdí 5 Settembre 2020

## Esame di Informatica Musicale

Oltre a spiegare come si è fatto qualcosa, interessante applicare tutto questo ad una composizione. Ulteriore peso con composizioni che prendano come punto di partenza, l'organizzazione spaziale, e la relazione tra quello che scrivi e come si comporta nello spazio.

Applicazione pratica e non solo teorica.
Perchè costruire un riverbero? Esigenze che si hanno che non possono riprendere nello spazio. -> relazioni di spazi e ambienti.


- prima parte propedeutica -> ho costruito questo, questo e questo(2/3 spazi), parte teorica
- seconda parte pratica -> applicazione pratica ad una composizione -> come strutturarla? che tipo di elaborazione dello stadio tra spazio e suono e come viene veicolato il suono? partiamo dall'idea di spazio prima dell'idea del materiale!

_Relazione molto stretta tra lo spazio e la composizione_

**l'ambiente è lo strumento che prende il suono degli altri strumenti e li porta al pubblico**

_non esiste uno studio per strumento solo_ -> poichè serve lo spazio ed esso cambia totalmente il suono

Pezzi di Celibidache sono molto lenti -> perchè dipende dal luogo e da come suona il tutto

________

Lo spazio modifica il timbro, le altezza, le ampiezze, il ritmo

Marce:
1. militari -> tema e ritmica fissa
2. sinfoniche -> anche elaborate

Il primo clarinetto davanti decideva che marcia suonare

Le marce sinfoniche si facevano in vicoli piccoli e lunghi, posti risonanti, in piazza si disperderebbe

In piazza marce militari

Celibidache odiava le registrazioni, poichè l'acustica era diversa

Pezzo di luciano Berio _Differences_ mettere in relazioni differenze e consonanze tra ció che c'è dal vivo e registrato, quello che succede in sala, è che determinate cose si fondono ed altre si dividono, _Mnemosin_ di Ferneyough, _Dialogue_ di Boulez, _Sofferte onde serene_ di Nono; ovvero tutti quei pezzi tra strumento e ombra dello stesso.

________________

1. lo spazio è imprescindibile
2. lo spazio creato da noi è imprescindibile

Ciò determina le scelte, e dato che lo spazio è sintetico, possiamo cambiare anche lo spazio intorno

Non c'è dunque un'idea di naturalezza, ma un'idea di composizione

Perchè piaceva di piú la sintesi sottrativa di quella additiva -> poichè da un rumore filtrandolo -> avrò delle frequenze che oscilleranno piú di una sintesi additiva -> fai ció con filtraggi e basta e si muove già di suo

Chowning quando arriva a studiare l'FM capisce che i suoni naturali sono sempre in movimento, dunque anche per i suoni sintetici per renderli interessanti ci devono essere diverse sfumature.

Paricolarità granulazione è che nessun tipo di suono è continuo e fisso che è fatto da milioni di particelle che si muovono fra loro, possiamo dunque creare dei suoni pseudo-acustici partendo da basi di microsuoni.

- esempio -> scomposizione in particelle di un suono fisico -> analizzo con FFT in bande -> granulazione è un sezionamento nel tempo

1. prima lo analizzo in FFT
2. poi granulo le armoniche -> dividendo nello spettro spaziale le componenti armoniche del suono
________________
## Allpass

Noi stiamo costruendo unità riverberatrici, quella più semplice è realizzata da un filtro comb

Le unità piú complesse sono costruite da comb+lowpass nella retroazione

Avere un'apertura spaziale

allpass ha 2 ruoli nella catena della riverberazione digitale:
1. muovere la fase, a seconda del tempo di ritardo, con rotazione di fase diverse a frequenze diverse(con 2 o 3 fasi in parallelo perdiamo un po' la cognizione di come muove le fasi)
2. smussa i transienti, delle cose un minimo dunque le abbassa di intensità, specialmente sui transienti, una cosa che si smussa di piú sono i picchi del comb

________

Implementiamo la variante di Moore del filtro comb, ovvero un comb con lowpass

![LPCF](LPCF.png)

________

è essenziale capire quali ritardi mettere e mettere dunque i ritardi giusti

I ritardi saranno sempre fissi poichè vai a modificare il tempo di riverberazione

Il nostro g è:

![formula_g](formula_g.png)

dove d è il delay che diamo al comb e T60 è il tempo di riverberazione che noi mettiamo

quando tocchiamo g, stiamo toccando il T60

![g_calcolo](g_calcolo.png)
Prossima volta vediamo il T60

L'allpass viene realizzato con 2 comb sovrapposti che vanno uno a interferire con l'altro

![allpass](allpass_primoord.png)

è una legenda che l'allpass non colori, dato che è costruito con 2 comb, ci potrebbe servire un oggetto solo con comb e allpass, _Universal comb filter_ ovvero un doppio comb

- allpass
- conversione T60 a gain
- early reaflections -> multi tap delay con lp

![universal_comb_filter](universal_comb_filter.png)

![universal_comb_filter_gen](universal_comb_filter_gen.png)

-> FDN -> Feedback Delay Network
