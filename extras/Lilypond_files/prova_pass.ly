\version "2.19.80"

 \relative c'  % do centrale come punto di riferimento
 { 
  \clef  treble %chiave
  \time 134/64 % tempo
  c4 d e f 		% quattro note: do 1/4, re, mi, fa (quando non scrivi il valore, resta invariato l'ultimo)
  c8 d e f g a b c
  c2 c4 c8 c16 c 32 c 64 c128
  }
  
  
   \relative c % do ottava sotto il do centrale come punto di riferimento
 { 
  \clef bass
  \time 2/10
  c8 d 	e f 
  c d [	e f]
  c[ d	e f]
  c[d]	e[f]
  }
  
  
     \relative c' % do ottava sotto il do centrale come punto di riferimento
 { 
  \clef treble
  \time 2/4
  <c e>4			<c e g bes c>4
  <c c' c' c'>4 		r4
  }