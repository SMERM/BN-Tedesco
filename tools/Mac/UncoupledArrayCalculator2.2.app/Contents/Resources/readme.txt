## Uncoupled Array Calculator 2.01 ##
## 13.05.05
## Created by Daniel Lundberg
## daniel@lundbergsound.com
#########################################################################################
This program calculates parameters for uncoupled loudspeaker arrays for sound reinforcement.

These arrays are common on stage lips, under theatre balconies, on parade routes, and in similar applications, and have four primary inter-element parameters: loudspeaker spacing, loudspeaker splay angle, the angular coverage afforded by each individual loudspeaker, and the distance from the loudspeakers to the listeners.

For any three of the above parameters, this software calculates the fourth. It also computes quantity of loudspeakers necessary given a length of audience plane, or vice versa. It then creates a graphical representation of the array described in those parameters.

One additional calculated parameter is DMax: the distance at which audience members are necessarily in the coverage pattern of three or more loudspeakers, where destructive acoustical interference can be insurmountable.

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
TO RUN: Use a python launcher to open UncoupledArrayCaculator2.py, e.g., go into terminal, type 'python', followed by a space, followed by the file path of UncoupledArrayCalculator2.py, and hit 'return'.
/////////////////////////////////////////////////////////////////////////////////////////

For more information about uncoupled arrays, see: 
http://bobmccarthy.wordpress.com/2010/03/28/uncoupled-array-design-beginnings-and-endings/

For information about the math, see:
http://www.lundbergsound.com/?p=130