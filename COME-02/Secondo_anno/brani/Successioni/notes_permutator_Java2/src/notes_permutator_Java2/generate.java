package notes_permutator_Java2;

//https://www.baeldung.com/java-combinations-algorithm

public List<int[]> generate(int n, int r) {
	List<int[]> combinations = new ArrayList<>();
	int[] combination = new int[r];

	// initialize with lowest lexicographic combination
	for (int i = 0; i < r; i++) {
		combination[i] = i;
	}

	while (combination[r - 1] < n) {
		combinations.add(combination.clone());

		// generate next combination in lexicographic order
		int t = r - 1;
		while (t != 0 && combination[t] == n - r + t) {
			t--;
		}
		combination[t]++;
		for (int i = t + 1; i < r; i++) {
			combination[i] = combination[i - 1] + 1;
		}
	}

	return combinations;
}
}