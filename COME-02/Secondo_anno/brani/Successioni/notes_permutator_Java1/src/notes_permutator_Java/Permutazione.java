package notes_permutator_Java;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class Permutazione {
	static final private String[] elencoAltezze = {" A "," A# "," B "," C "," C# "," D ", " D# ", " E ", " F ", " F# ", " G ", " G# ", " pausa "};

	//private Nota[] permutazione;

	public String[] permutatore(String[] a) {
		int lun = a.length;
		Random rand = new Random();

		String[] copiaAltezze = Arrays.copyOf(a, lun);
		Collections.shuffle(Arrays.asList(copiaAltezze));



		return copiaAltezze;
	}



	public static void main(String[] argc) {
		Permutazione p = new Permutazione();

		System.out.println(Arrays.toString(p.permutatore(elencoAltezze)));	
	}
}