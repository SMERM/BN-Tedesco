package notes_permutator_Java;

public class Nota {
	private String altezza;
	private int durata;
	private String dinamica;
	private String timbro;
	
	public String getAltezza() {
		return altezza;
	}
	public void setAltezza(String altezza) {
		this.altezza = altezza;
	}
	public int getDurata() {
		return durata;
	}
	public void setDurata(int durata) {
		this.durata = durata;
	}
	public String getDinamica() {
		return dinamica;
	}
	public void setDinamica(String dinamica) {
		this.dinamica = dinamica;
	}
	public String getTimbro() {
		return timbro;
	}
	public void setTimbro(String timbro) {
		this.timbro = timbro;
	}


}