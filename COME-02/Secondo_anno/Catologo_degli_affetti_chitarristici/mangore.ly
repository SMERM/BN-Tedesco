%\version "2.19.83 "

\header {
  title = "Preludio en Do menor"
  subtitle = "(San Salvador Octubre 1940)"
  composer = "Agustín Pío Barrios (»Mangoré«) (1885 – 1944)"
  poet = "Trascritto da Davide Tedesco"
  meter = "		Adagio"
  tagline = ""
}
\score {
  \new Staff {
    \clef "treble_8"
    \time 2/4
    \key c \minor
    \set Score.currentBarNumber = #1
     
    \tuplet 6/4 {c 16 g 16 aes' 16 ees' 16 g' 16 g  16}
    \tuplet 6/4 {c 16 g 16 aes' 16 ees' 16 g' 16 g  16}

    \tuplet 6/4 {bes, 16 g 16 aes' 16 d' 16 g' 16 g  16}
    \tuplet 6/4 {bes, 16 g 16 aes' 16 d' 16 g' 16 g  16}


    \tuplet 6/4 {d 16 g 16 g' 16 bes 16 f' 16 g  16}
    \tuplet 6/4 {d 16 g 16 g' 16 bes 16 f' 16 g  16}


    \tuplet 6/4 {c 16 g 16 f' 16 c' 16 ees' 16 g  16}
    \tuplet 6/4 {c 16 g 16 f' 16 c' 16 ees' 16 g  16}
    
    \tuplet 6/4 {bes, 16 g 16 f' 16 c' 16 ees' 16 g  16}
    \tuplet 6/4 {bes, 16 g 16 f' 16 c' 16 ees' 16 g  16}
    
    \tuplet 6/4 {a, 16 fis 16 ees' 16 c' 16 d' 16 fis  16}
    \tuplet 6/4 {a, 16 fis 16 ees' 16 c' 16 d' 16 fis 16}
    
    \tuplet 6/4 {aes, 16 f 16 ees' 16 c' 16 d' 16 f 16}
    \tuplet 6/4 {aes, 16 f 16 ees' 16 c' 16 d' 16 f 16}

    \tuplet 6/4 {g, 16 g 16 ees' 16 c' 16 d' 16 g 16}
    \tuplet 6/4 {g, 16 g 16 ees' 16 c' 16 d' 16 g 16}

    \tuplet 6/4 {g, 16 f 16 ees' 16 c' 16 d' 16 f  16}
    \tuplet 6/4 {g, 16 f 16 ees' 16 b 16 d' 16 f  16}
    
    \tuplet 6/4 {g, 16 ees 16 d' 16 g 16 c' 16 ees  16}
    \tuplet 6/4 {g, 16 ees 16 d' 16 g 16 c' 16 ees  16}
    
    \tuplet 6/4 {g, 16 d 16 c' 16 f 16 bes 16 d  16}
    \tuplet 6/4 {g, 16 d 16 c' 16 f 16 bes 16 d  16}

    \tuplet 6/4 {g, 16 c 16 bes 16 ees 16 aes 16 c  16}
    \tuplet 6/4 {g, 16 c 16 bes 16 ees 16 aes 16 c  16}

    \tuplet 6/4 {g, 16 b, 16 aes 16 d 16 g 16 b,  16}
    \tuplet 6/4 {g, 16 b, 16 aes 16 d 16 g 16 b,  16}

    \tuplet 6/4 {f, 16 d 16 d' 16 g 16 b 16 d  16}
    \tuplet 6/4 {f, 16 d 16 d' 16 g 16 b 16 d  16}

    \tuplet 6/4 {e, 16 g 16 g' 16 bes 16 des' 16 g  16}
    \tuplet 6/4 {e, 16 g 16 g' 16 bes 16 des' 16 g  16}

    \tuplet 6/4 {f, 16 aes 16 g' 16 c 16 f' 16 aes  16}
    \tuplet 6/4 {f 16 aes 16 g' 16 c 16 f' 16 aes  16}
    
    \tuplet 6/4 {ees 16 aes 16 aes' 16 c' 16 f' 16 aes  16}
    \tuplet 6/4 {ees 16 aes 16 aes' 16 c' 16 f' 16 aes  16}

    \tuplet 6/4 {d 16 f' 16 ees'' 16 aes' 16 c'' 16 f'  16}
    \tuplet 6/4 {d 16 f' 16 ees'' 16 aes' 16 c'' 16 f'  16}
    
    \tuplet 6/4 {ees 16 ees' 16 des'' 16 g' 16 bes' 16 ees'  16}
    \tuplet 6/4 {ees 16 ees' 16 des'' 16 g' 16 bes' 16 ees'  16}

    \tuplet 6/4 {e 16 des' 16 des'' 16 g' 16 bes' 16 des'  16}
    \tuplet 6/4 {e 16 des' 16 des'' 16 g' 16 bes' 16 des'  16}

    \tuplet 6/4 {f 16 c' 16 bes' 16 f' 16 aes' 16 c'  16}
    \tuplet 6/4 {f 16 c' 16 bes' 16 f' 16 aes' 16 c'  16}

    \tuplet 6/4 {d 16 c' 16 c'' 16 f' 16 aes' 16 c'  16}
    \tuplet 6/4 {d 16 c' 16 c'' 16 f' 16 aes' 16 c'  16}

    \tuplet 6/4 {g 16 b 16 b' 16 f' 16 g' 16 b  16}
    \tuplet 6/4 {g 16 b 16 b' 16 f' 16 g' 16 b  16}

    \tuplet 6/4 {g 16 c' 16 c'' 16 ees' 16 g' 16 c'  16}
    \tuplet 6/4 {g 16 c' 16 c'' 16 ees' 16 g' 16 c'  16}

    \tuplet 6/4 {f 16 c' 16 c'' 16 ees' 16 aes' 16 c'  16}
    \tuplet 6/4 {f 16 c' 16 c'' 16 ees' 16 aes' 16 c'  16}
    
    \tuplet 6/4 {f 16 bes 16 bes' 16 d' 16 aes' 16 bes  16}
    \tuplet 6/4 {f 16 bes 16 bes' 16 d' 16 aes' 16 bes  16}
    
    \tuplet 6/4 {ees 16 bes 16 bes' 16 ees' 16 g' 16 bes  16}
    \tuplet 6/4 {ees 16 bes 16 bes' 16 ees' 16 g' 16 bes  16}

    \tuplet 6/4 {f 16 aes 16 aes' 16 des' 16 f' 16 aes  16}
    \tuplet 6/4 {f, 16 aes 16 aes' 16 des' 16 f 16 aes  16}

    \tuplet 6/4 {g, 16 g 16 g' 16 c' 16 ees' 16 g  16}
    \tuplet 6/4 {g, 16 g 16 g' 16 c' 16 ees' 16 g  16}

    \tuplet 6/4 {g, 16 g 16 f' 16 b 16 d' 16 g  16}
    \tuplet 6/4 {g, 16 g 16 ees' 16 b 16 d' 16 g  16}
    
    \tuplet 6/4 {c 16 ees 16 d' 16 g 16 c' 16 ees  16}
    \tuplet 6/4 {c 16 ees 16 ees' 16 g 16 c' 16 ees  16}

    \tuplet 6/4 {c 16 ees 16 ees' 16 g 16 c' 16 ees  16} c 4 \fermata \bar "|."



  }
}	
