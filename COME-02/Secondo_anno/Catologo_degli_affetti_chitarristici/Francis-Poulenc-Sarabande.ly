\version "2.19.83"

\header {
  dedication = "To Ida Presti"
  title = "Sarabande"
  instrument = "For Guitar"
  composer = "Francis Poulenc"
  meter = "Molto calmo e melanconico"
}

\relative c''{ \time 5/4 a8 b8 c4 b8 a8 b8 g8 e4}
{c8 d8 e4 c8 b8 a8 g8 b4}