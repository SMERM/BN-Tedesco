# Comandi utili e utilities utili ruby YAML files

`ruby -ryaml -e "p YAML.load(STDIN.read)" < catalogo.yml`

`ruby -r yaml -e 'YAML.load_file ARGV[0];puts "ok"' catalogo.yml`
[YAML validator](http://www.yamllint.com/)

[Validating YAML with Ruby](https://floatingoctothorpe.uk/2016/validating-yaml-with-ruby.html)

[Saving/Retrieving Data With a YAML file in Ruby: The Basics](https://medium.com/@kristenfletcherwilde/saving-retrieving-data-with-a-yaml-file-in-ruby-the-basics-e45232903d94)

[Learn TDD in Ruby in 5 easy steps](https://medium.com/@micosmin/learn-tdd-in-ruby-in-5-easy-steps-3ab28014fec4)
