# Appunti della lezione di Martedì 19 Gennaio 2021

## Riepilogo delle puntate precedenti

- Introduzione dello strumento a Pasquale

- Come realizzare degli studi?

- idea di fare uno studio di studi -> raccogliendo un tot di studi  in un database

- idea è fare citazioni messe ritagliate per bene o citazioni tagliare nettamente

- avevamo iniziato a vedere il parser, ma `lyv` non era stato utile

- come leggere le singole note

- si potrebbe usare il parser di Lilypond

__________

Parser prende un file .ly:

- parser prende file tutti uguali

- piú semplice ricercare nel file .xml o .midi che tira fuori Lilypond

[MusicXML](http://lilypond.org/doc/v2.19/input/regression/musicxml/collated-files.html)

[parser LISP](https://svenssonjoel.github.io/pages/lispbm_parsing_expressions/index.html)

### Caratteristiche del MusicXML

Descrizione di oggetti specializzanda per la musica

[](http://lilypond.org/doc/v2.19/input/regression/musicxml/MusicXML-TestSuite-0.1.zip)

`pip3 install python-ly`

`ly musicxml mangore.ly > mangore.xml`

__________

usare noko-giri -> legge tutto e si mette tutto in nodi -> esso fa la lettura brutale e la suddivisione in nodi -> e dobbiamo fare la distinzione tra le note

XML viene tutto catalogato secondo le Caratteristiche

- parser noko-giri
- proprietà che ci sono catalogate **poichè l'XML non è semantico**

__________

Costruire la gem in XML
