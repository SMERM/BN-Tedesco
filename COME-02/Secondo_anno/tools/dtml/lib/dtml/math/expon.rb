module Dtml
  module Math
    class Expon < Base
      def y(x)
        ::Math.exp(@afact*x+@bfact)
      end
    protected
      def setup()
        raise StandardError,"parametri non sufficienti" unless @parameters.size >= 4
        p=@parameters.map {|x| x.to_f}
        plog=p.map {|x| ::Math.log(x)}
        @afact=(plog[3]-plog[1])/(p[2]-p[0])
        @bfact=plog[1]-@afact*p[0]
      end
    end
  end
end
