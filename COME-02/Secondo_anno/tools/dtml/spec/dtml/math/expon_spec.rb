RSpec.describe Dtml::Math::Expon do
  before :example do
    @good_parmeters = [0.1,10,10,20]
    @expected_values = [5,14.092714193031645]
    @bad_parameters = ["wrong"]
    @eps = 1e-6
  end

  it "creates itself properly" do
    expect(Dtml::Math::Expon.new(@good_parmeters)).not_to be nil
  end
  it "recovers from errors" do
    expect{Dtml::Math::Expon.new(@bad_parmeters)}.to raise_error(StandardError)
  end
  it "works properly" do
    l=Dtml::Math::Expon.new(@good_parmeters)
    expect(l.y(@expected_values[0])).to be_within(@eps).of(@expected_values[1])
    expect(l.y(@good_parmeters[0])).to be_within(@eps).of(@good_parmeters[1])
    expect(l.y(@good_parmeters[2])).to be_within(@eps).of(@good_parmeters[3])
  end
end
