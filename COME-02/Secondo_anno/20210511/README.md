# Appunti della lezione di martedì 11 Maggio 2021

- [http://www.essl.at/works/sequitur/sequitur-8.html](http://www.essl.at/works/sequitur/sequitur-8.html)
- [http://www.essl.at/works/chemise.html](http://www.essl.at/works/chemise.html)
- parlare di Karlheinz Essl
- in attesa di altre esecuzioni con Francesco Ferracuti
- parte per la Successione 2 con una serie dodecafonica mutevole
- cosa e dove per la dodecafonia?
- ha senso usare le serie dodecafoniche oggi?
***
Github -> fork di Bernardini octave-ruby

Bug su CSound -> file mp3 aperto che non viene richiuso, l'opcode mp3in in CSound non chiude i file che apre.
***
## Ascolto di Sequitor VIII di Essl

Sequitur VIII non ha come Sequenza V una collezione di gesti iterati e ribatutti che fanno parte della retorica di un certo di scrittura.

Vinko Globokar con cui ha realizzato la Sequenza V...

(Sequenza acusmatica per Monica Vitti, Visage dedicata segretamente a Monica Vitti)

Il problema di Sequitor VIII sono i gesti inequivocabili che sono un segno, e poi vengono sfumati senza un senso.

(Studio del pianoforte -> studio dei calvicembalisti italiani, con clavicembalisti con brani inutili.)

(Compositori barocchi avevano delle posizioni e se li conosci sono citati nel Don Giovanni...)

## La musica da supermercato

Gli elementi come la scrittura di Essl, sono sempre molto utili, ma Berio stava culturalmente in un altro luogo.

## Gesti o non gesti

Se si fanno gesti, abbiamo dei segni che devono avere un senso.

Berio si chiese, cosa deve fare un trombone?

Fisso sul problema musicale, trasformando il problema del clown in una collezione di gesti musicali...

Trovato e sciolto il nodo del perchè scriverla, la sequenza V è tutta una trovata e affonda nel mondo dei gesti.

## Problema che abbiamo noi è il tempo per lo studio

Situazione diversa

## Serie e utilizzo della serie

Uso di una serie per dare alla parte seriale un onere...

Dare un onere alla serie ma non è cosí

### Serie di Nono

Utilizzo della serie a cuneo come nel Canto Sospeso.

Nono usa sempre la serie originale con quasi una cosa ideologica, contro il consumo bulimico della serie, sempre restringendo le possibilità.

Nella fabbrica illuminata la cantante deve rischiare di affogare nei suoni elettroacustici.

## Perchè esistono le serie?

Constatazione che faceva Schönberg, ovvero che se si sta in un contesto atonale, ciò che rimane sono le interazioni intervallari in brani come quello di Essl.

La relazione di ottava, è una relazione senza essere la stessa nota.

**Le serie servono a strutturare delle relazioni intervallari**, ovvero dei colori cromatici.

Invece di avere un quadro figurativo ed avere colori con un senso, i colori hanno un senso di per se.

Non è la struttura seriale, che fa la serie, ma come la si realizza.

## Approcciarsi alle serie

**Non fermarsi alla prima parte ovvero che si possano realizzare serie di tutto.**

- Berg
- Variazioni di Op.27
- Mode de valeurs et intesites di Messiaen
- Studiarsi Structures pour deux piano di Boulez, per capire il problema.

Boulez e Messiaen, avevano consapevolezza notevole...

Ascolto di Haas che ha un atteggiamento semi-seriale e semi-spettralista, con una sua visione moderna della serie.

(Il problema di Haas è che la maggior parte delle cose interessanti sono molto grandi, leggere [l'analisi sulle tensioni armoniche in Limited Approximations](https://www.academia.edu/41626026/Harmonic_tensions_in_limited_approximations_by_Georg_Friedrich_Haas))

In Haas la serie è un'arma in piú usata come linguaggio estetico, dato che veniamo dopo i manifesti culturali, dobbiamo porci nella trans-avanguardia, o nel post-modernismo, usando qualsiasi cosa come se fosse linguaggi, Haas è: microtonale, spettrale e seriale allo stesso tempo.

Gli spettralisti sono una forma di reazione, con un mondo che aveva la guerra fredda e si combatteva la guerra fredda che era sostanzialmente culturale.

## Guerra fredda e musica
A vedere la guerra fredda ora, chi ha vinto?

Le sinfonie di Shostakovic che fine hanno fatto?

## Cosa spetta a noi fare ora?

Riunire le cose, non in maniera ecumenica:
- cage
- surrealismo
- dadaismo

## Usare il serialismo per qualcosa come le altezze, e non sapere cosa farci in altri ambiti

La serie di Nono, in Canto Sospeso la serie è una tecnica e si puó usare...

## Il funzionamento delle serie con gli acrostici

Gli acrostici, per scrivere alle fidanzate, sono un messaggio incorporato all'interno di un messaggio e si fanno delle scelte per dare un layer in piú.

Si fa un certo tipo di scelte per ottenere una cosa che è sottesa nel messaggio con il colore cromatico.

## Interazione fra docenti e paper
[LEADERSHIP LAB: The Craft of Writing Effectively](https://youtu.be/vtIzMaLkCaM)

Scrivere per convincere le persone che non sono pagate per leggere o ascoltare ciò che tu fai, è un fatto diverso.

Maestri che devono contemperare due esigenze:
- è importante quello che si fa e quanto si fa per il ciclo didattico
- è importante individuare i problemi nella scrittura

### Che significa che la LIDL faccia il field recordings?

I supermercati esistono in rete e nell'esistenza in rete devono costantemente produrre contenuti.

Il fatto che esista una rete di comunicazione è perchè essa serve a comunicare qualcosa.

### Compositori come inventori di contenuti

In teoria noi siamo le pietre preziose del mondo della creazione.

Ciò che è importante è la riflessione che si riesce a produrre sulle cose.

>Piú si riesce ad analizzare, produrre, fare qualcosa, piú andiamo avanti.

### Cercare di liberarsi da tutti i limiti che abbiamo

***

Essere di manica larga con i voti in Conservatorio non aiuta a lungo andare...

***

## Cose di cui abbiamo parlato:

- [Sinfonia Turangalîla](https://it.wikipedia.org/wiki/Sinfonia_Turangal%C3%AEla)

- [Swedish Lidl released an album of field recordings from the supermarket](https://www.dummymag.com/news/swedish-lidl-field-recordings/)

- [Alfredo Bernardini, fratello di Nicola Bernardini, oboista barocco italiano](https://www.chigiana.org/alfredo-bernardini/)

- [Libro non libro](https://www.amazon.it/Non-libro-pi%C3%B9-disco-Audio/dp/8860872766)

- [Film il petroliere](https://it.wikipedia.org/wiki/Il_petroliere)

- Come funzionano i messaggi in Pure Data

- Esami e valutazione a Musica Elettronica

## Obiettivo ultimo del corso? Cosa manca?

Manca l'esecuzione dei brani.

Saggio finale della Scuola di Musica Elettronica

Confronto con l'organizzazione e la gestione di un'esecuzione in concerto.

***

## Con Pasquale

La struttura e dichiarazione degli intenti è solamente uno schema ad alto livello.

### Eco e il suo metodo di scrittura

Umberto Eco caratterizza i personaggi ed i luoghi, li [disegna](https://rep.repubblica.it/pwa/robinson/2020/05/13/news/il_metodo_eco_per_la_rosa-256413358/) e li descrive prima di scrivere.

![rosa](rosa.png)

Il processo di scrittura, una volta caratterizzati bene i personaggi, viene da se.

Come in _Nel nome della rosa_, nell'edizione del 2020 de la nave di teseo.

Gli elementi studiati prima della scrittura sono finiti di costruire come personaggi, poi vengono utilizzati per la scrittura.

### Mantra e le serie difettive

In Mantra manca qualcosa in tutte le serie che vi sono, che viene completato dall'ultima serie, con un procedimento che implica quindi delle serie difettive...

### Significante e relazioni

Il significante è quindi determinato dalle relazioni fra i vari personaggi di un discorso, con relazioni di altezze e tempi.

Ragionare sui punti e le relazioni(partendo dal concetto di suono-forma), puó portare ad esempio alla scelta di serie in cui vi sono elementi di altezze cosí disposti:
semitono -> intervallo ampio

### E il ritmo?

Dopo aver parlato delle serie, ci si chiede cosa renda il ritmo basico, e ciò lo vediamo nelle espressioni ritmiche legate a tutti i soggetti poco esposti alla composizione con ritmi piú avanzati, essi permettono di esprimersi con:
- duine
- terzine

Il tactus porta con se anche altro, oltre i tempi suddivisi in due e tre tempi; nel Canto Sospeso di Nono lo vediamo bene, nelle sezioni in cui "non viene mai data l'aria di un qualcosa a tempo". Ciò rende intricato e meno marziale la scrittura:
![no](no.png)

>Vediamo che in Nono, l'uso di elementi su parti di una figura complessa con pause prima della nota introdotta, realizza un discorso molto contemporaneo.
Perseguire un percorso analogo con figurazioni ritmiche piú complesse e che si incastrino aiuta a realizzare ciò.

Ciò è visibile anche nell'Introduzione all'oscuro di Sciarrino, in cui è presente un 4/4 ma di fatto la suddivisione in quattro quarti la si sente solamente quando il compositore vuole farla sentire.
