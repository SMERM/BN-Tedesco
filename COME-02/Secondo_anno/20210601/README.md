# Appunti della lezione di martedì 1mo giugno 2021

## Brani

- [Brani Oppedisano per chitarra elettrica](https://marcooppedisano.musicaneo.com/sheetmusic/sm-44204_urban_mosaic_for_solo_electric_guitar.html)
- [Brani Oppedisano](https://marcooppedisano.musicaneo.com/sheetmusic/)
- [Brani Scodanibbio per chitarra](http://www.stefanoscodanibbio.com/discography/23.htm)
- Scodanibbio - Quando le montagne si colorano di rosa

Il chitarrista che descrive l'opinione

## La chitarra come strumento da camera

## Problema del tapping

Il fatto che si senta o no, abbiamo molti altri problemi...

Il tapping ha una profonda matrice ritmica, poliritmia.

(Pornografia musicale -> ci si muove con il contesto pornografico di riferimento)

## Tapping

- microtoni
- dinamica
- vibrati
- bicordi
- tapping artificiale

1:15 ->
6:44 ->

>Colpi singoli individuali che lasciano perplessi a cui bisogna dare un senso...

I colpi potrebbero essere abbelliti con delle fioriture, ad esempio delle acciaccature con un lavoro di decorazione.

Il lavoro di fioritura deve avere un suo sviluppo...

## Diversi comportamenti dal catalogo

Importante è isolare i comportamenti musicali che interessa prendere come punti di partenza di arrivo...

>Frammentando un comportamento sonoro senza una stesura lunga puó funzionare lo stesso?

## Cosa fare?
- usare meno materiale e usarlo piú a lungo
- decidere quale materiale usare

***
Pulizia sul tapping di un chitarrista:

[chitarrista metal tapping](https://www.youtube.com/watch?v=g-gE0T3Kp7w)

- ritmo quadrato e simmetrico
- non c'è armonia -> tutte scale
- tecnica come il tapping usata in un altro contesto, ha qualcosa da dire

Tappeziere -> quello che fa tapping

## Dove eccelle il tapping come tecnica?

Lo strumento condiziona il risultato musicale...

Questo tipo di tapping realizzato su una chitarra elettrica ha una velocità impensabile ottenere da pizzicato.

La chitarra elettrica è uno degli strumenti che se li tocchi poco suonano anche abbastanza forti.

Chitarre specifiche per realizzare tapping...

>Quando il tapping è pulito ha un effetto completamente diverso...

## Ha senso il tapping?

Carattere timbrico della corda è un po' diverso...


## Tosin Abasi
[Tosin Abasi](https://www.youtube.com/watch?v=j9gY4FRc1Ig&t=44s&pp=ugMICgJpdBABGAE%3D)

Sfrutta una tecnica bassistica su chitarra...

***
Dato che non siamo in grado di ascoltare un costrutto musicale piú complesso di questo, vediamo una sorta di circobarnum dello strumento che non ha alcuna valenza...

***

Aspetto idelogico del suonare lo strumento, discorso da portare strumentalmente per rivoluzionare il discorso, dal popit musicale, dalla pornografia...

***

Il fatto che in alcuni ambiti succede che bisogna essere iper pindarico, percepiamo come contenuto semplice un qualcosa di scarso. Si nota il fatto che vi sono delle piccole idee di frase in mezzo ad un discorso complesso.

Il passaggio è se stesso il pezzo e le frasi principali diventano la parte minore.

(Beethoven lo accusavano di essere troppo semplice, ma nella costruzione compositiva smonta)

Brahms -> 3 note nel secondo concerto compositivo

Nella musica contemporanea capita spesso che vi sono elementi molto specifici e leggeri che alcune cose si perdono.

[](https://www.youtube.com/watch?v=7cnyGScs0Ho&ab_channel=GodspeedYou%21BlackEmperor-Topic)

[Sinfonie per chitarre di Glenn Branca](https://daily.redbullmusicacademy.com/2016/05/glenn-branca-symphony-guide)

***

## Cose di cui abbiamo parlato

- [Fourier analysis in PD](http://www.pd-tutorial.com/english/ch03s08.html)
- [STFT](https://towardsdatascience.com/practical-cython-music-retrieval-short-time-fourier-transform-f89a0e65754d)
- [onset detection](https://www.cycfi.com/2021/01/onset-detection/)
  >The two concepts are related to two different dimensions or aspects of music which might or might not be correlated.
  Onset detection is concerned with finding the points in time at which sounds start. Doing this does not require prior knowledge of the particular pitch (or fundamental frequency) of the sound. It may indeed rely on the property that at the beginning of a sound, there is an increase of energy. Actually, you can very well perform onset detection on recordings which do not have a well-defined pitch (for example: drums, machine noises...).
  You can think of pitch and onsets/rhythm as related to the "vertical" and "horizontal" organization of music. There exists types of material in which changes along these two dimensions coincide - so that a detection algorithm can take advantage of a change in one dimension to infer that a change has occurred on the other dimension. But this coincidence/correlation is contingent.
- [onset detection in SuperCollider](https://doc.sccode.org/Classes/Onsets.html)
- possiamo sviluppare delle competenze nostre, ma funziona qualcosa di minimale?!?
- elettronica come aumentazione o come spiazzamento? spiazzando si perde la posizione della cantante
- Gubaidulina e compositori degenerati e come sono scappati o si sono salvati

***
Incontro per la trasformata di Fourier di 2 ore Martedí 8 Giugno dalle 11:30 in poi.
