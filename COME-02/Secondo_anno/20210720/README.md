# Appunti della lezione di martedì 20 luglio 2021

## Amplificazione trasparente o non?

Elevare uno strumento dal rumore di fondo in modo che tutto sia percepibile.

## Simulazione 3D o story board

Inquadratura come realizzazione nel cinema di idee musicali...

## Coscenzizzazione musicale della scrittura dei brani

Perchè un qualcosa ci piace?

Da professionisti chiederci perchè ci piace...

___

>La composizione non può essere in nessun caso monodirezionale.

## Il progetto non è la composizione

Il progetto quindi non è un progetto...
