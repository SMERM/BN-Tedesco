# Appunti della lezione di Martedí 24 Novembre 2020

## Regole per l’estrazione dei frammenti per gli studi di chitarra:

- un frammento di un brano puó essere inserito solo se i due frammenti precedenti e successivi non solo dello stesso brano
- la dinamica del frammento puó essere espansa solamente se il contesto ne esalterebbe le qualità timbriche(controllo di dinamica su frammento precedente e successivo)
- l’utilizzo della mano destra come percussione può essere attribuito solamente a frammenti realizzati prima del 1900
- le altezze devono essere necessariamente modificate se nel frammento precedente appaiono piú di tre altezze uguali a quelle del frammento in esame(per diverso è inteso anche il quarto di tono o il semitono)
- si segue il tempo inserito nel brano originale solamente dove sono inseriti dei marker
- gli abbellimenti da eseguire sono solamente se gli anni di realizzazione di due frammenti apposti sono maggiori di 50 anni
- alcuni particolari frammenti hanno la dinamica imposta dall’esterno per motivazioni di rilevanza musicale

**Come effettuare l'estrazione?**

- quante estrazioni effettuare?
- sovrapposizione di piú estrazioni
- schema metrico

________

Quando si trasporta, bisogna fare

## Appunti

usare la stessa chiave funziona? no

Array associativo o array normale?

### I frammenti
Grandezza dei frammenti??

#### Come inserirli nel database?

Come frammento di lilypond, senza header e tutto, ma solo con il gruppo di note, i ritmi e tutte le informazioni rilevanti, con dei riferimenti di battuta

#### Come ritagliare i frammenti?

prendendo il terzo moviemnto di Sinfonia di Berio, vediamo che lui fa un certo tipo di ritaglio, molto musicale, molto dotto e musicale. Nonostante si riconosce la bellezza musicale, non vi è attrazione dello show off del circo compositivo.

Se dovessi pensare all'oggi, il taglio di Cage ha molto piú senso di quello di Berio.

I frammenti e la lunghezza degli stessi determinerà lo studio.

I frammenti devono essere tagliati brutalmente?

Nella partita di Bernardini i frammenti sono tagliati e vi è un contrappunto tra il macro ritaglio.

Negli studi per pianoforte ha realizzato Bernardini dei ritagli basati sull'anno di composizione.

Il modo di tagliare le cose è esteticamente rilevante. (i frammenti)

- si puó avere un senso di differenza musicale (sentendo le regole se esse sono sufficientemente musicali)
  - organizzare i frammenti in accellerando, funziona ad esempio e risulta molto musicale

Negli studi per pianoforte, la macroforma è un pezzo in se.

Thelonius Monk, improvvisazione di Blue Monk(blues che rifà 3 volte)-> prese le note e trascritte in valori assoluti -> poichè aveva una tecnica particolare

Monk è molto vicino a Glenn Gould e non esiste un metronomo, vi è un vero e proprio metronomo umano.

Radu Lupu -> concezione del ritmo

Le note sono diventate i nodi centrali dei frammenti, su questi nodi centrali si stabiliscono delle finestre su cui si centrano dei frammenti ed i frammenti si deformano andando avanti.

- frammenti espansi
- frammenti serializzati

Si ottiene l'effetto che questo è qualcosa, ma non si capisce che sia cosí

#### Come scegliere i frammenti
- dotto musicalemente
- bruto e brusco

#### Cosa c'è di importante?

Modo di scegliere i frammenti Charles Ives, anche Mozart ha organizzato le tre orchestrine nel Don Giovanni che sono tenute insieme dall'aria di Don Giovanni.

Sia Berio che Ives, padri di Bernardini, sottolineano l'aspetto musicale ed affettivo specifico relativo ai brani, che nasconde un po' l'aspetto connotativo.

Del cavaliere della Rosa di Strauss sceglie uno specifico accordo che si deve incastrare con La mer di Debussy.

Ritagliando le cose e mettendole la, si scrive un messaggio con le lettere di giornale. Si troveranno caratteri di riviste diverse, giocando e componendo le connotazioni.

#### Modo 1
- inserisco tutto il brano
- decido e taglio in frammenti
- analisi deterministiche

Compilatore online per ogni strumento dello stesso brano.

Compilatore online per ogni studio dello stesso brano.

________
### Utilizzo dell'aumentazione
- ogni tot c'è un intervento che oblitera il frammento -> brusco
- macro forma

### Vari elementi
- gesti
- timbri

### Rilevanza della disposizione
- ridondanza
- statisticamente
- espone

(Nel terzo movimento di Sinfonia, vi è un trucco geniale, ovvero l'utilizzo dell'adagietto di Malher)

Sorta di dizionario, ma bisogna avere la costola del libro.

L'aumentazione o alcune tecniche possono essere il collante, ma tiene attaccato il discorso.

Si può ragionare in tutte le accezioni, con idee che vanno raccolte e ponderate!

________
Realizzare una gem Ruby:
- come funziona [RVM](rvm.io)?
- GEM e bundler di [Ruby](https://www.ruby-lang.org/it/documentation/) e tutta una forma di automatizzare
- per
