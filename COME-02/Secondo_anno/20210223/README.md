# Appunti della lezione di Martedì 23 Febbraio 2021

**Migliorare la grafia**

Uso di un righello è di fondamentale importanza.

I glissandi sono un gesto molto forte, se non volgare, essi sono un gesto molto evidente.

Glissando di terza minore e terza maggiore = lagne

Glissandi più ampi, vanno bene, ma dipendono molto dalla velocità di esecuzione.

Studio interamente su uno di questi tipi di glissando...

tunnel -> errore grafico

## Migliorare impaginazione

Togliere pentagramma su righi di elettronica e spazio...

(jack noise incluso nella terzina sopra)


1. rigo per gesti del live electronics -> Serve una cosa di azioni da compiere

2. risultato del tape del live electronics -> partitura di risultanti

- secondo sistema -> con grazia -> lasciar vibrare il do e inserisco la corda

porsi il problema di chi suona -> mettere tutto in riga -> tape quando finisce??

linee tratteggiate per far si che delle cose siano sincronizzati gli accadimenti

![corr](corr.png)

burst voce da rivedere -> massa timbrica o non??

far fare la stessa cosa alla voce ad una delle frasi della chitarra -> reiterazione del gesto con la voce -> constatare una reiterazione

Sviluppo?? Ce la si fa a sviluppare cosí tanta roba in un tempo cosí breve?

Fare un qualcosa pieno di roba e poi reiterarla... -> gran paraculata che è un meccanismo per far passare dei pilloloni per buchi stretti.

Considerare bene la quantità di materiale che si mette dentro.

Riverberi -> discrasia tra ciò che succede e la rappresentazione che se ne da

Qual'è la necessità compositiva del riverbero?

(interviste di Celibidache)

gesto musicale e lunghezza del suono

Riverbero dannoso per il brano??

**Rifiuto del riverbero come una componente timbrica.**

Riverbero è un modo di rappresentare l'architettura di quello che è intorno al pezzo.

Dal vivo -> si sta facendo un film se si mette riverbero...

(Scrivere all'interprete lontano e vicino, si capisce l'intenzione senza inserire le parole romantico etc...) Il senso di prossimità e lontananza lo si rende anche senza la maionese(il riverbero), ma possiamo realizzare anche una cosa fatta in casa.

Prossimità o distanza, con una logica un po' impressionista...

(Prossimità con chitarra elettrica)

Diverso è il discorso su Turenas -> costruzione dello spazio...

Nel Prometeo di Nono, ci sono delle voci recitanti con un altoparlante all'opposto delle voci che fa una sorta di eco distorto, all'interno di uno spazio grande costruisce una sorta di riflessione storta, che puó avere un senso timbrico.

Non realizzare una ricerca naturalistica del riverbero, ma strutturale...

**Lasciar perdere la collezione di riverberi naturalistica.**

Forma stereotipica di affondare dentro i luoghi e gli spazi, la ricchezza del suono dal vivo, in se e per se, e eseguendo un brano dal vivo vi è una circostanza dell'esecuzione che rende la cosa nuova ogni volta, rendendola imponderabile(Branchi), e consente in ciascuna esecuzione di aggiustare le cose.

_________

Bisogna essere molto precisi nella scrittura per ottenere -> come se nessuno avesse letto o visto mai la partitura
