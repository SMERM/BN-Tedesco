# Appunti della lezione di lunedí 28 giugno 2021

---
**Appunti su gitlab disponibili al [link](https://gitlab.com/SMERM/acustica-degli-strumenti-musicali)**
---

## Panoramica del corso

In questo corso avremo un primo avvicinamento alle tematiche dell'acustica degli strumenti musicali.

Questi campi costituiscono una buona parte della musica tecnlogica come il MIR, ovvero il Music Information Retrival.

### MIR

Dal materiale audio grezzo si estraggono delle informazioni a diversi livelli dal materiale audio.

Possiamo ad esempio estrarre informazioni a livello molto basso come estrarre dB RMS; oppure descrivere la tipologia di musica che si sta ascoltando o realizzando.

Ci si aspetta che si parli di musica tonale, strumentazione tradizionale ed il massimo a cui si arriva è il Jazz ed il Pop.

È molto difficile avere a che fare con una generalità e pluralità di declinazioni del linguaggio musicale di ciò con cui abbiamo a che fare rispetto alla tonalizzazione stretta della musica tonale o pop.

Gli obiettivi di ricerca sono quelli di classificare il materiale musicale e cercare di capire un file audio a che tipo di genere appartiene e gli strumenti che suonano a quale categoria appartengono.

Essendo invece il materiale musicale elettroacustico difficilmente categorizzabile.

>è importante capire quali siano i metodi che si hanno per poter procedere da un punto di vista qualitativo.

Quando ad esempio ricavarmi dei descrittori di alto livello, devo ripercorrere il percorso del cervello umano e il sistema deve cercare di riconoscere ciò che sta ascoltando, cercando di costruire una rappresentazione di quello che si sta percependo che si basa su elementi, memorie e categorie già note.

Si riuscirà quindi a riconoscere elementi e generi da un qualcosa che ho già precedentemente sentito.

Il percorso di questi filoni di ricerca ha molto a che fare con filoni di ricerca della percezione umana...

Questi modelli prendono sempre piú piede nel machine learning e nelle intelligenze artificiali, per capire ad esempio come funzionano gli algoritmi di intelligenza artificiale.

>I classificatori scimmiottano le capacità umane di riconoscere, classificare e prevedere elementi.

Dal punto di vista dell'informazione, gli algoritmi ci inducono a una sorta di visione apocalittica, mentre ci sono sempre dietro gli uomini con una visione umana.

L'unico problema che le intelligenze artificiali sono una sorta di scatole nere, gli algoritmi sono molto complessi dal punto di vista filosofico e io riuscirò ad osservare dei comportamenti emergenti(pilotandoli anche) ma non sarò complessivamente a conoscenza di ció che verrà fuori.

Si usano delle strategie dei sistemi non lineari e dunque non si hanno delle prevedibilità certe...

Abbiamo questa area grigia sugli strumenti di intelligenza artificiale in cui non sappiamo come funzionano alcune parti di un algoritmo.

(Seguire la catena di apprendimento degli algoritmi di intelligenza artificiale per non farli divenire discriminatori.)

Sia nella raccolta dei dati, sia nell'elaborazione dei dati, non vi è alcuna oggettività ma abbiamo il frutto di una catena di scelte.

Possiamo parlare di deficienza artificiale, e non di intelligenza artificiale...

### Presupposti per il funzionamento di queste tipologie di macchine

Uno dei presupposti di queste macchine è la raccolta di informazioni, abbiamo poi livelli di aggregazione di informazioni, fino poi arrivare a spedire il tutto ai circuiti corticali che ci fanno associare al genere ciò che sentiamo.

Dovremmo quindi capire come funziona l'estrazione di informazioni nel caso di materiale audio.

### Primo campo che affrontiamo

Molto vasto per quanto riguarda l'estrazione di descrittori è il campo che analizza con la trasformata di Fourier.

Siamo abituati a pensare a Fourier come un parallelo immanente e di rappresentazione del suono, ciò non è sempre presente, e vi sono comunque poi altre tipologie di trasformate come le wavelet.

Potremmo ad esempio trasformare il segnale per mezzo della compressione...

#### Esempio: la compressione
Se dovessimo trasmettere un segnale sinusoidale da un punto all'altro della terra come facciamo?

Dovremmo trasmettere ad esempio 88000 Bytes al secondo con 16 bit...

Se sorgente e destinatario condividono un'informazione, possiamo tranquillamente trasmettere frequenza ed ampiezza dell'oscillatore e potremo trasmettere informazioni a 16 bit, ovvero 4 Byte alla volta.

Avremo bisogno di molta meno banda rispetto al caso della trasmissione del segnale brutale.

Tra questi due estremi vi è tutta una varietà possibile di trasmissione di segnale audio.

L'obiettivo fondamentale sarà trasportare segnale risparmiando banda.


#### Esempio: sintesi additiva

Immaginando di dover trasmettere di ogni segnale sono frequenza e ampiezza, sto comunque risparmiando banda.

Ad esempio la trasmissione telefonica è una sorta di chimera della voce della persona che parla, non vi è nulla della persona originale, ma vi è una ricostruzione.

La voce viene campionata, rappresentata e il ricevitore condividendo con la sorgente un certo modello del segnale usa i descrittori per ricostruire la voce della persona.

>Quando una serie di informazioni mancano, vengono sintetizzate delle voci robotiche e sintetiche e di fatto non si hanno tutte le informazioni che servono per realizzare una costruzione puntuale.

### Il successo di Fourier

Esso fu dovuto all'espandersi in vari rami per la rappresentazione di segnali indipendenti dal tempo e nel quale rappresentazione non pone un problema; nel segnale musicale vi è però l'informazione del tempo che è molto importante, poichè abbiamo uno strumento per la rappresentazione di scale temporali diverse.

### Il modello matematico di Fourier è un modello a risoluzione costante

Partendo da un esperimento mentale: "vado in un auditorium dove suonano la nona di Beethoven", essa dura un'ora, la registro, prendo il file audio e lo trasformo con la FFT o DFT.

Ottengo quindi un vettore di numeri complessi, con la stessa dimensione del vettore iniziale del dato costituito però da numeri complessi.

44100*3600 = 158760000 campioni per un solo canale, ne realizzo la DFT, e mi tirerà un vettore di 160MLN di componenti complesse.

Ora metto in piedi una sintesi additiva con 160MLN di oscillatori, ognuno di questi oscillatori avrà ampiezza e fase iniziale pari a fase e modulo che sono usciti dalla DFT.

Nel momento in cui schiaccio il tasto start, gli oscillatori si mettono a girare con la sua frequenza o velocità angolare.

Avremmo quindi la Nona di Beethoven perfettamente riprodotta.

#### Cosa succede alla fine dell'ora?

La nona di Beethoven inizierà da capo, la rappresentazione della DFT implica che il segnale sia periodico, perchè quando tutti gli oscillatori finiscono poi si rinizia da capo con una periodicità di 1 ora.

#### Come sono rappresentate tutte le periodicità del segnale?

Sono rappresentate con 44100 campioni al secondo, avendo rappresentato una banda di 22050.

### DFT per segnali reali

![f](f.png)
Sappiamo che la DFT è perfettamente uguale per frequenze positive e negative, se ho quindi un segnale reale, quando pass nel dominio dal tempo alla rappresentazioen di Fourier, ottengo una rappresentazione a specchio.

Ciò che mi da informazione è solo la parte positiva delle frequenze e quindi non guardiamo proprio la parte negativa:
![dft](dft.png)

>Sappiamo quindi che la DFT fa un confronto con tutti e due gli aspetti seno e coseno...

Una trasformata come quella coseno non utilizza la parte negativa, risparmiando calcoli per la trasmissione GSM.

### Qual'è il periodo fondamentale di questo segnale?

Sappiamo che esso è 3600 secondi, il periodo è l'inverso della frequenza fondamentale:
![t](t.png)

Stiamo quindi rappresentando la banda di Nyquist attraverso un certo numero di fasori/oscillatori.

Se prendo la banda di Nyquist e la divido per il numero di oscillatori(diviso 2 perchè solo la metà di oscillatori mi servono per rappresentare il segnale che l'altra metà è simmetrica), verra fuori :
![df](df.png)

Stiamo tagliando quindi la banda in fettine, chidendomi quanto sia grande ogni fettina, ovvero 1/deltat.

Nel caso della nona di Beethoven avremo una risoluzione di 1/3600 ovvero una risoluzione un po' esigua e girerà con una frequenza di 2*10^-4.

La seconda armonica completerà un giro nella metà del tempo.

Avremo quindi un'informazione molto specifica dal punto di vista della frequenza.

Dal punto di vista del dominio del tempo, 1 ora è la fondamentale di quel segnale.

Mi dovrò sentire tutta la Nona per capire cosa succede in un'ora.

Ciò lo vedremo con il prodotto tra la risoluzione in frequenza e la risoluzione nel tempo è uguale ad 1, ovvero il principio di indeterminazione in fisica:
![p](p.png)

### Perchè la DFT?

Consumo una parte di risoluzione nel dominio della frequenza per poterla acquistare nel dominio del tempo.

Applico la stessa definizione ad una porzione di segnale ridotto, chiamato finestra o frame.

Quindi se volessimo applicare la stessa definizione ad una finestra di 1000 campioni, prendo quindi il segnale nel tempo e lo divido in 1000 campioni.

Dopodichè trasformo tutto con la DFT ottengo N campioni complessi.

![1000](1000.png)

Avremo una risoluzione in frequenza in base ai campioni ed alla finestra che abbiamo a disposizione:
![dff1](dff1.png)

Prendendo la frequenza di campionamento e dividendeno per 1000 ottengo 4410 Hz, essa sarà la mia risoluzione in frequenza.

Vediamo che le cose sono cambiate ed ora è come se io fossi cieco in una banda e io non riesco a vederci dentro, tutta l'energia della banda è rappresentata da un solo numero.

Il mio delta T sarà:
![dt](dt.png)

Avró ogni 23 ms un vettore di dati che mi darà informazioni.

Avró 1000 oscillatori e mi permetteranno di ricostruire perfettamente il segnale di partenza senza dover aspettare un'ora.

>Vi sono in realtà molte considerazioni matematiche da fare, ma in teoria si puó ricostruire il segnale originale.

Abbiamo quindi una latenza molto minore ma sempre fedele al segnale iniziale.

### Compressione flac
Nel caso della compressione flac abbiamo una compressione che dipende molto dal segnale che stiamo rappresentando.

>Tutti i modelli di compressione si aspettano che i segnali siano realizzati in un certo modo.

### Perchè è importante per noi?

Ciò ci permette di seguire il segnale nel tempo senza dover aspettare troppo, dal punto di vista del tempo reale ciò è molto utile e importante.

### Diagramma a blocchi: dal segnale discreto a...

Le varie sezione:

1. la prima operazione che realizziamo su questo segnale è la suddivisione in frame(ovvero suddividere il segnale a fettine) denominata _framing_
2. la seconda operazione è una sorta di moltiplicazione di una finestra per una funzione di finestra che ha diversi scopi:
  - fare a fettine il segnale vuol dire realizzare un taglio in finestre rettangolari da 1000 campioni
  - questa moltiplicazione ha degli effetti dal punto di vista spettrale che si ripercuotono nel fatto che quando ricostruisco il segnale ho dei problemi di fase e la rappresentazione puó non essere esatta con _problemi di cucitura dei bordi_
  - per avere una rappresentazione migliore nel dominio della frequenza, moltiplico il segnale per delle funzioni che sono migliori per poter ricucire i bordi
  - ognuna delle funzioni ha caratteristiche specifiche
  ![blo](blo.png)
  - ad esempio la trasformata discreta di un seno infinito sarebbe:  ![s](s.png) tagliando il seno con una finestra rettangolare avremo la creazione di artefatti laterali all'energia che dovrebbe corrispondere a una sola frequenza:  ![ss](ss.png) avremo quindi delle energie laterali; ciò succede perchè la DFT assume che il segnale sia periodico, supponendo quindi una sinusoide infinita, allora la periodicità è un concetto accessorio, ma tagliando la sinusoide e prendendone una porzione di sinusoide con una finestra rettangolare, quando lo periodizzo ho un qualcosa di strano con salti e discontinuità ed essa avrà un contenuto armonico infinito:  ![ta](ta.png)
  - tagliando il segnale in N periodi perfetti si ricuce perfettamente il segnale, essa viene chiamata DFT Pitch-synchronus
  - per poter smussare le discontinuità si utilizzano quindi delle funzioni che smussano le discontinuità

>Quando si entra nel dominio tempo discreto, non sappiamo nulla dell'origine del segnale, otteniamo quindi una sequenza di campioni digitalizzati, passando la sequenza ad un'altra persona, avremo solamente l'esistenza del tempo discreto, con una successione di campioni senza alcun legame con il segnale continuo. Abbiamo quindi un dato che è la frequenza di campionamento...

Potremmo ottenere la stessa sequenza di numeri digitalizzati in n-modi diversi.

Dal punto di vista del mondo tempo-discreto tutte le sequenze sono uguali, per ritornare al segnale continuo dovremmo chiederci come sia fatta...

Nel contesto del diagramma a blocchi non ci interessa da dove provenga il segnale, ma abbiamo già un segnale discreto.

3. Ne realizzimo la DFT:
![blo1](blo1.png)

### Ricavare delle informazioni

Possiamo ricavare delle informazioni derivate dai vari blocchi, avremo quindi dei descrittori temporali, come RMS...

Posso sennò ricavarmi delle grandezze derivate nel dominio di Fourier, come la centroide spettrale:
![ddd](ddd.png)

Ció vorrà dire che per i descrittori temporali lavoreremo nel tempo con i campioni discreti, mentre per i descrittori frequenziali lavoreremo con il dominio complesso.

La DFT serve per ricavarmi dei descrittori o per una rappresentazione tempo frequenza di un segnale ovvero lo spettrogramma.

#### Lo spettrogramma

Esso significa mettere in fila, tutte le finestre...

Abbiamo un array x[n] che facciamo a fettine ponendolo in una window[n] esso sarà lungo n campioni.

![ma](ma.png)

Mi costruiró 2 matrici:
1. matrice delle frame temporali: matrice delle finestre nel tempo, una colonna della matrice conterrà una finestra della mia analisi, la seconda colonna i secondi mille campioni etc...
2. matrice nel dominio della frequenza, ovvero le DFT

Abbiamo in ascissa a sinistra il tempo, che corrisponde a ognuna delle fettine temporali che avanza ad esempio a 23 ms alla volta.

Avremmo nella seconda matrice la dft di ognuna delle fettine.

>Lo spettrogramma è il primo tipo di rappresentazione tempo-frequenza che posso ottenere nel momento in cui rappresento ogni finestra di analisi e assegno il colore in base al modulo di ognuno degli elementi della matrice.

Ognuno degli elementi della seconda matrice è un bin, un secchio da cui attingere.

All'interno di un bin vi è quindi un intervallo di frequenza non una sola frequenza.

## Realizziamo un piccolo progetto in Python
Potremmo costruirci la funzione STFT da zero, ma in realtà useremo la funzione da libreria, `numpy` e `scypy` hanno già alcune funzioni.

La scelta delle finestre determina una possibilità di scegliere la precisione in vari ambiti...

In genere si accetta di avere un lobo centrale della sinusoide che stiamo analizzando per ottenere un abbassamento dei lobi laterali.

```
import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
from scipy.io.wavfile import read

#leggiamo un file audio(file audio mono)
[fs, x] = read("daniel.wav")
print("SR: ",fs,"\ndimensione del file audio in campioni: ", np.size(x))

#normalizziamo il file audio(a 0 dB)
x = x/np.max(abs(x)) #dividiamo il file per l'assoluto del suo massimo

#calcoliamo la matrice stft
#nperseg sono il numero di campioni della stft
#noverlap serve per realizzare un salto di 1000 campioni, riconsiderando campioni gia precedentemente considerato
#f, t, STFT = signal.stft(x, fs, nperseg=2048, noverlap=1024, window='blackmanharris')
#f, t, STFT = signal.stft(x, fs, nperseg=4096, noverlap=1024, window='blackmanharris')
f, t, STFT = signal.stft(x, fs, nperseg=4096, noverlap=512, window='blackmanharris')
print("dimensioni della matrice STFT: ", STFT.shape)

#plot
plt.figure(1)
plt.ylabel('segnale')
plt.grid()
plt.plot(x)
plt.show()

plt.figure(2)
plt.pcolormesh(t, f, np.abs(STFT), vmin=0, vmax=np.max(abs(STFT)), shading='auto')
plt.xlabel('Tempo [s]')
plt.ylabel('Frequenza [Hz]')
plt.show()
```

![plt](plt.png)


### Qual'è l'equilibrio tra risoluzione in frequenza e nel tempo?

Dipende da ciò che sono interessato a realizzare, ad esempio per determinare qualcosa in un parlato, qual'è la velocità del parlato?

Per l'applicazione di osservazione di un fenomeno che si dipana nel tempo, la risoluzione del tempo è molto importante, mentre per altre sarà importante la frequenza.

### La risoluzione della STFT è costante

Scegliendo un numero di campioni, la risoluzione cambierà dal punto di vista percettivo musicale...

40 Hz dal punto di vista musicale a 20 Hz sono un cambiamento enorme, mentre a 2000 Hz il cambiamento è minimo.

Realizzazione di modelli di segnale multirisoluzione servono per ovviare a questo cambiamento lineare e costante.

## Cosa fare ora?

Possiamo realizzare delle prove con una rappresentazione pitch-synchronus e pitch a-synchronus.

L'obiettivo è l'analisi e realizzazione di descrittori audio applicabili a strumenti tradizionali per capire e cercare di osservare diverse cose.

Ci concentreremo quindi su alcuni descrittori e cercheremo di capire come leggere questi descrittori nel caso di descrittori tradizionali.

- cercare campionamenti anecoici di strumenti
___
## Cose di cui abbiamo parlato

- [Gruppo filosofico TLON](https://tlon.it/)
- Wu-Ming
- [Q come complotto di Wu Ming 1](https://b-ok.org/book/15653588/adbbce)
- [https://developer.apple.com/videos/play/wwdc2021/10036/](https://developer.apple.com/videos/play/wwdc2021/10036/)
- [registrazioni delle lezioni](https://elearning-santacecilia.it/mod/forum/view.php?id=1604)
