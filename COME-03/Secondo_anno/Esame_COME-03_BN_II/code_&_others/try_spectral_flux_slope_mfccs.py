import numpy as np
import matplotlib.pyplot as plt
import pyACA

#leggiamo un file audio(file audio mono)
path = "DavideTedesco_Piano_A2_mono.wav"
[fs, x] = pyACA.ToolReadAudio(path)

#normalizziamo il file audio(a 0 dB)
x = x/np.max(abs(x)) #dividiamo il file per l'assoluto del suo massimo

#calcolo del descrittore
#centroide spettrale
#[dcs, t] = pyACA.computeFeature("SpectralCentroid",x,fs,iBlockLength=1024, iHopLength=512) #dcs= descrittore centroide spettrale
#spectral flattness
[dsf, t] = pyACA.computeFeature("SpectralSlope",x,fs,iBlockLength=1024, iHopLength=512)
#spectral flux
[dsx, t] = pyACA.computeFeature("SpectralFlux",x,fs,iBlockLength=1024, iHopLength=512)
#spectral MFCCs
[dsmfcc, t] = pyACA.computeFeature("SpectralMfccs",x,fs,iBlockLength=1024, iHopLength=512)

#plottiamo
#tratto da https://matplotlib.org/devdocs/gallery/subplots_axes_and_figures/two_scales.html

plt.figure(1)
color = 'tab:red'
plt.ylabel('Scala SpectralFlatness', color=color)
plt.xlabel('Tempo')
plt.plot(t, dsf, color=color)

plt.figure(2)

color = 'tab:blue'
plt.ylabel('Scala SpectralFlux', color=color)  # we already handled the x-label with ax1
plt.xlabel('Tempo')
plt.plot(t, dsx, color=color)

plt.figure(3)

color = 'tab:green'
plt.ylabel('Scala SpectralMfccs', color=color)  # we already handled the x-label with ax1
plt.xlabel('Tempo')
plt.plot(t, dsmfcc, color=color)

plt.show()
