# Spectral Slope

Links utili:
- https://meyda.js.org/audio-features.html
- https://it.mathworks.com/help/audio/ref/spectralslope.html
- https://www2.humusoft.cz/www/papers/tcp11/125_volin.pdf
- https://en.wikipedia.org/wiki/Spectral_slope

> The spectral "slope" of many natural audio signals (their tendency to have less energy at high frequencies) has been known for many years,[3] and the fact that this slope is related to the nature of the sound source. One way to quantify this is by applying linear regression to the Fourier magnitude spectrum of the signal, which produces a single number indicating the slope of the line-of-best-fit through the spectral data.[1]
Alternative ways to characterise a sound signal's distribution of energy vs. frequency include spectral rolloff, spectral centroid.[1]
