# Appunti della lezione di Mercoledì 16 Settembre 2020

(Ambisonics è tra i sistemi di rendering audio, mentre l'ITD e l'IID sono ad esempio sistemi percettivi.)

## Introduzione
Frequenza di taglio dell'aria varia in base alla distanza, e nei modelli classici non si può intervenire puntualmente su ogni riflessione. I modelli classici di riverberazione modellano la risposta di un ambiente in grande, mentre nella realtà se volessi costruire un sistema di emulazione di uno spazio dovrei suddividerlo in:
- prima parte con le prime riflessioni
- seconda parte con risposta piú diffusa all'impulso (coda)

Se ho questi contributi separati posso intervenire e regolare separatamente, in modo da rispettare le distanze, e calcolare anche il filtro che simula l'assorbimento dell'aria.

Siti che abbiamo usato fin ora fanno riferimento a spazi molto semplici, mentre un algoritmo che calcola le prime riflessioni per qualunque geometria di uno spazio, i calcoli si complicano.

All'epoca della realizzazione dei primi riverberi i computer erano piú lenti e bisognava rendere piú piacevole il modello telefonico(modello di Schroeder), mentre quello di Chowing era già piú musicale.

L'algoritmo di Schroeder parte monofonico, poichè il telefono è mono.
(paper riverbero di Schroeder)

Quando si realizza un qulacosa di stereo si duplicano i canali e ci deve essere una asimmetria dei canali, per aumentare la percezione dello spazio, ovvero realizzando la decorrelazione tra i due canali. Poichè sennò si disattiverebbe totalmente la percezione dello spazio.


## Descrittori spaziali in ambito statistico

Chiarezza e intellegibilità in ambito del parlato e musicale, legati a considerazioni di carattere energetico utile:
- D50 (frazione di energia utile, primi 50ms IR/ IRtot)
- C50 (rapporto tra energia utile in primi 50ms e energia dannosa, ovvero da 80ms fino all'infinito)
- C80

### Centre Time(ts)
Questa definizione parte dal presupposto che l'energia utile decade nei primi 50/80 ms. Ma non i tutte le situazioni questo dato è uguale. Quindi è definito un altro parametro ovvero:
- Centre Trime -> tempo baricentrico della risposta all'impulso, ovvero esso è il tempo che separa il 50% dell'energia prima di un istante e dopo un istante, individua dunque la metà di una IR dal punto di vista energetico, quindi il tempo passato il quale, sono stato in vestito dal 50% dell'energia.

![centretime.png](centretime.png)

Calcola l'energia della IR, nel dominio tempo discreto, ovvero prendendo e sommando i quadrati della IR. Facendo questa operazione, o ho tutta l'IR o faccio una stima della IR. Ció è legato al C50 e C80, poichè mi da una misura della chiarezza del suono, _poichè piú basso è il tempo centrale, piú chiaro risulterà il suono_, poichè il 50% dell'energia è spostato sulle prime riflessioni; piú grande è il tempo centrale, piú l'energia è spalmata su tutta l'IR e quindi la riverberazione della parte diffusa è piú forte.
Tipicamente per le applicazioni musicali e del parlato, il valore del ts è compreso **tra 140 e 180 ms**, in una **banda tra i 250 e i 2000 Hz**.

Ti dice come è fatta dal punto di vista energetico la IR; se il tempo centrale è troppo alto, il secondo 50% dell'energia è presente sulla coda dell'IR, ovvero sulla parte diffusa. Hai una IR molto piú omogenea(scende piú lentamente), se hai invece una IR molto forte all'inizio e molto rapida a scendere, quindi la parte diffusa della IR è molto corta e hai meno riverbero, e dunque avrai un ts piú piccolo sbilanciato verso l'inizio.

ts piú piccolo -> IR più compatta -> energia su prime riflessioni

ts piú verso destra -> riverbero piú lungo, energia spalmata su parte diffusa(coda)

(Ambiente troppo asciutto non è percepito come confortevole, quindi è bene che un certo ambiente abbia una certa spazialità, anche se è bene che il contributo sia concentrato all'inizio)

### Rinforzo acustico(G[dB])

Altro parametro utilizzato che riguarda l'acustica delle sale è l'indice di rinforzo acustico, indicato la lettera _G_. Esso è una misura dell'amplificazione acustica introdotta attraverso un apparato di amplificazione confrontata con la risposta dell'ambiente a una sorgente sonora senza amplificazione acustica.
(Similare ad il concetto di amplificazione trasparente, ovvero atteggiamento che dipende da vari fattori)

L'indice di rinforzo acustico da delle indicazioni su un apparato acustico che non si vuole che interferisca troppo con la sorgente naturale.

(Sorgente attraverso l'apparato di amplificazione confrontato con la sorgente senza apparato)

Ci sono delle normative che definiscono delle sorgenti standard per poter confrontare i diversi ambienti. Ci sono diversi modi di definire questo rinforzo acustico, c'è ad esempio una norma che definisce 2 possibili criteri di misura di questo indice di rinforzo acustico:
1. primo criterio è legato alla misura dell'energia, ovvero l'energia che arriva dalla sorgente amplificata in rapporto dall'energia che arriva senza amplificazione a 10 metri di distanza(misuro energia che mi arriva da apparato di amplificazione, e energia senza amplificazione a 10m e svolgo il calcolo per trovare la G) (vi sono degli altri parametri che vengono utilizzati in base a necessità e a ció che devo realizzare)

![rinfacustico](rinfacustico.png)

L'indice G con sala vuota tra 500Hz e 1Khz
- G >= -4dB per sale destinate alla musica
- G >= +4dB per sale destinate al parlato (intellegibilità)

2. secondo criterio è il rumore di fondo, che puó dipendere da tante cose, come l'isolamento dello spazio dall'ambiente esterno, rumore da impianto circolazione aria, ed a sala piena il rumore dipende da fattori come la sollecitazione delle strutture, e dunque per valutare i criteri di fruizione percettiva della sala, andrebbe fatta un'analisi del rumore di fondo, che è un aspetto importante.
In una sala grande, il rumore di fondo puó arrivare anche a 40, 50dB.




## I meccanismi percettivi dello spazio e della percezione della sorgente

Bisogna pensare che il nostro sistema percettivo-cognitivo è un grande riconoscitore di pattern, quindi tutti i meccanismi introdotti sono sempre continuamente concorrenti, cioè tutti i sensori dell'udito(e della vista) sono tutti continuamente e contemporaneamente attivi. Dunque una immensa mole di dati ed elaborate in alcune zone periferiche del cervello(non nell'encefalo), ovvero i gangli. Questi dati sono processati in parallelo e contribuisce a costruire dei pattern cerebrali(mappe cerebrali, ovvero dei pattern di attivazione neurale spazio-tempo varianti, come dei lampi nei temporali che cambiano nel tempo), ognuno di essi corrisponde a condizioni di percezione. Il nostro cervello è un'oggetto complesso con immensa capacità di riconoscere pattern che è organizzato e immagazzinato nella nostra memoria. E riconosce ed individua un certo tipo di informazione.

In alcune condizioni alcuni meccanismi sono piú o meno utili, ma il cervello elabora i dati da piú luoghi sensoriali.
__________________
### Meccanismi di lateralizzazione

Riconoscimento dell'angolo della sorgente sul piano azimuthale(orizzontale), usando un riferimento polare a 3 dimensioni con:
1. un parametro per la distanza (modulo del vettore)
2. azimuth -> altezza
3. zenith  -> inclinazione
__________________
### Meccanismi di localizzazione laterale(direzione)
#### ITD
_Interaural time difference_ è la differenza di tempo di arrivo del segnale alle due orecchie. La valutazione della differenza di tempo di arrivo non è banale per tutti i tipi di sorgente perchè i gangli fanno una misura di correlazione tra i segnali che arrivano alle due orecchie in un tot. di tempo. Se il tempo è al di sotto di un certo valore il sitema percettivo non riesce a stargli dietro e la misura di valutazione è poco significativa.

Al di sotto degli 800 Hz e sopra gli 80 Hz viene valutata la differenza di fase, sopra gli 800 Hz  il nostro orecchio non è in grado di fare questa distinzione e viene dunque valutato il ritardo di gruppo, ovvero la differenza di arrivo dell'inviluppo dei due segnali alle due orecchie.

Ciò è importante nella percezione dello spazio, poichè le riflessioni dello spazio arrivano come degli impulsi e proprio su ciò viene valutata la differenza dei ritardi di arrivo alle due orecchie.

#### IID (ILD)

_Interaural intensity difference_ o _Interaural intensity difference_ anche qui vi è una differenza di livello tra i segnali che arrivano alle due orecchie. Ma da cosa può dipendere la differenza di livello che arriva alle due orecchie? Cosa puó portare a una differenza di livello percepito dalle due orecchie? La presenza della testa ad esempio non vale per tutto lo spettro ma c'è la diffrazione e dunque alcune frequenze che hanno piú difficoltà ad attraversa ostacoli ovvero le lunghezze d'onda confrontabili con la testa non oltrepasseranno la stessa, la testa realizzerà un ostacolo. Le frequenze basse hanno una lunghezza d'onda e quindi la capacità di aggirare la testa mentre le frequenze piú alte non riescono ad aggirarla.

L'ILD dove funziona bene e dove non funziona?
Quando le lunghezze d'onda sono piú grandi delle dimensioni medie della testa il campo aggira la testa con stesso livello su orecchio destro e sinistro, man mano che si sale in frequenza la testa diviene sempre piú visibile.

Campo come una persona che cammina che vede o non vede ostacoli, se l'ostacolo non è confrontabile, allora esso non tocca ne ostacola il passaggio della persona.
Se l'ostacolo appare, la testa inizia a fare ombra, allora l'intensità sulle due orecchie è diversa(parlando sempre ovviamente di sorgenti fuori asse).

Dimensioni medie della testa 21,5 cm , se la confrontiamo con la formula f=c/lambda -> dunque f funziona bene su frequenze superiori ai 1600 Hz.

#### Cono di confusione

![The-concept-of-the-cone-of-confusion.png](The-concept-of-the-cone-of-confusion.png)

Ci sono delle superfici dello spazio che danno luogo a delle ambiguità, poichè se si pensa ad una sorgente che è su un certo punto dello spazio, e da dei risultati, tutte le sorgenti che sono all'interno del cono ha stessi valori di IID e ITD a entrambe le orecchie e vi è dunque un'ambiguità intrinseca. Teoricamente il nostro sistema percettivo si troverebbe in uno stato di confusione(usando solo IID e ITD) poichè non saprebbe determinare la reale posizione della sorgente. Allora la nostra percezione viene guidata anche dagli altri meccanismi (adattivi di movimento in questo caso), tendiamo a girare la testa per assumere posizioni dinamiche, o a prolungare il padiglione auricolare, o sennó usiamo le _HRTF_.

#### HRTF

Le _Head related transfer functions_.

Da piccoli eravamo felici perchè il cervello era vergine, con una mente psichedelica che aggira tutte le costruzioni della nostra storia psichica. Il bambino impara come è fatta la realtà esplorandola in tutti i sensi, perchè volgiono costruirsi una rappresentazione della realtà. Questa rappresentazione inizia a costruire delle cattedrali percettive. Una delle attività che il bambino fa è lavorare sull'udito. Il bambino ha dei padiglioni auricolari che per effetto di diffrazione e rifrazione generano un certo tipo di filtraggio dipendente dalla lunghezza d'onda del segnale. A seconda della lunghezza d'onda i padiglioni possono essere percepiti come inesistenti oppure si possono sentire tutte le curve. Da una certa lunghezza d'onda in giù e da una certa lunghezza d'onda in su i padiglioni auricolari avranno un ruolo nell'ascolto. I padiglioni a seconda della frequenza attueranno un filtraggio a seconda della direzione e altezza sorgente. Il bambino quando apprende la localizzazione delle sorgenti, il bambino memorizza i pattern di filtraggio (mettendoci anche la verifica visuale), il cervello fa una verifica di direzione.
Dopo anni i meccanismi del bambino si rinforzano, il nostro cervello quindi impara e classifica pattern e li realizza quando li ri-ascolta. Il cervello associa dei pattern di filtraggio(delle mappe) con delle direzioni nello spazio che riesce a stimolare quando gli si ripresentano.

![cipic_hrtf_HRTF_med](cipic_hrtf_HRTF_med.png)

Questi sono dei pattern di filtraggio.
In ascissa abbiamo la frequenza in KHz(dai 2KHz poichè per interagire con il padiglione, testa e torso, deve avere una certa forma d'onda), il profilo di filtraggio cambia a seconda della direzione della sorgente (sulle ordinate a destra). C'è ad esempio un notch sui 6KHz ed il notch si sposta progressivamente sulle frequenze piú alte. Qui si vedono tutte le frequenze che si spostano in frequenza a secondo della direzione.

L'HRTF per la composizione è utile per dare l'impressione che un suono provenga da un certo punto specifico.

Come vengono utilizzate le HRTF in simulazione?
Queste funzioni trasformate e portate su dataset, possono esere codificate in una libreria di filtri a seconda della direzione che vogliamo imprimere alla sorgente.

Se ho un tot di curve di filtraggio, posso interpolare lo spostamento di suono tra un punto e l'altro.

Attraverso un algoritmo si riescono dunque a spostare i suoni e si possono anche interpolare suoni tra una curva ed un altra.

Questo meccanismo aiuta insieme a ITD e IID aiuta a defiire la posizione del suono alto, basso e dietro(sul piano zenithale ovvero verticale).

Questi meccanismi sono di lateralizzazione, poi ci sono
__________________
### Meccanismi di percezione della distanza

La sorgente è individuata da due angoli e una distanza.
Tutti questi meccanismi sono attivi sempre in maniera simultanea e sono organizzati e messi insieme dal nostro cervello.
Il bagaglio esperienziale viene utilizzato e che permette di realizzare delle triangolazioni creando delle aspettative.

#### Loudness

Il primo meccanismo è la loudness ovvero l'intensità della sorgente. Come facciamo però ad esempio a distinguere una sorgente debole vicina ed una sorgente potente lontana?

#### Filtraggio dell'aria

Un indizio ed un primo riconoscimento puó essere il filtraggio che applica l'aria sul suono, e dunque il nostro sistema percettivo riconosce il filtraggio e fa un ipotesi al contrario quando si tratta di determinare la distanza di una sorgente.

#### Movimento (parallasse)

Che è denotato dal feedback visivo(se siamo in grado di vedere la sorgente), poichè il movimento ci da l'indizio che ci permette di fare triangolazioni dinamiche.

#### Rapporto tra suono diretto e suono riverberato(ITDG)

Perchè se io ho una sorgente vicina in un ambiente chiuso ITDG(ovvero rapporto tra suono diretto e prima riflessione) avrà un certo valore. Ci sarà una differenza di tempo di arrivo tra due suoni, se la sorgente è lontana, questo spazio sarà molto minore.

__________________

### Localizzazione in spazi chiusi

Tutto ciò che mi interessa è racchiuso in un singolo istante.

#### Effetto HAAS o precedenza

Che permette di minimizzare l'energia percettiva e realizzare la forma globale dell'oggetto sonoro, e ciò che mi interessa è che tutte le riflessioni siano minimizzate nel primo istante.

Il cervello usata la minimizzazione sempre, e allora il cervello si è abituato a percepire alcune caratteristiche del timbro, etc... (come le caratteristiche di un volto)

Non perdo tempo in dettagli per cose che non mi servono -> minimizzazione.

#### Caratteristiche spettrali della sorgente (Cues spettrali)

Noi nei primi anni dell'apprendimento, nei pattern, ci mettiamo anche le caratteristiche spettrali di quella sorgente, ed utilizziamo quelle informazioni. Come il parlato filtrato dall'aria; e riconosciamo la voce e il filtraggio facendoci trovare la sorgente e la distanza.

[link a wikipedia dove sono trattati i temi della lezione](https://en.wikipedia.org/wiki/Sound_localization)
__________________
## REW calibrazione

- calibrazione scheda audio -> risposta in frequenza scheda audio che usa per calcolare la risposta all'impulso, compensando la risposta all'impulso
- calibrazione speaker
- calibrazione microfono -> microfoni di misura (isotropi) con diagramma sferico(omnidirezionali) e rispsosta in frequenza molto piatta [microfono da misurazione beyerdynimic](https://europe.beyerdynamic.com/mm-1.html), questa tipologia di microfono è molto costosa, poichè ogni singolo microfono ha la risposta in frequenza di ogni singolo microfono, ciò ci da la possibilità di avere le caratteristiche della risposta in frequenza.
