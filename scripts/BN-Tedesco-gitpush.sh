#!/bin/sh

#  BN-Tedesco-gitpush.sh
#
# use it by terminal typing:
# bash BN-Tedesco-gitpush.sh

cd /Users/davide/gitlab/SMERM/BN-Tedesco

git add .
DATE=$(date)
git commit -am "changes made on $DATE"
git push
