#!/usr/bin/env bash

# BN-Tedesco-gitpush-linux.sh
#
# use it by terminal typing:
# bash BN-Tedesco-gitpush-linux.sh

cd /home/davide/gitlab/SMERM/BN-Tedesco
git add .
DATE=$(date)
git commit -am "changes made on $DATE"
git push
