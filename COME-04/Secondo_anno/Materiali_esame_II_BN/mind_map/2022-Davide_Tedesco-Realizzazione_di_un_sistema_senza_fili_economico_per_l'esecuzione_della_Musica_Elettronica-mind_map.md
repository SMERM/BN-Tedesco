---
markmap:
  initialExpandLevel: 0
---

# Realizzazione di un sistema senza fili economico per l’esecuzione della Musica Elettronica

## L’idea e lo sviluppo di essa
### I vari step di evoluzione e lo schema di funzionamento generale
### Il funzionamento
### Perché un sistema senza fili?
### Perchè riproduzione audio su un solo dispositivo?
## Perchè utilizzare Raspberry Pi Pico?
### Economico
### Efficiente
### Open Source
## La realizzazione
### L’hardware utilizzato
#### [Raspberry Pi Pico](https://gitlab.com/SMERM/BN-Tedesco/-/raw/master/COME-04/Secondo_anno/Materiali_esame_II_BN/images/picow-pinout.png)
#### [Waveshare Pico-Audio](https://gitlab.com/SMERM/BN-Tedesco/-/raw/master/COME-04/Secondo_anno/Materiali_esame_II_BN/images/Pico-Audio-details-inter.jpg)
#### Lettore di micro SD
#### Interruttore a pulsante
#### Led di vari colori
### [Schema dei collegamenti del progetto](https://gitlab.com/SMERM/BN-Tedesco/-/raw/master/COME-04/Secondo_anno/Materiali_esame_II_BN/images/drawn_scheme_complete.png)
### Il software utilizzato e quello realizzato
#### MicroPython e Thonny
#### [Codice utilizzato per il controllo dell’hardware](https://gitlab.com/DavideTedesco/rppico4em)
## Analisi del sistema realizzato e suoi utilizzi
### Come l’utente si approccia all’utilizzo del sistema
### Problemi rilevanti riscontrati durante l’utilizzo
## Conclusioni e sviluppi futuri
## [Demo](https://www.davidetedesco.it/vault/index.php/s/DbTT9soy8fqpSDb)
