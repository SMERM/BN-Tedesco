# Appunti della lezione di martedì 23 marzo 2021

registrazione simil-podcast

## Strumenti per l'uso del software

### Source code managment system

Sistemi per gestire il codice.

Tante persone lavorano insieme sullo stesso codice, e devono lavorare e gestire insieme il codice.

Questo ha generato SCM (Source Managment System), di cui il piú rinomato è git.

#### Storia dei Source Code Managment
![sistemi_scm.png](sistemi_scm.png)

##### SCCS
Il primo SCM si chiama SCCS a cui si affidava un file e faceva una copia del file.

SCCS riproduceva una copia del file dal file `s.hello.c` e questo file conteneva tutte le versioni della sorgente.

Quando vi era un file fuori dal `s.hello.c` non si potevano fare modifiche.

##### RCS

Un altro versioning system che faceva la stessa cosa `hello.c,v` e venivano salvate le sole differenze dei file.

**Tutto era sullo stesso disco dove vi era la sorgente stessa.**

##### CVS

Sistema in cui si poteva salvare intere alberature, con backend RCS, e si potevano tenere cartelle, files etc...

CVS introdusse l'idea che vi fosse una macchina remota, e ciò è tenuto in un'altra macchina in cui esiste una copia del file.

Di queste copie ce ne possono essere molte in tante macchine diverse, e si può quindi lavorare in parallelo(remote CVS).

**Viene introdotta quindi una gestione dei congflitti**

### Come funziona un SCM?
![scm_working.png](scm_working.png)

A partire da un sistema di versiongi si ha:
- una macchina remota
- programmatore A
- programmatore B

Il programmatore A e B fanno una copia del software sulla propria macchina, e programma.

Il programmatore A, chiude la giornata e fa un `commit` dentro il repository remoto. La copia che è in locale del programmatore A diviene la copia remota.

Quando il programmatore B, vuole fare un commit, deve prima scaricare il file aggiornato dal programmatore A.

Le due sorgenti si divaricano allontandosi...

(generalmente ci si organizza per lavorare a cose diverse)

Se il programmatore A tocca un file che interessa il programmatore B, allora bisogna realizzare un `merge`, se non si riesce a realizzare un merge, si hanno delle parti in conflitto.

Se vi è un conflitto bisogna risolverlo, altrimenti non si puó fare un commit.

Nei repository vecchio stampo, il repository remoto è detto master.

Tipologie di SCM: CVS, SVN, ...

Gli ultimi ritrovati di SCM sono:
- Git
- mercurial
- arch

### Come funziona Git?

Git ha un repository remoto, quando si vuole il sorgente su un computer, si realizza un `clone`.

Qunado si programma e si fa un `commit`, si fa un cabiamento sul repository remoto.
![git_working.png](git_working.png)

Posso fare tanti commit, e poi facendo un `push`, faccio quanti cambiamenti voglio.

Quando un altro programmatore deve fare un `push`, fa prima il `pull`, scaricando tutti i dati.

Si può lavorare benissimo senza rete, e quando si è pronti si fa un `push`.

#### `fork`

Il repository remoto viene clonato su un altro repository indipendente. Si fa questo, perchè se voglio modificare del software che hanno realizzato altri, perchè il software libero mi consente di fare ciò che voglio.

Nei software manager precedenti, non potevo reinserire il mio sviluppo privato nel codice originale, con Git si può:
- fare un `fork` -> segnato che ciò proviene da qualcosa
- posso andare incidentalmente, fare un `pull request` al repository principale, aggiungendo quello che ho fatto nel mio `fork`, riniserendole nel branch principale

### "branching"

Con Git posso fare branching molto facilmente, le modifiche le posso fare su un ramo principale, o su rami adiacenti, e far rientrare le modifiche nel ramo principale.

Abbiamo ad esempio un repository Git, per il corso di fondamenti di informatica.

### Cosa succede quando faccio un commit?

Repository che è un clone di ciò che è in rete in locale...

Provo a fare vari commit notando che i numeri di commit sono diversi, per recuperare un commit faccio `git checkout ...`

Posso ritirare fuori la versione precedente a quella in cui sono, tutte le versioni precedenti sono quindi recuperabili.

Si puó vedere qual'è la modifica fra due files...

Posso anche vedere la differenza fra due files...

Posso dare un file in pasto ad un altro software(patch), per attaccarlo con righe nuove e vecchie.

>Esistono il SCM, usiamoli per gestire bene testi e file binari.

Sui testi si possono vedere le differenze, ma su un file binario, si devono tenere tutte le versioni di un file binario, mentre sui file di testo si possono fare delle ottimizzazioni.

Un file audio non ha altra possibilità che essere sostituito.

Non conviene usare Git per caricare files audio.

#### `git lfs`

Esso permette di fare cambiamenti continui di files pesanti. Ma non conviene salvare varie versioni audio di uno stesso brano, ma serve il file finale che puó essere salvato in altri luoghi.

Tutti i file sorgente, posso essere salvati su Git.

### Strumenti di continuus integration

Gli SCM sono molto utili per noi.

Altri software hanno bisogno di altri elementi, e ci interessa sapere che un programma giri su altre macchine.

Ma come si fa ad avere un sistema per verificare la compilazione e i test?

Come fare verifiche in continuazione?

Si usano dei servizi di CI(Continuos Integration) che accedono ai nostri repository e facendo un clone, fanno tutto quello che gli si dice di fare, con tante macchine virtuali che sono legati tutti quanti a GitHub.

Il piú famoso CI software è [Travis](https://travis-ci.org/)

Esso fa il clone dei repository, in cui possiamo dirgli di fare i test su tutte le macchine ed abbiamo poi dei reports.

Ve ne sono altri come [Jenkins](https://www.jenkins.io/)

Questi servizi servono per fare test a tutto tondo.

Appena il CI vede che ho fatto un pull request, mi viene fatta il test.

Tutte le macchine virtuali sono vergini, rimuoviamo tutte le dipendenze, per verificare che funzioni tutto.

La macchina virtuale viene creata sul momento per fare la prova.

### Cointainerizzazione

Abbiamo capito che attraverso la rete si posso realizzare molte cose, e si possono far interagire le stesse. Ci si è accorti che se un server girano molti servizi, tutti i servizi interagiscono fra loro e possono andare in conflitto. Vi sono delle interazioni che sono negative fra i vari servizi.

Si creano allora dei cointainer, che sono delle macchine virtuali su cui far girare un sistema operativo separato che gira come un processo della macchina fisica, esso alleggerisce molto il carico. I container sono molto snelli e veloci.

Se realizziamo un servizio che gira su Python ed abbiamo bisogno di una certa versione su Python, possiamo mettere una versione della macchina virtuale specifica.

I container piú famosi sono:
- [docker](https://www.docker.com/)
- ...

I container possono essere aggiornati e trovarsi tutta l'applicazione aggiornata...

I container sono molto importanti, per il mondo che stiamo realizzando.

Abbiamo tutto depositato su molti cloud diversi ed è molto importante capire bene come funzionano i containers.

## Software libero

La necessità di utilizzare software libero per la creatività.

All'inizio tutto il software era libero perchè era complicato farlo.

Il software ad un certo punto è divenuto un mercato, ed il software era il mercato della società dell'informazione.

Mentre ora il vero motore della società dell'informazione sono i dati.

Avverrà un cambio di paradigma quando vi sarà un'altra società, la società dell'informazione nacque circa 25 anni fa.

Il modello di società che abbiamo ora è che si muore di obesità e non piú di fame.

Il paradigma della società industriale ha portato alla massificazione del mercato portando a grandi produzioni di energia.

Nella società dell'informazione i ricchi sono i dati.

L'asset della società dell'informazione sono i dati.

Il software è un mercato difficile da portare avanti.

Chi ha portato avanti il software come prodotto, è chi ha attuato meccanismi di user-lock-in come quello di Motorola realizzato da Bill Gates con Microsoft(obbligavano Motorola ad usare Windows).

È chiaro che si capisce che a 30 anni di distanza, il software chiuso non conviene, ma a Google non conviene realizzare software per venderlo, ma il vero asset sono i dati.

Nel caso di Facebook siamo noi che contribuiamo ad arricchire Facebook.

### Come funziona Google?

Google ha dei robot o Crawlers(bot particolari), ovvero programmi che vanno in giro per la rete.

I Crawlers sono dei bot particolari, che vanno per pagine, di cui ve ne sono miliardi sparsi in giro che leggono i link e mandano indietro a Google.

I contenuti di Google sono le pagine cercate dai Crawlers ed indicizzate.

Nel fare questo, Google realizza anche un ranking, quindi se tante pagine linkano ad un link, il ranking della pagina sale.

Google ha iniziato a fare molti soldi, dopo aver realizzato il servizio pubblico, con AdSense per fare pubblicità.

### Perchè per la composizione elettroacustica è meglio usare software libero?

Il software proprietario non è modificabile, il software proprietario è elaborato da programmatori esperti, come Max MSP, che veniva da un'idea di Joel Chadabe mentre discuteva con Miller Puckette.

Realizzando quindi il patching con oggetti che si connettevano fra loro.

Fino alla versione 0.56 Max si faceva girare su Floppy.

Dopodichè l'IRCAM vendette la licenza di Max MSP a OpCode, casa di produzione di software.

Questa idea è un'idea di costruire qualcosa che possa fare da strumento.

CSound in contrasto è un software mirato alla composizione per realizzare uno strumento compositivo(si scrive e poi lo strumentista la suona).

CSound è piú nell'ottica compositiva.

Per fare Live Electronics, Pure Data o Max MSP è più adeguato.

Ognuno di noi per fare quello che vuole fare ha bisogno di adattare quello che vuole fare a se stessi.

Il software proprietario ti fa lavorare come vuole lui senza darti possibilità.

Ableton è considerato molto sofisticato, anche se per adattarlo a qualcosa di particolare è molto complicato.

(FAUST funziona bene solo per fare strumenti, ha una logica strumentale e non compositiva, serve per fare un certo tipo di cose. Se per caso si è in una logica strumentale e non rientra in quello che c'è in giro, è relativamente semplice. FAUST piace molto a JOS perchè a lui piace molto l'elaborazione dei segnali.)

### I diritti del software libero
Il Software libero ha una licenza ha dare 4 diritti/libertà all'utente:

0. diritto ad eseguire il programma
1. diritto di accesso alla sorgente
2. diritto di modifica delle sorgenti(attribuendo da dove provengono)
3. ridistribuire le sorgenti nello stesso in cui le hai trovate

Se si usa software libero, non si puó ripackegare, riutilizzarlo e farlo divenire proprietario, perchè deve rimanere libero.

>L'importante è che ci sia del software che rimanga libero.

### Cosa troviamo di software libero
- strumenti
- software di lavoro
- sistemi operativi
- ...

In generale tutti i linguaggi di programmazione sono liberi...

Fare un linguaggio di programmazione proprietario, significa condannarlo.

Le sorti dei linguaggi di programmazione sono molto fluidi e legati all'ambiente.

Python dal punto di vista delle librerie matematiche è molto rifornito.

Ruby ha meno successo anche se è molto piú strutturato ed è piú efficiente.

## Come funziona un programma?
![programmazione.png](programmazione.png)

Il programma è in memoria, ed è scritto in codice macchina, abbiamo un'istruzione e la CPU non fa altro che scorrere la memoria.

Certe istruzioni della CPU possono far saltare il Program Counter(con un JUMP), questo è il funzionamento interno e finale di un programma.

All'inizio si riempivano le caselle di memoria un po' per volta, attraverso degli interruttori. Gli errori erano all'ordine del giorno.

La prima cosa che si pensò, fu cambiare la collezione di istruzioni che sono in memoria(Von Neumann).

L'idea della programmazione è che vi è un software intermedio che traduce un testo scritto in un linguaggio in un set di istruzioni per fare una collezione di azioni.

L'idea è che il codice, che è del testo puro scritto in una sorta di inglese, in cui vi è un codice che legge il testo e lo converte in codice macchina per essere eseguito.

(Von Neumann Documentario)

### Tipologie di Linguaggi

#### Linguaggi Compilati

Il programma che interpreta il linguaggio, il compilatore, legge il codice sorgente e produce un file eseguibile, che puó essere messo nella RAM per essere eseguito. Questo file è la collezione di istruzioni già pronte per l'uso per una macchina specifica.

(Teoria dei compilatori interessante)

Linguaggi di questo tipo sono:
- Pascal
- C
- C++
- GO

Linguaggi musicali di questo tipo sono:

- CSound
- FAUST (è compilato ma compilato in modo diverso da CSOUND)

#### Linguaggi interpretati

Non vi è un compilatore, ma un interprete che legge il codice e lo trasforma senza file intermedio in codice macchina.

Possiamo prendere una console e scrivere del codice in Python senza compilarlo.

Linguaggi di questo tipo sono:

- Perl
- Python
- awk
- Ruby

Linguaggi musicali di questo tipo sono:
- Pure Data
- Super Collider

>A un certo punto si fece un linguaggio a metà tra interpretato e compilato come Java, approfondito nella prossima lezione.

## Programmazione

Un uso adeguato dei computer è quello che si ottiene programmando.

Quando si sa programmare il computer, esso diviene molto utile.
