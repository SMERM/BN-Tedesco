# Appunti della lezione di martedì 2 marzo 2021

## Ripasso
Avevamo visto le variabili d'ambiente.

Come fare dei comandi che realizzano degli script

## Link simbolico
Comando `locate .wav` simile a spotlight di mac, serve a cercare un determinato elemento nel disco.

```
ln -s path suoni
```

Avremo ora una cartella linkata simbolicamente a suoni

`cd suoni` possiamo vedere tutti i suoni che vi sono nella cartella suoni

Ora leggiamo con `sndinfo` le informazioni dei files

Avevamo fatto uno script che ci contava le durate totali dei files wav.

### Script per la durata totale dei file .wav in una cartella


```
#! /bin/bash

if [ $# -lt 2 ]
then
  echo "usage: $0 file [...]" 1>&2  #uno o piú file
  exit -1
fi #chiusura dell'if

sndinfo $@ |& grep seconds| sed -e "s/^\s*//"| cut -d" " -f7

```
- se gli argomenti sono meno di due devi uscire
- tutti gli elementi li prendo e tolgo gli spazi, ciò che è fra gli slash è un espressione regolare, prendiamo una classe di caratteri e li strasformiamo con quelli nella seconda parte dello slash

**attivo il permesso di esecuzione**

Lettura scrittura ed esecuzione

3 bit, 3 bit e 3 bit
Primo numero -> r,w,x al proprietario 111 -> 7
Secondo numero -> r,x al gruppo di esecuzione 101 ->5
Terzo numero -> r,x al gruppo  101 ->5

`chmod 760` -> lettura, scrittura ed esecuzione per il proprietario,  lettura e scrittura per gruppo francesco

`chmod 755`-> abbiamo realizzato i tre numeri

Ci ritorna 255 -> poichè vedendo in maniera segnata il numero -1 in 8 bit è 255, 7 bit segnati

comando che fa calcoli su basi qualsiasi: `bc`

Avevamo aggiunto un comando di AWK per fare la somma di tutte le durate
```
#! /bin/bash

if [ $# -lt 2 ]
then
  echo "usage: $0 file [...]" 1>&2
  exit -1
fi

sndinfo $@ |& grep seconds| sed -e "s/^\s*//"| cut -d" " -f7| awk 'BEGIN {totale=0}{totale+=$1} END{print totale}'
exit 0
```

**le doppie virgolette sono diverse dalle virgolette singole**... lo abbiamo visto con awk

Le doppie virgolette vengono interpretate dalla shell, mentre le virgolette singole non vengono interpretate dalla shell(non fa il name globbing)

### inode

Sono unici per ogni identificativo, anche se il file ha tanti nomi infatti facendo un link fisico
`ls -l`

`ls -li`

Se ho un file con link fisico, se cambio qualcosa, sto cambiando entrambi i files.

Ci sono sempre 2 directory in piú: `.` e `..`

## Compito per la prossima volta

Comando che mette il nome del file e la sua durata(anche altri dati)

E alla fine realizza una serie di totali.

Per fare un comando del genere, usare il comando `awk` che è un filtro molto completo.

Conviene usare piccoli comandi che fanno cose molto specifiche secondo il paradigma UNIX:
>Small is beatiful

Abbiamo usato infatti:
- grep
- cut
- sndinfo
- awk

L'idea nel programmare da linea di comando è l'opposto dei comandi grafici monolitici(photoshop), mentre da CLI possiamo trovare sempre ciò che fa per noi.

## Altri comandi utili e elementi utili

### `alias`

Se usiamo molto il comando `dur2` creaimao un alias facendo: `alias l='ls -l'`

`alias d = './dur'` bisogna mettere il path per il comando dur.

### file di profile
Di tutti gli shell ci sono alcuni file molto importanti, che sono file di comandi di shell

`less .profile`

Ci sono una serie di comandi che stabiliscono il path...

Quando profile viene eseguito vengono cercati ed eseguiti dei files.

Il profile viene letto da qualsiasi shell, mentre il file bashrc viene letto solo da bash.

Se si voglio settare degli alias che ci siano sempre li inseriamo in bashrc... (in linux)


### `unalias alias`

possiamo usare `unalias` oppure `\alias`

### /etc/profile

Quando una shell parte viene lanciato anche un altro file che setta una serie di cose per tutti:
`cat /etc/profile`

### `grep`

`ls - l |sed -e "s/\s\s*/ /g"|"cut -d" " -f9`

Se voglio tutte le righe che abbiamo il nome del file che comincia con la s.

Il `cut` serve per tagliare.

Con il `grep` troviamo il _Get Regular Expression Pattern_, un pattern è una formazione di caratteri formattata in un certo modo.

`ls - l |sed -e "s/\s\s*/ /g"|"cut -d" " -f9| grep "^[S s]"`

Ora voglio tutte le stringhe che ad inizio riga hanno s maisucola o minuscola

`ls - l |sed -e "s/\s\s*/ /g"|"cut -d" " -f9| grep "^[D-S]"`

`ls - l |sed -e "s/\s\s*/ /g"|"cut -d" " -f9| grep "^[D-Sd-s]"`

Le espressioni regolari permettoni di cercare all'interno di testi e permettono di filtrare a seconda di espressioni regolari e di realizzare operazioni diverse.

### `sed`

Esso è uno streaming editor, e permette di fare dei cambiamenti in un file senza manco guardare.

```
cat dur2 #vedo il file dur2
cat dur2 | sed "s/[Ss]/@@@@"
#per farlo su tutte le righe farò
cat dur2 | sed "s/[Ss]/@@@@/g"
```

Il sed viene fatto in streaming, il file non è cambiato

### `make`

Comando che permette di stabilire l'ordine in cui devono essere eseguiti un certo numero di comandi e non ripeterli se essi sono già stati eseguiti.

Possiamo realizzare una serie di lanci di comandi con uno shell script, ma il comando `make` permette di realizzare attraverso un file denominato _Makefile_ che stabilisce anche delle dipendenze.

#### Makefile
- target
- comando da fare

Il Makefile può avere diversi files, puó avere sequenze di comandi molto complesse.

Nel Makefile si possono mettere variabili etc...

L'unica cosa di ricordarsi è fare il `make`, dopo aver realizzato il Makefile.

## Comandi utili per l'audio

### `sox`

è il coltellino svizzero dei file audio

#### Ricampionare un file
Voglio ricampionare un file a 48000 Hz stereo un file.

`sox suoni/id_c1.wav -c2 -r48000 xxx.aiff`

sox può fare missaggi, filtrare etc...

`man sox`

[sito di SoX](http://sox.sourceforge.net/)

### `lame`

Esso trasforma i file in mp3
`lame xxx.info`

Esso è un encoder con trademark, quindi usare altri encoder.

### `oggenc` o `vorbis-tools`

Esso trasforma i file in OGG vorbis

### `flac`

Esso è un encoder che trasforma file in flac

### `ffmpeg`

È l'ultimo comando che vediamo.
Esso è l'equivalente di `sox` ma per video...

Permette di fare encoding e decoding da diversi formati, e si puó fare in maniera leggera, ed è fatto molto bene.

Esso realizza anche encoding audio.
_________
Dalla prossima lezione passiamo alle reti.

Dobbiamo fare il `superdur` per la prossima volta, ovvero un comando tiri fuori tutti i nomi dei file con le durate relative e alla fine il valore totale. Per fare questo bisogna lavorare molto su `awk`.
