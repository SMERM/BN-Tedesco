# Appunti della lezione di martedì 9 marzo 2021

Per avere piú informazioni oltre a quelle della durata bisogna prendere piú informazioni.

## Durata totale di una serie di files con uno script in bash che utilizza `awk`
Realizziamo tutto in `awk`.

`awk` individua le righe con le espressioni regolari, poi esegue all'inizio ciò contrassegnato con `BEGIN` ed alla fine ciò contrassegnato con`END`.

```
#! /bin/bash

if [ i# -lt 2]
then
      echo "usage: $0 file [...]" 1>&2
      exit -1
fi

sndinfo $@ |& awk 'BEGIN {totdur=0}
/\.[WwAa][AaIi][VvFf]*:$/ {nome=$1}
/seconds$/ {dur=$7}
/frames)$/ {totdur+=dur;print nome, dur}
END {print totdur}'
exit 0
```

Possiamo formattarlo meglio:

```
#! /bin/bash

if [ i# -lt 2]
then
      echo "usage: $0 file [...]" 1>&2
      exit -1
fi

sndinfo $@ |& awk 'BEGIN {totdur=0}
/\.[WwAa][AaIi][VvFf]*:$/ {nome=$1}
/seconds$/ {dur=$7}
/frames)$/ {totdur+=dur;printf("%-60s %12.4f\n", nome, dur)}
END {print "gran totale:", totdur}'
exit 0
```
Lo richiamo con `./dur nomecartella`

Per salire di complessità non basta `awk`, ma serve un linguaggio di programmazione piú complessa.

## Reti

### Connessione seriale
Connetto due o piú computer attraverso una coppia di cavi in modo seriale.

>Vi è un orologio e quando il clock glielo dice(la portante), scandisce a che velocità passa un segnale da una parte all'altra.

Il cavo sotto è il carrier signal o CAS

Le connessioni seriali si sono sviluppate fino a 96K.
![configurazione_di_rete.png](configurazione_di_rete.png)


### Il terrorismo e le linee seriali

Dato che le connessioni erano molto fragili, il Pentagono volle sviluppare una tecnologia sempre piú veloce e meno fragile che resistesse ad attacchi informatici.

Fino a quando le leggi andavano molto lente, non si doveva trasmettere nulla che non fosse necessario.

### L'introduzione dei pacchetti

(Circa dagli anni '70)

Poi si iniziò ad avere l'idea dello sviluppo di scambio di dati a pacchetto.

Ogni pacchetto ha:
- testata -> origine e destino del pacchetto
- dati
- coda

Le reti erano fatte inizialmente a token ring, esso è simile al progetto ARPANET, ogni computer parlava con uno e poi con l'altro, e dovevano passare un pacchetto al computer destinatario.

Il problema del token ring è che interrompendo da due parti, la rete si sarebbe isolata.

>Il protocollo TCP/IP è un protocollo realizzato a pacchetti ad esempio.

### Configuarazioni a stella

Si è passato poi a configurazioni stellari

Il router gestisce i vari dispositivi e si ha un cancello di casa(gateway), sulla rete vi sono configurazioni stellari.

La rete è distribuita ed i percorsi tra A e B sono molti, e la rete è inattaccabile, come sono fatte le piante.

>Werner Herzog Film consigliato sulla nascita di Internet

### Identificazione di un computer sulla rete

Nacque il protoccolo IP, che serve a dare un numero ad ogni computer.

IPv4 serve a dare numeri da 0 a 255 ovvero con 4 numeri da 8 bit che identificano ciascun computer.

Ciascuna parte di questi indirizzi identifica una zona.

L'ultimo è l'indirizzo locale del computer, il penultimo è un gruppo locale di computer.

![ip.png](ip.png)

Possiamo indirizzare 2^32 computer

192.168.w.z, 10.y.w.z e 172.y.w.z non possono essere utilizzati come indirizzi.

Esiste una Local Area Network (LAN) o rete locale, che è identificata tutta da indirizzi locali.

Possiamo visionare con `ifconfig` l'indirizzo della macchina sulla rete.

Quando ci connettiamo a una rete locale c'è un router che gestisce fa il NAT (Network Address Translation), nel pacchetto IP, sappiamo dove deve andare un pacchetto.

Nella rete WAN si hanno indirizzi pubblici che vengono gestiti dalla IANA, che vengono comprate dai gestori telefonici e rivendute ai propri utenti.

### TCP

La parte TCP è come è fatto il dato ed ha tutte caratteristiche. TCP ha il controllo di errore, in cui i dati vengono codificati con una coda di dati di parità, e se il dato è corrotto viene rinviato.

La rete diviene più lenta e vi sono un sacco di errori nel pacchetto che devono essere rinviati.

### UDP

Protocollo meno usato, e vicino al nostro MIDI, utilizzato in OSC.

Esso non ha il controllo di errore.

UDP viene usato poco, perchè nel web passando caratteri, potrebbero arrivare messaggi non corretti... Ciò non va bene, poichè la correttezza del messaggio è principe.

#### JACKTrip
Vi è un progetto cominciato con la pandemia, JACKTrip, che funziona connettendo via rete locale.

Questa rete permette di connettere il programma con più computer e nello stesso computer con lo stesso protocollo.

JACK fu realizzato per connettere streaming audio nella stessa piattaforma.

JACKTrip si può usare con il protocollo UDP perchè perdendo dei campioni comunque lo streaming non viene inficiato molto.

Nei CD era stato realizzata una correzione perchè vi erano problemi sui CD.

JACKTrip è stato pensato per aver minor latenza possibile, per suonare insieme a distanza.

[sistema per suonare in sincrono](https://spacelic.altervista.org/ninjam/)

>Si vuole allargare il pubblico per interagire attraverso questi mezzi, sennò si userebbe la televisione.

### Modelli di rete

#### Modello client-server

In cui vi è un modello come il negozio.

Il servente, non ha nessuna idea di quando il cliente vada a comprare qualcosa in negozio.

Prima bisognava avere il modem sempre connesso.

Ora abbiamo un processo server in una macchina con un IP del server e poi un'altra macchina che è il cliente che ha un IP.

![client_server.png](client_server.png)


Il server ha un indirizzo IP, e un numero di porte(al momento 65536) e sono una porta per servizio. Ogni IP si connette a una certa porta ascoltando le richieste attraverso il processo LISTEN.

Tra le porte ce ne sono 1024 e da 0-123 sono riservate.

La porta 80 è la porta riservata http.

Se non è la porta 80 sarà un'altra.

25 -> SMTP

443 -> https

Quando l'applicazione del cliente si connette fa partire un altro processo, restituisce un indirizzo al cliente. E il cliente parlerà con il servente specifico.

In un'applicazione client-server, non c'è un limite di clienti per la connessione.

Fare un processo separato per ogni cliente è la procedura normale.

Questo protocollo puó funzionare anche nella stessa macchina, da una macchina all'altra funziona semplicemente cambiando IP.

![lavagna_reti.jpg](lavagna_reti.jpg)

>Il browser è il principe dei clienti.

### Cosa succede quando digito google.com

Quando scrivo "google.com" sul mio cliente, il mio indirizzo va al server denominato DNS(Domain Name Server), e mi ritorna l'indirizzo del domain 8.8.8.8

Il DNS puó stabilire dove indirizzare il carico in base alle aree geografiche ed al grafico.

#### Come è fatto il DNS?

Non esiste un solo DNS, ma vi è una rete di server distribuita geograficamente. Se non trovo il nome nel DNS, andrò a cercare in altri server vicini localmente.

## Comandi sulle reti

### `ping`

Comando per vedere se una macchina è viva dall'altra parte del mondo.

Molti server disabilitano la risposta al ping.

DOS Attack -> pacchetti che mando molto pesanti.

>Ping vuol dire dare una schicchera

#### SonicPing
Ping che misura il ritardo del suono per misurare la latenza reale in JACKTrip

### Come funziona una VPN?

Abbiamo la WAN, che è un posto brutto...

Sono deitro un router che fa il NAT, ed il mio computer non è visibile dalla WAN, ma è solo visibile dal router.

(Il DHCP distribuisce gli indirizzi in locale con altro protocollo)

![vpn.png](vpn.png)

Come faccio a connettermi al computer dell'ufficio, come se fosse nella mia LAN?

Lo faccio con dei servizi che girano nel router, come il VPN, essa stabilisce un tunnel criptato (SSH o SSL encrypted) con una serie di traduzioni di indirizzi e tutto ciò viene fatto attraverso un canale criptato.

Per accedere alla VPN ci sono una serie di regole per criptare etc...

### Come si fa a fare il criptaggio di informazioni?

Non posso dire a chi non conosco come si fa a fare...

Devo spedire un baule con tanti dobloni, solo che non do la chiave, a una persona che non conosco, e con cui non comunico, come si fa?

Tutta la comunicazione https si basa sulla cifratura, di un'informazione.

>Mando il baule e lo chiudo con una catena con un mio lucchetto, il destinatario riceve il baule e lo chiude con una sua catena e con un suo lucchetto, me lo rispedisce, tolgo la mia catena e glielo rimando.

In questo modo il messaggio è sempre criptato.

Gli algortimi di criptaggio è tosto da crackare.
