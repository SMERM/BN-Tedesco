# Ascolto del Terzo pezzo dell'op.11 di Webern

![web](web.png)

## Domande
1. trillo iniziale del violoncello che è come se volesse svelare qualcosa, ma svela veramente qualcosa?
2. susseguirsi di riflessioni non dialoganti ma che realizzano un pensiero unico
3. la pausa riflessiva cosa indica? è una pausa riflessiva? o è una pausa per rimarcare ciò che viene dopo?
4. l'accordo del pianoforte che conferma la riflessione del violoncello nella prima parte e viceversa nella seconda, cosa significa?
5. mistura dei timbri e attualizzazione della melodia di timbri, vi è il passaggio attraverso le altezze e i timbri della melodia, è effettivamente verificato sempre?
6. che ruolo ha nella raccolta dei 3 brani quest'ultimo?

[link al brano](https://youtu.be/BzYBLDyKvYo?t=75)

## Analisi grafica

con carta da lucido

[pedale sordina](https://www.pianosolo.it/l-uso-dei-pedali/)

## Risposte

1. rispetto al trillo iniziale -> si accende la lampadina -> un suono tramite un timbro si smaterializza e non percepisco più l'altezza.

2. andare a indagare nell'indicazione della scrittura, si va ad indagare nei timbri puri. Nella musica da camera l'aspetto timbrico viene un po' meno, dunque se si scrive un brano per violoncello e pianoforte, la nota acuta prima della pausa esce dal resto perchè è una nota reale e non un armonico. Cambia il rapporto col silenzio della musica, il silenzio ha un peso specifico. Il dialogo degli strumenti è implicito. Mentre il vero dialogo è il silenzio. C'è un discorso anche di densità musicale.


6. che ruolo ha nella raccolta dei 3 brani quest'ultimo?
  1. esposizione dei timbri a mo di colori, con estratti variegati, armonici, accordi, suoni singoli, arpeggi, glissati, lascia con una domanda finale! (Vocabolario -> Domanda)
  2. affermazione forte troncata, presenza di dinamica forte e velocità (Distrazione/Confusione)
  3. riflessione timbrica con doppio pensiero, quasi a stabilire una morale, un filo unico finale.(Risposta pacata)


**Togliendo i suoni e lasciando le dinamiche, c'è già un brano completo!**
