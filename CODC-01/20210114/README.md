# Appunti della lezione di Giovedì 14 Gennaio 2021

Partitura di Berio e la notazione proporzionale nella Sequenza di Berio.

Alea controllata di Stockhausen ed in generale europea è stringente rispetto ad un'alea americana:
![stock](stock.png)

Chi inizia a eseguire brani, sono:
1. esecutori
2. compositori che eseguono

Serie di sovrastrutture da combattere.

Nel concerto di Cage ad esempio la grafia entra a far parte della composizione che viene identificata con dei simboli grafici.
![cage](cage.png)

Simboli interpretati con certa e grafia.

L'idea del brano diviene piú importante della realizzazione stessa.

Si prendono delle partiture antiche stravolgendole.

## Schema musicale
![sch](sch.png)

L'autore si mangia l'esecutore e potrebbe creare dei problemi al destinatario.

![sch1](sch1.png)

Lo schema iniziale si semplifica ed in regioni geografiche diverse si è cercato di risolvere in maniera diversa.

Essendo destinatario ed esecutore per deve mettere in condizioni di prestarsi a questo.

Il gioco delle notazioni è legato molto alla concezione di base.

1. Appropriarsi della prassi esecutiva e farla divenire grafia
2. Usare il codice e realizzare la prassi esecutiva

(Gould smetterà di fare concerti per realizzare sole realizzazioni)

Registrare su nastro e su disco ha fatto cambiare la prassi esecutiva.

Vi è una serie di elementi che vanno da:
- notazione
- prassi esecutive
- strumentisti

Corsi di Cesare Brandi, faceva delle lezioni chiarissime.

Prima del tg delle 13, 20 minuti di analisi d'arte di Cesare Brandi.

"Verba volant scripta manent" -> puó essere interpretato anche al contrario.

Alberoni -> "abbiate coraggio" -> società democratica che si basa sulla prassi

Testo di Daniele Lombardi sulla notazione

## Tendenze fondamentali per la codifica
### Ipercodifica
![cod](cod.png)
### Ipocodifica
Necessità di ampliare l'intervento esecutivo ->margine di liberta estemporanea.
![cod1](cod1.png)

### Rapporto tra cognizione e realtà
- percezione (varie correnti, semplificazione e incamerazione della libertà esterna)
- rappresentazione -> **essa rimane molto misteriosa** -> come realizziamo la realtà all'interno del nostro cervello?
- manipolazione (rappresentazioni mentale aperte a manipolazione, idee che vanno ad influenzare il mio modo di percepire la realtà)

#### Rappresentazione

![rapp](rapp.png)

Un oggetto puó essere rappresentato in vari modi come una rappresentazione di tipo:
1. esecutivo -> modo di usare l'oggetto
2. iconica -> oggetto schematizzato nei tratti caratteristici -> "velocizza su due ruote" -> disegno mentale comodo
3. simbolica

#### Rappresentazioni interpretate per mezzo di codici

![codi](codi.png)

I codici ci danno modo di avere a disposizione una tabella di ciò che si può fare e ciò che è stato fatto fino ad ora:
![codici](codici.png)

##### Cifrato
Prendendo come riferimento un codice cifrato, abbiamo un codice cifrato come una partitura o un'intavolatura.

##### Visivo

![card](card.png)

Fra codici vi è una differenza enorme di rappresentazione mentale, che influenza la musica e la rappresentazione e non è una sola scelta di campo notazionale.

##### Codice verbale

![koe](koe.png)

È difficile dire quanto l'atteggiamento compositivo dipenda dalla scrittura e viceversa.

Perchè posso pensare che la realizzazione di un'opera parta da un'idea realizzativa grafica.

### Notazione secondo Stockhausen
![stock1](stock1.png)

1. Scritture di azione per produrre un particolare suono
2. scrittura cifrata con la possibilità di svincolare un'eventuale realizzazione
3. musica da leggere riferiti a percezione visiva
4. musica da ascoltare solamente e non traducibile in notazione
5. gradi intermedi di musica da leggere e vedere

Esempi di scritture:
- Cowell
- scrittura di cluster
- elementi nuovi di scrittura

[Ascolto di Banshee for piano strings](https://www.youtube.com/watch?v=WaIByDlFINk&ab_channel=V%C3%ADctorTrescol%C3%AD)

Con tipo di notazione verbale accompagnata da un codice cifrato ed ogni indicazione ha una sua rappresentazione importante nella lettura.

Caso in cui diviene molto importante la notazione è Ionisation di Varése:
![var](var.png)

Ballet Mecanique di Antheil -> con motore di aereoplano che non era possibile trascrivere con notazione tradizionale.

Tentativo di riprodurre e mettere in ordine i simboli prodotti negli ultimi anni(tratto da Villa Rojo):
![villa](villa.png)

Differenziando tra punti e linee.
![villa1](villa1.png)

### La notazione di Porena
![villa2](villa2.png)

In cui si puó dare delle indicazioni su durate, dinamiche e secondi.
![por](por.png)

Creando dei fatti musicali con questa tipologia di scrittura

![por2](por2.png)

Aspetto che mette in condizioni di realizzare una nostra rappresentazione  che abbiamo in mente.

Cercare come le rappresentazioni interne possono essere rappresentate? -> cercare di mettere in luce ciò che è la nostra idea

La scrittura puó essere anche un utile mezzo per scrivere la creatività verso nuove strade.

![por3](por3.png)

Partendo dalla notazione e semiografia raggiungiamo livelli compositivi di un certo spessore.

(Fabbrica illuminata di Luigi Nono)

![por4](por4.png)

Partendo da elementi grafici si realizza un qualcosa di strettamente musicale.

Questo tipo di modalità mette in condizioni di scrivere.

#### The Rara Requiem di Bussotti

Rilettura di una partitura varie volte, realizzando oggetti sonori ad ogni lettura.

In effetti alla fine si è all'interno della stessa pagina -> sensazione di omogeneità e compattezza tradizionale.

![busotti-rara-requiem-ms-5270_f.jpg](busotti-rara-requiem-ms-5270_f.jpg)

Meccanismo che prevede la partecipazione piú attiva dell'esecutore.

__________
