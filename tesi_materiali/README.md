 # Second Level Diploma Thesis in Electronic Music

## Index

### Chapters

1. Introduction
2. Sustainability of Electronic Music: roots and significance
3. How an existent musical piece could be made sustainable?
4. Sustained Electro-Acoustic works
5. Thinking sustainable today: guideline for the sustainable Electronic Music composition
6. Conclusions
7. Bibliography and Appendices

## Chapter 1: Introduction
<!--
short and synthetic synopsis of the work
-->

- Why this thesis
- What is in this thesis <!--indice ragionato-->

## Chapter 2: Sustainability of Electronic Music: roots and significance

- Etymology of the world "sustainable"
- The general problem of sustainability <!-- T. Gregory - L'eclisse delle memorie-->
- Sustainability of music <!-- il problema della riproducibilità musicale, forse tale paragrafo potrebbe essere rinominato Was music sustainable before sound reproduction? come potremmo spiegarci le diverse interpretazioni di Karajan delle Sinfonie di Beethoven? vi sono quindi degli elementi nella musica che non contemplano la sola partitura-->
<!--di conseguenza tali paragrafi andrebbero eliminati del tutto-->
  - Which are the sustained musical aspects before sound reproduction
  - Examples of written music before sound reproduction <!-- Types of instruments; cosa si sa delle Suite di Bach? due esempi contrapposti: qualcosa che mostri una under-specification ed esempio che mostri la over-specification-->
- The state of the art of the Electronic Music Sustainability Business <!-- analisi ragionata della bibliografia, trovare il motivo per cui si sta realizzando la tesi -->
  - SEAM<!-- che cos'è e perchè esiste-->

## Chapter 3: How an existent musical piece could be made sustainable?
<!-- descrizione dei problemi esistenti al momento ed esempi dei problemi descritti, ragionare su cosa è stato importante per i compositori
e metodi CAPITOLO TEORICO -->

- Over-specification and under-specification of musical aspects:
  - Under-specification of technical aspects/under-specification of musical aspects-> Fedele - Due Notturni con Figura oppure Propaganda di Bedrossian <!--patches non descritte-->
  - Under-specification musical aspects /over-specification technical aspects -> Di Scipio - AE2 <!--errors in music sheet-->, Stoackhausen- Oktophonie <!--over specification non porta alla sostenibilità, consegnare un documento leggibile diviene più o meno comprensibile-->
- Sustainable then interpretable
  - Re-building the composition environment with multiple sources <!-- ricostruire l'ambiente di composizione, per mezzo di testi, conferenze,  -->
  - Human legacy and oral transmission <!--termine musicologico-->
  - Why we don't have a general electroacustic musical praxis <!-- ad esempio gli abbellimenti nel Barocco, qui si potrebbero citare nuovamente le diverse interpretazioni delle Sinfonie di Beethoven; testo di riferimento con esempi di manuali di Percussioni - Battistelli; DAFX libro che descrive come sono fatti gli effetti possibili ed immaginabili con codice MATLAB (cosa ci si fa a livello musicale, cos'è un ambiente esecutivo?); testo di Isherwood Vocal...; il problema dell'esecuzione di un brano sta nei controlli (Harmonizer e Delay)-->
- Methods and consequences
  - The undocumented
  - The words-hole
  - The porting
  - Combination of Methods
  - searching other methods

## Chapter 4: Sustained Electro-Acoustic works

<!-- CAPITOLO PRATICO -->

- Collecting first hand experience (oral transmitted information)
- Case studies: <!--used Methods-->
  - Lucier - I am sitting in a room
  - Nono - Risonanze Erranti <!-- il problema degli esecutori storici, over-specification -->
  - Nono - PPPD
  - Cage - Cartridge Music
  - Stoackhausen - Mantra
  - Battistelli - I Cenci

## Chapter 5: Thinking sustainable today: guidelines for sustainable Electronic Music compositions

<!-- LINEE GUIDA PER LA SOSTENIBILITÀ FUTURA -->

- Data availability
  - Licenses
  - File formats
  - Protocols
- Which instruments to start a new work
- How to archive all the useful elements for the composition

## Chapter 6: Conclusions

- Futures Thinking <!-- non essere un profeta tecnologico, porla come elemento critico -->
  - Reproduction technologies <!-- perchè i mezzi tecnologici che usiamo non riescono a mantenere la pratica musicale -->
  - Backward compatibility <!-- Tube wagneriane ed altri esempi, come rendere qualcosa di nuovo compatibile con il passato? -->
  - Make it sustainable for the foreword compatibility <!-- come rendere qualcosa di compatibile con il futuro -->
- Conclusions
  - The problem of sustainability of Installations <!-- appiglio per GMD-->

## Chapter 7: Bibliography and Appendices

### Appendix I: International musical centers and individuals devoted to the restoration of audio heritage and technical documentation for electroacustic music

#### Italy
- [Laboratorio Mirage](http://mirage.uniud.it/)
- [Centro di Sonologia Computazionale di Padova](http://csc.dei.unipd.it/)
- [SEAM]()

#### France
- [IRCAM Sidney Server](https://brahms.ircam.fr/sidney)

#### Portugal

- [António de Sousa Dias](www.sousadias.com)

### Appendix II: International Standards for Audio Recording Preservation
- [An MPAI/IEEE International Standard for Audio: Overview of
CAE Audio Recording Preservation (ARP) Technology](https://www.aes.org/e-lib/browse.cfm?elib=22136)
- [Care and preservation of magnetic audio recordings for motion pictures and television](https://www.iso.org/standard/2459.html)
