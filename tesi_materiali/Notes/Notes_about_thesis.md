# Sustainability Thesis Notes 5 december 2022

- testo Vidolin e Bernardini
- testo Polfreman https://eprints.soton.ac.uk/151167/
- Risposta all'impulso come metodologia, ovvero una prova del nove realizzata
- metodologia di schematizzazione
- ambiente esecutivo del Live Electronics è un qualcosa che va suonato
- Esempio: Cenci di Battistelli con schema a blocchi astratto dove con unità di misura MKS
  - si è avuta la prova del nove con le esecuzioni successive erano abbastanza fedeli all'originale

## Difficoltà di mantenere un'opera d'arte

## Sarebbe bello che il Live Electronics fosse interpretabile

## Ricordi pubblicazione delle opere di Nono

In UR editioncon le registrazioni originali, ma ciò è vero?

Sappiamo che l'esecuzione è una versione di interpretazione.

## Problemi

1. problema dello strumento e di come ricrearlo da zero
2. si può poi interpretare

## Lo strumento si deve poter ricreare da zero

## È importante tutto l'ambiente esecutivo di controllo

Questo è utile per far sviluppare la sensibilità e far interpretare in maniera diversa il brano.

---

# Esempi

- partitura dei Cenci -> con aggiunta della risposta all'impulso degli algoritmi
  - da realizzare con attori shakesperiani in izialmente con collaborazione Ian McDiarmid
  - duplicazione del canale del microfono
    - efficacia della realizzazione
- partitura del Dialogue -> spazializzazione con elementi che compaiono

# In alcuni casi la IR non basta
