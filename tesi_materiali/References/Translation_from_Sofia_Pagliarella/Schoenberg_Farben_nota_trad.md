# DE

# ITA
Non è compito unicamente del direttore d’orchestra far emergere le singole voci che gli appaiono significative (a livello tematico) in questo brano, oppure abbassare tonalità di mescolanze sonore apparentemente sbilanciate. Dove una voce deve emergere più delle altre, viene orchestrata di conseguenza e i suoni non avranno bisogno di scendere di tonalità.
Invece il compito del direttore è assicurarsi che ogni strumento suoni esattamente quel grado di intensità imposto; esattamente (a livello soggettivo) in funzione del proprio strumento e non subordinato all'intero suono (a livello oggettivo).
*Il cambio di accordi deve avvenire così gradualmente da non rendere evidente alcuna accentuazione degli strumenti impiegati, cosicché si possa notare solo attraverso un diverso colore.

Traduzione dal tedesco di Sofia Pagliarella
---
# EN

It is not solely the task of the conductor to bring out the individual voices that appear significant to them (in terms of the musical theme) in this piece or to lower the tonalities of seemingly unbalanced sonic blends. When one voice needs to stand out more than the others, it is orchestrated accordingly, and the sounds won't need to decrease in tonality.
Instead, the conductor's task is to ensure that each instrument plays at exactly the degree of intensity required, precisely (on a subjective level) according to their instrument and not subordinated to the overall sound (objectively).
*Chord changes must occur so gradually that no accentuation of the instruments used is evident, so that it can only be noticed through a different color.

Translation from German to Italian by Sofia Pagliarella, translation from Italian to English by Davide Tedesco.
